<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    
    <title>Kuoll pricing</title>
    <link rel="canonical" href="https://www.kuoll.com/pricing.html">

    {{>inhead}}

    <link rel="stylesheet" href="/resources/css/landing.css">

</head>
<body class="">

{{>topnav}}

<main class="pageScrollOverflowY">

<div class="container">

    <div class="jumbotron">
        <h1>Quick Start</h1>
        
    </div>
           
        <h3>Installation</h3>
        <p class="small">You install Kuoll by including a script on your website. That script listens for errors and monitor conversion and sends actionable data back to your account.</p>
        <p class="small">Copy and paste the code below to the bottom of web pages of your web application.</p>
        
        <figure class="highlight">
          <pre><code class="language-html" data-lang="html"><span class="nt"><span><</span>script></span>(<span class="na">function</span> (w, k, t){<span class="na">if</span> (!window.Promise){w[k]<span class="eq">=</span>w[k]||<span class="na">function</span>(){};<span class="na">return</span>;}w[k] <span class="eq">=</span> w[k] || <span class="na">function</span> () {<span class="na">var</span> a <span class="eq">=</span> arguments; <span class="na">return</span> new Promise(<span class="na">function</span> (y, n) {(w[k].q <span class="eq">=</span> w[k].q || []).<span class="s">push</span>({a: a, d: {y: y, n: n}});});};<span class="na">var</span> s <span class="eq">=</span> <span class="s">document.createElement</span>(t),f <span class="eq">=</span> <span class="s">document.getElementsByTagName</span>(t)[0];s.async <span class="eq">=</span> 1;s.src <span class="eq">=</span> <span class="l">'https://cdn.kuoll.com/bootloader.js'</span>;f.parentNode.insertBefore(s, f);}(window, <span class="s">'kuoll'</span>, <span class="s">'script'</span>)); kuoll(<span class="s">'startRecord'</span>, { API_KEY: <span class="s">"YOUR_API_KEY"</span>, userId: <span class="nt">1</span> }); <span class="nt"><span><</span>/script></span></code></pre>
        </figure>

        <p class="small">This code should include your unique Kuoll API key and should be copied directly into your code. Please, verify your unique API key at <a class="api" href="https://app.kuoll.com/support.html">Kuoll dashboard</a>.</p>

        <h3>Browser Support</h3>
        <p class="small">The monitoring.js script works on all modern browsers. If the script is loaded on an unsupported browser, it deactivates itself without causing issue.</p>

        <h3>Advanced Configuration</h3>
        <p class="small">We understand that each store is different, and that a single configuration won’t work for everyone. If you’re looking for something more than the default, please visit <a class="api" href="https://app.kuoll.com/js-doc/module-kuollAPI.html#.startRecord">complete Kuoll API documentation</a>.</p>

</div>

<style>
@import "/resources/css/installation.css";
</style>

{{>footer}}

</main>

{{>scripts}}

</body>
</html>