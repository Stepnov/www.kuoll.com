define(["jquery", "react", "app/utils/api", "app/User", "ua-parser",
    "app/react/records/RecordsListSettings", "app/react/records/Record", "jquery.cookie"],
    function ($, React,api, User, UAParser, RecordsListSettings, Record) {

    var initialLoadingStarted = false;

    var RecordsContainer = React.createClass({displayName: "RecordsContainer",

        /* State declaration */
        getInitialState: function () {
            return {
                settings: undefined,

                loading: 0,

                ownRecordsLoaded: false,
                teamRecordsLoaded: false,

                records: []
            };
        },

        render: function () {
            var records = this.getRecordsToShow();
            var self = this;
            return (
                React.createElement("div", null, 
            initialLoadingStarted && this.state.loading && !this.state.records.length ? null :
                React.createElement(RecordsListSettings, {onSettingsChanged: this.onNewSettings}), 
                
            !this.state.settings ? null :
                records.length === 0 ?
                    React.createElement("div", {className: "records-loading-wrapper"}, 
                    !initialLoadingStarted || this.state.loading === 0 ? [
                        React.createElement("p", null, "Loading your records.."),
                        React.createElement("i", {className: "fa fa-spinner fa-spin", "aria-hidden": "true"})
                    ] : this.state.records.length ?
                        React.createElement("p", null, "No records satisfy your criteria")
                        :
                        React.createElement("div", {className: "container"}, 
                            React.createElement("div", {className: "row"}, 
                                React.createElement("div", {className: "col-md-4 col-md-offset-2 "}, 
                                    React.createElement("div", {className: ""}, 
                                        React.createElement("h3", null, "1. Add Kuoll to Your Web App"), 
                                        React.createElement("p", null, 
                                            "Check ", React.createElement("a", {href: "/quick-start.html"}, "Kuoll API documentation"
                                        ), " to install single line of script to your website."), 
                                        React.createElement("p", null, "This option allows web application customers create Kuoll records.")
                                    )
                                ), 
                                React.createElement("div", {className: "col-md-4"}, 
                                    React.createElement("div", null, 
                                        React.createElement("h3", null, " 2. Install Chrome Extension"), 
                                        React.createElement("div", null, 
                                            "Alternatively, you can make Kuoll record of ", React.createElement("strong", null, "any"
                                        ), " web site using ", React.createElement("strong", null, "free"), " ", React.createElement("a", {
                                            href: "https://chrome.google.com/webstore/detail/kuoll/ljfpdodjbnkpbaenppoiifjkdlhfjcgn"}, "Kuoll" + ' ' +
                                            "extension for Chrome"), ".", 
                                            React.createElement("p", null, 
                                                React.createElement("a", {href: "https://chrome.google.com/webstore/detail/kuoll/ljfpdodjbnkpbaenppoiifjkdlhfjcgn"}, 
                                                    React.createElement("img", {src: "/resources/img/ChromeWebStore_BadgeWBorder_v2_206x58.png"})
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                        
                    )
                    :
                    records.map(function (record) {
                        return (
                            React.createElement(Record, {record: record, key: record.recordCode, 
                                onRecordDeleted: self.onRecordDeleted, onRecordRestored: self.onRecordRestored})
                        )
                    })
                
                )
            )
        },

        onNewSettings: function (settings) {
            if (settings.showTeam && !this.state.teamRecordsLoaded) {
                this.loadTeamRecords();
            }
            if (settings.showOwn && !this.state.ownRecordsLoaded) {
                this.loadOwnRecords();
            }
            if (this.state.settings && settings.order !== this.state.settings.order) {
                this.setState({
                    records: this.state.records.sort(function (a, b) {
                        if (self.state.settings.order === "lastIssue") {
                            return b.lastIssueTime - a.lastIssueTime;
                        } else {
                            return b.startTime - a.startTime;
                        }
                    })
                });
            }
            this.setState({ settings: settings });
        },

        onNewRecordsLoaded: function (records, isTeamRecord) {
            var self = this;
            records.forEach(function (record) {
                if (isTeamRecord) {
                    record.team = true;
                } else {
                    record.own = true;
                }
                if (record.recordInfo && record.recordInfo.userAgent) {
                    record.recordInfo.userAgent = new UAParser(record.recordInfo.userAgent).getResult();
                }
            });
            records = this.state.records
                .concat(records)
                .sort(function (a, b) {
                    if (self.state.settings.order === "lastIssue") {
                        return b.lastIssueTime - a.lastIssueTime;
                    } else {
                        return b.startTime - a.startTime;
                    }
                });
            this.setState({
                records: records,
                loading: this.state.loading - 1
            })
        },

        loadTeamRecords: function () {
            this.setState({
                teamRecordsLoaded: true,
                loading: this.state.loading + 1
            });

            var self = this;
            initialLoadingStarted = true;
            User.getInfo(function (user) {
                api("getOrgRecords", {
                    orgId: user.orgId
                }, function (resp) {
                    self.onNewRecordsLoaded(resp.records.filter(function (record) {
                        return record.userId !== user.userId;
                    }), true);
                });
            })
        },

        loadOwnRecords: function () {
            this.setState({
                ownRecordsLoaded: true,
                loading: this.state.loading + 1
            });

            var self = this;
            initialLoadingStarted = true;
            api("getRecords", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                self.onNewRecordsLoaded(resp.records, false);
            });
        },

        onRecordDeleted: function (recordCode) {
            api("api/deleteRecord", {
                recordCode: recordCode
            });
            var records = this.state.records;
            records.forEach(function (record) {
                if (record.recordCode === recordCode) {
                    record.deleted = true;
                }
            });
            this.setState({
                records: records
            });
        },

        onRecordRestored: function (recordCode) {
            api("restoreRecord", {
                recordCode: recordCode
            });
            var records = this.state.records;
            records.forEach(function (record) {
                if (record.recordCode === recordCode) {
                    record.deleted = false;
                }
            });
            this.setState({
                records: records
            });
        },

        /**
         * Filters loaded records and returns only those that must be shown now based on user settings
         */
        getRecordsToShow: function () {
            var settings = this.state.settings;
            return this.state.records.filter(function (record) {
                if (record.own && !settings.showOwn) {
                    return false;
                }
                if (record.team && !settings.showTeam) {
                    return false;
                }
                if (settings.userIdFilter && record.externalUserId.indexOf(settings.userIdFilter) === -1) {
                    return false;
                }
                if (record.issuesCount === 0 && !settings.showRecordsWithoutIssues) {
                    return false;
                }
                return true;
            });
        }

    });

    return RecordsContainer;

});