define(["jquery", "react", "app/utils/DateUtils", "app/utils/TipUtils", "app/utils/api", "app/react/records/RecordDescription",
    "app/react/records/IssuesList", "app/react/play/RecordStateBadges", "app/react/BrowserIcon", "app/react/OsIcon",
        "app/react/DeviceIcon"],
    function ($, React,DateUtils, TipUtils, api, RecordDescription, IssuesList, RecordStateBadges, BrowserIcon, OsIcon, DeviceIcon) {

    var Record = React.createClass({displayName: "Record",

        /* State declaration */
        getInitialState: function () {
            return {
                issuesShown: false,
                issues: []
            };
        },

        /* Props declaration */
        propTypes: {
            record: React.PropTypes.object.isRequired,

            onRecordDeleted: React.PropTypes.func.isRequired,
            onRecordRestored: React.PropTypes.func.isRequired
        },

        render: function () {
            var record = this.props.record;

            var a = document.createElement("a");
            a.href = record.firstFrameUrl;
            var origin = a.origin;

            if (record.deleted) {
                return (
                    React.createElement("div", {className: "record deleted", ref: "record"}, 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "deleted-record-description col-xs-11"}, 
                                record.description || "Deleted record with no description"
                            ), 
                            React.createElement("div", {className: "col-xs-1"}, 
                                React.createElement("div", {className: "deleted-record-restore-btn"}, 
                                    React.createElement("i", {className: "fa fa-times", title: "Restore deleted record", onClick: this.restoreRecord})
                                )
                            )
                        )
                    )
                )
            } else {
                return (
                    React.createElement("div", {className: "record", ref: "record"}, 
                        React.createElement("div", {className: "record-info row"}, 
                            React.createElement("div", {className: "record-info-column-1 col-xs-10 col-sm-3 col-md-3 col-lg-5"}, 
                                React.createElement(RecordDescription, {record: record, recordDeleted: false})
                            ), 
                            React.createElement("div", {className: "record-info-column-5 col-xs-2 col-sm-1 col-md-1 col-lg-1 col-sm-push-8 col-md-push-8 col-lg-push-6"}, 
                                React.createElement("p", null, 
                                    React.createElement(BrowserIcon, {userAgent: record.recordInfo.userAgent})
                                ), 
                                React.createElement("p", null, 
                                    React.createElement(OsIcon, {userAgent: record.recordInfo.userAgent})
                                ), 
                                React.createElement("p", null, 
                                    React.createElement(DeviceIcon, {userAgent: record.recordInfo.userAgent})
                                ), 
                                React.createElement("p", {className: "record-delete-btn"}, 
                                    React.createElement("i", {className: "fa fa-times", title: "Delete record", onClick: this.deleteRecord})
                                )
                            ), 
                            React.createElement("div", {className: "record-info-column-4 col-xs-4 col-sm-3 col-md-3 col-lg-2 col-xs-push-8 col-sm-push-4 col-md-push-4 col-lg-push-3"}, 
                                React.createElement(RecordStateBadges, {record: this.props.record, showTimeInfo: false})
                            ), 
                            React.createElement("div", {className: "record-info-column-2 col-xs-4 col-sm-3 col-md-3 col-lg-2 col-xs-pull-4 col-sm-pull-4 col-md-pull-4 col-lg-pull-3"}, 
                                React.createElement("div", {className: "user-id-wrapper"}, 
                                    React.createElement("p", {className: "caption"}, "User ID"), 
                                    React.createElement("p", null, record.externalUserId)
                                ), 
                                React.createElement("div", {className: "domain-wrapper"}, 
                                    React.createElement("p", {className: "caption"}, "Domain"), 
                            origin ?
                                React.createElement("p", null, origin)
                                :
                                React.createElement("p", {className: "low-priority"}, "<Not provided>")
                                
                                )
                            ), 
                            React.createElement("div", {className: "record-info-column-3 col-xs-4 col-sm-2 col-md-2 col-lg-2 col-xs-pull-4 col-sm-pull-4 col-md-pull-4 col-lg-pull-3"}, 
                                React.createElement("div", {className: "start-time-wrapper"}, 
                                    React.createElement("p", {className: "caption"}, "Start time"), 
                                    React.createElement("p", null, DateUtils.formatDate(record.startTime))
                                ), 
                                React.createElement("div", {className: "duration-wrapper"}, 
                                    React.createElement("p", {className: "caption"}, "Duration"), 
                                    React.createElement("p", null, DateUtils.formatDuration((record.completeTime || Date.now()) - record.startTime).short)
                                )
                            )
                        ), 
                        React.createElement("div", {className: "issues-list-wrapper"}, 
                            React.createElement("div", {className: "issues-dropdown", onClick: this.onIssuesListToggled}, 
                        this.state.issuesShown ?
                            React.createElement("i", {className: "fa fa-arrow-circle-down", "aria-hidden": "true"}) :
                            React.createElement("i", {className: "fa fa-arrow-circle-right", "aria-hidden": "true"}), 
                            
                                "Issues (", record.issuesCount, ")"
                            ), 

                    this.state.issuesShown ?
                        React.createElement(IssuesList, {issues: this.state.issues, record: record})
                        : null
                        )
                    )
                )
            }
        },

        onIssuesListToggled: function () {
            if (!this.state.issuesShown && this.state.issues.length === 0) {
                var self = this;
                api("getIssues", {
                    orgId: this.props.record.orgId,
                    userId: this.props.record.userId,
                    recordId: this.props.record.recordId
                }, function (resp) {
                    self.setState({
                        issues: resp.issues
                    });
                });
            }
            if (this.props.record.issuesCount !== 0) {
                this.setState({
                    issuesShown: !this.state.issuesShown
                });
            }
        },

        deleteRecord: function () {
            $(React.findDOMNode(this.refs.record)).find('[data-hasqtip]').qtip("hide");
            this.props.onRecordDeleted(this.props.record.recordCode);
        },

        restoreRecord: function () {
            $(React.findDOMNode(this.refs.record)).find('[data-hasqtip]').qtip("hide");
            this.props.onRecordRestored(this.props.record.recordCode);
        },

        componentDidMount: function () {
            TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.record)).find('*[title]:not([data-hasqtip])'));
        },

        componentDidUpdate: function () {
            TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.record)).find('*[title]:not([data-hasqtip])'));
        }

    });

    return Record;

});