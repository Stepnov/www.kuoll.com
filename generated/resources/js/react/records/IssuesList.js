define(["jquery", "react", "app/utils/DateUtils", "app/utils/TipUtils"], function ($, React,DateUtils, TipUtils) {

    var IssuesList = React.createClass({displayName: "IssuesList",

        /* State declaration */
        getInitialState: function () {
            return {

            };
        },

        /* Props declaration */
        propTypes: {
            record: React.PropTypes.object.isRequired,
            issues: React.PropTypes.array.isRequired
        },

        render: function () {
            var issues = this.props.issues;
            var record = this.props.record;
            return (
                React.createElement("div", {className: "issues-list row", ref: "issuesList"}, 
                issues.length === 0 ?
                    React.createElement("div", {className: "issues-loading-wrapper"}, 
                        React.createElement("i", {className: "fa fa-spinner fa-spin", "aria-hidden": "true"})
                    ) :
                    issues.map(function (issue) {
                        function onClick() {
                            window.open("/play.html?recordCode=" + issue.recordCode + "&issueId=" + issue.id);
                        }
                        return (
                            React.createElement("div", {className: "col-md-6 col-lg-4", key: issue.id}, 
                                React.createElement("div", {className: "issue row"}, 
                                    React.createElement("div", {className: "go-to-issue-btn col-xs-1 col-sm-1 col-md-1 col-lg-1", title: "Go to the issue", onClick: onClick}, 
                                        React.createElement("i", {className: "fa fa-chevron-circle-right", "aria-hidden": "true"})
                                    ), 
                                    React.createElement("div", {className: "col-xs-4 col-sm-4 col-md-4 col-lg-4"}, 
                                        React.createElement("div", {className: "issue-description-wrapper"}, 
                                            React.createElement("p", {className: "caption"}, "Description"), 
                                            issue.description ?
                                                React.createElement("p", null, issue.description)
                                                    :
                                                React.createElement("p", {className: "low-priority"}, "<No description>")
                                        )
                                    ), 
                                    React.createElement("div", {className: "col-xs-3 col-sm-3 col-md-3 col-lg-3"}, 
                                        React.createElement("div", {className: "issue-type-wrapper"}, 
                                            React.createElement("p", {className: "caption"}, "Type"), 
                                            React.createElement("p", null, issue.type)
                                        )
                                    ), 
                                    React.createElement("div", {className: "col-xs-4 col-sm-4 col-md-4 col-lg-4"}, 
                                        React.createElement("div", {className: "issue-time-wrapper"}, 
                                            React.createElement("p", {className: "caption"}, "Time"), 
                                            React.createElement("p", null, DateUtils.formatDate(issue.time))
                                        ), 
                                        React.createElement("div", {className: "issue-time-after-record-wrapper"}, 
                                            React.createElement("p", {className: "caption"}, "From record start"), 
                                            React.createElement("p", null, DateUtils.formatDuration(issue.time - record.startTime).short)
                                        ), 
                                        React.createElement("div", {className: "issue-time-ago-wrapper"}, 
                                            React.createElement("p", {className: "caption"}, "Ago"), 
                                            React.createElement("p", null, DateUtils.formatDuration(Date.now() - issue.time).short)
                                        )
                                    )
                                )
                            )
                        )
                    })
                
                )
            )
        },

        componentDidMount: function () {
            TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.issuesList)).find('[title!=""]'));
        }

    });

    return IssuesList;

});
