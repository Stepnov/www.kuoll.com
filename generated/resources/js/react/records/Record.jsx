define(["jquery", "react", "app/utils/DateUtils", "app/utils/TipUtils", "app/utils/api", "app/react/records/RecordDescription",
    "app/react/records/IssuesList", "app/react/play/RecordStateBadges", "app/react/BrowserIcon", "app/react/OsIcon",
        "app/react/DeviceIcon"],
    function ($, React,DateUtils, TipUtils, api, RecordDescription, IssuesList, RecordStateBadges, BrowserIcon, OsIcon, DeviceIcon) {

    var Record = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                issuesShown: false,
                issues: []
            };
        },

        /* Props declaration */
        propTypes: {
            record: React.PropTypes.object.isRequired,

            onRecordDeleted: React.PropTypes.func.isRequired,
            onRecordRestored: React.PropTypes.func.isRequired
        },

        render: function () {
            var record = this.props.record;

            var a = document.createElement("a");
            a.href = record.firstFrameUrl;
            var origin = a.origin;

            if (record.deleted) {
                return (
                    <div className="record deleted" ref="record">
                        <div className="row">
                            <div className="deleted-record-description col-xs-11">
                                {record.description || "Deleted record with no description"}
                            </div>
                            <div className="col-xs-1">
                                <div className="deleted-record-restore-btn">
                                    <i className="fa fa-times" title="Restore deleted record" onClick={this.restoreRecord}></i>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            } else {
                return (
                    <div className="record" ref="record">
                        <div className="record-info row">
                            <div className="record-info-column-1 col-xs-10 col-sm-3 col-md-3 col-lg-5">
                                <RecordDescription record={record} recordDeleted={false} />
                            </div>
                            <div className="record-info-column-5 col-xs-2 col-sm-1 col-md-1 col-lg-1 col-sm-push-8 col-md-push-8 col-lg-push-6">
                                <p>
                                    <BrowserIcon userAgent={record.recordInfo.userAgent} />
                                </p>
                                <p>
                                    <OsIcon userAgent={record.recordInfo.userAgent} />
                                </p>
                                <p>
                                    <DeviceIcon userAgent={record.recordInfo.userAgent} />
                                </p>
                                <p className="record-delete-btn">
                                    <i className="fa fa-times" title="Delete record" onClick={this.deleteRecord}></i>
                                </p>
                            </div>
                            <div className="record-info-column-4 col-xs-4 col-sm-3 col-md-3 col-lg-2 col-xs-push-8 col-sm-push-4 col-md-push-4 col-lg-push-3">
                                <RecordStateBadges record={this.props.record} showTimeInfo={false} />
                            </div>
                            <div className="record-info-column-2 col-xs-4 col-sm-3 col-md-3 col-lg-2 col-xs-pull-4 col-sm-pull-4 col-md-pull-4 col-lg-pull-3">
                                <div className="user-id-wrapper">
                                    <p className="caption">User ID</p>
                                    <p>{record.externalUserId}</p>
                                </div>
                                <div className="domain-wrapper">
                                    <p className="caption">Domain</p>
                            {origin ?
                                <p>{origin}</p>
                                :
                                <p className="low-priority">&lt;Not provided&gt;</p>
                                }
                                </div>
                            </div>
                            <div className="record-info-column-3 col-xs-4 col-sm-2 col-md-2 col-lg-2 col-xs-pull-4 col-sm-pull-4 col-md-pull-4 col-lg-pull-3">
                                <div className="start-time-wrapper">
                                    <p className="caption">Start time</p>
                                    <p>{DateUtils.formatDate(record.startTime)}</p>
                                </div>
                                <div className="duration-wrapper">
                                    <p className="caption">Duration</p>
                                    <p>{DateUtils.formatDuration((record.completeTime || Date.now()) - record.startTime).short}</p>
                                </div>
                            </div>
                        </div>
                        <div className="issues-list-wrapper">
                            <div className="issues-dropdown" onClick={this.onIssuesListToggled}>
                        {this.state.issuesShown ?
                            <i className="fa fa-arrow-circle-down" aria-hidden="true"></i> :
                            <i className="fa fa-arrow-circle-right" aria-hidden="true"></i>
                            }
                                Issues ({record.issuesCount})
                            </div>

                    {this.state.issuesShown ?
                        <IssuesList issues={this.state.issues} record={record} />
                        : null}
                        </div>
                    </div>
                )
            }
        },

        onIssuesListToggled: function () {
            if (!this.state.issuesShown && this.state.issues.length === 0) {
                var self = this;
                api("getIssues", {
                    orgId: this.props.record.orgId,
                    userId: this.props.record.userId,
                    recordId: this.props.record.recordId
                }, function (resp) {
                    self.setState({
                        issues: resp.issues
                    });
                });
            }
            if (this.props.record.issuesCount !== 0) {
                this.setState({
                    issuesShown: !this.state.issuesShown
                });
            }
        },

        deleteRecord: function () {
            $(React.findDOMNode(this.refs.record)).find('[data-hasqtip]').qtip("hide");
            this.props.onRecordDeleted(this.props.record.recordCode);
        },

        restoreRecord: function () {
            $(React.findDOMNode(this.refs.record)).find('[data-hasqtip]').qtip("hide");
            this.props.onRecordRestored(this.props.record.recordCode);
        },

        componentDidMount: function () {
            TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.record)).find('*[title]:not([data-hasqtip])'));
        },

        componentDidUpdate: function () {
            TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.record)).find('*[title]:not([data-hasqtip])'));
        }

    });

    return Record;

});