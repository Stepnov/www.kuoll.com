define(["jquery", "react", "app/react/LinkedStateMixin", "app/utils/api", "app/User"], function ($, React, LinkedStateMixin, api, User) {

    function pairEquals(startStep, endStep) {
        return p => p.startStep === startStep && p.endStep === endStep;
    }
    function pairNotEqual(startStep, endStep) {
        return p => p.startStep !== startStep || p.endStep !== endStep;
    }

    const StepPairs = React.createClass({

        mixins: [LinkedStateMixin],

        getInitialState: function () {
            return {
                stepPairs: [],

                newStartStep: "",
                newEndStep: "",

                alreadyExists: false,
                alreadyRemoved: false,
                                                                                            
                errorMsg: null
            };
        },
        
        propTypes: {

        },

        pairExists: function (startStep, endStep) {
            return this.state.stepPairs.filter(pairEquals(startStep, endStep)).length === 1;
        },

        addPair: function (e) {
            e.preventDefault();
            var self = this;
            var startStep = this.state.newStartStep;
            var endStep = this.state.newEndStep;

            if (!startStep || !endStep) {
                this.setState({
                    errorMsg: "Both steps must be set"
                });
                return;
            }
            if (this.pairExists(startStep, endStep)) {
                this.setState({
                    errorMsg: "No need to a the same pair twice"
                });
                return;
            }

            var oldPairs = this.state.stepPairs;
            var newPairs = oldPairs.concat([{
                startStep: startStep,
                endStep: endStep
            }]);

            this.setState({
                stepPairs: newPairs,
                newStartStep: "",
                newEndStep: "",
                alreadyExists: false,
                alreadyRemoved: false,
                errorMsg: null
            });
            $(this.refs.newStartStep.getDOMNode()).focus();

            api("convmon/add_step_pair", {
                userToken: $.cookie("userToken"),
                startStep: startStep,
                endStep: endStep
            }, function (resp) {
                if (resp.alreadyExists) {
                    self.setState({
                        steps: oldPairs,
                        alreadyExists: true
                    });
                }
            });
        },

        removePair: function (pair) {
            return () => {
                var self = this;
                var oldPairs = this.state.stepPairs;
                var newPairs = oldPairs.filter(pairNotEqual(pair.startStep, pair.endStep));

                this.setState({
                    stepPairs: newPairs,
                    newStartStep: "",
                    newEndStep: "",
                    alreadyExists: false,
                    alreadyRemoved: false,
                    errorMsg: null
                });

                api("convmon/remove_step_pair", {
                    userToken: $.cookie("userToken"),
                    startStep: pair.startStep,
                    endStep: pair.endStep
                }, function (resp) {
                    if (resp.alreadyRemoved) {
                        self.setState({
                            steps: oldPairs,
                            alreadyRemoved: true
                        });
                    }
                });
            };
        },

        render: function () {
            return (
                <div className="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                    <h2>Ecommerce steps</h2>

                    <p>
                        Use this form to add pairs of ecommerce steps (e.g. add -> checkout, checkout -> purchase)
                        that you're interested in. We will help you to find out how different errors in your app affect
                        conversion between this steps.
                    </p>

                    <p>
                        If you don't specify steps for analytics explicitly, we will try to guess them based on your
                        actual data.                                                               
                    </p>

                    {this.state.stepPairs.map((pair, i) => (
                        <div className="step-pair" key={i}>
                            <span className="start-step">{pair.startStep}</span>
                            <span className="step-pair-separator">-></span>
                            <span className="end-step">{pair.endStep}</span>

                            <button className="btn btn-default"
                                    onClick={this.removePair(pair)} title="Delete pair">
                                <i className="fa fa-times"></i> Remove step pair
                            </button>
                        </div>
                    ))}

                    <div className="new-step-pair">
                        <form className="form-inline" onSubmit={this.addPair}>
                            <input type="text" className="form-control" id="new-start-step" valueLink={this.linkState("newStartStep")}
                                ref="newStartStep"/>
                            <div id="new-step-pair-separator">-></div>
                            <input type="text" className="form-control" id="new-end-step" valueLink={this.linkState("newEndStep")}/>

                            <button className="btn btn-success" title="Add pair" id="new-step-pair-btn">
                                <i className="fa fa-check"></i>
                            </button>
                        </form>
                        <label className="control-label">
                            {this.state.errorMsg}
                        </label>
                    </div>

                </div>
            )
        },

        componentDidMount: function () {
            var self = this;
            User.getInfo(function (user) {
                self.setState({
                    stepPairs: user.ecommerceStepPairs
                });
            });
        }

    });

    return StepPairs;

});