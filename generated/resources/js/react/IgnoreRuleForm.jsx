define(["jquery", "react"], function ($, React) {

    var IgnoreRuleForm = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                domainErrorMsg: null,
                selectorErrorMsg: null
            };
        },

        /* Props declaration */
        propTypes: {
            rule: React.PropTypes.object,
            onSubmit: React.PropTypes.func.isRequired,
            onDelete: React.PropTypes.func
        },

        render: function () {
            var rule = this.props.rule;
            return (
                <div>
                    <form className="form-horizontal" onSubmit={this.onSubmit}>
                        <div className={"form-group " + (this.state.domainErrorMsg ? " has-error" : "")}>
                            <label className="control-label col-md-3">CSS selector</label>
                            <div className="col-md-9">
                                <input className="domain-input form-control" type="text" ref="domain"
                                       placeholder="Domain (RegExp)"/>
                                <span className="help-block">{this.state.domainErrorMsg}</span>
                            </div>
                        </div>
                        <div className={"form-group " + (this.state.selectorErrorMsg ? " has-error" : "")}>
                            <label className="control-label col-md-3">CSS selector</label>
                            <div className="col-md-9">
                                <textarea className="selector-input form-control" ref="selector"
                                          placeholder="CSS selector"></textarea>
                                <span className="help-block">{this.state.selectorErrorMsg}</span>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-md-9 col-md-offset-3">
                                {rule ?
                                    <div className="delete-btn btn btn-danger" onClick={this.props.onDelete}>
                                        <i className="fa fa-times"></i>
                                    </div>
                                    : null }
                                <button className="rule-save btn btn-lg btn-success" type="submit">
                                    <i className="fa fa-plus"></i> {rule ? "Save the rule" : "Add new rule"}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            )
        },

        componentDidMount: function () {
            var $domain = $(React.findDOMNode(this.refs.domain));
            var $selector = $(React.findDOMNode(this.refs.selector));
            if (!$domain.val()) $domain.val(this.props.rule ? this.props.rule.domain : "");
            if (!$selector.val()) $selector.val(this.props.rule ? this.props.rule.selector : "");
        },

        onSubmit: function (e) {
            var key = this.props.rule ? this.props.rule.key : null;
            var $domain = $(React.findDOMNode(this.refs.domain));
            var $selector = $(React.findDOMNode(this.refs.selector));
            var domain = $domain.val() || ".*";
            var selector = $selector.val();

            e.preventDefault();

            var hasErrors = false;
            try {
                new RegExp(domain);
            } catch (e) {
                hasErrors = true;
                this.setState({
                    domainErrorMsg: e.message
                });
            }
            try {
                document.querySelector(selector);
            } catch (e) {
                hasErrors = true;
                this.setState({
                    selectorErrorMsg: "Must be a valid CSS selector"
                });
            }
            if (hasErrors) {
                return;
            }

            this.props.onSubmit(domain, selector, key);
            $domain.val("");
            $selector.val("");
            this.setState({
                domainErrorMsg: "",
                selectorErrorMsg: ""
            })
        }

    });

    return IgnoreRuleForm;

});