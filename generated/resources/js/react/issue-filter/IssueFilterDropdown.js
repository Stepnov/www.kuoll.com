define(["jquery", "react", "app/react/play/NativeListener", "app/react/play/LongText", "app/utils/api", "jquery.cookie"], 
    function ($, React, NativeListener, LongText, api) {

    const IssueFilter = React.createClass({displayName: "IssueFilter",
        
        getInitialState: function () {
            return {
                // TODO vlad: that's getting complicated. Maybe use some lightweight FSM library?
                showingFilter: false,
                applying: false,
                applied: false,

                failed: false,
                errorMsg: "",

                filterValue: this.createRegExpThatMatches(this.props.initialFilterValue)
            }
        },

        propTypes: {
            editable: React.PropTypes.bool.isRequired,
            caption: React.PropTypes.string.isRequired,
            onAdded: React.PropTypes.func.isRequired,

            type: React.PropTypes.string.isRequired,

            initialFilterValue: React.PropTypes.string.isRequired,
        },

        showFilter: function (e) {
            e.stopPropagation();

            if (this.state.showingFilter || this.state.applied || this.state.applying) return;

            if (this.props.editable) {
                this.setState({
                    showingFilter: true
                });
            } else {
                this.applyFilter();
            }
        },
                                                   
        applyFilter: function (e) {
            e && e.preventDefault();
            this.setState({
                applying: true
            });
            this.props.onAdded(this.props.type, this.props.type === "stacktraceHash" ? this.props.initialFilterValue : this.state.filterValue)
                .then(() => this.setState({
                    showingFilter: false,
                    applying: false,
                    applied: true
                }))
                .catch((errorMsg) => this.setState({
                    failed: true,
                    applying: false,
                    errorMsg
                }));
        },

        updateFilterValue: function (e) {
            this.setState({
                filterValue: e.target.value
            });
        },

        createRegExpThatMatches: function (target) {
            return '^' + target.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&') + '$';
        },

        render: function () {
            return (
            React.createElement(NativeListener, {onClick: this.showFilter}, 

                React.createElement("li", {className: "dropdown-menu-item"}, 

                    React.createElement("strong", null, this.props.caption), 

                    this.state.applied ?
                        React.createElement("div", {title: ""}, 
                            React.createElement("span", {className: "label label-success"}, 
                                React.createElement("i", {className: "fa fa-check", "aria-hidden": "true"}), 
                                " ", 
                                "Applied. Please, refresh the page" 
                            ), 
                            " "
                        )
                    : null, 

                    this.state.showingFilter ?
                        React.createElement("form", {onSubmit: this.applyFilter}, 
                            React.createElement("div", {className: "form-group" + (this.state.failed ? " has-error" : "")}, 
                                this.state.failed ?
                                    React.createElement("label", {className: "control-label"}, 
                                        this.state.errorMsg ||
                                        React.createElement("span", null, React.createElement("strong", null, "Failed"), " Please change filter and try again")
                                        
                                    )
                                : null, 
                                React.createElement("div", {className: "input-group"}, 
                                    React.createElement("input", {type: "text", className: "form-control", defaultValue: this.props.initialFilterValue, 
                                           value: this.state.filterValue, onChange: this.updateFilterValue}), 
                                    React.createElement("span", {className: "input-group-btn"}, 
                                        React.createElement("input", {type: "submit", className: "btn btn-success", value: "Apply"})
                                    )
                                )
                            )
                        )
                    : ['firstStacktraceFile', 'lastStacktraceFile', 'description'].indexOf(this.props.type) !== -1 ?
                        React.createElement("div", null, 
                            React.createElement("code", null, 
                                React.createElement(LongText, {text: this.props.initialFilterValue})
                            )
                        )
                    : null
                )
            )
            )
        }
    });

    var IssueFilterDropdown = React.createClass({displayName: "IssueFilterDropdown",

        /* State declaration */
        getInitialState: function () {
            return {
                stacktrace: this.props.issue.stacktrace ? JSON.parse(this.props.issue.stacktrace) : null
            };
        },

        /* Props declaration */
        propTypes: {
            issue: React.PropTypes.object.isRequired
        },

        onFilterAdded: function (type, value) {
            return new Promise((resolve, reject) => {
                type === "stacktraceHash" ?
                api("add_stacktrace_issue_filter", {
                    userToken: $.cookie("userToken"),
                    issueId: this.props.issue.id
                }, resolve, reject)
                :
                api("add_issue_filter", {
                    userToken: $.cookie("userToken"),
                    type,
                    value,
                    url: this.props.issue.url
                }, resolve, reject);
            });
        },

        render: function () {
            const stacktracePresent = this.state.stacktrace && this.state.stacktrace.length;
            let firstStacktraceFile, lastStacktraceFile;
            if (stacktracePresent) {
                firstStacktraceFile = this.state.stacktrace[0].fileName;
                lastStacktraceFile = this.state.stacktrace[this.state.stacktrace.length - 1].fileName;
            }

            return (
                React.createElement("div", {className: "dropdown"}, 
                    React.createElement("div", {className: "dropdown-toggle", id: "issue-filter-dropdown-toggle", "data-toggle": "dropdown", 
                         "aria-haspopup": "true", "aria-expanded": "true"}, 
                        React.createElement("i", {className: "fa fa-filter"}), 
                        React.createElement("span", {className: "caret"})
                    ), 

                    React.createElement(NativeListener, {stopClick: true}, 
                        React.createElement("ul", {className: "dropdown-menu dropdown-menu-right error-filter-dropdown", "aria-labelledby": "dropdownMenu1", 
                            id: "issue-filter-dropdown-menu"}, 
                            React.createElement("li", {className: "dropdown-header dropdown-header-filter"}, 
                                React.createElement("h3", null, "Filter by ")
                            ), 
                            React.createElement("li", {role: "separator", className: "divider"}), 
                            this.props.issue.stacktrace && this.props.issue.stacktrace !== '[]' ?
                                React.createElement(IssueFilter, {caption: "the same error stack", editable: false, type: "stacktraceHash", 
                                             initialFilterValue: this.props.issue.stacktrace, 
                                             onAdded: this.onFilterAdded})
                                : null, 
                            firstStacktraceFile ?
                                React.createElement(IssueFilter, {caption: "the same caller domain", editable: true, 
                                             type: "firstStacktraceFile", 
                                             initialFilterValue: firstStacktraceFile, 
                                             onAdded: this.onFilterAdded})
                                : null, 
                            lastStacktraceFile ?
                                React.createElement(IssueFilter, {caption: "the same last domain file", editable: true, 
                                             type: "lastStacktraceFile", 
                                             initialFilterValue: lastStacktraceFile, 
                                             onAdded: this.onFilterAdded})
                                : null, 

                            this.props.issue.description ?
                                React.createElement(IssueFilter, {caption: "the same error description", editable: true, type: "description", 
                                             initialFilterValue: this.props.issue.description, 
                                             onAdded: this.onFilterAdded})
                            : null
                        )
                    )
            )
            )
        }

    });

    return IssueFilterDropdown;

});