define(["jquery", "react", "app/react/play/NativeListener", "app/react/play/LongText", "app/utils/api", "jquery.cookie"], 
    function ($, React, NativeListener, LongText, api) {

    const IssueFilter = React.createClass({
        
        getInitialState: function () {
            return {
                // TODO vlad: that's getting complicated. Maybe use some lightweight FSM library?
                showingFilter: false,
                applying: false,
                applied: false,

                failed: false,
                errorMsg: "",

                filterValue: this.createRegExpThatMatches(this.props.initialFilterValue)
            }
        },

        propTypes: {
            editable: React.PropTypes.bool.isRequired,
            caption: React.PropTypes.string.isRequired,
            onAdded: React.PropTypes.func.isRequired,

            type: React.PropTypes.string.isRequired,

            initialFilterValue: React.PropTypes.string.isRequired,
        },

        showFilter: function (e) {
            e.stopPropagation();

            if (this.state.showingFilter || this.state.applied || this.state.applying) return;

            if (this.props.editable) {
                this.setState({
                    showingFilter: true
                });
            } else {
                this.applyFilter();
            }
        },
                                                   
        applyFilter: function (e) {
            e && e.preventDefault();
            this.setState({
                applying: true
            });
            this.props.onAdded(this.props.type, this.props.type === "stacktraceHash" ? this.props.initialFilterValue : this.state.filterValue)
                .then(() => this.setState({
                    showingFilter: false,
                    applying: false,
                    applied: true
                }))
                .catch((errorMsg) => this.setState({
                    failed: true,
                    applying: false,
                    errorMsg
                }));
        },

        updateFilterValue: function (e) {
            this.setState({
                filterValue: e.target.value
            });
        },

        createRegExpThatMatches: function (target) {
            return '^' + target.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&') + '$';
        },

        render: function () {
            return (
            <NativeListener onClick={this.showFilter}>

                <li className="dropdown-menu-item">

                    <strong>{this.props.caption}</strong>

                    {this.state.applied ?
                        <div title="">
                            <span className="label label-success">
                                <i className="fa fa-check" aria-hidden="true"></i>
                                {" "}
                                Applied. Please, refresh the page 
                            </span>
                            {" "}
                        </div>
                    : null}

                    {this.state.showingFilter ?
                        <form onSubmit={this.applyFilter}>
                            <div className={"form-group" + (this.state.failed ? " has-error" : "")}>
                                {this.state.failed ?
                                    <label className="control-label">
                                        {this.state.errorMsg ||
                                        <span><strong>Failed</strong> Please change filter and try again</span>
                                        }
                                    </label>
                                : null}
                                <div className="input-group">
                                    <input type="text" className="form-control" defaultValue={this.props.initialFilterValue}
                                           value={this.state.filterValue} onChange={this.updateFilterValue}/>
                                    <span className="input-group-btn">
                                        <input type="submit" className="btn btn-success" value="Apply"/>
                                    </span>
                                </div>
                            </div>
                        </form>
                    : ['firstStacktraceFile', 'lastStacktraceFile', 'description'].indexOf(this.props.type) !== -1 ?
                        <div>
                            <code>
                                <LongText text={this.props.initialFilterValue}/>
                            </code>
                        </div>
                    : null }
                </li>
            </NativeListener>
            )
        }
    });

    var IssueFilterDropdown = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                stacktrace: this.props.issue.stacktrace ? JSON.parse(this.props.issue.stacktrace) : null
            };
        },

        /* Props declaration */
        propTypes: {
            issue: React.PropTypes.object.isRequired
        },

        onFilterAdded: function (type, value) {
            return new Promise((resolve, reject) => {
                type === "stacktraceHash" ?
                api("add_stacktrace_issue_filter", {
                    userToken: $.cookie("userToken"),
                    issueId: this.props.issue.id
                }, resolve, reject)
                :
                api("add_issue_filter", {
                    userToken: $.cookie("userToken"),
                    type,
                    value,
                    url: this.props.issue.url
                }, resolve, reject);
            });
        },

        render: function () {
            const stacktracePresent = this.state.stacktrace && this.state.stacktrace.length;
            let firstStacktraceFile, lastStacktraceFile;
            if (stacktracePresent) {
                firstStacktraceFile = this.state.stacktrace[0].fileName;
                lastStacktraceFile = this.state.stacktrace[this.state.stacktrace.length - 1].fileName;
            }

            return (
                <div className="dropdown">
                    <div className="dropdown-toggle" id="issue-filter-dropdown-toggle" data-toggle="dropdown"
                         aria-haspopup="true" aria-expanded="true">
                        <i className="fa fa-filter"></i>
                        <span className="caret"></span>
                    </div>

                    <NativeListener stopClick>
                        <ul className="dropdown-menu dropdown-menu-right error-filter-dropdown"  aria-labelledby="dropdownMenu1"
                            id="issue-filter-dropdown-menu">
                            <li className="dropdown-header dropdown-header-filter">
                                <h3>Filter by </h3>
                            </li>
                            <li role="separator" className="divider"></li>
                            {this.props.issue.stacktrace && this.props.issue.stacktrace !== '[]' ?
                                <IssueFilter caption={"the same error stack"} editable={false} type="stacktraceHash"
                                             initialFilterValue={this.props.issue.stacktrace}
                                             onAdded={this.onFilterAdded}/>
                                : null}
                            {firstStacktraceFile ?
                                <IssueFilter caption={"the same caller domain"} editable={true}
                                             type="firstStacktraceFile"
                                             initialFilterValue={firstStacktraceFile}
                                             onAdded={this.onFilterAdded}/>
                                : null}
                            {lastStacktraceFile ?
                                <IssueFilter caption={"the same last domain file"} editable={true}
                                             type="lastStacktraceFile"
                                             initialFilterValue={lastStacktraceFile}
                                             onAdded={this.onFilterAdded}/>
                                : null}

                            {this.props.issue.description ?
                                <IssueFilter caption={"the same error description"} editable={true} type="description"
                                             initialFilterValue={this.props.issue.description}
                                             onAdded={this.onFilterAdded}/>
                            : null}
                        </ul>
                    </NativeListener>
            </div>
            )
        }

    });

    return IssueFilterDropdown;

});