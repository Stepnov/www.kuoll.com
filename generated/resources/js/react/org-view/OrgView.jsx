define(["jquery", "react", "app/react/org-view/UsersList", "app/react/org-view/InvitationForm", "app/User"],
function ($, React, UsersList, InvitationForm, User) {

    const OrgView = React.createClass({

        getInitialState: function () {
            return {
                isAdmin: false,

                newInvitations: []
            };
        },

        propTypes: {

        },

        onInvitationSent: function (invitation) {
            const newInvitations = this.state.newInvitations.concat([invitation]);
            this.setState({
                newInvitations
            });
        },

        onInvitationCanceled: function (inv) {
            this.setState({
                newInvitations: this.state.newInvitations.filter(i => i.code !== inv.code)
            });
        },

        render: function () {
            // TODO vlad: nice place to try react-router and then start using it wherever appropriate
            return (
                <div>
                    <h1>Team users and roles</h1>

                    <InvitationForm onInvitationSent={this.onInvitationSent}/>
                    <UsersList isAdmin={this.state.isAdmin} newInvitations={this.state.newInvitations}
                        onInvitationCanceled={this.onInvitationCanceled}/>
                </div>
            )
        },

        componentDidMount: function () {
            User.getInfo((user) => this.setState({
                isAdmin: user.isAdmin
            }));
        }

    });

    return OrgView;

});