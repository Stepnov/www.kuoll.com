define(["jquery", "react", "app/utils/api"], function ($, React, api) {

    const InvitationForm = React.createClass({displayName: "InvitationForm",

        getInitialState: function () {
            return {
                email: "",

                sent: false,

                failed: false,
                errorMsg: null
            };
        },

        propTypes: {
            onInvitationSent: React.PropTypes.func.isRequired
        },

        emailUpdated: function (e) {
            this.setState({
                email: e.target.value,
                failed: false,
                sent: false,
                errorMsg: null
            });
        },

        inviteUser: function (e) {
            e.preventDefault();
            api("org/invite_user", {
                userToken: $.cookie("userToken"),
                email: this.state.email
            }, (resp) => {
                this.props.onInvitationSent(resp.invitation);
                this.setState({
                    sent: true
                });
            }, (errorMsg) => {
                this.setState({
                    errorMsg,
                    failed: true
                });
            });
        },

        render: function () {
            return (
                React.createElement("div", {className: "form-inline"}, 
                    React.createElement("h3", null, "Invite new users to your team"), 
                    this.state.failed ?
                        React.createElement("p", null, 
                            React.createElement("label", {className: "control-label"}, 
                                this.state.errorMsg ||
                                React.createElement("span", null, React.createElement("strong", null, "Failed"), " Please check the email and try again")
                                
                            )
                        )
                    : null, 

                    React.createElement("p", null, 
                        React.createElement("input", {type: "text", className: "form-control", value: this.state.email, size: "30", 
                                onChange: this.emailUpdated, placeholder: "Enter email address", focus: true}), 
                        this.state.sent ?
                            React.createElement("span", {className: ""}, 
                                React.createElement("i", {className: "fa fa-check"})
                            )
                        : null, 
                        " ", 

                        React.createElement("button", {className: "btn btn-success", onClick: this.inviteUser}, "Invite")
                    )
                )
            )
        }

    });

    return InvitationForm;

});