define(["jquery", "react", "app/utils/api"], function ($, React, api) {

    const InvitationForm = React.createClass({

        getInitialState: function () {
            return {
                email: "",

                sent: false,

                failed: false,
                errorMsg: null
            };
        },

        propTypes: {
            onInvitationSent: React.PropTypes.func.isRequired
        },

        emailUpdated: function (e) {
            this.setState({
                email: e.target.value,
                failed: false,
                sent: false,
                errorMsg: null
            });
        },

        inviteUser: function (e) {
            e.preventDefault();
            api("org/invite_user", {
                userToken: $.cookie("userToken"),
                email: this.state.email
            }, (resp) => {
                this.props.onInvitationSent(resp.invitation);
                this.setState({
                    sent: true
                });
            }, (errorMsg) => {
                this.setState({
                    errorMsg,
                    failed: true
                });
            });
        },

        render: function () {
            return (
                <div className="form-inline">
                    <h3>Invite new users to your team</h3>
                    {this.state.failed ?
                        <p>
                            <label className="control-label">
                                {this.state.errorMsg ||
                                <span><strong>Failed</strong> Please check the email and try again</span>
                                }
                            </label>
                        </p>
                    : null}

                    <p>
                        <input type="text" className="form-control" value={this.state.email} size="30"
                                onChange={this.emailUpdated} placeholder="Enter email address" focus/>
                        {this.state.sent ?
                            <span className="">
                                <i className="fa fa-check"/>
                            </span>
                        : null}
                        {" "}

                        <button className="btn btn-success" onClick={this.inviteUser}>Invite</button>
                    </p>
                </div>
            )
        }

    });

    return InvitationForm;

});