define(["react", "app/react/records-old/RecordTableHeader", "app/react/records-old/RecordRow"], function (React, RecordTableHeader, RecordRow) {
    return React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {
            records: React.PropTypes.array.isRequired
        },

        render: function () {
            if (this.props.records.length === 0) {
                return null;
            }
            return (
                React.createElement("div", {className: "recordsTableContainer"}, 

                    React.createElement("table", {id: "recordsTable", className: "table table-striped"}, 
                        React.createElement(RecordTableHeader, null), 
                        React.createElement("tbody", {id: "recordsTableBody"}, 
                    this.props.records.map(function (record) {
                        return (
                            React.createElement(RecordRow, {record: record, key: record.recordCode})
                        )
                    })
                        )
                    )

                )
            )
        }
    });
    return RecordsTable;
});
