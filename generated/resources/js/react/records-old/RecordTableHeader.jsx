define(["react"], function (React) {
    var RecordTableHeader = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {},

        render: function () {
            return (
                <thead>
                    <tr className="myTableHeader">
                        <th className="descriptionColumn">Description</th>
                        <th className="durationColumn">Duration</th>
                        <th className="startTimeColumn">Created</th>
                        <th className="lastOpenedColumn">Last opened</th>
                        <th className="autonomousModeColumn">Autonomous</th>
                        <th className="deleteRecordColumn">Delete</th>
                    </tr>
                </thead>
            )
        }

    });
    return RecordTableHeader;
});
