define(["react", "app/react/records-old/RecordTableHeader", "app/react/records-old/RecordRow"], function (React, RecordTableHeader, RecordRow) {
    return React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {
            records: React.PropTypes.array.isRequired
        },

        render: function () {
            if (this.props.records.length === 0) {
                return null;
            }
            return (
                <div className="recordsTableContainer">

                    <table id="recordsTable" className="table table-striped">
                        <RecordTableHeader />
                        <tbody id="recordsTableBody">
                    {this.props.records.map(function (record) {
                        return (
                            <RecordRow record={record} key={record.recordCode}/>
                        )
                    })}
                        </tbody>
                    </table>

                </div>
            )
        }
    });
    return RecordsTable;
});
