define(["jquery", "react", "app/react/records-old/RecordsTable", "jquery.cookie"], function ($, React,RecordsTable) {
    return React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                records: [],
                config: this.getDefaultConfig()
            };
        },

        /* Props declaration */
        propTypes: {
            RecordsSource: React.PropTypes.object.isRequired,
            config: React.PropTypes.object
        },

        getDefaultConfig: function () {
            return {
                ignoredColumns: []
            }
        },

        render: function () {
            if (this.state.records && this.state.records.length > 0) {
                return (
                    React.createElement(RecordsTable, {records: this.state.records})
                )
            } else if (typeof this.state.ownRecord != "undefined") {
                return (
                    React.createElement("p", {className: "noRecordsMsg"}, 
                    this.state.ownRecord === true ?
                        "Here will be your records"
                        : "This user hasn't created any record yet."
                        
                    )
                );
            } else {
                return null;
            }
        },

        ignoreColumns: function (columns) {
            var style = document.createElement("style");
            var styleText;
            for (var i in  columns) {
                if (columns.hasOwnProperty(i)) {
                    if (styleText) {
                        styleText += ", #recordsTable ." + columns[i];
                    } else {
                        styleText = "#recordsTable ." + columns[i];
                    }
                }
            }
            if (styleText) {
                styleText += "{ display: none; }";
                style.appendChild(document.createTextNode(styleText));
                document.head.appendChild(style);
            }
        },

        reloadRecords: function () {
            var rs = this.props.RecordsSource;
            rs.getRecords(function (data) {
                this.setState(data);
            }.bind(this));
        },

        componentWillMount: function () {
            var config = this.props.config;
            if (config) {
                this.setState({
                    config: config
                });
                this.ignoreColumns(config.ignoredColumns);
            }

            this.reloadRecords();

            window.addEventListener("message", function (event) {
                var data = JSON.parse(event.data);
                if (data.action == "reloadRecords") {
                    this.reloadRecords();
                }
            }.bind(this));
        }

    })
});