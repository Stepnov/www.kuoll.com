define(["jquery", "react", "jquery.ui", "css!/resources/css/jquery-ui", "css!/resources/css/notification"], function ($, React) {

    var notificationContentByType = {
        "NoRecordsBrowserNotification": (
            <span className="user-notification">
                <p>Hi! You haven't created any records yet. </p>
                <p>Do you want to include Kuoll into your website to catch bugs easier? Check out{" "}
                    <a href="/quick-start.html">API quide</a>
                </p>
                <p>
                    Just want to try Kuoll without installing anything? Check our{" "}
                    <a href="/test.html">test page</a>.
                </p>
            </span>
        )
    };
    var notificationTypes = Object.keys(notificationContentByType);

    var Notification = React.createClass({

        /* Props declaration */
        propTypes: {
            type: React.PropTypes.oneOf(notificationTypes).isRequired
        },

        render: function () {
            return (
                <div className="user-notification" ref="notification">
                {notificationContentByType[this.props.type]}
                </div>
            )
        },

        componentDidMount: function () {
            var $notification = $(React.findDOMNode(this.refs.notification));
            $notification.dialog({
                autoOpen: true,
                title: "Kuoll notification",
                resizable: false,
                draggable: false,
                dialogClass: "user-notification-dialog",
                width: 350,
                position: {
                    my: "right bottom",
                    at: "right bottom"
                },
                modal: false
            });
        }

    });

    return Notification;

});
