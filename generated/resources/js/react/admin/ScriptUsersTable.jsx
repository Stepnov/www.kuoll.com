define(["jquery", "react", "app/react/Table", "app/utils/api", "app/utils/DateUtils", "jquery.cookie"], function ($, React,Table, api, DateUtils) {

    var ScriptUsersTable = React.createClass({

        columns: {
            domain: "Domain",
            loadingCount: "embedScript loaded",
            lastLoadTimestamp: "Last load date",
            users: "User info"
        },

        cells: {
            lastLoadTimestamp: function (row) {
                return DateUtils.formatDate(row.lastLoadTimestamp);
            },
            users: function (row) {
                return (
                    <div>
                        {row.users.map(function (user) {
                            return (
                                <div key={user.userId}>
                                    User ID: {user.userId};
                                    Org ID: {user.orgId};
                                    Plan name: {user.planName};
                                    Issues quota left: {user.issuesLeft}
                                </div>
                            )
                        })}
                    </div>
                )
            }
        },

        /* State declaration */
        getInitialState: function () {
            return {
                userToken: null
            };
        },

        render: function () {
            return (
                <Table columns={this.columns} data={this.state.scriptUsers} cells={this.cells}/>
            )
        },

        componentDidMount: function () {
            var self = this;
            api("getScriptUsers", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                if (resp.scriptUsers) {
                    var scriptUsers = resp.scriptUsers;
                    self.setState({
                        scriptUsers: scriptUsers
                    })
                }
            });
        }

    });

    return ScriptUsersTable;

});
