define(["jquery", "react", "app/react/BrowserIcon", "app/react/OsIcon", "app/react/DeviceIcon", "app/react/issue-filter/IssueFilterDropdown",
        "app/utils/DateUtils", "app/utils/api"],
    function ($, React, BrowserIcon, OsIcon, DeviceIcon, IssueFilterDropdown, DateUtils, api) {

    var builtInIssueTypes = {
        "consoleError": "Console Error",
        "JavaScript error": "JavaScript Error",
        "Server error": "Server error"
    };

    var IssueRow = React.createClass({
    
        /* State declaration */
        getInitialState: function () {
            return {
                isDeleted: false
            };
        },
        
        /* Props declaration */
        propTypes: {
            issue: React.PropTypes.object.isRequired
        },

        toggleIssue: function (e) {
            e.stopPropagation();
            api(this.state.isDeleted ? "restoreIssue" : "deleteIssue", {
                issueId: this.props.issue.id
            });
            this.setState({
                isDeleted: !this.state.isDeleted
            });
        },
    
        render: function () {
            var link = document.createElement("a");
            link.href = this.props.issue.url;
            var domain = link.host;

            var isBuiltInIssueType = builtInIssueTypes[this.props.issue.type];
            var issueType = isBuiltInIssueType || (this.props.issue.type === "error" ? "End user report" : this.props.issue.type);

            return (
                <tr className={"issue" + (this.state.isDeleted ? " deleted" : "")}>
                    <td className="open-record-column">
                        <a href={"/play.html?recordCode=" + this.props.issue.recordCode + "&frameNum=" +
                    this.props.issue.endFrameNum + "&sequentNum=" + this.props.issue.endSequentNum}>
                            <i className="fa fa-link"></i>
                        </a>
                    </td>
                    <td className="id-column">{this.props.issue.externalUserId}</td>
                    <td className="website-column">{domain}</td>
                    <td>
                        <BrowserIcon userAgent={this.props.issue.userAgent} withText={true} />
                    </td>
                    <td>
                        <span title={new Date(this.props.issue.time).toUTCString()}>
                            {DateUtils.formatDate(this.props.issue.time)}
                        </span>
                    </td>
                    <td>{issueType}</td>
                    <td>
                        <OsIcon userAgent={this.props.issue.userAgent} withText={true}/>
                        <DeviceIcon userAgent={this.props.issue.userAgent} />
                    </td>
                    <td className="description-column">
                        {isBuiltInIssueType ?
                            <code>{this.props.issue.description}</code>
                            : this.props.issue.description}
                    </td>
                    <td></td>
                    <td className="deleteIssueIcon">
                        <i className="fa fa-times" aria-hidden="true" onClick={this.toggleIssue}></i>
                        {this.state.isDeleted ? "Restore" : null}
                    </td>
                    <td>
                        <IssueFilterDropdown issue={this.props.issue} />
                    </td>
                </tr>
            )
        }
        
    });
    
    return IssueRow;    

});