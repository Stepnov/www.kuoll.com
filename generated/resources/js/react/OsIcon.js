define(["jquery", "react"], function ($, React) {

    var OsIcon = React.createClass({displayName: "OsIcon",

        /* Props declaration */
        propTypes: {
            userAgent: React.PropTypes.object,
            withText: React.PropTypes.bool
        },

        render: function () {
            var iconClass, title, fullName;
            var userAgent = this.props.userAgent;
            if (userAgent && userAgent.os && userAgent.os.name) {
                var os = userAgent.os.name;
                fullName = userAgent.os.version;
                title = "User's OS is " + userAgent.os.name + " " + fullName;
                if (os.indexOf("Windows") === 0) {
                    iconClass = "fa-windows";
                } else if (["Ubuntu", "Mint", "Linux", "Gentoo", "Debian", "Fedora"].indexOf(os) !== -1) {
                    iconClass = "fa-linux";
                } else if (["Mac OS", "iOS"].indexOf(os) !== -1) {
                    iconClass = "fa-apple";
                } else if ("Android" === os) {
                    iconClass = "fa-android";
                } else {
                    iconClass = "fa-linux"; // Most other possible OS names are different distributions of Linux, so show Linux icon by default
                }
            } else {
                title = "No information about user's OS";
                fullName = "Unknown";
                iconClass = "fa-linux default-icon";
            }

            return (
                React.createElement("span", {title: title}, 
                React.createElement("i", {className: "fa fa-fw " + iconClass}), 
                    this.props.withText ?
                        React.createElement("span", {className: "os-icon-text"}, fullName)
                    : null, 
                    " "
                )
            )
        }

    });

    return OsIcon;

});