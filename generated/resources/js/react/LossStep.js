define(["jquery", "react"], function ($, React) {

    var LossStep = React.createClass({displayName: "LossStep",

        /* Props declaration */
        propTypes: {
            revenueLossPct: React.PropTypes.number,
            conversionDropPct: React.PropTypes.number,
            sessionsAffectedPct: React.PropTypes.number,
            stepName: React.PropTypes.string
        },

        getDefaultProps: function () {
            return {
                loadStepLoss: 0,
                loadStepConvDrop: 0,
                loadStepAffectedPct: 0,
    
                withVersion: true
            };
        },

        render: function () {

            return (
                React.createElement("div", {className: "stepChart"}, 
                React.createElement("div", {className: "stepChartStep stepChartLoad " + (this.props.revenueLossPct == 0 ? " stepChartStepNull": "")}, 
                    React.createElement("div", {className: "stepChartColumn stepChartLossColumn", style: {height: "100%"}, 
                            title: "Revenue loss on " + this.props.stepName + " step, " + (100 * this.props.revenueLossPct).toFixed(2) + "%"
                            }, 
                        React.createElement("div", {className: "  stepChartLoss", style: {height: (this.props.revenueLossPct * 100) + "%"}}
                        ), 
                        React.createElement("div", null, 
                            React.createElement("span", {className: "fa-stack fa-stack-small"}, 
                                React.createElement("i", {className: "fa fa-circle fa-stack-2x"}), 
                                React.createElement("i", {className: "fa fa-usd fa-stack-1x fa-inverse"})
                            )
                        )
                    ), 
                    React.createElement("div", {className: "stepChartColumn stepChartDropColumn", style: {height: "100%"}, 
                            title: "Conversion drop on " + this.props.stepName + " step, " + (100 * this.props.conversionDropPct).toFixed(2) + "%"
                            }, 
                        React.createElement("div", {className: "  stepChartDrop", style: {height: (this.props.conversionDropPct * 100) + "%"}}
                        ), 
                        React.createElement("div", null, 
                            React.createElement("span", {className: "fa-stack fa-stack-small"}, 
                                React.createElement("i", {className: "fa fa-circle fa-stack-2x"}), 
                                React.createElement("i", {className: "fa fa-user fa-stack-1x fa-inverse"})
                            )
                        )
                    ), 
                    React.createElement("div", {className: "stepChartColumn stepChartAffectedColumn", style: {height: "100%"}, 
                            title: "User sessions affected on " + this.props.stepName + " step, " + (100 * this.props.sessionsAffectedPct).toFixed(2) + "%"
                            }, 
                        React.createElement("div", {className: "  stepChartAffected", style: {height: (this.props.sessionsAffectedPct * 100)+ "%"}}
                        ), 
                        React.createElement("div", null, 
                            React.createElement("span", {className: "fa-stack fa-stack-small"}, 
                                    React.createElement("i", {className: "fa fa-circle fa-stack-2x"}), 
                                    React.createElement("i", {className: "fa fa-filter fa-stack-1x fa-inverse"})
                            )
                        )
                    )
                    /* {this.props.revenueLossPct} = {this.props.conversionDropPct} * {this.props.sessionsAffectedPct} */
                )
                )
            )
        }

    });

    return LossStep;

});