define(["react"], function (React) {

    var Table = React.createClass({displayName: "Table",

        /* Props declaration */
        propTypes: {
            data: React.PropTypes.array,
            columns: React.PropTypes.object.isRequired,
            cells: React.PropTypes.object
        },

        render: function () {
            var self = this;
            return (
                React.createElement("table", {className: "table table-striped"}, 
                    React.createElement("thead", null, 
                Object.getOwnPropertyNames(self.props.columns).map(function (columnName) {
                    return (
                        React.createElement("th", {key: columnName}, 
                            self.props.columns[columnName]
                        )
                    )
                })
                    ), 
                    React.createElement("tbody", null, 
                (this.props.data || []).map(function (data, index) {
                    return (
                        React.createElement("tr", {key: index}, 
                            Object.getOwnPropertyNames(self.props.columns).map(function (columnName) {
                                var cellContent;
                                if (self.props.cells && self.props.cells[columnName]) {
                                    cellContent = self.props.cells[columnName](data);
                                } else {
                                    cellContent = data[columnName];
                                }
                                return (
                                    React.createElement("td", {key: columnName}, 
                                        cellContent
                                    )
                                )
                            })
                        )
                    )
                })
                    )
                )
            )
        }

    });

    return Table;

});
