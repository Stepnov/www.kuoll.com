define(["jquery", "react", "app/react/play/Stacktrace", "app/react/BrowserIcon", "app/react/OsIcon",
        "app/react/DeviceIcon", "app/react/play/LongText", "app/utils/api", "app/utils/DateUtils", "app/utils/UrlUtils", "ua-parser", "jquery.cookie"],
    function ($, React, Stacktrace, BrowserIcon, OsIcon, DeviceIcon, LongText, api, DateUtils, UrlUtils, UAParser) {

    // http://stackoverflow.com/a/34890276/3478389
    function groupBy(array, key) {
        return array.reduce(function(result, obj) {
            (result[obj[key]] = result[obj[key]] || []).push(obj);
            return result;
        }, {});
    }

    var IssuesGroup = React.createClass({displayName: "IssuesGroup",

        /* State declaration */
        getInitialState: function () {
            return {
                issuesGroup: {
                    issues: {},
                    platforms: {
                        browser: {},
                        os: {},
                        device: {}
                    },
                    stacktrace: []
                }
            };
        },

        /* Props declaration */
        propTypes: {

        },

        render: function () {
            const self = this;
            let rowIndex = 0;

            const recordCodes = Object.keys(this.state.issuesGroup.issues);
            const allIssues = recordCodes
                .map(r => this.state.issuesGroup.issues[r]).reduce((a, b) => a.concat(b), []); // flatMap

            const recordDurations = allIssues
                .map(i => i.effectiveRecordDuration)
                .filter(d => !!d);
            const averageRecordDuration = recordDurations.reduce((a, b) => a + b, 0) / recordDurations.length;

            const averageFramesPerRecord = allIssues.map(i => i.totalFrames).reduce((a, b) => a + b, 0) / allIssues.length;

            return (
                React.createElement("div", {className: "container"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: " col-md-12"}, 
                            React.createElement("h1", null, this.state.issuesGroup.description, React.createElement("br", null), 
                            React.createElement("small", null, "JavaScript Error group")
                            )
                        ), 

                        React.createElement("div", {className: " col-md-6"}, 
                            React.createElement("h2", null, "Story"), 
                            React.createElement("p", null, 
                                "First found ", DateUtils.formatDate(this.state.issuesGroup.firstOccurrence), ".", 
                                " ", 
                                "Happened ", this.state.issuesGroup.issuesCount, " times.",  
                                " ", 
                                "Last time ", DateUtils.formatDate(this.state.issuesGroup.lastOccurrence), "."
                            ), 

                            React.createElement("h2", null, "Avg. stats"), 
                            React.createElement("p", null, 
                                DateUtils.formatDuration(averageRecordDuration).short, " is average session duration" + ' ' +
                                ". ", averageFramesPerRecord.toFixed(1), " pages views per session"
                                /*. 2:23 before error first happens.*/
                                /*1:19 until session is closed*/
                                /*. 13 seconds is average page load time.*/
                            ), 

                            React.createElement("h3", null, "[By hour report goes here]"), 

                            React.createElement("h3", null, "[By stage report goes here]")

                            
                        ), 

                        React.createElement("div", {className: "col-md-6"}, 
                            React.createElement("h2", null, "Stack trace example"), 
                            React.createElement(Stacktrace, {stacktrace: this.state.issuesGroup.stacktrace, 
                                description: this.state.issuesGroup.description}), 

                            React.createElement("div", null, 
                                React.createElement("h4", null, "Browsers"), 
                                Object.keys(this.state.issuesGroup.platforms.browser).map(function (browser) {
                                    return (
                                        React.createElement(BrowserIcon, {userAgent: {browser: self.state.issuesGroup.platforms.browser[browser]}, 
                                                     withVersion: false})
                                    )
                                }), 

                                React.createElement("h4", null, "Operating systems"), 
                                Object.keys(this.state.issuesGroup.platforms.os).map(function (os) {
                                    return (
                                        React.createElement(OsIcon, {userAgent: {os: self.state.issuesGroup.platforms.os[os]}})
                                    )
                                }), 

                                React.createElement("h4", null, "Devices"), 
                                Object.keys(this.state.issuesGroup.platforms.device).map(function (device) {
                                    return (
                                        React.createElement(DeviceIcon, {userAgent: {device: self.state.issuesGroup.platforms.device[device]}})
                                    )
                                })
                            )
                        )
                    ), 

                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: " col-md-12"}, 
                            React.createElement("h2", null, "Recorded user sessions"), 
                            React.createElement("table", {className: "table table-striped"}, 
                                React.createElement("thead", null, 
                                React.createElement("tr", null, 
                                    React.createElement("th", null, "#"), 
                                    React.createElement("th", null, "Date"), 
                                    React.createElement("th", {title: "Session duration"}, "Duration"), 
                                    React.createElement("th", {title: "Pages viewed"}, "Pages"), 
                                    React.createElement("th", null, "Browser"), 
                                    React.createElement("th", null, "User ID"), 
                                    React.createElement("th", {title: "User email"}, "Email"), 
                                    React.createElement("th", {title: "Page load time"}, "Load")
                                )
                                ), 
                                React.createElement("tbody", null, 
                                recordCodes.map(function (recordCode) {
                                    return self.state.issuesGroup.issues[recordCode].map((issue) => {
                                            ++rowIndex;
                                            return (
                                                React.createElement("tr", {key: rowIndex}, 
                                                    React.createElement("td", null, rowIndex), 
                                                    React.createElement("td", null, 
                                                        React.createElement("a", {href: "/play.html?recordCode=" + recordCode +
                                                        "&issueId=" + issue.issueId, target: "_blank"}, 
                                                            DateUtils.formatDate(issue.time)
                                                        )
                                                    ), 
                                                    React.createElement("td", null, 
                                                        DateUtils.formatDuration(issue.recordDuration).short, 
                                                        issue.isRecordCompleted ? " and counting" : null
                                                    ), 
                                                    React.createElement("td", null, issue.totalFrames), 
                                                    React.createElement("td", null, "Chrome"), 
                                                    React.createElement("td", null, issue.externalUserId), 
                                                    React.createElement("td", null, issue.externalUserEmail), 
                                                    React.createElement("td", null, "13s")
                                                )
                                            );
                                        }
                                    )
                                })
                                )
                            )
                        )
                    )
                )
            )
        },

        componentDidMount: function () {
            const self = this;
            const id = UrlUtils.getParameterByName("id");
            const hash = UrlUtils.getParameterByName("hash");
            api("get_issues_group_by_hash", {
                issuesGroupId: id,
                stacktraceHash: hash,
                userToken: $.cookie("userToken")
            }, function (resp) {
                var issuesGroup = resp.issuesGroup;
                issuesGroup.issues = groupBy(issuesGroup.issues, "recordCode");

                var platforms = {
                    browser: {},
                    os: {},
                    device: {}
                };
                issuesGroup.userAgents.forEach(function (ua) {
                    var result = new UAParser(ua).getResult();
                    if (result.browser)
                        platforms.browser[result.browser.name] = result.browser;
                    if (result.os)
                        platforms.os[result.os.name] = result.os;
                    if (result.device)
                        platforms.device[result.device.vendor] = result.device;
                });
                issuesGroup.platforms = platforms;
                delete issuesGroup.userAgents;

                self.setState({
                    issuesGroup: issuesGroup
                });
            });
        }

    });

    return IssuesGroup;

});