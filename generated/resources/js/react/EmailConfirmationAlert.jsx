define(["jquery", "react", "app/utils/api", "jquery.cookie"], function ($, React,api) {
    return React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                emailConfirmed: true
            };
        },

        /* Props declaration */
        propTypes: {},

        render: function () {
            return (
                <div className="col-md-8 col-md-offset-2" id="emailConfirmationAlert"
                    role="alert" style={{display: (this.state.emailConfirmed ? "none" : "block")}}>
                    <div className="alert alert-warning">
                        You have not confirmed your email yet. Check your mailbox for confirmation email, please.
                    </div>
                </div>
            )
        },

        componentDidMount: function () {
            var self = this;
            api("checkConfirmation", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                self.setState({
                    emailConfirmed: resp.confirmed
                })
            });
        }

    })
});