define(["jquery", "react", "app/react/play/Stacktrace", "app/react/BrowserIcon", "app/react/ConversionSteps", "app/react/OsIcon",
        "app/react/DeviceIcon", "app/react/play/LongText", "app/react/issue-filter/IssueFilterDropdown",
        "app/utils/api", "app/utils/DateUtils", "app/utils/UrlUtils", "ua-parser", "jquery.cookie"],
    function ($, React, Stacktrace, BrowserIcon, ConversionSteps, OsIcon, DeviceIcon, LongText, IssueFilterDropdown, api, DateUtils, UrlUtils, UAParser) {

    var IssuesDashboard = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                issuesGroups: []
            };
        },

        /* Props declaration */
        propTypes: {

        },

        render: function () {
            return (
                <div>
                <h2 style={{marginBottom: "30px"}}>
                    {UrlUtils.getParameterByName("type") === "xhr" ? "Server" : UrlUtils.getParameterByName("type") === "console" ? "Console" : "JS"} errors for the last 2 days
                </h2>
                <p>
                    Use filters 
                    (<i className="fa fa-filter" style={{color: "#2ecc71"}} aria-hidden="true"></i>)
                    to hide errors from this page. 
                    {" "}
                    Manage active filters on set <a href="/errorFilters.html">Error filters</a> page.
                
                </p>

                <table className="table table-hover">
                    <thead>
                    <th>Occurrences</th>
                    <th>Platform</th>
                    <th title="When error happended">Since</th>
                    <th>Latest</th>
                    <th>Stack trace</th>
                    <th title="Hide specific errors">Filter</th>
                    </thead>
                    <tbody>
                    {this.state.issuesGroups.map(function (group, index) {
                        const firstIssue = group.firstIssue;
                        return (
                            <tr key={index}>
                                <td className="centerText">
                                    <p>
                                        <a href={"/issues-group.html?id=" + group.id} target="_blank"
                                            title="See all records for this issue"
                                            >
                                                <span className="issuesNumberInGroup">{group.issuesCount}
                                                </span>
                                        </a>
                                    </p>
                                    <p >
                                        <a href={"/play.html?recordCode=" + firstIssue.recordCode +
                                            "&issueId=" + firstIssue.id} target="_blank"
                                           title={"Recorded " + DateUtils.formatDate(firstIssue.time)}>
                                            Example 
                                            
                                        </a>
                                    </p>
                                </td>
                                <td>
                                    <span className="browser-icons">
                                        {Object.keys(group.platforms.browser).map(function (browser) {
                                            return (
                                                <BrowserIcon userAgent={{browser: group.platforms.browser[browser]}}
                                                             withVersion={false}/>
                                            )
                                        })}
                                    </span>
                                    <span className="os-icons">
                                        {Object.keys(group.platforms.os).map(function (os) {
                                            return (
                                                <OsIcon userAgent={{os: group.platforms.os[os]}}/>
                                            )
                                        })}
                                    </span>
                                    <span className="device-icons">
                                        {Object.keys(group.platforms.device).map(function (device) {
                                            return (
                                                <DeviceIcon userAgent={{device: group.platforms.device[device]}}/>
                                            )
                                        })}
                                    </span>
                                </td>
                                <td className="first-occurrence">
                                    {DateUtils.formatDate(group.firstOccurrence)}
                                </td>
                                <td className="last-occurrence">
                                    {DateUtils.formatDate(group.lastOccurrence)}
                                </td>
                                <td style={{maxWidth: "50%"}}>
                                    <Stacktrace stacktrace={group.stacktrace} description={group.description}/>
                                </td>
                                <td>
                                    <IssueFilterDropdown issue={firstIssue} />
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>

                {this.state.issuesGroups.length === 0 ? 
                    <div className="jumbo">
                        <p>
                            No errors caught yet.
                        </p>
                        <h2 style={{color: "#8e44ad"}}>Install Kuoll to your web application to see error reports</h2>
                        <p>
                            <a href="#onboard" className="btn btn-lg btn-success">Install</a>
                        </p>

                    </div>
                :""}

                </div>
            )
        },

        componentDidMount: function () {
            const self = this;
            const type = UrlUtils.getParameterByName("type") === "xhr" ? "Server error" :
                UrlUtils.getParameterByName("type") === "console" ? "consoleError" : "JavaScript error";
            api("get_issues_groups", {
                userToken: $.cookie("userToken"),
                type: type
            }, function (resp) {
                const issuesGroups = resp.issuesGroups;
                issuesGroups.forEach(function (group) {
                    const platforms = {
                        browser: {},
                        os: {},
                        device: {}
                    };
                    group.userAgents.forEach(function (ua) {
                        const result = new UAParser(ua).getResult();
                        if (result.browser)
                            platforms.browser[result.browser.name] = result.browser;
                        if (result.os)
                            platforms.os[result.os.name] = result.os;
                        if (result.device)
                            platforms.device[result.device.vendor] = result.device;
                    });
                    group.platforms = platforms;
                    delete group.userAgents;
                });
                self.setState({
                    issuesGroups: issuesGroups
                })
            });
        }

    });

    return IssuesDashboard;

});