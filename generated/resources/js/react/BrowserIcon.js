define(["jquery", "react"], function ($, React) {

    var BrowserIcon = React.createClass({displayName: "BrowserIcon",

        /* Props declaration */
        propTypes: {
            userAgent: React.PropTypes.object,
            withText: React.PropTypes.bool,
            withVersion: React.PropTypes.bool
        },

        getDefaultProps: function () {
            return {
                withVersion: true
            };
        },

        render: function () {
            var iconClass, title, fullName, browserName, browserVersion;
            var userAgent = this.props.userAgent;

            if (userAgent && userAgent.browser) {
                browserName = userAgent.browser.name;
                browserVersion = userAgent.browser.version;
                title = "User's browser is " + browserName + " " + browserVersion;
                if ("Safari" === browserName) {
                    iconClass = "fa-safari";
                } else if ("Mobile Safari" === browserName) {
                    iconClass = "fa-safari";
                } else if ("Facebook" === browserName) {
                    iconClass = "fa-facebook-square";
                } else if ("Opera" === browserName) {
                    iconClass = "fa-opera";
                } else if ("Firefox" === browserName) {
                    iconClass = "fa-firefox";
                } else if ("MSIE" === browserName) {
                    iconClass = "fa-internet-explorer";
                } else if ("Chrome" === browserName) {
                    iconClass = "fa-chrome";
                } else {
                    iconClass = "fa-chromium";
                }
            } else {
                title = "No information about user's browser";
                browserName = "Unknown";
                browserVersion = "";
                iconClass = "fa-chromium default-icon";
            }

            return (
                React.createElement("span", {title: title}, 
                    React.createElement("i", {className: "fa fa-fw " + iconClass}), 
                    this.props.withText ?
                        React.createElement("span", {className: "browser-icon-text"}, browserName)
                    : null, 
                    " ", 
                    this.props.withVersion ?
                        React.createElement("span", {className: "browserVersion"}, 
                            browserVersion
                        )
                        :
                        null
                    
                )
            )
        }

    });

    return BrowserIcon;

});