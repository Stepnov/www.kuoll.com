define(["jquery", "react", "app/utils/MessageUtils", "app/utils/api", "jquery.cookie"], function ($, React,MessageUtils, api) {

    var JiraIssueForm = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                summaryError: "",
                descriptionError: "",
                projectKeyError: "",
                issueTypeError: ""
            };
        },

        /* Props declaration */
        propTypes: {
            onCreate: React.PropTypes.func.isRequired,
            updateSharingState: React.PropTypes.func.isRequired
        },

        authenticateUser: function (authorizationUrl, summary, description, projectKey, issueType, recordLink, success, error) {
            window.open(authorizationUrl, "_blank", "width=500,height=500");
            var self = this;
            MessageUtils.onMessage("jira-authentication-finished", function () {
                self.createIssue(summary, description, projectKey, issueType, recordLink, success, error);
            });
        },

        createIssue: function (summary, description, projectKey, issueType, recordLink, success, error) {
            var self = this;
            api("jira/create_issue", {
                userToken: $.cookie("userToken"),
                summary: summary,
                description: description,
                projectKey: projectKey,
                issueType: issueType,
                recordLink: recordLink
            }, function (resp) {
                if (resp.done) {
                    success();
                } else if (resp.authorizationUrl) {
                    self.authenticateUser(resp.authorizationUrl, summary, description, projectKey, issueType, recordLink, success, error);
                } else {
                    window.open("/settings.html#tab-jira");
                }
            }, error);
        },

        onSubmit: function (e) {
            var summary = $(React.findDOMNode(this.refs.summary)).val();
            var description = $(React.findDOMNode(this.refs.description)).val();
            var projectKey = $(React.findDOMNode(this.refs.projectKey)).val();
            var issueType = $(React.findDOMNode(this.refs.issueType)).val();

            var hasErrors = false;
            if (!summary) {
                this.setState({
                    summaryError: "Summary can not be empty"
                });
            }
            if (!description) {
                this.setState({
                    descriptionError: "Description can not be empty"
                });
            }
            if (!projectKey) {
                this.setState({
                    projectKeyError: "Project key can not be empty"
                });
            }
            if (!issueType) {
                this.setState({
                    issueTypeError: "Issue type can not be empty"
                });
            }
            if (hasErrors) {
                return;
            }

            var self = this;
            this.createIssue(summary, description, projectKey, issueType, document.location.href,
                function () {
                    self.setState(self.getInitialState());
                    self.props.updateSharingState("shared");
                    window.setTimeout(function () {
                        self.props.updateSharingState("share");
                    }, 5000);
                }, function (error) {
                    self.props.updateSharingState("error", error);
                });
            this.props.updateSharingState("sending");

            e.preventDefault();

            this.props.onCreate();
        },

        render: function () {
            return (
                <div>
                    <p>Create a Jira issue with this record</p>
                    <form id="create-issue-form" onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label htmlFor="summary-fld">Summary</label>
                            <input className="form-control" id="summary-fld" type="text"
                                placeholder="Issue summary" ref="summary"/>
                            <span className="help-block" ref="summaryMsg">{this.state.summaryError}</span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="description-fld">Description</label>
                            <textarea className="form-control" id="description-fld" type="text"
                                placeholder="Issue description" ref="description"/>
                            <span className="help-block" ref="descriptionMsg">{this.state.descriptionError}</span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="project-key-fld">Project key</label>
                            <input className="form-control" id="project-key-fld" type="text"
                                placeholder="Project key" ref="projectKey"/>
                            <span className="help-block" ref="projectKeyMsg">{this.state.projectKeyError}</span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="issue-type-fld">Issue type</label>
                            <input className="form-control" id="issue-type-fld" type="text"
                                placeholder="Issue type" ref="issueType"/>
                            <span className="help-block" ref="issueTypeMsg">{this.state.issueTypeError}</span>
                        </div>
                        <div className="form-group">
                            <input type="submit" className="btn btn-success" value="Create"/>
                        </div>
                    </form>
                </div>
            )
        }

    });

    return JiraIssueForm;

});