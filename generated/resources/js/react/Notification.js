define(["jquery", "react", "jquery.ui", "css!/resources/css/jquery-ui", "css!/resources/css/notification"], function ($, React) {

    var notificationContentByType = {
        "NoRecordsBrowserNotification": (
            React.createElement("span", {className: "user-notification"}, 
                React.createElement("p", null, "Hi! You haven't created any records yet. "), 
                React.createElement("p", null, "Do you want to include Kuoll into your website to catch bugs easier? Check out", " ", 
                    React.createElement("a", {href: "/quick-start.html"}, "API quide")
                ), 
                React.createElement("p", null, 
                    "Just want to try Kuoll without installing anything? Check our", " ", 
                    React.createElement("a", {href: "/test.html"}, "test page"), "."
                )
            )
        )
    };
    var notificationTypes = Object.keys(notificationContentByType);

    var Notification = React.createClass({displayName: "Notification",

        /* Props declaration */
        propTypes: {
            type: React.PropTypes.oneOf(notificationTypes).isRequired
        },

        render: function () {
            return (
                React.createElement("div", {className: "user-notification", ref: "notification"}, 
                notificationContentByType[this.props.type]
                )
            )
        },

        componentDidMount: function () {
            var $notification = $(React.findDOMNode(this.refs.notification));
            $notification.dialog({
                autoOpen: true,
                title: "Kuoll notification",
                resizable: false,
                draggable: false,
                dialogClass: "user-notification-dialog",
                width: 350,
                position: {
                    my: "right bottom",
                    at: "right bottom"
                },
                modal: false
            });
        }

    });

    return Notification;

});
