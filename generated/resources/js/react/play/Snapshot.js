define(
    ["jquery", "react", "app/react/play/SequentInfo", "app/utils/SnapshotMessaging",
        "app/utils/SequentUtils"],
    function ($, React, SequentInfo, SnapshotMessaging, SequentUtils) {

    var Snapshot = React.createClass({displayName: "Snapshot",

        /* State declaration */
        getInitialState: function () {
            return {
                snapshotSequent: undefined,
                snapshotSequents: []
            };
        },

        /* Props declaration */
        propTypes: {
            mutationSequents: React.PropTypes.array,
            recordCode: React.PropTypes.string.isRequired,
            activeSequent: React.PropTypes.object,
            activeFrame: React.PropTypes.object.isRequired,
            getSlider: React.PropTypes.func.isRequired,
            onSnapshotUpdated: React.PropTypes.func.isRequired,
            frames: React.PropTypes.array,
            issue: React.PropTypes.object
        },

        // TODO vlad: I don't understand what's going on here anymore, but the method seems to smell. Try to refactor
        updateSnapshot: function (sequent) {
            var prevSequent = this.props.activeSequent;
            var newIndex, oldIndex = this.state.oldIndex;
            var snapshotInfo;

            // repeat until both newIndex and oldIndex are set
            for (var i = this.props.mutationSequents.length - 1; i >= 0 && (!newIndex || !(oldIndex && prevSequent)); --i) {
                snapshotInfo = this.props.mutationSequents[i];

                if (SequentUtils.isAfterIssue(snapshotInfo, this.props.issue)) continue;

                if (typeof newIndex === "undefined") {
                    if (snapshotInfo.frameNum == sequent.frameNum && snapshotInfo.sequentNum <= sequent.sequentNum) {
                        newIndex = i;
                    }
                }

                if (typeof oldIndex === "undefined" && prevSequent) {
                    if (snapshotInfo.frameNum == prevSequent.frameNum && snapshotInfo.sequentNum <= prevSequent.sequentNum) {
                        oldIndex = i;
                    }
                }
            }

            if (typeof newIndex == "undefined") newIndex = Math.max(i, 0);

            if (prevSequent && prevSequent.frameNum !== sequent.frameNum) {
                this.setState({
                    snapshotSequent: sequent,
                    oldIndex: newIndex
                }, function () {
                    this.setSnapshot();
                });
            }

            snapshotInfo = this.props.mutationSequents[newIndex];

            if (snapshotInfo.sequentNum === 0) {
                this.setState({
                    oldIndex: newIndex,
                    snapshotSequent: null
                }, function () {
                    this.setSnapshot();
                });
                return;
            }

            if (!snapshotInfo || snapshotInfo.frameNum != sequent.frameNum) {
                sequent = SequentUtils.getFrame(sequent.frameNum, this.props.frames).sequents[0];
            } else {
                sequent = SequentUtils.getSequentByNum(snapshotInfo.sequentNum, SequentUtils.getFrame(snapshotInfo.frameNum, this.props.frames));
                for (var i = newIndex + 1; i < this.props.mutationSequents.length; ++i) {
                    var nextSnapshotInfo = this.props.mutationSequents[i];
                    var nextSequent = SequentUtils.getSequentByNum(nextSnapshotInfo.sequentNum, SequentUtils.getFrame(nextSnapshotInfo.frameNum, this.props.frames));
                    if (!nextSequent || !nextSequent.isSplitMutation || nextSequent.sequentNum !== sequent.sequentNum + 1 || nextSequent.frameNum !== sequent.frameNum) {
                        break;
                    } else {
                        sequent = nextSequent;
                        snapshotInfo = nextSnapshotInfo;
                        newIndex = i;
                    }
                }
            }

            if (oldIndex !== undefined && newIndex == oldIndex + 1) {
                this.setState({
                    oldIndex: newIndex,
                    snapshotSequent: sequent
                }, function () {
                    this.nextSnapshot();
                });
            } else if (oldIndex !== undefined && newIndex - oldIndex > 1 && (!prevSequent || prevSequent.frameNum == sequent.frameNum)) {
                var sequents = [];
                var frame = SequentUtils.getFrame(sequent.frameNum, this.props.frames);
                for (var i = oldIndex + 1; i <= newIndex; ++i) {
                    sequents.push(SequentUtils.getSequentByNum(this.props.mutationSequents[i].sequentNum, frame));
                }
                this.setState({
                    oldIndex: newIndex,
                    snapshotSequent: sequents[sequents.length - 1],
                    snapshotSequents: sequents
                }, function () {
                    this.nextSnapshot(true);
                });
            } else if (oldIndex !== newIndex) {
                this.setState({
                    oldIndex: newIndex,
                    snapshotSequent: sequent
                }, function () {
                    this.setSnapshot();
                });
            }
        },

        render: function () {
            var snapshotNode;
            if (!this.props.mutationSequents || (!this.state.snapshotSequent && typeof this.state.oldIndex === "undefined")) {
                return (
                    React.createElement("div", {id: "main-caption-container"}, 
                        React.createElement("h2", {id: "main-caption"}, 
                            "Fetching snapshot data..."
                        )
                    )
                );
            }

            snapshotNode = (
                React.createElement("iframe", {src: config.unsecure + "/html?recordCode=" + this.props.recordCode + "&frameNum=" +
                    this.props.activeFrame.frameNum, id: "snapshot", ref: "iframe", onLoad: this.setSnapshot}
                )
            );

            return (
                React.createElement("div", null, 
                    React.createElement("div", {
                        className: "loadingSnapshotContainer" + this.props.mutationSequents.length > 0 ? "" : " loadingSnapshotHidden", 
                        ref: "loadingSnapshotContainer"}, 
                        React.createElement("ul", {className: "loadingSnapshot"}, 
                            React.createElement("li", null), 
                            React.createElement("li", null), 
                            React.createElement("li", null)
                        )
                    ), 
                    React.createElement("div", {className: "snapshotContainer"}, 
                    snapshotNode
                    )
                )
            )
        },

        componentDidMount: function () {
            if (this.refs.iframe) {
                var $iframe = $(React.findDOMNode(this.refs.iframe));
                window.addEventListener("message", function (event) {
                    var data = JSON.parse(event.data);
                    if (data.action == "resizeSnapshot") {
                        $iframe.width(data.width).height(data.height);
                    }
                });
            }
        },

        onSnapshotUpdated: function (snapshotSequent) {
            var snapshotHref = React.findDOMNode(this.refs.iframe).getAttribute("src");
            if (snapshotSequent) {
                snapshotHref += "&sequentNum=" + snapshotSequent.sequentNum;
            }
            this.props.onSnapshotUpdated(snapshotHref);
        },

        setSnapshot: function () {
            var $loadingSnapshotContainer = $(React.findDOMNode(this.refs.loadingSnapshotContainer));
            $loadingSnapshotContainer.addClass("loadingSnapshotHidden");
            setTimeout(function () {
                $loadingSnapshotContainer.hide();
            }, 1000);

            var baseSnapshot, mutations, sequent;
            if (!this.state.snapshotSequent) {
                baseSnapshot = this.props.activeFrame.baseSnapshot;
                mutations = [];
                sequent = this.props.activeSequent;
            } else {
                sequent = this.state.snapshotSequent;
                var frame = SequentUtils.getFrame(sequent.frameNum, this.props.frames);
                baseSnapshot = frame.baseSnapshot;

                var sequentNum = sequent.sequentNum;
                mutations = [];
                frame.sequents.forEach(function (sequent) {
                    if (sequent.sequentNum <= sequentNum && sequent.mutations) {
                        mutations.push(SequentUtils.parseMutations(sequent));
                    }
                });
            }
            SnapshotMessaging.setSnapshot(baseSnapshot, mutations, sequent);

            this.onSnapshotUpdated(this.props.activeSequent);

            if (sequent.windowWidth) {
                $(React.findDOMNode(this.refs.iframe)).width(sequent.windowWidth);
            }
            if (sequent.windowHeight) {
                $(React.findDOMNode(this.refs.iframe)).height(sequent.windowHeight);
            }
            SnapshotMessaging.createMouseTrail(sequent.mouseTrail);
        },

        nextSnapshot: function (batch) {
            var sequent = this.state.snapshotSequent;
            var mutations;
            if (batch) {
                mutations = this.state.snapshotSequents.map(function (s) { return SequentUtils.parseMutations(s); });
            } else {
                mutations = SequentUtils.parseMutations(sequent);
            }

            SnapshotMessaging.nextSnapshot(mutations, sequent);

            this.onSnapshotUpdated(this.props.activeSequent);

            if (sequent.windowWidth) {
                $(React.findDOMNode(this.refs.iframe)).width(sequent.windowWidth);
            }
        },

        componentWillReceiveProps: function (nextProps) {
            var sequent = nextProps.activeSequent;
            var prevSequent = this.props.activeSequent;
            if (sequent != prevSequent) {
                if (this.props.frame == nextProps.frame) {
                    this.updateSnapshot(sequent, prevSequent);
                } else {
                    // iframe's src will be updated (in render method) and setSnapshot will be performed after load is finished
                }
            }
        }

    });

    return Snapshot;

});
