define(["jquery", "react", "app/utils/TipUtils"], function ($, React,TipUtils) {

    var MouseMark = React.createClass({displayName: "MouseMark",

        /* Props declaration */
        propTypes: {
            mutationSequents: React.PropTypes.array,
            mouseSequents: React.PropTypes.array,
            snapshotsAmount: React.PropTypes.number,
            onClick: React.PropTypes.func.isRequired
        },

        onClick: function (e) {
            var index = e.target.getAttribute("data-index");
            var sequent = this.props.mouseSequents[index];
            this.props.onClick(sequent);
        },

        findTheClosestSnapshotIndex: function (mouseSequent) {
            for (var i = 0; i < this.props.mutationSequents.length; ++i) {
                var sequent = this.props.mutationSequents[i];
                if (mouseSequent.frameNum == sequent.frameNum && mouseSequent.sequentNum <= sequent.sequentNum) {
                    return i - 1;
                } else if (sequent.frameNum > mouseSequent.frameNum) {
                    return -1;
                }
            }
            return -1;
        },

        render: function () {
            if (this.props.mouseSequents && this.props.mutationSequents) {
                var self = this;
                var total = this.props.snapshotsAmount;
                var marks = {};
                for (var i = 0; i < this.props.mouseSequents.length; ++i) {
                    var mouseSequent = this.props.mouseSequents[i];
                    var index = this.findTheClosestSnapshotIndex(mouseSequent);
                    var position = (index / total) * 100;
                    marks[index] = (
                        React.createElement("div", {className: "mouse-sequent-mark-pod " + "mouse-sequent-mark-" + mouseSequent.type, key: index, onClick: self.onClick, 
                            "data-index": index, style: {left: position + "%"}}, 
                            React.createElement("div", {className: "mouse-sequent-mark"})
                        )
                    )
                }
                return (
                    React.createElement("div", {id: "mouse-sequent-mark-container"}, 
                    Object.keys(marks).map(function (name) {
                        return (name == -1 ? null : marks[name]);
                    })
                    )
                )
            } else {
                return null;
            }
        },

        componentDidMount: function () {
            var $click = $(".mouse-sequent-mark-pod.mouse-sequent-mark-click");
            var $dragStart = $(".mouse-sequent-mark-pod.mouse-sequent-mark-dragstart");
            TipUtils.makeSimpleTip($click, "User clicked mouse");
            TipUtils.makeSimpleTip($dragStart, "User started dragging an item");
        },

        shouldComponentUpdate: function (nextProps) {
            return !(
                this.props.mutationSequents.length == nextProps.mutationSequents.length
                && this.props.mouseSequents.length == nextProps.mouseSequents.length
                && this.props.snapshotsAmount == nextProps.snapshotsAmount
            );
        },

        componentDidUpdate: function () {
            function hasNoTip(index, node) {
                return !$(node).qtip().id;
            }
            var $click = $(".mouse-sequent-mark-pod.mouse-sequent-mark-click").filter(hasNoTip);
            var $dragStart = $(".mouse-sequent-mark-pod.mouse-sequent-mark-dragstart").filter(hasNoTip);
            TipUtils.makeSimpleTip($click, "User clicked mouse");
            TipUtils.makeSimpleTip($dragStart, "User started dragging an item");
        }

    });

    return MouseMark;

});