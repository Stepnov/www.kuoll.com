define(
    ["jquery", "react", "app/react/play/FrameLine", "app/react/play/Sequent", "app/react/play/SequentTabs"],
    function ($, React,FrameLine, Sequent, SequentTabs) {

    var Timeline= React.createClass({

        /* Props declaration */
        propTypes: {
            onChangeActiveSequent: React.PropTypes.func.isRequired,
            onChangeActiveFrame: React.PropTypes.func.isRequired,

            frames: React.PropTypes.array.isRequired,
            activeFrame: React.PropTypes.object.isRequired,

            activeSeriesNum: React.PropTypes.number,
            activeSequent: React.PropTypes.object,

            issue: React.PropTypes.object
        },

        onGoToPrevSequent: function (prevSequentNum) {
            this.props.onChangeActiveSequent(prevSequentNum);
        },

        getSequentLine: function () {
            return this.refs.sequentLine;
        },

        render: function () {
            return (
                <div ref="timelineNode" id="timelineWrapper">

                    <div id="sequentPanel">

                        <div id="sequentCategories">
                            <div className="sequentGroupTitle" title="User actions">
                                <i className="fa fa-user"></i>
                            </div>
                            <div className="sequentGroupTitle" title="Web page changes">
                                <i className="fa fa-eye"></i>
                            </div>
                            <div className="sequentGroupTitle" title="Network requests and responses">
                                <i className="fa fa-wifi"></i>
                            </div>
                            <div className="sequentGroupTitle" title="Code execution flow">
                                <span className="codeSign">{"{}"}</span>
                            </div>
                        </div>

                        <div id="timeline">

                            <FrameLine frames={this.props.frames}
                                activeFrameNum={this.props.activeFrame.frameNum}
                                onFrameClick={this.props.onChangeActiveFrame}/>

                            <div className="clear"></div>

                            <div id="sequentLineScroll" className="sequentLineScroll alwaysShowScrollbar"
                                ref="scrollbar">
                                <div className="sequentLine" id="sequentLine" key={this.props.activeFrame.frameNum}
                                    ref="sequentLine">
                                {
                                    (this.props.activeFrame.sequents || []).map(function (sequent) {
                                        return (
                                            <Sequent sequent={sequent} key={sequent.sequentNum}
                                                onClickCallback={this.props.onChangeActiveSequent}
                                                isHighlighted={this.props.activeSeriesNum == sequent.seriesNum}
                                                isActive={this.props.activeSequent.sequentNum == sequent.sequentNum}
                                                frame={this.props.activeFrame}/>
                                        )
                                    }.bind(this))
                                    }
                                </div>
                            </div>

                            <div className="clear"></div>
                        </div>

                    </div>

                    <SequentTabs sequent={this.props.activeSequent} goToPrevSequent={this.onGoToPrevSequent}
                        activeFrame={this.props.activeFrame} issue={this.props.issue}/>

                </div>
            )
        },

        componentDidMount: function () {
            var $scroll = $(React.findDOMNode(this.refs.scrollbar));
            $scroll.on("mousewheel", function (e) {
                e = e.originalEvent;
                e.preventDefault();
                $scroll.scrollLeft($scroll.scrollLeft() + e.deltaY);
            })
        }

    });

    return Timeline;

});
