define(["jquery", "react", "app/utils/TipUtils", "app/react/play/Link", "app/react/play/RecordStateBadges"], function ($, React,TipUtils, Link, RecordStateBadges) {

    var RecordInfo = React.createClass({

        /* Props declaration */
        propTypes: {
            record: React.PropTypes.object.isRequired
        },

        render: function () {
            var record = this.props.record;
            var recordInfo = record.recordInfo;
            var isRecordGoingOn = (this.props.record.status === "started");

            return (
                <span>

                    <span id="info-icon-wrapper" onClick={this.onInfoIconClick}>
                        {this.props.children || <i className="fa fa-info-circle"></i>}
                    </span>

                    <div id="record-info" ref="infoDialog">

                    {recordInfo && recordInfo.userAgent ? [
                        <h3>User agent</h3>,
                        <p>
                            <span className="user-agent-info">Browser: {recordInfo.userAgent.browser.name} {recordInfo.userAgent.browser.version}</span>
                            <br/>
                            <span className="user-agent-info">OS: {recordInfo.userAgent.os.name} {recordInfo.userAgent.os.version}</span>
                        </p>
                    ] : null }

                        <h3>Record state</h3>
                        <RecordStateBadges record={record} showTimeInfo={true} />

                        <p>
                        {recordInfo && !isRecordGoingOn ?
                            <span id="first-url-label" className="record-label">
                                This record was started at the page
                                <br/>
                                <Link href={recordInfo.firstUrl} target="_blank" />
                            </span>
                        : null}
                        </p>

                    {record.notes ? [
                        <h3>Notes to record</h3>,
                        <span ref="recordNotes">{record.notes}</span>
                    ] : null}

                    </div>

                </span>
            )
        },

        onInfoIconClick: function () {
            var $infoDialog = $(React.findDOMNode(this.refs.infoDialog));
            if ($infoDialog.dialog("isOpen")) {
                $infoDialog.dialog("close");
                $(".start-params-icon").qtip("api").hide();
            } else {
                $infoDialog.dialog("open");
            }
        },

        componentDidMount: function () {
            const $infoDialog = $(React.findDOMNode(this.refs.infoDialog));
            $infoDialog.dialog({
                autoOpen: false,
                title: "Record info",
                resizable: false,
                draggable: false,
                appendTo: "#record-info-wrapper",
                width: 400,
                position: {
                    my: "center center+350",
                    at: "center top"
                },
                modal: true,
                beforeClose: function () {
                    $(".start-params-icon").qtip("api").hide();
                }
            });
            TipUtils.makeSimpleTip($('[title!=""]', $infoDialog));
        }

    });

    return RecordInfo;

});