define(["jquery", "react", "app/react/play/LongText", "app/utils/SnapshotMessaging"], function ($, React,LongText, SnapshotMessaging) {

    var SnapshotElementPath = React.createClass({

        /* Props declaration */
        propTypes: {
            path: React.PropTypes.string.isRequired
        },

        getInitialState: function() {
            return {
                highlighted: false,
                shortened: true
            }
        },

        onHighlightClick: function (e) {
            if (this.state.highlighted) {
                SnapshotMessaging.unhighlight(this.props.path);
            } else {
                SnapshotMessaging.highlight(this.props.path);
            }
            e.stopPropagation();
            this.setState({
                highlighted: !this.state.highlighted
            });
        },

        onTextClick: function () {
            // Prevents re-rendering if user selects some text
            if (window.getSelection().isCollapsed) {
                this.setState({
                    shortened: !this.state.shortened
                });
            }
        },

        render: function () {
            return (
                <span
                    className={"snapshot-element-path-wrapper" + (this.state.highlighted || this.state.hovered ? " highlighted" : "")}
                    onClick={this.onTextClick}>
                    <i className="fa fa-lightbulb-o" onClick={this.onHighlightClick} ref="lightbulb"
                       title="Highlight this elements at snapshot"></i>
                    <code className="snapshot-element-path">
                        <LongText text={this.props.path} title={this.props.path} shortened={this.state.shortened}/>
                    </code>
                </span>
            )
        },

        componentDidMount: function () {
            var self = this;
            var $node = $(React.findDOMNode(this.refs.lightbulb));
            $node.on("mouseenter", function () {
                if (!self.state.highlighted) {
                    SnapshotMessaging.highlight(self.props.path);
                }
                self.setState({
                    hovered: true
                });
            }).on("mouseleave", function () {
                if (!self.state.highlighted) {
                    SnapshotMessaging.unhighlight(self.props.path);
                }
                self.setState({
                    hovered: false
                });
            });
            this.setState({
                highlighted: SnapshotMessaging.isHighlighted(this.props.path)
            });
        },

        componentWillUnmount: function () {
            if (this.state.highlighted) {
                SnapshotMessaging.unhighlight(this.props.path);
            }
        }

    });

    return SnapshotElementPath;

});