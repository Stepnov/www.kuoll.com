define(
    ["jquery", "react", "app/react/play/FrameLine", "app/react/play/Sequent", "app/react/play/SequentTabs"],
    function ($, React,FrameLine, Sequent, SequentTabs) {

    var Timeline= React.createClass({displayName: "Timeline",

        /* Props declaration */
        propTypes: {
            onChangeActiveSequent: React.PropTypes.func.isRequired,
            onChangeActiveFrame: React.PropTypes.func.isRequired,

            frames: React.PropTypes.array.isRequired,
            activeFrame: React.PropTypes.object.isRequired,

            activeSeriesNum: React.PropTypes.number,
            activeSequent: React.PropTypes.object,

            issue: React.PropTypes.object
        },

        onGoToPrevSequent: function (prevSequentNum) {
            this.props.onChangeActiveSequent(prevSequentNum);
        },

        getSequentLine: function () {
            return this.refs.sequentLine;
        },

        render: function () {
            return (
                React.createElement("div", {ref: "timelineNode", id: "timelineWrapper"}, 

                    React.createElement("div", {id: "sequentPanel"}, 

                        React.createElement("div", {id: "sequentCategories"}, 
                            React.createElement("div", {className: "sequentGroupTitle", title: "User actions"}, 
                                React.createElement("i", {className: "fa fa-user"})
                            ), 
                            React.createElement("div", {className: "sequentGroupTitle", title: "Web page changes"}, 
                                React.createElement("i", {className: "fa fa-eye"})
                            ), 
                            React.createElement("div", {className: "sequentGroupTitle", title: "Network requests and responses"}, 
                                React.createElement("i", {className: "fa fa-wifi"})
                            ), 
                            React.createElement("div", {className: "sequentGroupTitle", title: "Code execution flow"}, 
                                React.createElement("span", {className: "codeSign"}, "{}")
                            )
                        ), 

                        React.createElement("div", {id: "timeline"}, 

                            React.createElement(FrameLine, {frames: this.props.frames, 
                                activeFrameNum: this.props.activeFrame.frameNum, 
                                onFrameClick: this.props.onChangeActiveFrame}), 

                            React.createElement("div", {className: "clear"}), 

                            React.createElement("div", {id: "sequentLineScroll", className: "sequentLineScroll alwaysShowScrollbar", 
                                ref: "scrollbar"}, 
                                React.createElement("div", {className: "sequentLine", id: "sequentLine", key: this.props.activeFrame.frameNum, 
                                    ref: "sequentLine"}, 
                                
                                    (this.props.activeFrame.sequents || []).map(function (sequent) {
                                        return (
                                            React.createElement(Sequent, {sequent: sequent, key: sequent.sequentNum, 
                                                onClickCallback: this.props.onChangeActiveSequent, 
                                                isHighlighted: this.props.activeSeriesNum == sequent.seriesNum, 
                                                isActive: this.props.activeSequent.sequentNum == sequent.sequentNum, 
                                                frame: this.props.activeFrame})
                                        )
                                    }.bind(this))
                                    
                                )
                            ), 

                            React.createElement("div", {className: "clear"})
                        )

                    ), 

                    React.createElement(SequentTabs, {sequent: this.props.activeSequent, goToPrevSequent: this.onGoToPrevSequent, 
                        activeFrame: this.props.activeFrame, issue: this.props.issue})

                )
            )
        },

        componentDidMount: function () {
            var $scroll = $(React.findDOMNode(this.refs.scrollbar));
            $scroll.on("mousewheel", function (e) {
                e = e.originalEvent;
                e.preventDefault();
                $scroll.scrollLeft($scroll.scrollLeft() + e.deltaY);
            })
        }

    });

    return Timeline;

});
