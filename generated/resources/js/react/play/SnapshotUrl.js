define(["react", "app/utils/SequentUtils", "app/react/play/Link", "app/react/play/RecordInfo", "app/react/BrowserIcon"], function (React, SequentUtils, Link, RecordInfo, BrowserIcon) {

    var SnapshotUrl = React.createClass({displayName: "SnapshotUrl",

        /* State declaration */
        getInitialState: function () {
            return {
                firstPushStateSequentNums: {}
            };
        },

        /* Props declaration */
        propTypes: {
            snapshotUrl: React.PropTypes.string,
            sequent: React.PropTypes.object,
            frame: React.PropTypes.object,
            record: React.PropTypes.object.isRequired
        },

        /**
         * Checks previous sequents to find last History.pushState sequent and use its URL as caption of link
         * to current snapshot. Uses URL of current frame if there is no pushState sequents.
         */
        getLinkCaption: function () {
            var linkCaption;
            var startSequentNum = this.props.sequent.sequentNum;
            var firstPushStateSequentNum = this.state.firstPushStateSequentNums[this.props.frame.frameNum];
            SequentUtils.iterateReverseOrder(startSequentNum, this.props.frame, function (sequent) {
                if (sequent.sequentSubtype === "pushState" && sequent.sequentClass === "History") {
                    SequentUtils.parseRawData(sequent);
                    if (sequent.rawData.args[2]) {
                        var url = new URL(sequent.rawData.args[2], this.props.frame.url || this.props.frame.base);
                        linkCaption = url.href;
                        if (!firstPushStateSequentNum) {
                            this.state.firstPushStateSequentNums[this.props.frame.frameNum] = {
                                num: sequent.sequentNum,
                                doesSequentExist: true
                            };
                        }
                        return false;
                    }
                }
                if (firstPushStateSequentNum && sequent.sequentNum <= firstPushStateSequentNum) {
                    return false;
                }
                return true;

            }.bind(this));
            if (!linkCaption) {
                linkCaption = this.props.frame.url;
                if (!firstPushStateSequentNum || firstPushStateSequentNum < startSequentNum) {
                    this.state.firstPushStateSequentNums[this.props.frame.frameNum] = {
                        num: startSequentNum,
                        doesSequentExist: false
                    };
                }
            }
            return linkCaption;
        },

        render: function () {
            if (!this.props.snapshotUrl) return null;

            var linkCaption = this.getLinkCaption();
            return (
                React.createElement("span", {id: "snapshotUrlWrapper", title: linkCaption}, 
                    React.createElement(RecordInfo, {record: this.props.record}, 
                        React.createElement(BrowserIcon, {userAgent: this.props.record.recordInfo.userAgent})
                    ), 
                    React.createElement(Link, {href: linkCaption, target: "_blank", id: "snapshotUrl"}, 
                    linkCaption
                    )
                )
            )
        }

    });

    return SnapshotUrl;

});