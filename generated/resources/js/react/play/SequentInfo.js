define(["jquery", "react", "app/utils/DateUtils", "app/utils/SequentUtils", "app/utils/SnapshotMessaging",
        "app/react/play/LongText", "app/react/play/Stacktrace", "app/react/play/SnapshotElementPath", "highlightjs"],
    function ($, React, DateUtils, SequentUtils, SnapshotMessaging, LongText, Stacktrace, SnapshotElementPath, HighlightJS) {

    var SequentInfo = React.createClass({displayName: "SequentInfo",

        statics: {
            findRendering: findRendering,
            getSequentTip: getSequentTip
        },

        /* State declaration */
        getInitialState: function () {
            return {
                mutationInfo: null
            };
        },

        /* Props declaration */
        propTypes: {
            sequent: React.PropTypes.object,
            activeFrame: React.PropTypes.object,
            goToPrevSequent: React.PropTypes.func.isRequired
        },

        goToPrevSequent: function (e, sequentNum) {
            this.props.goToPrevSequent(sequentNum);
            e.preventDefault();
        },

        goToSequent: function (sequentNum) {
            return function (e) {
                this.props.goToPrevSequent(sequentNum);
                e.preventDefault();
            }.bind(this);
        },

        render: function () {
            if (!this.props.sequent) return null;

            var rendering = findRendering(this.props.sequent);
            return rendering.prepare.bind(this)(this.props.sequent);
        },

        componentDidMount: function () {
            window.addEventListener("message", function (event) {
                if (["http://localhost:8080", "http://api.kuoll.com", "https://api.kuoll.com"].indexOf(event.origin) != -1) {
                    var data = JSON.parse(event.data);
                    if (data.action == "updateSequentInfo") {
                        this.setState({
                            mutationInfo: data
                        });
                    }
                }
            }.bind(this));
        }

    });

    function parseCookie(cookie) {
        var cookieObj = {};
        cookie.split(';').forEach(function(val, index) {
            var tokens = val.split("=");

            if (index == 0) {
                cookieObj["__key"] = tokens[0];
            }
            cookieObj[tokens[0]] = tokens[1];
        });
        return cookieObj;
    }

    function findRendering(sequent) {
        try {
            var l1 = sequentRendering[sequent.sequentType];
            if (!l1) {
                return DEFAULT_RENDERING[sequent.sequentType];
            }
            if (l1.isRendering) {
                return l1;
            }

            var l2;
            var classes = Object.getOwnPropertyNames(l1);
            for (var i = 0; i < classes.length; ++i) {
                if (classes[i].split("|").indexOf(sequent.sequentClass) != -1) {
                    l2 = l1[classes[i]];
                }
            }
            if (!l2) {
                l2 = l1.other;
            }
            if (!l2) {
                return DEFAULT_RENDERING[sequent.sequentType];
            }

            if (l2.isRendering) {
                return l2;
            }

            var l3 = l2[sequent.sequentSubtype] || (l2.other && l2.other[sequent.sequentSubtype]) || l2.default;

            if (!l3) {
                return DEFAULT_RENDERING[sequent.sequentType];
            }
            return l3;
        } catch (e) {
            console.error(e.stack);
            return DEFAULT_RENDERING[sequent.sequentType];
        }
    }

    //noinspection JSUnusedLocalSymbols
    function getSequentTip(frame, sequent) {
        SequentUtils.parseRawData(sequent);

        var titleTemplate = SequentInfo.findRendering(sequent).config().titleTemplate;
        var title = (function (sequent, template) {
            return template.replace(/{{([\w.\[\]\(\)\s,=\?:"\|]+)}}/g,
                function (match, p1, offset, string) {
                    return eval(p1);
                })
        })(sequent, titleTemplate);

        return title;
    }

    function createRendering(info) {
        return {
            prepare: function (sequent) {
                SequentUtils.parseRawData(sequent);

                try {
                    var sequentInfo = info.prepare.bind(this)(sequent);
                    return sequentInfo;
                } catch (e) {
                    console.debug(e);
                    return (
                        React.createElement("div", null, 
                            "Error while preparing sequent info"
                        )
                    );
                }
            },

            config: function () {
                return info;
            },

            isRendering: true,

            getCssClasses: function () {
                return info.cssClasses;
            },

            getIconClass: function () {
                return info.iconClass;
            },

            getColor: function () {
                return info.color;
            }
        }
    }

    var DEFAULT_RENDERING = {
        "event": createRendering({
            cssClasses: ["eventSequent", "executionSequentGroup"],
            iconClass: 'fa-asterisk',
            prepare: prepareDefaultEventSequent,
            titleTemplate: 'Browser event {{sequent.rawData.type}} happened'
        }),
        "interception": createRendering({
            cssClasses: ["interceptionSequent", "executionSequentGroup"],
            iconClass: 'fa-terminal',
            prepare: prepareDefaultInterceptionSequent,
            titleTemplate: 'Method {{sequent.sequentClass}}.{{sequent.rawData.methodName}} was invoked'
        })
    };

    function shortString(str) {
        if (str === null || str === undefined) {
            console.warn("1 argument was expected");
            return "";
        }

        if (str.length > 40) {
            str = str.slice(0, 30) + "(...)" + str.slice(str.length - 10);
        }
        return str;
    }

    var sequentRendering = {
        "event": {
            "MouseEvent|PointerEvent|MSEventObj": createRendering({
                cssClasses: ["mouseClickSequent", "userActionSequentGroup"],
                iconClass: 'fa-location-arrow',
                prepare: prepareMouseSequent,
                titleTemplate: 'Mouse click: {{shortString(sequent.rawData.target ? sequent.rawData.target.HTMLElementPath : sequent.rawData.srcElement.HTMLElementPath)}}'
            }),
            "KeyboardEvent": createRendering({
                cssClasses: ["keyboardSequent", "userActionSequentGroup"],
                iconClass: 'fa-keyboard-o',
                prepare: prepareKeyboardSequent,
                titleTemplate: 'Key: {{makeKeyString(sequent.rawData.altKey, sequent.rawData.ctrlKey, sequent.rawData.shiftKey, sequent.rawData.metaKey, sequent.rawData.charCode || sequent.rawData.keyCode || sequent.rawData.which)}}'
            }),
            XMLHttpRequestProgressEvent: createRendering({
                cssClasses: ["xhrResponseSequent", "networkingSequentGroup"],
                iconClass: 'fa-long-arrow-left',
                prepare: prepareXhrResponseSequent,
                titleTemplate: 'XHR response: {{sequent.rawData.target.status}} {{sequent.rawData.target.statusText}} {{shortString(sequent.rawData.target.responseURL)}}'
            }),
            MessageEvent: createRendering({
                cssClasses: ["messageSequent", "networkingSequentGroup"],
                iconClass: 'fa-envelope-o',
                prepare: prepareMessageSequent,
                titleTemplate: 'Message from {{sequent.rawData.origin}}'
            }),
            ProgressEvent: {
                "load": createRendering({
                    cssClasses: ["executionSequentGroup", "xhrLoadSequent"],
                    iconClass: 'fa-check-square',
                    prepare: prepareXhrLoadSequent,
                    titleTemplate: 'XHR complete'
                }),
            },
            other: {
                "scroll": createRendering({
                    cssClasses: ["scrollSequent", "visualChangeSequentGroup"],
                    iconClass: 'fa-arrows-v',
                    prepare: prepareScrollSequent,
                    titleTemplate: 'Scroll to x: {{sequent.scrollX}}; y: {{sequent.scrollY}}'
                }),
                "[initialPageLoad]": createRendering({
                    cssClasses: ["initialLoadSequent", "executionSequentGroup"],
                    iconClass: 'fa-circle',
                    prepare: prepareInitialLoadSequent,
                    titleTemplate: 'Record start'
                }),
                "[createIssue]": createRendering({
                    cssClasses: ["createIssueSequent", "executionSequentGroup"],
                    iconClass: 'fa-exclamation-circle',
                    prepare: prepareCreateIssueSequent,
                    titleTemplate: 'New issue'
                }),
                "error": createRendering({
                    cssClasses: ["errorSequent", "executionSequentGroup"],
                    iconClass: 'fa-times-circle',
                    prepare: prepareErrorSequent,
                    titleTemplate: 'JS Error {{shortString(sequent.rawData.message)}}:{{sequent.rawData.lineno}}'
                }),
                "visibilitychange": createRendering({
                    cssClasses: ["visibilitySequent", "visualChangeSequentGroup"],
                    iconClass: 'fa-folder-o',
                    prepare: prepareVisibilitySequent,
                    titleTemplate: 'Visibility change'
                }),
                "beforeunload": createRendering({
                    cssClasses: ["beforeUnloadSequent", "executionSequentGroup"],
                    iconClass: 'fa-sign-out',
                    prepare: prepareBeforeUnloadSequent,
                    titleTemplate: 'Page beforeunload'
                }),
                "resize": createRendering({
                    cssClasses: ["resizeSequent", "visualChangeSequentGroup"],
                    iconClass: 'fa-arrows-alt',
                    prepare: prepareResizeSequent,
                    titleTemplate: 'window.resize'
                }),
                "focus": createRendering({
                    cssClasses: ["focusSequent", "executionSequentGroup"],
                    iconClass: 'fa-plus-square-o',
                    prepare: prepareFocusSequent,
                    titleTemplate: 'Focus on {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "blur": createRendering({
                    cssClasses: ["focusSequent", "executionSequentGroup"],
                    iconClass: 'fa-minus-square-o',
                    prepare: prepareBlurSequent,
                    titleTemplate: 'Focus blur {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "DOMFocusIn": createRendering({
                    cssClasses: ["focusSequent", "executionSequentGroup"],
                    iconClass: 'fa-plus-square-o',
                    prepare: prepareFocusSequent,
                    titleTemplate: 'DOM focus in {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "DOMFocusOut": createRendering({
                    cssClasses: ["focusSequent", "executionSequentGroup"],
                    iconClass: 'fa-minus-square-o',
                    prepare: prepareBlurSequent,
                    titleTemplate: 'DOM focus lost: {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "readystatechange": createRendering({
                    cssClasses: ["readyStateChangeSequent", "networkingSequentGroup"],
                    iconClass: 'fa-check-circle-o',
                    prepare: prepareReadyStateChangeSequent,
                    titleTemplate: 'XHR state {{readyStateByNumber[sequent.rawData.target.readyState]}}'
                }),
                "input": createRendering({
                    cssClasses: ["inputSequent", "userActionSequentGroup"],
                    iconClass: 'fa-pencil-square-o',
                    prepare: prepareInputSequent,
                    titleTemplate: 'Input event {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "load": createRendering({
                    cssClasses: ["executionSequentGroup", "loadSequent"],
                    iconClass: 'fa-check-square',
                    prepare: prepareLoadSequent,
                    titleTemplate: 'onload'
                }),
                "DOMContentLoaded": createRendering({
                    cssClasses: ["executionSequentGroup", "domContentLoadedSequent"],
                    iconClass: 'fa-check-square',
                    prepare: prepareDOMContentLoadedSequent,
                    titleTemplate: 'document.DOMContentLoaded'
                }),
                // Used 2 icons for 6 different sequent types
                "dragstart": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragStartSequent"],
                    iconClass: 'fa-hand-rock-o',
                    prepare: prepareDragStartSequent,
                    titleTemplate: 'Drag start'
                }),
                "drop": createRendering({
                    cssClasses: ["userActionSequentGroup", "dropSequent"],
                    iconClass: 'fa-hand-paper-o',
                    prepare: prepareDropSequent,
                    titleTemplate: 'Drag drop'
                }),
                "dragleave": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragLeaveSequent"],
                    iconClass: 'fa-hand-rock-o',
                    prepare: prepareDragLeaveSequent,
                    titleTemplate: 'Drag leave'
                }),
                "dragend": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragEndSequent"],
                    iconClass: 'fa-hand-paper-o',
                    prepare: prepareDragEndSequent,
                    titleTemplate: 'Drag end'
                }),
                "dragover": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragOverSequent"],
                    iconClass: 'fa-hand-rock-o',
                    prepare: prepareDragOverSequent,
                    titleTemplate: 'Drag over'
                }),
                "dragenter": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragEnterSequent"],
                    iconClass: 'fa-hand-rock-o',
                    prepare: prepareDragEnterSequent,
                    titleTemplate: 'Drag enter'
                })
            }
        },
        "mutation": createRendering({
            cssClasses: ["domMutationSequent", "visualChangeSequentGroup"],
            iconClass: 'fa-code',
            prepare: prepareDomMutationSequent,
            titleTemplate: 'DOM mutation'
        }),
        interception: {
            XMLHttpRequest: {
                "open": createRendering({
                    cssClasses: ["xhrRequestSequent", "networkingSequentGroup"],
                    iconClass: 'fa-circle-o',
                    prepare: prepareOpenXhrSequent,
                    titleTemplate: 'XMLHttpRequest open: {{sequent.rawData.args[0]}} {{shortString(sequent.rawData.args[1])}}'
                }),
                "send": createRendering({
                    cssClasses: ["xhrRequestSequent", "networkingSequentGroup"],
                    iconClass: 'fa-long-arrow-right',
                    prepare: prepareSendXhrSequent,
                    titleTemplate: 'XMLHttpRequest send'
                }),
                abort: createRendering({
                    cssClasses: ["xhrAbortSequent", "networkingSequentGroup"],
                    iconClass: 'fa-ban',
                    prepare: prepareAbortXhrSequent,
                    titleTemplate: 'XMLHttpRequest abort'
                })
            },
            Console: {
                log: createRendering({
                    cssClasses: ["consoleSequent", "executionSequentGroup"],
                    iconClass: 'fa-terminal',
                    prepare: prepareConsoleSequent,
                    titleTemplate: 'console {{shortString(sequent.rawData.args[0])}}'
                })
            },
            History: {
                pushState: createRendering({
                    cssClasses: ["historyPushSequent", "executionSequentGroup"],
                    iconClass: 'fa-caret-square-o-down',
                    prepare: prepareHistoryPushSequent,
                    titleTemplate: 'history.pushState {{shortString(sequent.rawData.args[2])}}'
                })
            },
            Promise: {
                constructor: createRendering({
                    cssClasses: ["execution"],
                    color: "#dd6",
                    iconClass: 'fa-bookmark',
                    prepare: function (sequent) {
                        return (
                            React.createElement("span", null, 
                                React.createElement("div", null, 
                                    React.createElement("strong", null, "New Promise")
                                ), 
                                React.createElement("div", null, 
                                    React.createElement(Stacktrace, {stacktrace: sequent.stack})
                                )
                            )
                        )
                    },
                    titleTemplate: 'Promise created'
                }),
                onFulfilled: createRendering({
                    cssClasses: ["execution"],
                    color: "#5c5",
                    iconClass: 'fa-bookmark',
                    prepare: function (sequent) {
                        return (
                            React.createElement("span", null, 
                                React.createElement("div", null, 
                                    React.createElement("a", {href: "", onClick: this.goToSequent(sequent.previousSequentNum)}, "Previously created promise"
                                    ), " successfully executed. Stacktrace:"
                                ), 
                                React.createElement("div", null, 
                                    React.createElement(Stacktrace, {stacktrace: sequent.stack})
                                )
                            )
                        )
                    },
                    titleTemplate: 'Promise successfully executed'
                }),
                onRejected: createRendering({
                    cssClasses: ["execution"],
                    color: "#f77",
                    iconClass: 'fa-bookmark',
                    prepare: function (sequent) {
                        return (
                            React.createElement("span", null, 
                                React.createElement("div", null, 
                                    React.createElement("a", {href: "", onClick: this.goToSequent(sequent.previousSequentNum)}, "Previously created promise"
                                    ), " successfully executed. Stacktrace:"
                                ), 
                                React.createElement("div", null, 
                                    React.createElement(Stacktrace, {stacktrace: sequent.stack})
                                )
                            )
                        )
                    },
                    titleTemplate: 'Promise rejected'
                })
            },
            Window: {
                "setTimeout": createRendering({
                    cssClasses: ["timerSequent", "executionSequentGroup"],
                    iconClass: 'fa-clock-o',
                    prepare: prepareSetTimeoutSequent,
                    titleTemplate: 'window.setTimeout'
                }),
                "clearTimeout": createRendering({
                    cssClasses: ["timerSequent", "executionSequentGroup"],
                    iconClass: 'fa-circle-o',
                    prepare: prepareClearTimeoutSequent,
                    titleTemplate: 'window.clearTimeout'
                }),
                "setInterval": createRendering({
                    cssClasses: ["intervalSequent", "executionSequentGroup"],
                    iconClass: 'fa-history',
                    prepare: prepareSetIntervalSequent,
                    titleTemplate: 'window.setInterval'
                }),
                "clearInterval": createRendering({
                    cssClasses: ["intervalSequent", "executionSequentGroup"],
                    iconClass: 'fa-undo',
                    prepare: prepareClearIntervalSequent,
                    titleTemplate: 'window.clearInterval'
                }),
                "postMessage": createRendering({
                    cssClasses: ["postMessageSequent", "executionSequentGroup"],
                    iconClass: 'fa-envelope',
                    prepare: preparePostMessageSequent,
                    titleTemplate: 'Message was sent to origin {{shortString(sequent.rawData.args[1])}}'
                })
            },

            "[cookie_set]": createRendering({
                cssClasses: ["executionSequentGroup", "setCookieSequent"],
                iconClass: "fa-sticky-note-o",
                prepare: prepareSetCookieSequent,
                titleTemplate: "Cookies {{shortString(sequent.rawData.value)}}"
            }),

            "[setTimeout-callback]": {
                default: createRendering({
                    cssClasses: ["timerSequent", "executionSequentGroup"],
                    iconClass: 'fa-clock-o',
                    prepare: prepareTimeoutCallbackSequent,
                    titleTemplate: 'window.setTimeout callback'
                })
            },
            "[setInterval-callback]": {
                default: createRendering({
                    cssClasses: ["intervalSequent", "executionSequentGroup"],
                    iconClass: 'fa-history',
                    prepare: prepareIntervalCallbackSequent,
                    titleTemplate: 'window.setInterval callback'
                })
            }
        }
    };

    var mouseKeyTypeByNumber = [
        "Left",
        "Middle",
        "Right"
    ];

    var readyStateByNumber = ["UNSENT", "OPENED", "HEADERS_RECEIVED", "LOADING", "DONE"];

    var keyNameByCode = {
        8: "Backspace",
        32: "Spacebar",
        13: "Enter"
    };

    function makeKeyString(alt, ctrl, shift, meta, keyCode) {
        var key = keyNameByCode[keyCode];
        if (!key) {
            key = String.fromCharCode(keyCode);
        }

        return (alt ? "Alt + " : "")
            + (ctrl ? "Ctrl + " : "")
            + (shift ? "Shift + " : "")
            + (meta ? "Meta + " : "")
            + key;
    }

    function map(array, callback) {
        return array.map(function () {
            try {
                return callback.apply(this, arguments);
            } catch (e) {
                console.warn(e);
            }
        });
    }

    function prepareLoadSequent(sequent) {
        if (sequent.rawData.target.HTMLElementPath == "document") {
            return (
                React.createElement("div", null, "Document loaded")
            )
        }
        var scriptSrc, attributes = sequent.rawData.target.attributes;
        if (attributes) {
            for (var i = 0; i < attributes.length; ++i) {
                var attr = attributes[i];
                if (attr.name == "src") {
                    scriptSrc = attr.value;
                    break;
                }
            }
        }
        return (
            React.createElement("div", null, 
                "Resource ", scriptSrc ? React.createElement("code", null, React.createElement(LongText, {text: scriptSrc})) : "", " was loaded."
            )
        )
    }

    function prepareXhrLoadSequent(sequent) {
        var scriptSrc = sequent.rawData.target.responseURL;
        return (
            React.createElement("div", null, 
                "XMLHttpRequest ", scriptSrc ? React.createElement("span", null, "to ", React.createElement("code", null, React.createElement(LongText, {text: scriptSrc}))) : "", " completed."
            )
        )
    }

    function prepareDOMContentLoadedSequent() {
        return (
            React.createElement("div", null, 
                "The document loading finished."
            )
        )
    }

    function prepareKeyboardSequent(sequent) {
        var rawData = sequent.rawData;
        var keyString = makeKeyString(rawData.altKey, rawData.ctrlKey, rawData.shiftKey, rawData.metaKey, rawData.charCode || rawData.keyCode || rawData.which);

        return (
            React.createElement("div", null, 
                "Key pressed: ", React.createElement("code", {className: "keyboardButton"}, keyString), ". Focus in ", React.createElement(SnapshotElementPath, {path: rawData.target.HTMLElementPath})
            )
        )
    }

    function prepareMouseSequent(sequent) {
        var keys = [].concat(
            sequent.rawData.ctrlKey ? "Ctrl" : undefined,
            sequent.rawData.altKey ? "Alt" : undefined,
            sequent.rawData.shiftKey ? "Shift" : undefined,
            sequent.rawData.metaKey ? "Meta" : undefined
        );
        var keyString = (keys.filter(function (elem) {
                return !!elem;
            }).join(", ") || "No") + " key modifiers";

        var target = sequent.rawData.target || sequent.rawData.srcElement;
        return (
            React.createElement("div", null, 
                React.createElement("b", null, mouseKeyTypeByNumber[sequent.rawData.button]), " mouse click on", " ", 
                React.createElement(SnapshotElementPath, {path: target.HTMLElementPath}), " ", keyString
            )
        )
    }

    function prepareInitialLoadSequent(sequent) {
        function highlightedStartParams() {
            var formattedParams = JSON.stringify(JSON.parse(sequent.rawData.startParams), null, 2);
            return HighlightJS.highlight("json", formattedParams).value;
        }

        return (
            React.createElement("div", null, 
                React.createElement("strong", null, "Record started"), 
                sequent.rawData.startParams ?
                React.createElement("span", null, 
                    React.createElement("pre", {className: "start-params-json-block", 
                         dangerouslySetInnerHTML: {__html: highlightedStartParams()}})
                )
                : null
            )
        )
    }

    function prepareCreateIssueSequent(sequent) {
        var type = sequent.rawData.issueType;
        var description = sequent.rawData.description;
        return (
            React.createElement("div", null, 
                React.createElement("p", null, 
                    React.createElement("strong", null, "New issue"), " ", type ? React.createElement("code", null, React.createElement(LongText, {text: type})) : React.createElement("b", null, "empty")
                ), 
                React.createElement("strong", null, "Description"), React.createElement("br", null), " ", description ? React.createElement("code", null, React.createElement(LongText, {text: description})) : React.createElement("b", null, "empty")
            )
        )
    }

    function prepareXhrResponseSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent && prevSequent.previousSequentNum) {
            var initialSequent = SequentUtils.getSequentByNum(prevSequent.previousSequentNum, this.props.activeFrame);
            if (initialSequent) {
                var initialSequentNode = (
                    React.createElement("div", null, 
                        "Initial sequent is", " ", 
                        React.createElement("a", {onClick: this.goToSequent(prevSequent.previousSequentNum)}, initialSequent.sequentSubtype, " ", initialSequent.sequentType)
                    )
                )
            }
        }

        return (                                                                                                 
            React.createElement("span", null, 
                React.createElement("div", null, 
                    "XMLHttpRequest received response from server." + ' ' +
                    "Status ", React.createElement("strong", null, sequent.rawData.target.status, " ", sequent.rawData.target.statusText), "." + ' ' +
                    "Response body:"
                ), 
                React.createElement("pre", null, 
                    React.createElement(LongText, {text: sequent.rawData.target.response})
                ), 
                React.createElement("div", null, 
                    "Response headers:"
                ), 
                React.createElement("pre", null, 
                    React.createElement(LongText, {text: (sequent.rawData.target.responseHeaders)})
                ), 
                React.createElement("div", null, 
                    React.createElement("a", {href: "", onClick: this.goToSequent(sequent.previousSequentNum)}, "The XHR send request"), " caused this response."
                ), 
                initialSequentNode
            )
        )
    }

    function prepareAbortXhrSequent(sequent) {
        return (
            React.createElement("span", null, 
                React.createElement("div", null, 
                    React.createElement("a", {onClick: this.goToSequent(sequent.previousSequentNum)}, "XMLHttpRequest"), " was aborted."
                )
            )
        )
    }

    function prepareReadyStateChangeSequent(sequent) {
        var readyState = sequent.rawData.target.readyState;
        var isDone = (readyState == 4);
        var self = this;

        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent && prevSequent.previousSequentNum) {
            var initialSequent = SequentUtils.getSequentByNum(prevSequent.previousSequentNum, this.props.activeFrame);
            if (initialSequent) {
                var initialSequentNode = (
                    React.createElement("div", null, 
                        "Initial sequent is", " ", 
                        React.createElement("a", {onClick: self.goToSequent(prevSequent.previousSequentNum)}, initialSequent.sequentSubtype, " ", initialSequent.sequentType)
                    )
                )
            }
        }

        return (
            React.createElement("span", null, 
                React.createElement("div", null, 
                    "State of network request changed to ", React.createElement("b", null, readyStateByNumber[readyState])
                ), 

            !isDone ? "" : ([
                React.createElement("div", null, 
                    "XMLHttpRequest received response from server." + ' ' +
                    "Status ", React.createElement("strong", null, sequent.rawData.target.status, " ", sequent.rawData.target.statusText), "." + ' ' +
                    "Response body:"
                ),
                React.createElement("pre", null, 
                    React.createElement(LongText, {text: sequent.rawData.target.response})
                ),
                React.createElement("div", null, 
                    "Response headers:"
                ),
                React.createElement("pre", null, 
                    React.createElement(LongText, {text: (sequent.rawData.target.responseHeaders)})
                ),
                React.createElement("div", null, 
                    React.createElement("a", {href: "", onClick: this.goToSequent(sequent.previousSequentNum)}, "The XHR send request"), " caused this response."
                ),
                initialSequentNode
            ])
            )
        )
    }

    function prepareVisibilitySequent(sequent) {
        return (
            React.createElement("div", null, 
                "Page was ", sequent.rawData.hidden ? "hidden" : "reveal", "."
            )
        )
    }

    function prepareBeforeUnloadSequent(sequent) {
        return (
            React.createElement("p", null, React.createElement("strong", null, "Page unloaded"))
        )
    }

    function prepareSetTimeoutSequent(sequent) {
        return (
            React.createElement("div", null, 
                React.createElement("h3", null, "setTimeout call"), 

                React.createElement(Stacktrace, {stacktrace: sequent.stack, method: "window.setTimeout"})
            )
        )
    }

    function prepareSetIntervalSequent(sequent) {
        return (
            React.createElement("div", null, 
                React.createElement("h3", null, "setInterval call"), 

                React.createElement(Stacktrace, {stacktrace: sequent.stack, method: "window.setInterval"})
            )
        )
    }

    function prepareClearTimeoutSequent(sequent) {
        return (
            React.createElement("span", null, 
                React.createElement("div", null, 
                    React.createElement("code", null, "window.clearTimeout()"), " has been called. It canceled", " ", 
                    React.createElement("a", {href: "#", onClick: this.goToSequent(sequent.previousSequentNum)}, "previous setTimeout")
                ), 

                React.createElement("div", null, 
                    "Here is call stack trace:"
                ), 

                React.createElement(Stacktrace, {stacktrace: sequent.stack, method: "window.clearTimeout"})
            )
        )
    }

    function prepareClearIntervalSequent(sequent) {
        return (
            React.createElement("span", null, 
                React.createElement("div", null, 
                    React.createElement("code", null, "window.clearInterval()"), " has been called. It canceled", " ", 
                    React.createElement("a", {href: "#", onClick: this.goToSequent(sequent.previousSequentNum)}, "previous setInterval")
                ), 

                React.createElement("div", null, 
                    "Here is call stack trace:"
                ), 

                React.createElement(Stacktrace, {stacktrace: sequent.stack, method: "window.clearInterval"})
            )
        )
    }

    function prepareTimeoutCallbackSequent (sequent) {
        return (
            React.createElement("span", null, 
                "Invokation of callback passed to window.setTimeout" + ' ' +
                ". Caused by ", React.createElement("a", {href: "#", onClick: this.goToSequent(sequent.previousSequentNum)}, "this sequent"), 
                ". Stacktrace:", 
                React.createElement("p", null, React.createElement(Stacktrace, {stacktrace: sequent.stack}))
            )
        )
    }

    function prepareIntervalCallbackSequent (sequent) {
        return (
            React.createElement("span", null, 
                "Invokation of callback passed to window.setInterval" + ' ' +
                ". Caused by ", React.createElement("a", {href: "#", onClick: this.goToSequent(sequent.previousSequentNum)}, "this sequent"), 
                ". Stacktrace:", 
                React.createElement("p", null, React.createElement(Stacktrace, {stacktrace: sequent.stack}))
            )
        )
    }

    function prepareResizeSequent(sequent) {
        return (
            React.createElement("div", null, 
                "Window was resized. New width is ", React.createElement("b", null, sequent.rawData.width), " and height is ", React.createElement("b", null, sequent.rawData.height), "."
            )
        )
    }

    function prepareScrollSequent(sequent) {
        var offset;
        if (sequent.scrollX != 0) {
            offset = (
                React.createElement("span", null, 
                    React.createElement("b", null, sequent.scrollY, "px from top"), " and ", React.createElement("b", null, sequent.scrollX, "px from left")
                )
            )
        } else {
            offset = (
                React.createElement("span", null, 
                    React.createElement("b", null, sequent.scrollY, "px from top")
                )
            )
        }

        return (
            React.createElement("div", null, 
                "Page was scrolled to ", offset, "."
            )
        );
    }

    function prepareOpenXhrSequent(sequent) {
        return (
            React.createElement("div", null, 
                "Created new XMLHttpRequest with ", React.createElement("code", null, sequent.rawData.args[0]), " method to", " ", 
                React.createElement("code", null, 
                    React.createElement(LongText, {text: sequent.rawData.args[1]})
                ), 
                ".", sequent.previousSequentNum ?
                React.createElement("span", null, " Caused by ", React.createElement("a", {href: "", onClick: this.goToSequent(sequent.previousSequentNum)}, "this sequent"))
                : null
            )
        )
    }

    function prepareSendXhrSequent(sequent) {
        var requestBody;
        if (sequent.rawData.args[0]) {
            requestBody = (
                React.createElement("span", null, 
                    "Request body:", " ", 
                    React.createElement("pre", null, 
                        React.createElement(LongText, {text: sequent.rawData.args[0]})
                    )
                )
            )
        }

        return (
            React.createElement("div", null, 
                React.createElement("div", null, 
                    "XMLHttpRequest sent. ", requestBody || "No additional data added."
                )
            )
        );
    }

    function prepareConsoleSequent(sequent) {
        return (
            React.createElement("span", null, 
                React.createElement("div", null, 
                    "Text was written to browser's console:"
                ), 

                React.createElement("pre", null, 
                    React.createElement(LongText, {text: sequent.rawData.args[0]})
                )
            )
        );
    }

    function prepareErrorSequent(sequent) {
        var firstFramePresent = sequent.stack && sequent.stack[0];
        var filename, lineNum;
        if (firstFramePresent) {
            filename = sequent.stack[0].uri.slice(sequent.stack[0].uri.lastIndexOf("/") + 1);
            lineNum = sequent.stack[0].line;
        } else {
            filename = sequent.rawData.filename;
            lineNum = sequent.rawData.lineno;
        }
        return (
            React.createElement("span", null, 
                React.createElement("div", null, 
                    "Uncaught ", React.createElement("code", null, "Error"), ". File", " ", 
                    React.createElement("code", null, 
                        React.createElement(LongText, {text: filename})
                    ), " at line", " ", 
                    React.createElement("b", null, lineNum), "."
                ), 
                React.createElement("p", null, 
                    React.createElement("code", null, 
                        React.createElement(LongText, {text: sequent.rawData.message})
                    )
                ), 

            sequent.stack && sequent.stack && sequent.stack.length ? [

                React.createElement(Stacktrace, {stacktrace: sequent.stack})] :
                null
            )
        )
    }

    function prepareDomMutationSequent(sequent) {
        var mutationInfo = this.state.mutationInfo;
        if (!mutationInfo || !(mutationInfo.frameNum == sequent.frameNum && mutationInfo.sequentNum == sequent.sequentNum)) {
            return null;
        }
        var mutations = SequentUtils.parseMutations(sequent);
        var addedOrMoved = mutations.addedOrMoved, removed = mutations.removed, attributes = mutations.attributes,
            text = mutations.text, propertiesChanged = mutations.propertiesChanged;

        if (addedOrMoved.length) {
            var addedNodesElement = (
                React.createElement("span", null, 
                    React.createElement("div", null, "Added nodes: "), 
                    React.createElement("ul", null, 
                            map(addedOrMoved, function (node, index) {
                                var elem = mutationInfo[node.id];
                                if (node.nodeType == Node.TEXT_NODE) {
                                    return (
                                        React.createElement("li", {key: "added-" + index}, 
                                            "Text node", " ", 
                                            React.createElement("code", null, 
                                                React.createElement(LongText, {text: node.textContent})
                                            ), " added to parent", " ", 
                                            React.createElement(SnapshotElementPath, {path: elem.parentPath})
                                        )
                                    )
                                } else if (node.nodeType == Node.COMMENT_NODE) {
                                    // no need to display adding of HTML comments
                                    return null;
                                } else {
                                    return (
                                        React.createElement("li", {key: "added-" + index}, 
                                            React.createElement("code", null, elem.tagName), " CSS path", " ", 
                                            React.createElement(SnapshotElementPath, {path: elem.nodePath}), " added to parent", " ", 
                                            React.createElement(SnapshotElementPath, {path: elem.parentPath})
                                        )
                                    )
                                }
                            }), 
                            map(text, function createTextNode(node, index) {
                                var elem = mutationInfo[node.id];
                                return (
                                    React.createElement("li", {key: "added-" + index}, 
                                        "Text node", " ", 
                                        React.createElement("code", null, 
                                            React.createElement(LongText, {text: elem.text})
                                        ), " added to parent", " ", 
                                        React.createElement(SnapshotElementPath, {path: elem.parentPath})
                                    )
                                )
                            })
                    )
                )
            )
        }
        if (removed.length) {
            var removedNodesElement = (
                React.createElement("span", null, 
                    React.createElement("div", null, "Removed nodes: "), 
                    React.createElement("ul", null, 
                            map(removed, function (node, index) {
                                var elem = mutationInfo[node.id];
                                var description;
                                if (elem) {
                                    if (elem.tagName) {
                                        description = (
                                            React.createElement("span", null, 
                                                "Node", 
                                                React.createElement("code", null, elem.tagName)
                                            )
                                        )
                                    } else {
                                        description = (
                                            React.createElement("span", null, 
                                                "Text node", " ", 
                                                React.createElement("code", null, 
                                                    React.createElement(LongText, {text: elem.text})
                                                )
                                            )
                                        )
                                    }
                                    return (
                                        React.createElement("li", {key: "added-" + index}, 
                                        description, " removed from parent", " ", 
                                            React.createElement(SnapshotElementPath, {path: elem.parentPath})
                                        )
                                    )
                                }
                            })
                    )
                )
            )
        }
        if (attributes.length) {
            var attributesElement = (
                React.createElement("span", null, 
                    React.createElement("div", null, "Attributes value changed: "), 
                    React.createElement("ul", null, 
                            map(attributes, function (attrInfo, indexOuter) {
                                var attributes = attrInfo.attributes;
                                var elem = mutationInfo[attrInfo.id];
                                return Object.keys(attributes).map(function (attrName, indexInner) {
                                    return (
                                        React.createElement("li", {key: indexOuter + "_" + indexInner}, 
                                            "Attribute", " ", 
                                            React.createElement("code", null, 
                                                React.createElement(LongText, {text: attrName})
                                            ), " of node", " ", 
                                            React.createElement(SnapshotElementPath, {path: elem.nodePath}), " changed to", " ", 
                                            React.createElement("code", null, 
                                                React.createElement(LongText, {text: attributes[attrName]})
                                            )
                                        )
                                    )
                                })
                            })
                    )
                )
            )
        }
        if (propertiesChanged && propertiesChanged.length) {
            var valueChangedElement = (
                React.createElement("span", null, 
                    React.createElement("div", null, "HTML properties changed: "), 
                    React.createElement("ul", null, 
                    map(propertiesChanged, function (info, indexOuter) {
                        var keys = Object.keys(info.properties);
                        var elem = mutationInfo[info.id];
                        return map(keys, function (key, indexInner) {
                            return (
                                React.createElement("li", {key: indexOuter + "_" + indexInner}, 
                                    "Value of property ", React.createElement("code", null, key), " of element", 
                                    React.createElement(SnapshotElementPath, {path: elem.nodePath}), 
                                    "changed to ", React.createElement("code", null, React.createElement(LongText, {text: info.properties[key]}))
                                )
                            )
                        });
                    })
                    )
                )
            )
        }

        return (
            React.createElement("span", null, 
                addedNodesElement, 
                removedNodesElement, 
                attributesElement, 
                valueChangedElement
            )
        )
    }

    function prepareFocusSequent(sequent) {
        return (
            React.createElement("div", null, 
                "On focus ", React.createElement(SnapshotElementPath, {path: sequent.rawData.target.HTMLElementPath})
            )
        )
    }

    function prepareBlurSequent(sequent) {
        return (
            React.createElement("div", null, 
                "On focusout ", React.createElement(SnapshotElementPath, {path: sequent.rawData.target.HTMLElementPath})
            )
        )
    }

    function prepareHistoryPushSequent(sequent) {
        return (
            React.createElement("div", null, 
                "history.push method called. Url to push:", " ", 
                React.createElement("code", null, 
                    React.createElement(LongText, {text: sequent.rawData.args[2]})
                ), 
                ", page title:", " ", 
                React.createElement("code", null, 
                    React.createElement(LongText, {text: sequent.rawData.args[1]})
                )
            )
        )
    }

    function prepareInputSequent(sequent) {
        return (
            React.createElement("div", null, 
                "Value changed of ", " ", 
                React.createElement(SnapshotElementPath, {path: sequent.rawData.target.HTMLElementPath})
            )
        )
    }

    function prepareMessageSequent(sequent) {
        var ports;
        if (sequent.rawData.ports && sequent.rawData.ports.length > 0) {
            ports = (
                React.createElement("span", null, 
                    "in ports ", React.createElement(LongText, {text: sequent.rawData.ports.join(", ")})
                )
            )
        }

        var data;
        if (sequent.rawData.data) {
            data = (
                React.createElement("span", null, 
                    React.createElement("div", null, 
                        "Data that sent:"
                    ), 
                    React.createElement("pre", null, 
                        React.createElement(LongText, {text: sequent.rawData.data})
                    )
                )
            )
        } else {
            data = (
                React.createElement("div", null, 
                    "No data sent"
                )
            )
        }

        return (
            React.createElement("span", null, 
                React.createElement("div", null, 
                    "Message sent to ", React.createElement("code", null, React.createElement(LongText, {text: sequent.rawData.origin})), " ", ports, "."
                ), 
                data
            )
        )
    }

    function prepareSetCookieSequent(sequent) {
        var cookie = parseCookie(sequent.rawData.value);
        var oldValue = sequent.rawData.oldValue;
        var key = cookie.__key;
        return (
            React.createElement("span", null, 
                React.createElement("h3", null, "Cookies updated"), 

            React.createElement("table", {className: "table table-striped"}, 
                React.createElement("thead", null, 
                React.createElement("tr", null, 
                    React.createElement("th", null, "Property"), 
                    React.createElement("th", null, "Value")
                )
                ), 

                typeof cookie.__key != "undefined" ? React.createElement("tr", null, 
                    React.createElement("td", null, "Key"), 
                    React.createElement("td", null, 
                        React.createElement(LongText, {text: cookie.__key})
                    )
                ) : null, 
                typeof oldValue != "undefined" ? React.createElement("tr", null, 
                    React.createElement("td", null, "Previous value"), 
                    React.createElement("td", null, 
                        React.createElement(LongText, {text: oldValue})
                    )
                ) : null, 
                typeof cookie[key] != "undefined" ? React.createElement("tr", null, 
                    React.createElement("td", null, "Value"), 
                    React.createElement("td", null, 
                        React.createElement(LongText, {text: cookie[key]})
                    )
                ) : null, 
                typeof cookie.path != "undefined" ? React.createElement("tr", null, 
                    React.createElement("td", null, "Path"), 
                    React.createElement("td", null, 
                        React.createElement(LongText, {text: cookie.path})
                    )
                ) : null, 
                typeof cookie.domain != "undefined" ? React.createElement("tr", null, 
                    React.createElement("td", null, "Domain"), 
                    React.createElement("td", null, cookie.domain)
                ) : null, 
                typeof cookie["max-age"] != "undefined" ? React.createElement("tr", null, 
                    React.createElement("td", null, "Max age"), 
                    React.createElement("td", null, cookie["max-age"], " seconds")
                ) : null, 
                typeof cookie.expires != "undefined" ? React.createElement("tr", null, 
                    React.createElement("td", null, "Expires"), 
                    React.createElement("td", null, cookie.expires)
                ) : null, 
                typeof cookie.secure != "undefined" ? React.createElement("tr", null, 
                    React.createElement("td", null, "Secured"), 
                    React.createElement("td", null, cookie.secure)
                ) : null
            )
            )
        )
    }

    function prepareDragStartSequent(sequent) {
        var targetPath = sequent.rawData.target.HTMLElementPath;
        return (
            React.createElement("span", null, 
                "On dragstart ", React.createElement(SnapshotElementPath, {path: targetPath})
            )
        )
    }

    function prepareDropSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        SequentUtils.parseRawData(prevSequent);
        var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        var targetPath = sequent.rawData.target.HTMLElementPath;
        return (
            React.createElement("span", null, 
                React.createElement(SnapshotElementPath, {path: draggablePath}), " has been dropped to", 
                React.createElement(SnapshotElementPath, {path: targetPath}), 
                React.createElement("br", null), 
                "Drag started ", React.createElement("a", {onClick: this.goToSequent(sequent.previousSequentNum)}, "sequent")
            )
        )
    }

    function prepareDragLeaveSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent) {
            SequentUtils.parseRawData(prevSequent);
            var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        }
        var targetPath = sequent.rawData.target.HTMLElementPath;

        return (
            React.createElement("span", null, 
                draggablePath ?
                    React.createElement(SnapshotElementPath, {path: draggablePath}) :
                    "A draggable element", " has left the element ", React.createElement(SnapshotElementPath, {path: targetPath}), 
                prevSequent ?
                    React.createElement("span", null, "Drag started ", React.createElement("a", {onClick: this.goToSequent(sequent.previousSequentNum)}, "sequent")) :
                    null
                
            )
        )
    }

    function prepareDragEndSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent) {
            SequentUtils.parseRawData(prevSequent);
            var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        }

        return (
            React.createElement("span", null, 
                "Dragging an element", 
                draggablePath ?
                    React.createElement(SnapshotElementPath, {path: draggablePath}) : null, " has been ended" + ' ' +
                    ". ", prevSequent ?
                React.createElement("span", null, "Drag started ", React.createElement("a", {onClick: this.goToSequent(sequent.previousSequentNum)}, "sequent")) : null
                
            )
        )
    }

    function prepareDragOverSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent) {
            SequentUtils.parseRawData(prevSequent);
            var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        }
        var targetPath = sequent.rawData.target.HTMLElementPath;

        return (
            React.createElement("span", null, 
            draggablePath ? React.createElement(SnapshotElementPath, {path: draggablePath}) : "A draggable element", " has been dragged over ", React.createElement(SnapshotElementPath, {path: targetPath}), 
                ". ", prevSequent ?
                React.createElement("span", null, "Dragging was started on ", React.createElement("a", {onClick: this.goToSequent(sequent.previousSequentNum)}, "this sequent"), ".") : null
                
            )
        )
    }

    function prepareDragEnterSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent) {
            SequentUtils.parseRawData(prevSequent);
            var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        }
        var targetPath = sequent.rawData.target.HTMLElementPath;

        return (
            React.createElement("span", null, 
            draggablePath ? React.createElement(SnapshotElementPath, {path: draggablePath}) : "A draggable element", " has been dragged into ", React.createElement(SnapshotElementPath, {path: targetPath}), 
                ". ", prevSequent ?
                React.createElement("span", null, "Dragging was started on ", React.createElement("a", {onClick: this.goToSequent(sequent.previousSequentNum)}, "this sequent"), ".") : null
                
            )
        )
    }

    function preparePostMessageSequent(sequent) {
        let message;
        try {
            message = typeof sequent.rawData.args[0] === 'object' ?
                JSON.stringify(sequent.rawData.args[0], null, 2) : sequent.rawData.args[0];
        } catch (e) {
            message = sequent.rawData.args[0];
        }
        return (
            React.createElement("span", null, 
                React.createElement("div", null, 
                    React.createElement("code", null, "window.postMessage"), " was called. Message was sent to origin", " ", 
                    React.createElement("code", null, React.createElement(LongText, {text: sequent.rawData.args[1]})), "."
                ), 
                React.createElement("div", null, 
                    "Message that was sent:", 
                    React.createElement("pre", null, 
                        React.createElement(LongText, {text: message})
                    )
                ), 
                React.createElement("div", null, "Stacktrace:"), 
                React.createElement("div", null, 
                    React.createElement(Stacktrace, {stacktrace: sequent.stack})
                )
            )
        )
    }

    function prepareDefaultEventSequent(sequent) {
        return (
            React.createElement("span", null, 
                "Browser event of type ", React.createElement("code", null, sequent.rawData.type), " happened."
            )
        )
    }

    function prepareDefaultInterceptionSequent(sequent) {
        var args = Array.prototype.slice.call(sequent.rawData.args || [])
            .map(function (arg) {
                try {
                    return JSON.stringify(JSON.parse(arg));
                } catch (e) {
                    return JSON.stringify(arg);
                }
            }).join("\n");
        return (
            React.createElement("span", null, 
                React.createElement("div", null, "Method ", React.createElement("code", null, sequent.sequentClass, ".", sequent.rawData.methodName), " was invoked."), 
            sequent.rawData.args ?
                React.createElement("div", null, 
                    React.createElement("h3", null, "Arguments: "), 
                    React.createElement("code", null, React.createElement(LongText, {text: args}))
                )
            : null
            )
        )
    }

    return SequentInfo;

});