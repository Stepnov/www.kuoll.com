define(["jquery", "react", "app/utils/SequentLoader", "app/utils/SequentUtils", "app/react/play/MouseMark",
    "app/utils/TipUtils", "jquery.ui", "jquery.qtip"], function ($, React,SequentLoader, SequentUtils, MouseMark, TipUtils) {

    var SnapshotSlider = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {
            onSliderMoved: React.PropTypes.func.isRequired,
            mutationSequents: React.PropTypes.array,
            mouseSequents: React.PropTypes.array,
            frames: React.PropTypes.array,
            changeActiveFrame: React.PropTypes.func.isRequired,
            getSnapshot: React.PropTypes.func.isRequired
        },

        updateSnapshotTimeout: null,

        render: function () {
            return (
                <div id="slider-box" ref="sliderBox">
                    <div id="snapshotSlider" className="snapshotSlider" ref="slider">
                        <MouseMark mouseSequents={this.props.mouseSequents} onClick={this.moveSlider}
                            snapshotsAmount={this.props.mutationSequents.length} mutationSequents={this.props.mutationSequents}/>
                    </div>
                </div>
            )
        },

        updateSliderTip: function (frameNum, sequentNum) {
            $(React.findDOMNode(this.refs.slider)).find(".ui-slider-handle").qtip()
                .set("content.title", "Frame num: " + frameNum)
                .set("content.text", "Sequent num: " + sequentNum);
        },

        moveSlider: function (snapshotInfo) {
            if (!this.props.mutationSequents) {
                return;
            }

            var snapshot;
            if (typeof snapshotInfo == "number") {
                snapshot = this.props.mutationSequents[snapshotInfo];
            } else if (typeof snapshotInfo == "object") {
                for (var i = this.props.mutationSequents.length - 1; i >= 0; --i) {
                    var value = this.props.mutationSequents[i];

                    if (value.frameNum < snapshotInfo.frameNum || (value.frameNum == snapshotInfo.frameNum && value.sequentNum <= snapshotInfo.sequentNum) || i == 0) {
                        snapshot = this.props.mutationSequents[i];
                        break;
                    }
                }
                snapshotInfo = i;
            } else {
                throw new Error("TypeError: snapshotInfo must be either number or sequent")
            }
            if (this.props.mutationSequents.length != 0) {
                $(React.findDOMNode(this.refs.slider)).slider("value", snapshotInfo + 1);
                var snapshotSequentNum = snapshot.sequentNum;
                var snapshotFrameNum = snapshot.frameNum;
                this.updateSliderTip(snapshotFrameNum, snapshotSequentNum);
            }
        },

        updateSnapshotOnSliderMoved: function () {
            var self = this;
            var $slider = $(React.findDOMNode(this.refs.slider));

            var snapshotIndex = $slider.slider("value") - 1;
            var snapshot = self.props.mutationSequents[snapshotIndex];
            var snapshotSequentNum = snapshot.sequentNum;
            var snapshotFrameNum = snapshot.frameNum;

            this.props.onSliderMoved(snapshotIndex);

            var frame = SequentUtils.getFrame(snapshotFrameNum, this.props.frames);
            if (!frame) {
                throw new Error("No frame for specified frameNum: " + snapshotFrameNum);
            }
            SequentLoader.loadSequentsTo(snapshotSequentNum, frame, function () {
                self.props.changeActiveFrame(frame, snapshotSequentNum);
            });
        },

        componentDidMount: function () {
            var self = this;
            var $slider = $(React.findDOMNode(this.refs.slider));
            $slider.slider({
                range: "min",
                min: 1,
                max: self.props.mutationSequents ? self.props.mutationSequents.length : 1,
                value: 0,
                disabled: self.props.mutationSequents === false,
                slide: function (event, ui) {
                    var snapshotIndex = ui.value - 1;

                    var snapshot = self.props.mutationSequents[snapshotIndex];
                    var snapshotSequentNum = snapshot.sequentNum;
                    var snapshotFrameNum = snapshot.frameNum;
                    self.updateSliderTip(snapshotFrameNum, snapshotSequentNum);

                    if (self.updateSnapshotTimeout) {
                        clearTimeout(self.updateSnapshotTimeout);
                    }

                    self.updateSnapshotTimeout = setTimeout(function () {
                        if (snapshotIndex == $slider.slider("value") - 1) {
                            self.updateSnapshotOnSliderMoved();
                        }
                    }, 100);

                },
                stop: function () {
                    if (self.updateSnapshotTimeout) {
                        clearTimeout(self.updateSnapshotTimeout);
                        self.updateSnapshotTimeout = null;
                    }
                    self.updateSnapshotOnSliderMoved();
                }
            });
            if (this.props.mutationSequents === false) {
                TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.sliderBox)), "There is no snapshot for this records, slider is disabled.");
            }
        },

        componentDidUpdate: function (nextProps) {
            var $slider = $(React.findDOMNode(this.refs.slider));
            if (nextProps.mutationSequents.length > 0) {
                var snapshotIndex = $slider.slider("value") - 1;
                var snapshot = nextProps.mutationSequents[snapshotIndex];
                var snapshotSequentNum = snapshot.sequentNum;
                var snapshotFrameNum = snapshot.frameNum;

                $slider.find(".ui-slider-handle").qtip({
                    content: {
                        title: "Frame num: " + snapshotFrameNum,
                        text: "Sequent num: " + snapshotSequentNum
                    },
                    position: {
                        my: "top center",
                        at: "bottom center"
                    },
                    show: {
                        delay: 100
                    },
                    hide: {
                        delay: 100
                    },
                    style: {
                        classes: "qtip-dark qtip-rounded"
                    }
                });
            }
            if (this.props.mutationSequents.length != nextProps.mutationSequents) {
                $slider.slider("option", "max", this.props.mutationSequents.length);
            }
        }

    });

    return SnapshotSlider;

});
