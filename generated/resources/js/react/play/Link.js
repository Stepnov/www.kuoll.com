define(["react"], function (React) {

    var Link = React.createClass({displayName: "Link",

        /* State declaration */
        getInitialState: function () {
            return {

            };
        },

        /* Props declaration */
        propTypes: {
            href: React.PropTypes.string.isRequired,
            children: React.PropTypes.string
        },

        highlightUrl: function (url) {
            var regex = /\/\/([^/:]+)(.*)/;
            var split = regex.exec(url);
            return (
                React.createElement("span", {className: "url"}, 
                    React.createElement("span", {className: "host"}, split[1]), 
                    React.createElement("span", {className: "path"}, split[2])
                )
            );
        },

        render: function () {
            return (
                React.createElement("a", React.__spread({},  this.props), 
                this.highlightUrl(this.props.children || this.props.href)
                )
            )
        }

    });

    return Link;

});
