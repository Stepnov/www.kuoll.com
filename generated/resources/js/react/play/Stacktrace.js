define(["react"], function (React) {

    var Stacktrace = React.createClass({displayName: "Stacktrace",

        /* Props declaration */
        propTypes: {
            stacktrace: React.PropTypes.array.isRequired,
            description: React.PropTypes.string,
            method: React.PropTypes.string
        },

        methodInfo: {
            "window.setTimeout": "https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/setTimeout",
            "window.setInterval": "https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/setInterval",
            "window.clearTimeout": "https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/clearTimeout",
            "window.clearInterval": "https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/clearInterval"
        },

        render: function () {
            var methodInfo = this.methodInfo[this.props.method];
            return (
                React.createElement("pre", {className: "stacktrace"}, 
                methodInfo ?
                    React.createElement("div", null, 
                        React.createElement("a", {href: methodInfo, className: "stacktrace-method", target: "_blank"}, "window.setTimeout")
                    )
                : null, 

                    React.createElement("div", {className: "stacktrace-description"}, 
                        this.props.description
                    ), 

                    React.createElement("table", {className: "stacktrace"}, 
                        React.createElement("tbody", null, 
                    this.props.stacktrace.map(function (stack, index) {
                        var uri = stack.uri[stack.uri.length - 1] != "/" ?
                            stack.uri.slice(stack.uri.lastIndexOf("/") + 1)
                            : stack.uri;
                        var shortenedUri = uri.slice(0, 100);
                        return (
                            React.createElement("tr", {key: index, className: "stacktrace-item "}, 
                                React.createElement("td", {className: "stacktrace-item-divider stacktrace-function"}, "  at ", stack.function), 
                                React.createElement("td", null, " "), 
                                React.createElement("td", {title: stack.uri, className: "textOverflowEllipsis stacktrace-item-source-file"}, 
                                    uri
                                ), 
                                React.createElement("td", {className: "stacktrace-item-source stacktrace-lineNum"}, " ", stack.line), 
                                React.createElement("td", null, ":"), 
                                React.createElement("td", {className: "stacktrace-item-source"}, stack.columnNumber)
                            )
                        )
                    })
                        )
                    )
                )
            )
        }

    });

    return Stacktrace;

});