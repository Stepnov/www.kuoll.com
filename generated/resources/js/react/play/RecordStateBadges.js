define(["jquery", "react", "highlightjs", "jquery.qtip"], function ($, React,HighlightJS) {

    var RecordStateBadges = React.createClass({displayName: "RecordStateBadges",

        recordActiveTip: "Recording is still going on",
        recordNotActiveTip: "Recording has been finished",
        recordAutonomousTip: "Resources for this records are saved at Kuoll servers, so snapshots of this record will look the same way even if original site is modified.",
        recordNotAutonomousTip: "This record uses resources from original site, so if they are modified, record's snapshot may also be changed.",

        finishTypeCaption: {
            api: "Finished with JavaScript API",
            extensionPopup: "Finished with button in extension popup",
            pagePopup: "Finished with button in page popup",
            tabClosed: "Finished because browser tab was closed"
        },

        finishTypeTip: {
            api: "The record was finished by implicit calling kuoll.finishRecord method of API",
            extensionPopup: "User finished recording with button in extension popup",
            pagePopup: "User finished recording with button in Kuoll popup at the page",
            tabClosed: "Recorded tab was closed so recording was finished explicitly"
        },

        embedScriptTip: "The record was created using built-in script (bootloader.js)",
        extensionTip: "The record was created using Chrome extension",

        /* Props declaration */
        propTypes: {
            record: React.PropTypes.object.isRequired,
            showTimeInfo: React.PropTypes.bool.isRequired
        },

        render: function () {
            // TODO vlad: make RecordStateBadges understandable without reading captions (by colors)

            var record = this.props.record;
            var recordInfo = record.recordInfo;
            var isRecordGoingOn = (this.props.record.status === "started");
            return (
                React.createElement("p", null, 
                    React.createElement("span", {className: "record-label label" + (isRecordGoingOn ? " label-warning" : " label-success"), 
                        ref: "recordActiveBadge", title: isRecordGoingOn ? this.recordActiveTip : this.recordNotActiveTip}, 
                            isRecordGoingOn ? "Recording..." : "Recording completed"
                    ), 
                        recordInfo && recordInfo.recordingType === "embedScript" ? [
                            React.createElement("span", {className: "record-label label label-success embed-script-recording-label", ref: "recordTypeBadge", 
                                title: this.embedScriptTip}, 
                                "Recorded with built-in script"
                            ),
                            React.createElement("span", {className: "start-params-icon-wrapper"}, 
                                React.createElement("i", {className: "fa fa-info-circle start-params-icon", ref: "startParamsIcon"})
                            )
                        ] : null, 
                        recordInfo && recordInfo.recordingType === "extension" ?
                            React.createElement("span", {className: "record-label label label-success", ref: "recordTypeBadge", title: this.extensionTip}, 
                                "Recorded with browser extension"
                            )
                            : null, 
                        recordInfo && recordInfo.finishType ?
                            React.createElement("span", {className: "record-label label label-info", title: this.finishTypeTip[recordInfo.finishType]}, 
                                this.finishTypeCaption[recordInfo.finishType]
                            )
                            : null, 

                this.props.showTimeInfo ?
                    React.createElement("span", null, 
                        React.createElement("span", {className: "record-label"}, "Started:", 
                            React.createElement("b", null, new Date(record.startTime).toLocaleString())
                        ), 
                        !isRecordGoingOn && record.completeTime ?
                            React.createElement("span", {className: "record-label"}, "Finished:", 
                                React.createElement("b", null, new Date(record.completeTime).toLocaleString())
                            )
                            : null
                    )
                    : null, 

                    React.createElement("span", {className: "record-label label" + (record.autonomousMode ? " label-success" : " label-default"), 
                        ref: "recordAutonomousBadge", title: record.autonomousMode ? this.recordAutonomousTip : this.recordNotAutonomousTip}, 
                            record.autonomousMode ? "Autonomous mode is on" : "Autonomous mode is off"
                    )
                )
            )
        },

        componentDidMount: function () {
            if (this.refs.startParamsIcon) {
                var $icon = $(React.findDOMNode(this.refs.startParamsIcon));
                var startParams = JSON.stringify(JSON.parse(this.props.record.recordInfo.startParams), null, 2);
                var paramsJson = HighlightJS.highlight("json", startParams);
                $icon.qtip({
                    content: {
                        text: function () {
                            return React.renderToString(
                                React.createElement("div", null, 
                                    React.createElement("h4", {className: "start-params-header"}, "Params passed to kuoll.startRecord"), 
                                    React.createElement("pre", {className: "start-params-json-block", dangerouslySetInnerHTML: {__html: paramsJson.value}}
                                    )
                                )
                            )
                        }
                    },
                    style: {
                        classes: "qtip-tipped start-params-tip"
                    },
                    position: {
                        my: "left top",
                        at: "right top",
                        viewport: true
                    },
                    show: "click",
                    hide: "click"
                })
            }
        }

    });

    return RecordStateBadges;

});