define(["jquery", "react", "app/utils/SequentLoader", "app/utils/SequentUtils"], function ($, React,SequentLoader, SequentUtils) {

    var RecordPlayback = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                stop: true,
                waitingForSequentLoad: false,
                snapshotIndex: 0
            };
        },

        /* Props declaration */
        propTypes: {
            moveSlider: React.PropTypes.func.isRequired,
            changeFrame: React.PropTypes.func.isRequired,
            frames: React.PropTypes.array,
            mutationSequents: React.PropTypes.array,
            activeSequent: React.PropTypes.object,
            activeFrame: React.PropTypes.object
        },

        getMutationSequentIndex: function (sequent) {
            var mutationSequents = this.props.mutationSequents;
            for (var i = mutationSequents.length - 1; i >= 0; --i) {
                var num = mutationSequents[i];
                var isEarlierThanTargetSequent = (sequent.frameNum === num.frameNum && sequent.sequentNum >= num.sequentNum) || sequent.frameNum > num.frameNum;
                if (isEarlierThanTargetSequent) {
                    return i;
                }
            }
            return 0;
        },

        updateSequent: function (sequent, frame) {
            this.props.changeFrame(frame, sequent.sequentNum);
        },

        showTimeSkippedPopup: function (timeToSkip, popupTime) {
            var popupText = "Skipped " + timeToSkip + " second" + (timeToSkip === 1 ? "" : "s") + " of user's idle...";
            $("#playback-time-skipped-container").qtip({
                content: {text: popupText},
                show: {ready: true, event: false},
                hide: {inactive: popupTime || 2000, fixed: true, event: false},
                style: {classes: "qtip-tipsy playback-skipped-time-tip", tip: {corner: false}},
                position: {my: "bottom left", at: "bottom left"}
            });
        },

        tick: function () {
            var _this = this;

            var tickStartTime = Date.now();

            if (this.state.stop) {
                return;
            }
            if(this.state.snapshotIndex >= this.props.mutationSequents.length) {
                this.setState({
                    stop: true
                });
                return;
            }

            this.props.moveSlider(this.state.snapshotIndex);
            var sequent = this.props.mutationSequents[this.state.snapshotIndex];
            var frame = SequentUtils.getFrame(sequent.frameNum, this.props.frames);
            if (!frame) {
                throw new Error("No frame for specified frameNum: " + sequent.frameNum);
            }

            var isAnySequentsLeft = SequentLoader.isAnySequentsLeft(sequent.frameNum);
            if (!isAnySequentsLeft && !SequentLoader.isLoading()) {
                var nextFrame = SequentUtils.getFrame(frame.frameNum + 1, this.props.frames);
                if (nextFrame && !nextFrame.sequents && !SequentLoader.isLoading()) {
                    SequentLoader.loadSequents(0, 100, nextFrame, null, true);
                }
            }

            var isNextSequentLoaded = (SequentUtils.getSequentByNum(sequent.sequentNum, frame) !== null);
            var isCloseToLastLoadedSequent = (frame.sequents && frame.sequents[frame.sequents.length - 1].sequentNum < sequent.sequentNum + 100);
            if (!frame.sequents || !isNextSequentLoaded && isAnySequentsLeft) {
                if (SequentLoader.isLoading()) {
                    this.setState({
                        waitingForSequentLoad: true,
                        snapshotIndex: this.state.snapshotIndex + 1
                    });
                    return;
                } else {
                    SequentLoader.loadSequents(sequent.sequentNum, 200, frame, function () {
                        _this.updateSequent(sequent, frame);
                    });
                }
            } else if (isCloseToLastLoadedSequent && isAnySequentsLeft && !SequentLoader.isLoading()) {
                SequentLoader.loadSequents(sequent.sequentNum, 200, frame);
                this.updateSequent(sequent, frame);
            } else {
                this.updateSequent(sequent, frame);
            }

            var nextSnapshot = 1;
            var duration = 200;
            if (this.state.snapshotIndex + 1 < this.props.mutationSequents.length) {
                for (var i = 1; i <= 2 && duration <= 200 && i + this.state.snapshotIndex < this.props.mutationSequents.length; ++i) {
                    duration = this.props.mutationSequents[this.state.snapshotIndex + i].time - this.props.mutationSequents[this.state.snapshotIndex].time;
                }
                nextSnapshot = i;
                if (duration > 4000) {
                    var secondsToSkip = (Math.floor(duration / 1000) - 1);
                    duration -= secondsToSkip * 1000;
                    this.showTimeSkippedPopup(secondsToSkip, duration);
                }
            }

            this.setState({
                snapshotIndex: this.state.snapshotIndex + nextSnapshot
            }, function () {
                duration -= Date.now() - tickStartTime;
                if (duration < 0) {
                    console.debug("Snapshot rendering took too much time. Difference: " + duration);
                }
                setTimeout(_this.tick, Math.max(duration, 0));
            });

            // TODO run play only if there are no url params
            // TODO show mouse movements
        },

        onStartBtnClick: function () {
            this.setState({
                snapshotIndex: this.getMutationSequentIndex(this.props.activeSequent),
                stop: false
            }, this.tick);
        },

        onPauseBtnClick: function () {
            this.setState({
                stop: true
            })
        },

        onStopBtnClick: function () {
            var firstMutationSequent = this.props.mutationSequents[0];
            if (firstMutationSequent) {
                this.props.changeFrame(firstMutationSequent.frameNum, firstMutationSequent.sequentNum);
            }
            this.setState({
                snapshotIndex: 0,
                stop: true
            });
        },

        componentWillReceiveProps: function () {
            if (this.state.waitingForSequentLoad) {
                this.setState({
                    waitingForSequentLoad: false
                }, this.tick());
            }
        },

        render: function() {
            if (!this.props.mutationSequents) {
                return null;
            } else {
                return (
                    <span id="playback-buttons-wrapper">

                        {this.state.stop ?
                            <button className="btn btn-light " onClick={this.onStartBtnClick}>
                                <i className="fa fa-play fa-fw" ref="start"></i>
                            </button>
                            :
                            <button className="btn btn-light " onClick={this.onPauseBtnClick}>
                                <i className="fa fa-pause fa-fw" ref="pause"> </i>
                            </button>
                            }
                        <button className="btn btn-light" onClick={this.onStopBtnClick}>
                            <i className="fa fa-stop fa-fw" ref="stop"> </i>
                        </button>
                        <div id="playback-time-skipped-container"></div>
                    </span>
                )
            }
        }

    });

    return RecordPlayback;

});