define(["jquery", "react", "app/utils/TipUtils", "app/react/play/Link", "app/react/play/RecordStateBadges"], function ($, React,TipUtils, Link, RecordStateBadges) {

    var RecordInfo = React.createClass({displayName: "RecordInfo",

        /* Props declaration */
        propTypes: {
            record: React.PropTypes.object.isRequired
        },

        render: function () {
            var record = this.props.record;
            var recordInfo = record.recordInfo;
            var isRecordGoingOn = (this.props.record.status === "started");

            return (
                React.createElement("span", null, 

                    React.createElement("span", {id: "info-icon-wrapper", onClick: this.onInfoIconClick}, 
                        this.props.children || React.createElement("i", {className: "fa fa-info-circle"})
                    ), 

                    React.createElement("div", {id: "record-info", ref: "infoDialog"}, 

                    recordInfo && recordInfo.userAgent ? [
                        React.createElement("h3", null, "User agent"),
                        React.createElement("p", null, 
                            React.createElement("span", {className: "user-agent-info"}, "Browser: ", recordInfo.userAgent.browser.name, " ", recordInfo.userAgent.browser.version), 
                            React.createElement("br", null), 
                            React.createElement("span", {className: "user-agent-info"}, "OS: ", recordInfo.userAgent.os.name, " ", recordInfo.userAgent.os.version)
                        )
                    ] : null, 

                        React.createElement("h3", null, "Record state"), 
                        React.createElement(RecordStateBadges, {record: record, showTimeInfo: true}), 

                        React.createElement("p", null, 
                        recordInfo && !isRecordGoingOn ?
                            React.createElement("span", {id: "first-url-label", className: "record-label"}, 
                                "This record was started at the page", 
                                React.createElement("br", null), 
                                React.createElement(Link, {href: recordInfo.firstUrl, target: "_blank"})
                            )
                        : null
                        ), 

                    record.notes ? [
                        React.createElement("h3", null, "Notes to record"),
                        React.createElement("span", {ref: "recordNotes"}, record.notes)
                    ] : null

                    )

                )
            )
        },

        onInfoIconClick: function () {
            var $infoDialog = $(React.findDOMNode(this.refs.infoDialog));
            if ($infoDialog.dialog("isOpen")) {
                $infoDialog.dialog("close");
                $(".start-params-icon").qtip("api").hide();
            } else {
                $infoDialog.dialog("open");
            }
        },

        componentDidMount: function () {
            const $infoDialog = $(React.findDOMNode(this.refs.infoDialog));
            $infoDialog.dialog({
                autoOpen: false,
                title: "Record info",
                resizable: false,
                draggable: false,
                appendTo: "#record-info-wrapper",
                width: 400,
                position: {
                    my: "center center+350",
                    at: "center top"
                },
                modal: true,
                beforeClose: function () {
                    $(".start-params-icon").qtip("api").hide();
                }
            });
            TipUtils.makeSimpleTip($('[title!=""]', $infoDialog));
        }

    });

    return RecordInfo;

});