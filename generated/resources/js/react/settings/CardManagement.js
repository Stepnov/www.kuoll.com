define(["jquery", "react", "app/checkout", "app/utils/api"], function ($, React, checkout, api) {

    var CardManagement = React.createClass({displayName: "CardManagement",

        /* State declaration */
        getInitialState: function () {
            return {
            };
        },

        /* Props declaration */
        propTypes: {
            trialExpirationDate: React.PropTypes.number,
            customerId: React.PropTypes.string.isRequired,
            card: React.PropTypes.string,
            planName: React.PropTypes.string,
            onBillingUpdated: React.PropTypes.func.isRequired
        },

        promptCard: function () {
            var self = this;
            checkout.openCheckoutForm(null, this.props.customerId, function (token) {
                api("billing/attach_card", {
                    token: token.id,
                    userToken: $.cookie("userToken")
                }, function (resp) {
                    self.props.onBillingUpdated(resp);
                });
            });
        },

        detachCard: function () {
            var self = this;
            api("billing/detach_card", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                self.props.onBillingUpdated(resp);
            });
        },

        render: function () {
            return (
                React.createElement("div", {id: "card-management-form", className: "panel  panel-info"}, 
                    React.createElement("div", {className: "panel-heading"}, 
                        React.createElement("h3", {className: "panel-title"}, "Payment card")
                    ), 
                    React.createElement("div", {className: "panel-body"}, 
                        this.props.card ?
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-md-3", style: {"paddingTop": "8px"}}, 
                                React.createElement("strong", null, "Card number")
                            ), 
                            React.createElement("div", {className: "col-md-4"}, 
                                React.createElement("span", {className: "paymentCard "}, 
                                    React.createElement("i", {className: "fa fa-fw fa-credit-card", "aria-hidden": "true", style: {color: "rgba(189, 195, 199,1.0)"}}, " "), 
                                    " ", 
                                    React.createElement("span", {className: "paymentCardStarz"}, "•••• •••• •••• "), 
                                    this.props.card), 
                                this.props.trialExpirationDate ?
                                React.createElement("span", null, 
                                    "Your trial period is expiring on ", React.createElement("strong", null, 
                                    new Date(this.props.trialExpirationDate * 1000).toLocaleDateString()), 
                                    ". We won't charge you until then."
                                )
                                : null
                            ), 
                            React.createElement("div", {className: "col-md-2"}, 
                                React.createElement("button", {className: "btn btn-default", onClick: this.promptCard}, "Change card")
                            ), 
                            React.createElement("div", {className: "col-md-2"}, 
                                React.createElement("button", {className: "btn btn-warning", onClick: this.detachCard}, "Detach card")
                            )
                        )
                        :
                        this.props.trialExpirationDate ?
                            (
                                this.props.planName == "hobby" ?
                                    null
                                    :
                                    React.createElement("div", null, 
                                        React.createElement("p", null, 
                                            "Your trial period expires on ", " ", 
                                            React.createElement("strong", null, new Date(this.props.trialExpirationDate * 1000).toLocaleDateString()), 
                                            ". Please, provide billing details"
                                        ), 
                                        React.createElement("p", null, 
                                            React.createElement("button", {className: "btn btn-primary btn-lg", onClick: this.promptCard}, 
                                                "Provide billing details"
                                            )
                                        )
                                    )
                            )
                        :
                        React.createElement("div", null, 
                            React.createElement("p", null, 
                                "You account is limited to hobby plan. Please, provide billing details below."
                            ), 
                            React.createElement("button", {className: "btn btn-primary btn-lg", onClick: this.promptCard}, "Provide billing details")
                        )
                        
                    )
                )
            )
        }

    });

    return CardManagement;

});