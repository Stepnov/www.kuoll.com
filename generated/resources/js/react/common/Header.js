define(["jquery", "react", "app/react/common/IssuesQuotaExceededNotification", "app/google-signin", "app/utils/api", "jquery.cookie", "bootstrap"], function ($, React, IssuesQuotaExceededNotification, GoogleSignIn, api) {

    var Header = React.createClass({displayName: "Header",

        /* State declaration */
        getInitialState: function () {
            return {
                userToken: $.cookie("userToken")
            };
        },

        /* Props declaration */
        propTypes: {},

        onLogoutClick: function (e) {
            if ("localhost" != document.location.hostname && mixpanel) {
                mixpanel.identify("");
                mixpanel.unregister("User id");
                mixpanel.unregister("User token");
            }

            api("logout", {
                userToken: this.state.userToken
            });
            GoogleSignIn.logout();
            $.removeCookie("userToken", {domain: window.config.cookieDomain, path: "/"});
            document.location = "/";
            e.preventDefault();
        },

        render: function () {
            const isQuickStartPage = (document.location.pathname === "/quick-start.html");
            return (
                React.createElement("nav", {className: "navbar navbar-fixed-top navbar-inverse", role: "navigation"}, 
                    React.createElement("div", {className: "container"}, 
                        React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-sm-12"}, 
                        !isQuickStartPage ?
                            React.createElement("div", {className: "navbar-header"}, 
                                React.createElement("a", {className: "navbar-brand", href: "//www.kuoll.com/"}, 
                                    React.createElement("img", {src: "/resources/img/cat.png", className: "inAppCatty"})
                                )
                            )
                            : null, 
                            
                            React.createElement("div", {id: "navbar", className: "collapse navbar-collapse"}, 
                                this.state.userToken ? 
                                    React.createElement("ul", {className: "nav navbar-nav navbar-left"}, 
                                        React.createElement("li", {className: "dropdown"}, 
                                            React.createElement("div", {className: "dropdown-toggle dropdown-toggle-ala-slack", "data-toggle": "dropdown", "aria-haspopup": "true", "aria-expanded": "true"}, 
                                                React.createElement("i", {className: "fa fa-fw fa-bug"}), 
                                                " ", 
                                                "Errors", 
                                                React.createElement("span", {className: "caret"})
                                            ), 
                                            React.createElement("ul", {className: "dropdown-menu dropdown-menu-ala-slack", "aria-labelledby": "dropdownMenu1", id: "header-dropdown-menu"}, 
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/issues-dashboard.html?type=js", title: ""}, "JavaScipt errors")
                                                ), 
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/issues-dashboard.html?type=xhr", title: ""}, "XmlHttpRequest errors")
                                                ), 
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/issues-dashboard.html?type=console", title: ""}, "Console errors")
                                                ), 
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/records.html", title: "Your records"}, "Reports by Users")
                                                ), 
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/errorFilters.html", title: "Organization dashboard"}, "Error filters")
                                                )
                                            )
                                        )
                                    )
                                : null, 
                                React.createElement("ul", {className: "nav navbar-nav navbar-right"}, 
                                    React.createElement("li", {className: "dropdown"}, 
                                        React.createElement("div", {className: "dropdown-toggle dropdown-toggle-ala-slack", "data-toggle": "dropdown", "aria-haspopup": "true", "aria-expanded": "true"}, 
                                            React.createElement("i", {className: "fa fa-fw fa-user"}), 
                                            " ", 
                                            "Settings", 
                                            React.createElement("span", {className: "caret"})
                                        ), 
                                        React.createElement("ul", {className: "dropdown-menu dropdown-menu-ala-slack", "aria-labelledby": "dropdownMenu1", id: "menuDropdownSettings"}, 
                                            this.state.userToken ? [
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/org.html"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-users fa-fw"}), 
                                                        " ", 
                                                        "Team users"
                                                    )
                                                ),
                                                React.createElement("li", {className: "dropdown-header"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-comments fa-fw"}), 
                                                        " ", 
                                                        "Integrations"
                                                ),
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/settings.html#tab-slack"}, 
                                                        React.createElement("i", {className: "slack fa-fw"}), 
                                                        " ", 
                                                        "Slack"
                                                    )
                                                ),
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/settings.html#tab-telegram"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-telegram fa-fw"}), 
                                                        " ", 
                                                        "Telegram"
                                                    )
                                                ),
                                                React.createElement("li", {className: "dropdown-header"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-user-circle fa-fw"}), 
                                                        " ", 
                                                        "Account"
                                                ),
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/settings.html#tab-security"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-user-circle fa-fw"}), 
                                                        " ", 
                                                        "Account and security"
                                                    )
                                                ),
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/billing.html"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-credit-card fa-fw"}), 
                                                        " ", 
                                                        "Billing and usage"
                                                    )
                                                ),
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/support.html"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-support fa-fw"}), 
                                                        " ", 
                                                        "Support"
                                                    )
                                                ),
                                                React.createElement("li", {onClick: this.onLogoutClick}, 
                                                    React.createElement("a", {href: "/logout", id: "logoutLink", className: "logoutLink"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-sign-out fa-fw"}), 
                                                        " ", 
                                                        React.createElement("span", null, "Logout")
                                                    )
                                                )
                                            ] : [
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/signup.html"}, 
                                                        React.createElement("i", {className: "fa fa-fw fa-user-plus"}), 
                                                        " ", 
                                                        React.createElement("span", null, "Sign up")
                                                    )
                                                ),
                                                React.createElement("li", null, 
                                                    React.createElement("a", {href: "/login.html"}, 
                                                        React.createElement("i", {className: "fa  fa-fw fa-sign-in"}), 
                                                        " ", 
                                                        React.createElement("span", null, "Login")
                                                    )
                                                )
                                            ]
                                        )
                                    )
                                )
                            )
                        )
                        )
                    ), 

                    React.createElement(IssuesQuotaExceededNotification, null)
                )
            )
        },

        componentDidMount: function () {
            if (this.state.userToken) {
                GoogleSignIn.init();
            }
        }

    });

    return Header;

});
