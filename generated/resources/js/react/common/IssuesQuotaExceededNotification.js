define(["jquery", "react", "app/User"], function ($, React, User) {

    var IssuesQuotaExceededNotification = React.createClass({displayName: "IssuesQuotaExceededNotification",

        /* State declaration */
        getInitialState: function () {
            return {
                quotaExceeded: false,
                closed: false
            };
        },

        /* Props declaration */
        propTypes: {

        },

        closeNotification: function () {
            this.setState({
                closed: true
            });
        },

        render: function () {
            if (!this.state.quotaExceeded || this.state.closed) return null;

            return (
                React.createElement("div", {id: "issuesQuotaNotificationContainer"}, 
                    React.createElement("div", {className: "container", id: "issuesQuotaNotification"}, 
                    React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-md-12"}, 
                    React.createElement("div", null, 
                        React.createElement("img", {className: "issuesQuotaNotificationIcon", src: "/resources/img/cat.png"}), 
                        React.createElement("span", {className: "issuesQuotaNotificationClose", onClick: this.closeNotification}, 
                            React.createElement("i", {className: "fa fa-times", "aria-hidden": "true"})
                        ), 
                        React.createElement("div", {className: "issuesQuotaNotificationMessageText"}, 
                            React.createElement("h3", {className: "issuesQuotaNotificationMessageTitle"}, 
                                "Error quota exceeded"
                            ), 
                            React.createElement("p", {className: "issuesQuotaNotificationMessageContent"}, 
                                "Please, see ", React.createElement("a", {href: "/billing.html"}, "billing and usage"), " page"
                            )
                        ), 
                        React.createElement("div", {style: {clear: "both"}})
                    )
                    )
                    )
                    )
                )
            )
        },

        componentDidMount: function () {
            var self = this;
            User.getInfo(function (user) {
                self.setState({
                    quotaExceeded: user.issuesQuotaExceeded
                })
            });
        }

    });

    return IssuesQuotaExceededNotification;

});