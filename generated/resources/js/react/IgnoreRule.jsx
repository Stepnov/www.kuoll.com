define(["jquery", "react", "app/react/IgnoreRuleForm", "app/utils/api", "jquery.cookie"], function ($, React,IgnoreRuleForm, api) {

    var IgnoreRule = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                editing: false
            };
        },

        /* Props declaration */
        propTypes: {
            rule: React.PropTypes.object.isRequired,
            onRuleDeleted: React.PropTypes.func.isRequired,
            onRuleUpdated: React.PropTypes.func.isRequired
        },

        render: function () {
            var rule = this.props.rule;
            var selectorText = rule.selector;
            return (
                <div className="form-horizontal rule-wrapper">
                {this.state.editing ?
                    <IgnoreRuleForm onSubmit={this.updateRule} rule={rule} onDelete={this.deleteRule} />
                    :
                    <div className="">
                        <div className="form-group">
                            <label className="col-md-3 control-label">Domain</label>
                            <div  className="col-md-9 ">
                                <input className="form-control" disabled value={rule.domain} />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="col-md-3 control-label" htmlFor="css-selectror">CSS Selector</label>
                            <div  className="col-md-9 ">
                                <input className="form-control" disabled value={rule.selector} htmlId="css-selectror" />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-offset-3 col-sm-9">
                                <button className="btn btn-default" onClick={this.startEditing}><i className="fa fa-pencil-square-o"></i> Edit</button> &nbsp;
                                <button className="btn btn-danger" onClick={this.deleteRule}><i className="fa fa-times"></i> Remove</button>
                            </div>
                        </div>
                    </div>
                }
                </div>
            )
        },

        startEditing: function () {
            this.setState({
                editing: true
            })
        },

        deleteRule: function () {
            var self = this;
            api("deleteIgnoreRule", {
                userToken: $.cookie("userToken"),
                ruleKey: this.props.rule.key
            }, function () {
                self.props.onRuleDeleted(self.props.rule.key);
                self.setState({
                    editing: false
                });
            });
        },

        updateRule: function (domain, selector, key) {
            var self = this;
            api("updateIgnoreRule", {
                userToken: $.cookie("userToken"),
                key: key,
                domain: domain,
                selector: selector
            }, function () {
                self.props.onRuleUpdated(key, domain, selector);
                self.setState({
                    editing: false
                });
            });
        }

    });

    return IgnoreRule;

});