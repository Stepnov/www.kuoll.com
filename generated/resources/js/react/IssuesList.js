define(["jquery", "react", "app/react/IssueRow", "app/react/LinkedStateMixin", "app/utils/TipUtils", "app/utils/api",
        "ua-parser", "jquery.cookie"],
    function ($, React, IssueRow, LinkedStateMixin, TipUtils, api, UAParser) {

    var IssuesList = React.createClass({displayName: "IssuesList",

        mixins: [LinkedStateMixin],

        /* State declaration */
        getInitialState: function () {
            return {
                issues: [],
                userIdFilter: ""
            };
        },

        render: function () {
            var issues = this.getIssuesToShow();
            return (
                React.createElement("div", {className: "container"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-md-12"}, 
                            React.createElement("h1", {className: "issues-header"}, "User Issues"), 
                            React.createElement("p", null, 
                                "Find issues reports by user id", 
                                React.createElement("input", {className: "form-control user-id-fld", ref: "userIdFld", 
                                    valueLink: this.linkState("userIdFilter")})
                            )
                        )
                    ), 
                    React.createElement("div", {className: "row", ref: "issue"}, 
                        React.createElement("div", {className: "col-md-12"}, 
                            React.createElement("table", {className: "table table-striped"}, 
                                React.createElement("thead", null, 
                                    React.createElement("tr", null, 
                                        React.createElement("th", null, "Record"), 
                                        React.createElement("th", null, "User id"), 
                                        React.createElement("th", null, "Website"), 
                                        React.createElement("th", null, "Browser"), 
                                        React.createElement("th", null, 
                                            React.createElement("span", {title: "Time user reported the issue"}, "Time")
                                        ), 
                                        React.createElement("th", null, "Type"), 
                                        React.createElement("th", null, 
                                            React.createElement("span", {title: "Operating System"}, "OS")
                                        ), 
                                        React.createElement("th", {className: "description-column"}, 
                                            React.createElement("span", {title: "User message or javascript error stack"}, "Description")
                                        ), 
                                        React.createElement("th", null, "Notes"), 
                                        React.createElement("th", null, "Delete"), 
                                        React.createElement("th", null, "Hide")
                                    )
                                ), 
                                React.createElement("tbody", null, 
                                issues.map(function (issue) {
                                    return (
                                        React.createElement(IssueRow, {issue: issue})
                                    )
                                })
                                )
                            ), 
                            this.state.issues.length == 0 ?
                                React.createElement("div", {className: "alert alert-info col-md-offset-2 col-md-8", id: "no-issues-alert"}, 
                                    "Looks like there is no records . You can ", React.createElement("a", {href: "/quick-start.html"}, 
                                    "install Kuoll script"), " or look at the ", React.createElement("a", {
                                    href: "http://jsbin.com/nusehuwisa/edit?html,output"}, " JSBin sandbox "), " demo first."
                                )
                            : null
                        )
                    )
                )
            )
        },

        getIssuesToShow: function () {
            var userId = this.state.userIdFilter;
            return userId ? this.state.issues.filter(function (issue) {
                return (issue.externalUserId.indexOf(userId) !== -1);
            }) : this.state.issues;
        },

        componentDidMount: function () {
            var self = this;
            var userToken = $.cookie("userToken");
            if (!userToken) {
                document.location = "/login.html";
                return;
            }

            api("getIssues", {
                userToken: userToken
            }, function (resp) {
                var issues = resp.issues;
                issues.forEach(function (issue) {
                    issue.userAgent = new UAParser(issue.userAgent).getResult();
                });
                self.setState({
                    issues: resp.issues
                });
            });
        },

        componentDidUpdate: function () {
            TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.issue)).find('*[title]:not([data-hasqtip])'));
        }

    });

    return IssuesList;

});