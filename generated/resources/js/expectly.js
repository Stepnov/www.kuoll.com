$("#startTestsBtn").on("click", function () {
    function assert(val) {
        if (!val) {
            alert("Assertion failed");
            throw new Error("Assertion failed");
        }
    }

    assert(expectly);
    TestRunner.run();
});

var TestRunner = (function () {
    var testCases = [
        {
            timeout: 500,
            test: function() {
                expectly.expect("saveSequent", {sequentType: "mutation", domSnapshot: {}});

                $("#logToPageBtn").click();
            }
        }, {
            timeout: 300,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "scroll"});

                window.scrollTo(100, 100);
            }
        }, {
            timeout: 1000,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "open", sequentClass: "XMLHttpRequest"});
                expectly.expect("saveSequent", {sequentSubtype: "send", sequentClass: "XMLHttpRequest"});
                expectly.expect("saveSequent", {sequentSubtype: "load", sequentClass: "XMLHttpRequestProgressEvent"});
                expectly.expect("saveSequent", {sequentType: "mutation"});

                $("#ajaxBtn").click();
            }
        }, {
            timeout: 300,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "error"});

                window.setTimeout(function() {
                    throw new Error();
                }, 0);
            }
        }, {
            timeout: 200,
            test: function() {
                expectly.expect("saveSequent", {sequentClass: "Console", sequentSubtype: "log"});

                console.log("Test console sequent");
            }
        }, {
            timeout: 200,
            test: function() {
                expectly.expect("saveSequent", {sequentClass: "History", sequentSubtype: "pushState"});

                history.pushState("", "", "");
            }
        }, {
            timeout: 500,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "setInterval", sequentClass: "Window"});
                expectly.expect("saveSequent", {sequentSubtype: "setInterval", sequentClass: "[setInterval-callback]"});
                expectly.expect("saveSequent", {sequentSubtype: "clearInterval", sequentClass: "Window"});

                var interval = window.setInterval(function() {
                    window.clearInterval(interval);
                }, 100);
            }
        }, {
            timeout: 300,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "visibilitychange"});

                var event = new Event("visibilitychange");

                document.dispatchEvent(event);
            }
        }, {
            timeout: 300,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "beforeunload"});

                var event = new Event("beforeunload");
                window.dispatchEvent(event);
            }
        }, {
            timeout: 300,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "keypress", sequentClass: "KeyboardEvent"});
                expectly.expect("saveSequent", {sequentClass: "Console", sequentSubtype: "log"});

                var event = new KeyboardEvent("keypress", {bubbles: true, key : "Q"});
                document.getElementById("keyboardTest").dispatchEvent(event);
            }
        }, {
            timeout: 300,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "focus", sequentClass: "Event"});
                expectly.expect("saveSequent", {sequentClass: "Console", sequentSubtype: "log"});
                expectly.expect("saveSequent", {sequentSubtype: "blur", sequentClass: "Event"});
                expectly.expect("saveSequent", {sequentClass: "Console", sequentSubtype: "log"});

                var focusEvent = new Event("focus");
                var blurEvent = new Event("blur");
                var focusTest = document.getElementById("focusTest");
                focusTest.dispatchEvent(focusEvent);
                focusTest.dispatchEvent(blurEvent);
            }
        }, {
            timeout: 300,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "resize", sequentClass: "Event"});

                var event = new Event("resize");
                window.dispatchEvent(event);
            }
        }, {
            timeout: 300,
            test: function() {
                expectly.expect("saveSequent", {sequentSubtype: "click", sequentClass: "MouseEvent"});
                expectly.expect("saveSequent", {sequentClass: "Console", sequentSubtype: "log"});

                var event = new MouseEvent("click");
                document.getElementById("mouseTest").dispatchEvent(event);
            }
        }
    ];

    var currentTestIndex = 0;
    var runTest = function() {
        if (currentTestIndex == testCases.length) {
            currentTestIndex = 0;
            expectly.unignore("saveSequent", {sequentSubtype: "setTimeout", sequentClass: "Window"});
            expectly.unignore("saveSequent", {sequentSubtype: "setTimeout", sequentClass: "[setTimeout-callback]"});
            expectly.finishTesting();
            expectly.turn(false);
            return;
        }
        var testCase = testCases[currentTestIndex];

        try {
            testCase.test();
        } catch (Error) {
            console.error(Error);
        }
        currentTestIndex++;

        window.setTimeout(runTest, testCase.timeout);
    };

    return {
        run: function() {
            expectly.ignore("saveSequent", {sequentSubtype: "setTimeout", sequentClass: "Window"});
            expectly.ignore("saveSequent", {sequentSubtype: "setTimeout", sequentClass: "[setTimeout-callback]"});
            expectly.turn(true);
            expectly.expect("saveSequent", {sequentSubtype: "click", sequentClass: "MouseEvent"});
            runTest();
        }
    }
})();