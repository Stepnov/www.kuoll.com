define(function () {

    function inSearch(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(location.search);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function inHash(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\#\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(location.hash);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function getParameterByName(name) {
        return inSearch(name) || inHash(name);
    }

    return {
        getParameterByName: getParameterByName
    };

});
