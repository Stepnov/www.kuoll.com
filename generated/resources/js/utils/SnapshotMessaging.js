define(function () {

    var highlightedPaths = {};

    function highlight(selector) {
        highlightedPaths[selector] = true;
        sendMessage({
            "action": "highlight",
            "selector": selector
        });
    }

    function unhighlight(selector) {
        delete highlightedPaths[selector];
        var data = {
            "action": "unhighlight"
        };
        if (selector != undefined) {
            data.selector = selector;
        }

        sendMessage(data);
    }

    function isHighlighted(selector) {
        return (selector in highlightedPaths);
    }

    function scroll() {
        var args = arguments;
        if (typeof args[0] === "string" && typeof args[1] === "undefined") {
            // scroll to HTML element
            sendMessage({
                action: "scrollToElement",
                path: args[0]
            })
        } else if (typeof args[0] === "object" && typeof args[1] === "undefined") {
            // scroll to cords
            sendMessage({
                "action": "scroll",
                "left": args[0].x,
                "top": args[0].y
            });
        } else if (typeof args[0] === "number" && typeof args[1] === "number") {
            // scroll to cords
            sendMessage({
                "action": "scroll",
                "left": args[0],
                "top": args[1]
            });
        } else {
            throw new Error("Method is not applicable for such types of arguments");
        }
    }

    function setSnapshot(baseSnapshot, mutations, sequent) {
        sendMessage({
            action: "setSnapshot",
            baseSnapshot: baseSnapshot,
            mutations: mutations,
            sequent: sequent
        });
    }

    function nextSnapshot(mutation, sequent) {
        sendMessage({
            action: "nextSnapshot",
            mutation: mutation,
            sequent: sequent
        });
    }

    function createMouseTrail(mouseTrail) {
        sendMessage({
            action: "createMouseTrail",
            mouseTrail: mouseTrail
        });
    }

    function sendMessage(data) {
        document.getElementById("snapshot").contentWindow.postMessage(JSON.stringify(data), "*");
    }

    var callbacks = {};
    window.addEventListener("message", function (event) {
        var kuollOrigins = ["http://localhost:9000", "http://www.kuoll.com", "https://www.kuoll.com", "http://app.kuoll.com", "https://app.kuoll.com"];
        if (typeof event.data === "string" && kuollOrigins.indexOf(event.origin) !== -1) {
            var data = JSON.parse(event.data);
            if (data.action == "callCallback") {
                if (callbacks[data.token]) {
                    callbacks[data.token]();
                    delete callbacks[data.token];
                }
            }
        }
    });

    var SnapshotMessaging = {
        highlight: highlight,
        unhighlight: unhighlight,
        isHighlighted: isHighlighted,
        scroll: scroll,
        setSnapshot: setSnapshot,
        nextSnapshot: nextSnapshot,
        createMouseTrail: createMouseTrail
    };

    return SnapshotMessaging;

});
