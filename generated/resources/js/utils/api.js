define(["jquery"], function ($) {

    return function (apiPath, data, successCallback, failCallback, followRedirect) {
        $.ajax({
            url: config.apiServer + apiPath,
            type: "POST",
            contentType: "application/json",
            data: data ? JSON.stringify(data) : null
        }).done(function (data, textStatus, jqXHR) {
            if (data.hasOwnProperty("errorMsg")) {
                console.error(data);
                if (failCallback) {
                    failCallback(data.errorMsg, textStatus, jqXHR);
                }
            } else {
                if (jqXHR.status === 302) {  // moved temporarily
                    if (followRedirect !== false) {
                        window.location.href = "/login.html";
                    } else {
                        if (failCallback) {
                            failCallback(data.errorMsg, textStatus, jqXHR);
                        }
                    }
                } else if (jqXHR.status === 200) {
                    if (successCallback) {
                        successCallback(data);
                    }
                }
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 302) {  // moved temporarily
                if (followRedirect !== false) {
                    window.location.href = "/login.html";
                } else {
                    if (failCallback) {
                        failCallback(errorThrown, textStatus, jqXHR);
                    }
                }
            } else {
                if (jqXHR.responseJSON && jqXHR.responseJSON.errorMsg) {
                    errorThrown = jqXHR.responseJSON.errorMsg;
                }
                if (failCallback) {
                    failCallback(errorThrown, textStatus, jqXHR);
                } else {
                    console.warn("Unhandled failed request", jqXHR, textStatus, errorThrown);
                }
            }
        });
    };

});