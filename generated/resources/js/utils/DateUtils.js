define(function () {
    var oneSecondMillis = 1000;
    var fiveSecondsMillis = 5000;
    var oneMinuteMillis = 60000;
    var oneHourMillis = 3600000;
    var sixHoursMillis = 21600000;
    var oneDayMillis = 86400000;
    var monthShortName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    function formatDate(timestamp) {
        if (timestamp == 0 || isNaN(timestamp)) {
            return "";
        }

        var date = new Date(timestamp);
        var nowDate = new Date();
        var now = nowDate.getTime();
        var month, day, year, hours, minutes, seconds;

        if (now < timestamp + fiveSecondsMillis) {
            return "Just now";
        } else if (now < timestamp + oneMinuteMillis) {
            seconds = Math.floor((now - timestamp) / oneSecondMillis);
            return seconds + " sec ago";
        } else if (now < timestamp + oneHourMillis) {
            minutes = Math.floor((now - timestamp) / oneMinuteMillis);
            return minutes + " min ago";
        } else if (now < timestamp + sixHoursMillis) {
            hours = Math.floor((now - timestamp) / oneHourMillis);
            return hours + " h ago";
        } else if (nowDate.getYear() == date.getYear() && nowDate.getMonth() == date.getMonth() && nowDate.getDate() == date.getDate()) {
            hours = withLeadingZero(date.getHours());
            minutes = withLeadingZero(date.getMinutes());
            return "Today, " + hours + ":" + minutes;
        } else if (nowDate.getYear() == date.getYear() && nowDate.getMonth() == date.getMonth() && nowDate.getDate() == date.getDate() + 1) {
            hours = withLeadingZero(date.getHours());
            minutes = withLeadingZero(date.getMinutes());
            return "Yday, " + hours + ":" + minutes;
        } else if (nowDate.getYear() == date.getYear()) {
            day = withLeadingZero(date.getDate());
            month = date.getMonth();
            hours = withLeadingZero(date.getHours());
            minutes = withLeadingZero(date.getMinutes());
            return monthShortName[month] + " " + day + ",  " + hours + ":" + minutes;
        } else {
            day = withLeadingZero(date.getDate());
            month = date.getMonth();
            year = withLeadingZero(date.getFullYear());
            hours = withLeadingZero(date.getHours());
            minutes = withLeadingZero(date.getMinutes());
            return monthShortName[month] + " " + day + ", " + year + " " + hours + ":" + minutes;
        }
    }

    function formatDuration(duration) {
        var hours, minutes, seconds;
        if (duration == 0 || isNaN(duration)) {
            return {
                short: "",
                long: ""
            };
        } else if (duration < oneMinuteMillis) {
            seconds = Math.floor(duration / oneSecondMillis);
            return {
                short: seconds + "s",
                long: formatTimeUnit(seconds, "second")
            };
        } else if (duration < oneHourMillis) {
            seconds = Math.floor(duration % oneMinuteMillis / oneSecondMillis);
            minutes = Math.floor(duration / oneMinuteMillis);
            return {
                short: minutes + "m " + seconds + "s",
                long: formatTimeUnit(minutes, "minute") + formatTimeUnit(seconds, "second", ", ")
            };
        } else {
            hours = Math.floor(duration / oneHourMillis);
            minutes = Math.floor(duration % oneHourMillis / oneMinuteMillis);
            seconds = Math.floor(duration % oneMinuteMillis / oneSecondMillis);
            return {
                short: hours + "h " + minutes + "m",
                long: formatTimeUnit(hours, "hour") + formatTimeUnit(minutes, "minute", ", ") + formatTimeUnit(seconds, "second", ", ")
            };
        }
    }

    function timeBetween(from, to) {
        var diff = to - from;
        var millis = diff % 1000;
        var seconds = Math.floor(diff / 1000) % 60;
        var minutes = Math.floor(diff / oneMinuteMillis) % 60;
        var hours = Math.floor(diff / oneHourMillis) % 24;
        var days = Math.floor(diff / oneDayMillis);

        return shortUnitFormat(days, "d") + shortUnitFormat(hours, "h", " ") + shortUnitFormat(minutes, "m", " ")
            + shortUnitFormat(seconds, "s", " ") + shortUnitFormat(millis, "ms", " ");
    }

    function shortUnitFormat(number, unitName, leadingString) {
        if (number == 0) {
            return "";
        } else {
            return (leadingString != null ? leadingString : "") + number + unitName;
        }
    }

    function formatTimeUnit(number, unitName, leadingString) {
        if (number == 0) {
            return "";
        }
        return (leadingString != null ? leadingString : "") + number + " " + (number > 1 ? unitName + "s" : unitName);
    }

    function withLeadingZero(number) {
        return number <= 9 ? "0" + number : number;
    }

    var DateUtils = {
        formatDate: formatDate,
        formatDuration: formatDuration,
        timeBetween: timeBetween
    };

    return DateUtils;

});
