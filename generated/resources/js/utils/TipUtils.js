define(["jquery", "jquery.qtip"], function ($) {

    function makeSimpleTip($node, text, ready, style) {
        $node.qtip({
            content: {
                text: text
            },
            style: {
                classes: style || "qtip-tipsy"
            },
            position: {
                my: "top right",
                at: "bottom right",
                viewport: true
            },
            show: {
                ready: !!ready
            }
        });
    }

    var TipUtils = {
        makeSimpleTip: makeSimpleTip
    };

    return TipUtils;

});
