define(["jquery", "app/utils/api"], function ($, api) {

    var SEQUENTS_INITIALLY_LOADED = 400;
    var SEQUENTS_LOADED_PER_TIME = 150;

    var loading = false;

    var frameLoadingStatus = {};

    var recordCode;
    var onSequentsInitiallyLoaded;
    var onNewSequentsLoaded;
    var activeFrame;
    
    var getTimeline;

    var startFrameNum, startSequentNum;
    var endFrameNum, endSequentNum;

    var $scrollbar;

    function init(_recordCode, _initFrame, initialSequentNum, issue, _onSequentsInitiallyLoaded, _onNewSequentsLoaded, _getTimeline) {
        recordCode = _recordCode;
        onSequentsInitiallyLoaded = _onSequentsInitiallyLoaded;
        getTimeline = _getTimeline;
        onNewSequentsLoaded = _onNewSequentsLoaded;
        activeFrame = _initFrame;

        if (issue) {
            startFrameNum = issue.startFrameNum;
            startSequentNum = issue.startSequentNum;
            endFrameNum = issue.endFrameNum;
            endSequentNum = issue.endSequentNum;
        }

        $scrollbar = $("#sequentLineScroll");
        $scrollbar.on("scroll", onTimelineScrolled);

        loadSequentsInitially(initialSequentNum);
    }
    
    function getSequentLine() {
        return $(getTimeline().getSequentLine().getDOMNode());
    }

    function getFrameLoadingStatus(frameNum) {
        var num = frameNum || activeFrame.frameNum;
        if (!frameLoadingStatus[num]) {
            frameLoadingStatus[num] = {
                isSequentsBeforeLeft: true, // is current frame contains more sequents before first loaded sequent
                isSequentsAfterLeft: true  // is current frame contains more sequents after last loaded sequent
            };
        }
        return frameLoadingStatus[num];
    }

    /**
     * Actions with timeline scrollbar will work incorrectly if timeline is hidden. This method will show timeline,
     * execute callback and hide timeline (if necessary)
     * @param callback
     */
    function requiresShownTimeline(callback) {
        callback();
    }

    function scrollTimeline(left) {
        requiresShownTimeline(function () {
            $scrollbar.scrollLeft(typeof left !== "undefined" ? left : getSequentLine()[0].scrollWidth / 2);
        });
    }

    /**
     * @param params
     * @param {object} [params.frame=activeFrame] frame which sequents to load belongs to
     * @param {number} [params.startSequentNum=1] number of sequent from which loading will be started.
     *      Sequent with such sequentNum <b>will not</b> be loaded, but next one will.
     * @param {number} [params.limit] amount of sequents to load
     * @param {number} [params.endSequentNum] number of last sequent to load. Only one of limit and endSequentNum
     *      may be specified
     * @param {function} [params.onLoadCallback] callback which will be called when sequents will be loaded
     * @param {boolean} [params.preliminary=false] if true, PlayContainer.onNewSequentsLoaded won't be called
     */
    function loadSequents(params) {
        function onLoad(data) {
            $("#loading-spinner").css({visibility: "hidden"});
            if (data.errorMsg) {
                console.error(data.errorMsg);
                loading = false;
                return;
            }
            var sequentsLoaded = data.sequents.length;
            if (sequentsLoaded === 0) {
                getFrameLoadingStatus().isSequentsAfterLeft = false;
                frame.sequents = frame.sequents || [];
                if (params.onLoadCallback) {
                    params.onLoadCallback(data.sequents.length);
                }
                loading = false;
                return;
            }

            var initialLoading;
            if (!frame.sequents) {
                initialLoading = true;
                frame.sequents = data.sequents;
            } else {
                initialLoading = false;
                frame.sequents = frame.sequents.concat(data.sequents);
            }

            var loadingStatus = getFrameLoadingStatus();
            if (sequentsLoaded < params.limit) {
                loadingStatus.isSequentsAfterLeft = false;
            }

            if (initialLoading) {
                if (!params.preliminary) {
                    onSequentsInitiallyLoaded();
                }
                loading = false;
                if (params.onLoadCallback) {
                    params.onLoadCallback(data.sequents.length);
                }
            } else {
                requiresShownTimeline(function () {
                    onNewSequentsLoaded(function () {
                        if (params.onLoadCallback) {
                            params.onLoadCallback(data.sequents.length);
                        }
                        loading = false;
                    });
                });
            }
        }

        if (params.limit && params.endSequentNum) {
            throw new Error("Only one of limit and endSequentNum may be specified");
        }
        if (params.endSequentNum && params.endSequentNum < params.startSequentNum) {
            throw new Error("startSequentNum and endSequentNum params are inconsistent");
        }

        var frame = params.frame || activeFrame;

        loading = true;
        $("#loading-spinner").css({visibility: "visible"});

        var sequentNum = params.startSequentNum;
        var isFirstFrame = frame.frameNum === startFrameNum;
        if (isFirstFrame && (!sequentNum || sequentNum < startSequentNum)) {
            sequentNum = startSequentNum;
        }
        if (!sequentNum || sequentNum === 1) {
            sequentNum = 0;
        }

        var lastSequentNum = params.endSequentNum;

        var limit = params.limit;
        if (!lastSequentNum || sequentNum === lastSequentNum) {
            lastSequentNum = 0;
            limit = SEQUENTS_INITIALLY_LOADED;
        }

        api("getSequents", {
            recordCode: recordCode,
            frameNum: frame.frameNum,
            sequentNum: sequentNum,
            limit: limit || 0,
            endSequentNum: lastSequentNum
        }, onLoad);
    }

    function loadSequentsInitially(sequentNum, frame, callback, preliminary) {
        var frameToLoad = frame || activeFrame;
        var params = {
            frame: frameToLoad,
            onLoadCallback: function (count) {
                var sequentsTotal = frameToLoad.sequents.length;
                if (sequentsTotal < SEQUENTS_INITIALLY_LOADED && getFrameLoadingStatus(frameToLoad.frameNum).isSequentsAfterLeft) {
                    loadSequents({
                        frame: frameToLoad,
                        onLoadCallback: callback,
                        startSequentNum: frameToLoad.sequents[sequentsTotal - 1].sequentNum,
                        limit: (!sequentNum || sequentNum <= startSequentNum) ? SEQUENTS_INITIALLY_LOADED - sequentsTotal : null,
                        endSequentNum: (!sequentNum || sequentNum <= startSequentNum) ? null : sequentNum
                    });
                }

                if (callback) {
                    callback(count);
                }
            },
            preliminary: preliminary
        };
        if (!sequentNum || sequentNum <= SEQUENTS_INITIALLY_LOADED) {
            params.limit = SEQUENTS_INITIALLY_LOADED;
            loadSequents(params);
        } else {
            params.endSequentNum = sequentNum;
            loadSequents(params);
        }
    }

    function loadSequentsTo(targetSequentNum, frame, callback) {
        if (!frame.sequents || frame.sequents.length === 0) {
            loadSequentsInitially(targetSequentNum, frame, callback);
            return;
        }

        if (!getFrameLoadingStatus(frame.frameNum).isSequentsAfterLeft) {
            if (callback) {
                callback();
            }
            return;
        }

        var lastSequentNum = frame.sequents[frame.sequents.length - 1].sequentNum;
        if (targetSequentNum > lastSequentNum) {
            loadSequents({
                frame: frame,
                startSequentNum: lastSequentNum,
                endSequentNum: targetSequentNum,
                onLoadCallback: callback
            });
        } else {
            // target sequent has already been loaded, do nothing
            if (callback) {
                callback();
            }
        }
    }

    function loadSomeSequent(startSequentNum, limit, frame, callback, preliminary, force) {
        if (limit <= 0) {
            throw new Error("limit must be positive");
        }

        if (!startSequentNum) {
            startSequentNum = 0;
        }

        if (!frame.sequents || frame.sequents.length === 0) {
            loadSequentsInitially(0, frame, callback, preliminary);
            return;
        }

        if (!force && !getFrameLoadingStatus(frame.frameNum).isSequentsAfterLeft) {
            if (callback) {
                callback();
            }
            return;
        }

        if (frame.totalSequents && frame.sequents && frame.totalSequents === frame.sequents.length) {
            getFrameLoadingStatus(frame.frameNum).isSequentsAfterLeft = false;
            if (callback) {
                callback();
            }
            return;
        }

        loadSequents({
            frame: frame,
            startSequentNum: startSequentNum,
            limit: limit,
            onLoadCallback: callback
        });
    }

    function isScrollCloseToRight(left) {
        var width = getSequentLine()[0].scrollWidth;
        return width - left < 400 || left / width > 0.9;
    }

    function getSequentOffsetLeft($sequent) {
        var sequents = getSequentLine().find("div.sequentDiv");
        var sequent = $sequent[0];
        for (var i = 0; i < sequents.length; ++i) {
            if (sequents[i] == sequent) {
                break;
            }
        }
        return i / sequents.length * getSequentLine()[0].scrollWidth - $scrollbar.width() / 2;
    }

    function onTimelineScrolled() {
        if (loading) {
            return;
        }

        requiresShownTimeline(function () {
            var loadingStatus = getFrameLoadingStatus();
            var scrollStartPosition = $scrollbar.scrollLeft();
            var scrollEndPosition = scrollStartPosition + $scrollbar.width();
            if (isScrollCloseToRight(scrollEndPosition)) {
                if (!loadingStatus.isSequentsAfterLeft) return;

                var sequents = activeFrame.sequents;
                var lastSequentNum = sequents[sequents.length - 1].sequentNum;
                loadSequents({
                    startSequentNum: lastSequentNum,
                    limit: SEQUENTS_LOADED_PER_TIME
                });
            }
        });
    }

    function updateActiveFrame(params) {
        activeFrame = params.frame;
        if (!activeFrame.sequents || activeFrame.sequents.length == 0) {
            if (params.sequentNum) {
                loadSequentsInitially(params.sequentNum, activeFrame, params.callback);
            } else {
                loadSequentsInitially(null, activeFrame, params.callback);
            }
        } else {
            scrollTimeline();
            if (params.callback) {
                params.callback();
            }
        }
    }

    function scrollTo(sequentNum) {
        requiresShownTimeline(function () {
            var $sequentDiv = sequentNum ? getSequentLine().find("div#sequent-" + sequentNum) : getSequentLine().find(".sequent-highlighting");
            var scrollPosition = getSequentOffsetLeft($sequentDiv);
            $scrollbar.scrollLeft(scrollPosition);
        });
    }

    function isAnySequentsLeft(frameNum) {
        var loadingStatus = getFrameLoadingStatus(frameNum);
        return loadingStatus.isSequentsAfterLeft;
    }

    function isLoading() {
        return loading;
    }

    var SequentLoader = {
        init: init,
        loadSequentsTo: loadSequentsTo,
        loadSequents: loadSomeSequent,
        scrollTo: scrollTo,
        updateActiveFrame: updateActiveFrame,
        isAnySequentsLeft: isAnySequentsLeft,
        isLoading: isLoading
    };

    return SequentLoader;

});
