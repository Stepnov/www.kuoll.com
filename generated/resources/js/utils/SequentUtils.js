define([], function () {

    function getSequentIndexByNum(sequentNum, frame) {
        if (!frame.sequents) return -1;
        if (sequentNum == 0) return 0;
        var sequent = frame.sequents[sequentNum - 1];
        if (sequent && sequent.sequentNum == sequentNum) {
            return sequentNum - 1;
        } else {
            var less = sequent != null && sequent.sequentNum < sequentNum;
            var baseIndex = Math.min(sequentNum, frame.sequents.length) - 1;
            for (var i = 0; ; ++i) {
                var index = baseIndex + (less ? i : -i);
                sequent = frame.sequents[index];
                if (sequent != null && sequent.sequentNum == sequentNum) {
                    return index;
                } else if ((less && sequent != null && sequent.sequentNum > sequentNum)
                    || (!less && sequent != null && sequent.sequentNum < sequentNum)
                    || i > frame.sequents.length) {
                    return -1;
                }
            }
        }
    }

    function getSequentByNum(sequentNum, frame) {
        var index = getSequentIndexByNum(sequentNum, frame);
        return index == -1 ? null : frame.sequents[index];
    }

    function getFrame(frameNum, frames) {
        var length = frames.length;
        for (var i = 0; i < length; ++i) {
            var frame = frames[i];
            if (frame.frameNum === frameNum) {
                return frame;
            }
        }
        return null;
    }

    function iterateReverseOrder(sequentNum, frame, callback) {
        var index = getSequentIndexByNum(sequentNum, frame);
        if (index) {
            var result = true;
            for (var i = index; i >= 0 && result; --i) {
                result = callback(frame.sequents[i]);
            }
        } else {
            return null;
        }
    }

    function getPageOffset(sequent, frame) {
        var xOffset, yOffset;
        var i = sequent.sequentNum - 1,
            prevScrollSeq = sequent;
        while ("scroll" === prevScrollSeq.sequentSubtype && i > 0) {
            prevScrollSeq = SequentUtils.getSequentByNum(i, frame);
            if (prevScrollSeq && "scroll" === prevScrollSeq.sequentSubtype) {
                break;
            }
            --i;
        }

        if (prevScrollSeq && "scroll" === prevScrollSeq.sequentSubtype) {
            SequentUtils.parseRawData(prevScrollSeq);
            xOffset = prevScrollSeq.rawData.pageXOffset;
            yOffset = prevScrollSeq.rawData.pageYOffset;
        } else {
            xOffset = yOffset = 0;
        }
        return {x: xOffset, y: yOffset};
    }

    function parseRawData(sequent) {
        if (typeof(sequent.rawData) === "string") {
            try {
                sequent.rawData = JSON.parse(sequent.rawData);
                return true;
            } catch (e) {
                throw new Error("Sequent has invalid rawData. sequentNum: "
                + sequent.sequentNum + "; rawData: " + sequent.rawData);
            }
        } else if (typeof(sequent.rawData) === "object") {
            return true;
        } else {
            return false;
        }
    }

    function isBeforeIssue(sequent, issue) {
        return issue && sequent.frameNum < issue.startFrameNum;
    }

    function isAfterIssue(sequent, issue) {
        return issue && sequent.frameNum > issue.endFrameNum;
    }

    function parseMutations(sequent) {
        if (typeof sequent.mutations == "string") {
            try {
                sequent.mutations = JSON.parse(sequent.mutations);
            } catch (e) {
                console.warn("Could not parse sequent's mutations");
                sequent.mutations = {};
            }
        } else if (typeof sequent.mutations != "object") {
            console.error("Unexpected content in sequent.mutations.", sequent.mutations);
            sequent.mutations = {};
        }

        return sequent.mutations;
    }

    var SequentUtils = {
        getSequentByNum: getSequentByNum,
        getFrame: getFrame,
        getPageOffset: getPageOffset,
        parseRawData: parseRawData,
        iterateReverseOrder: iterateReverseOrder,

        isBeforeIssue: isBeforeIssue,
        isAfterIssue: isAfterIssue,

        parseMutations: parseMutations
    };

    return SequentUtils;

});
