define(["jquery", "app/utils/MessageUtils", "app/utils/api", "jquery.cookie"], function ($, MessageUtils, api) {

    function authenticateUser(subdomain, title, comment, recordLink, success, error) {
        var url = "https://" + subdomain + ".zendesk.com/oauth/authorizations/new?" +
                "&response_type=code" +
                "&redirect_uri=" + encodeURIComponent("http://localhost:8080/zendesk/handle_user_decision") +
                "&client_id=kuoll" +
                "&scope=" + encodeURIComponent("read write");
        window.open(url, "_blank", "width=500,height=500");
        MessageUtils.onMessage("zendesk-authentication-finished", function () {
            createTicket(title, comment, recordLink, success, error);
        });
    }

    function createTicket(title, comment, recordLink, success, error) {
        api("zendesk/create_ticket", {
            userToken: $.cookie("userToken"),
            title: title,
            comment: comment,
            recordLink: recordLink
        }, function (resp) {
            if (resp.done) {
                success();
            } else if (resp.subdomain) {
                authenticateUser(resp.subdomain, title, comment, recordLink, success, error);
            } else {
                window.open("/settings.html#tab-zendesk");
            }
        }, error);
    }

    return {
        createTicket: createTicket
    };

});