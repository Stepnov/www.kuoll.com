define(["jquery", "app/utils/api", "app/User", "app/utils/UrlUtils", "analytics/segment", "jquery.cookie"], function ($, api, User, UrlUtils, segment) {

    var onSuccessCallback, onLogoutCallback, onFailureCallback;

    var isAlreadySignedInShown = false;

    function renderAlreadySignedIn() {
        $(".hidden-logged-in").hide();
        $(".visible-logged-in").show();

        var $alreadySignedInWrapper = $(".already-signed-in-wrapper");

        if (isAlreadySignedInShown) {
            return;
        }

        $alreadySignedInWrapper.show();
        isAlreadySignedInShown = true;

        var $googleSigninWrapper = $(".google-signin-wrapper");
        $googleSigninWrapper.hide();

        if ($alreadySignedInWrapper) {
            $("<p></p>", { // TODO @Vlad move it to html file, change style display when necessary
                text: "You are already signed in"
            }).appendTo($alreadySignedInWrapper);
            var $buttonsWrapper = $("<div></div>", {
                "class": "buttons-wrapper"
            });
            $("<button></button>", {
                html: "<i class='fa fa-sign-out'></i> Log out",
                "class": "btn btn-default",
                on: {
                    click: function () {
                        $alreadySignedInWrapper.hide();
                        $googleSigninWrapper.show();
                        isAlreadySignedInShown = false;
                        onLogout();
                    }
                }
            }).appendTo($buttonsWrapper);
            $("<button></button>", {
                html: "Dashboard &rarr;",
                "class": "btn btn-primary",
                on: {
                    click: function (e) {
                        document.location = "/issues-dashboard.html#onboardOptional";
                        e.preventDefault();
                    }
                }
            }).appendTo($buttonsWrapper);
            $alreadySignedInWrapper.append($buttonsWrapper);
        }
        if (onSuccessCallback) {
            onSuccessCallback();
        }
    }

    function onSuccess(googleUser) {
        var userToken = googleUser.getAuthResponse().id_token;
        if ($.cookie("userToken") !== userToken) {
            api("google-signin", {
                userToken: userToken,
                planName: $.cookie("kuoll-preselected-plan")
            }, function (resp) {
                $.cookie("userToken", userToken, {domain: window.config.cookieDomain, path: "/"});

                var redirectUrl = UrlUtils.getParameterByName("redirectTo");

                segment.identify(resp);

                if (redirectUrl) {
                    document.location = redirectUrl;
                } else if (resp.alreadyRegistered) {
                    document.location = "/issues-dashboard.html#onboardOptional";
                } else if (resp.subscriptionCreated) {
                    $.removeCookie("kuoll-preselected-plan");
                    document.location = "/billing.html";
                } else {
                    document.location = "/issues-dashboard.html#onboardInstall";
                }
            }, onFailureCallback);
        } else {
            renderAlreadySignedIn();
        }
    }

    function onLogout() {
        $.removeCookie("userToken", {domain: config.cookieDomain, path: "/"});
        if (window.gapi && window.gapi.auth2) {
            gapi.auth2.getAuthInstance().signOut();
        }
        if (onLogoutCallback) {
            onLogoutCallback();
        }
    }

    function onFailure() {
        console.error(arguments);
        if (onFailureCallback) {
            onFailureCallback(arguments);
        }
    }

    function renderButton() {
        if ($("#google-signin-btn").length > 0) {
            gapi.signin2.render('google-signin-btn', {
                scope: "profile email",
                width: 330,
                longtitle: true,
                theme: "dark",
                onsuccess: onSuccess,
                onfailure: onFailure
            });
        }
    }


    function init(params) {
        if (window.gapi === null) {
            return false;
        }

        requirejs(['google-signin'], function() {
            if (params) {
                onSuccessCallback = params.onSuccess;
                onFailureCallback = params.onFailure;
                onLogoutCallback = params.onLogout;
            }

            if (window.gapi && window.gapi.load) {
                gapi.load("auth2", function () {
                    if (!gapi.auth2.getAuthInstance()) {
                        gapi.auth2.init({
                            scope: "profile email",
                            client_id: "646883123421-nilhd9eccgjcg9gsf56dfoa38hekrl0q.apps.googleusercontent.com"
                        });
                    }
                    renderButton();
                });
            }

            if (params && params.checkUserToken) {
                User.getInfo(function () {
                    renderAlreadySignedIn();
                }, null, false);
            }
        }, function() {
            window.gapi = null;
        });
    }

    return {
        init: init,
        logout: onLogout
    };

});