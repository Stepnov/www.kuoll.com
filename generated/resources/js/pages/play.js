define(["jquery", "react", "app/react/play/PlayContainer"], function ($, React,PlayContainer) {
    $(function () {
        React.render(React.createElement(PlayContainer), $("#playContainer")[0]);
    });
});