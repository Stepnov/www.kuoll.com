define(
    ["jquery", "react", "app/react/common/Header", "app/react/IgnoreRulesTable",
        "app/react/IgnoreUrlList",
        "app/utils/api", "app/User", "bootstrap", "jquery.cookie", "jquery.qtip"],
    function ($, React, Header, IgnoreRulesTable, IgnoreUrlList, api, User) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            // React.render(React.createElement(IgnoreRulesTable), $("#rules-table")[0]);
            // React.render(React.createElement(IgnoreUrlList), $("#tab-excluded-urls")[0]);

            var userToken = $.cookie("userToken");
            var $emailFld = $("#email-fld");
            var $passwordFld = $("#password-fld");
            var isRegisteredWithoutEmail = false;

            User.getInfo(function (user) {
                $(".api-key").text(user.apiKey);

                $emailFld.val(user.email);
                if (user.registrationSource !== "email") {
                    isRegisteredWithoutEmail = true;
                    $("#password-form-group").show();
                    $(".registration-source").text(user.registrationSource === "google" ? "Google" : user.registrationSource === "gitHub" ? "GitHub" : "Slack");
                } else {
                    $("#password-change-form").show();
                }
                if (user.isSlackSetup) {
                    $("#slack-setup-finished").show();
                    $("#slack-setup").hide();
                }
                if (user.isTelegramSetup) {
                    $("#telegram-setup-finished").show();
                    $("#telegram-setup").hide();
                } else {
                    $("#telegram-btn").attr("href", "https://telegram.me/KuollBot?start=" + user.apiKey + "&startgroup=" + user.apiKey);
                }

            }, null, true);

            $("#email-change-form").submit(function (e) {
                e.preventDefault();
                $("#email-change-form")
                    .find(".form-group").removeClass("has-error").removeClass("has-success")
                    .find(".help-block").text("");

                if (!$passwordFld.val() && isRegisteredWithoutEmail) {
                    $("#password-msg").text("You must enter a password")
                        .parent().addClass("has-error");
                    return;
                }

                var $emailChangeMsg = $("#email-change-msg");
                api("change_email", {
                    userToken: userToken,
                    newEmail: $emailFld.val(),
                    newPassword: $passwordFld.val()
                }, function () {
                    $("#email-change-alert").show().text("Email change requested. Check your old email for a confirmation letter");
                }, function (errorMsg) {
                    if (errorMsg === "occupied") {
                        errorMsg = "Sorry, this email is already occupied by another user";
                    } else if (errorMsg === "invalid") {
                        errorMsg = "This email isn't correct";
                    } else {
                        errorMsg = "Unknown error. Check email correctness or try later";
                    }
                    $emailChangeMsg.text(errorMsg).parent().removeClass("has-success").addClass("has-error");
                });
            });

            var $newPasswordFld = $("#new-password-fld");
            var $newPasswordRepeatFld = $("#new-password-repeat-fld");
            var $oldPasswordFld = $("#old-password-fld");
            var $oldPasswordMsg = $("#old-password-msg");
            $("#password-change-form").submit(function (e) {
                e.preventDefault();
                $("#password-change-form")
                    .find(".form-group").removeClass("has-error").removeClass("has-success")
                    .find(".help-block").text("");

                if (!$oldPasswordFld.val()) {
                    $oldPasswordMsg.text("You must enter your current password")
                        .parent().addClass("has-error");
                    return;
                }
                if ($newPasswordFld.val() !== $newPasswordRepeatFld.val()) {
                    $("#new-password-repeat-msg").text("Passwords don't match")
                        .parent().addClass("has-error");
                    return;
                }

                api("update_password", {
                    userToken: userToken,
                    newPassword: $newPasswordFld.val(),
                    oldPassword: $oldPasswordFld.val()
                }, function () {
                    $("#password-change-alert").show().text("Password successfully changed. Next time use your new password to log in.");
                }, function (errorMsg) {
                    $oldPasswordMsg.text(errorMsg)
                        .parent().removeClass("has-success").addClass("has-error");
                });
            });

            /* Slack */
            var $slackSuccessMsg = $("#slack-success-msg");
            var $slackErrorMsg = $("#slack-error-msg");
            function changeSlackOperationStatus(success, status) {
                if (success) {
                    $slackSuccessMsg.text(status).parent().show();
                    $slackErrorMsg.parent().hide();
                } else {
                    $slackErrorMsg.text(status).parent().show();
                    $slackSuccessMsg.parent().hide();
                }
            }

            var redirectUrl = config.apiServer + "slack/redirect_url";
            $("#slack-btn")
                .attr("href", encodeURI("https://slack.com/oauth/authorize?scope=incoming-webhook&client_id=" +
                    config.slackClientId + "&state=bugbot_integration&redirect_uri=" + redirectUrl))
                .on("click", function (e) {
                    var onAuthenticationFinished = function (event) {
                        var kuollOrigins = ["http://localhost:9000", "http://www.kuoll.com", "https://www.kuoll.com", "http://app.kuoll.com", "https://app.kuoll.com"];
                        if (typeof event.data === "string" && kuollOrigins.indexOf(event.origin) !== -1) {
                            var data = JSON.parse(event.data);
                            if (data.type === "slack-authentication-finished") {
                                changeSlackOperationStatus(true, "Slack bot successfully added");
                                window.removeEventListener("message", onAuthenticationFinished);
                                $("#slack-setup-finished").show();
                                $("#slack-setup").hide();
                            }
                        }
                    };
                    window.addEventListener("message", onAuthenticationFinished);
                    window.open(e.currentTarget.getAttribute("href"), "Kuoll integration");
                    e.preventDefault();
                });

            $("#slack-detach").on("click", function () {
                api("slack/detach_bot", {
                    userToken: userToken
                }, function (resp) {
                    if (resp.errorMsg) {
                        changeSlackOperationStatus(false, resp.errorMsg);
                    } else {
                        changeSlackOperationStatus(true, "Slack bot removed.");
                        $("#slack-setup-finished").hide();
                        $("#slack-setup").show();
                    }
                });
            });

            var $whenSendAutoIssueFirstTimeOnly = $("#whenSendAutoIssueFirstTimeOnly");
            var $whenSendAutoIssueEachTime = $("#whenSendAutoIssueEachTime");
            var $sendManualIssuesToSlack = $("#sendManualIssuesToSlack");
            $whenSendAutoIssueFirstTimeOnly.add($whenSendAutoIssueEachTime).add($sendManualIssuesToSlack).on("change", function () {
                api("slack/update_settings", {
                    userToken: userToken,
                    notifyOnEveryJsError: $whenSendAutoIssueEachTime.prop("checked"),
                    notifyOnManualIssues: $sendManualIssuesToSlack.prop("checked")
                });
            });

            /* Telegram */

            var $telegramSuccessMsg = $("#telegram-success-msg");
            var $telegramErrorMsg = $("#telegram-error-msg");
            function changeTelegramOperationStatus(success, status) {
                if (success) {
                    $telegramSuccessMsg.text(status).parent().show();
                    $telegramErrorMsg.parent().hide();
                } else {
                    $telegramErrorMsg.text(status).parent().show();
                    $telegramSuccessMsg.parent().hide();
                }
            }

            var $telegramNotifyOnEveryJsErrorFalse = $("#telegramNotifyOnEveryJsErrorFalse");
            var $telegramNotifyOnEveryJsErrorTrue = $("#telegramNotifyOnEveryJsErrorTrue");
            var $telegramNotifyOnManualIssues = $("#telegramNotifyOnManualIssues");
            $telegramNotifyOnEveryJsErrorFalse.add($telegramNotifyOnEveryJsErrorTrue).add($telegramNotifyOnManualIssues).on("change", function () {
                api("telegram/update_settings", {
                    userToken: userToken,
                    notifyOnEveryJsError: $telegramNotifyOnEveryJsErrorTrue.prop("checked"),
                    notifyOnManualIssues: $telegramNotifyOnManualIssues.prop("checked")
                });
            });

            $("#telegram-detach").on("click", function () {
                api("telegram/detach_bot", {
                    userToken: userToken
                }, function (resp) {
                    if (resp.errorMsg) {
                        changeTelegramOperationStatus(false, resp.errorMsg);
                    } else {
                        changeSlackOperationStatus(true, "Telegram bot removed");
                        $("#telegram-setup-finished").hide();
                        $("#telegram-setup").show();
                        User.getInfo(function user() {
                            $("#telegram-btn").attr("href", "https://telegram.me/KuollBot?start=" + user.apiKey + "&startgroup=" + user.apiKey);
                        });
                    }
                });
            });

            User.getInfo(function (user) {
                $whenSendAutoIssueEachTime.prop("checked", user.slackNotifyOnEveryJsError);
                $whenSendAutoIssueFirstTimeOnly.prop("checked", !user.slackNotifyOnEveryJsError);
                $sendManualIssuesToSlack.prop("checked", user.slackNotifyOnManualIssues);
                $telegramNotifyOnEveryJsErrorTrue.prop("checked", user.telegramNotifyOnEveryJsError);
                $telegramNotifyOnEveryJsErrorFalse.prop("checked", !user.telegramNotifyOnEveryJsError);
                $telegramNotifyOnManualIssues.prop("checked", user.telegramNotifyOnManualIssues);
            }, null, true);


            var $tabsNav = $("#tabs-nav,#menuDropdownSettings");
            $tabsNav.on("click", function (e) {
                var href = e.target.getAttribute("href");
                if (href){
                    document.location = e.target.getAttribute("href");
                }
            });

            if (document.location.hash) {
                $tabsNav.find("a[href='" + document.location.pathname + document.location.hash + "']").parent().addClass("active");
                $('.tab-content > .tab-pane').removeClass('active');
                $('div' + document.location.hash).addClass('active');
            }

        });
    }
);
