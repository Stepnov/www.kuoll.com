function getPath(elt) {

    function getIndexInParent(elt) {
        var index = 0;
        while (elt = elt.previousSibling) {
            index++;
        }
        return index;
    }

    if (!elt) {
        return "null";
    } if ("HTML" == elt.tagName) {
        return "HTML";
    } else if ("BODY" == elt.tagName) {
        return "BODY";
    } else if (window === elt) {
        return "window";
    } else if (document == elt) {
        return "document";
    }

    if (elt.id) {
        return "#" + elt.id;
    } else {
        return getPath(elt.parentNode) + ">" + elt.tagName + ":nth-child(" + getIndexInParent(elt) + ")";
    }
}

function getAllPropertyNames(obj) {
    var props = {};
    var prot = obj;
    var attr, value;
    while (prot !== Object.prototype) {
        var currentProps = Object.getOwnPropertyNames(prot);
        for (var i = currentProps.length - 1; i >= 0; i--) {
            try {
                attr = currentProps[i];
                value = obj[attr];
                if ("undefined" != typeof value && "function" != typeof value) {
                    props[attr] = true;
                }
            } catch (e) {}
        }
        prot = prot.__proto__;
    }
    return Object.getOwnPropertyNames(props);
}

/**
 * http://stackoverflow.com/questions/728360/most-elegant-way-to-clone-a-javascript-object?page=1&tab=votes
 * @param obj
 * @returns {*}
 */
function cutObject(obj) {
    if (null == obj || "object" != typeof obj) return obj;

    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = cutObject(obj[i]);
        }
        return copy;
    }

    if (obj instanceof Object) {
        var copy = {};
        if (obj instanceof HTMLElement) {
            return {HTMLElementPath: getPath(obj)};
        } else if (obj === document) {
            return {HTMLElementPath: "document"};
        } else if (obj === window) {
            return {HTMLElementPath: "window"};
        } else if (obj instanceof CSSStyleDeclaration) {
            return {CSSStyleDeclaration: "CSSStyleDeclaration"};
        } else if (obj instanceof CSSStyleRule) {
            return {CSSStyleRule: "CSSStyleRule"};
        } else if (obj instanceof CSSStyleSheet) {
            return {CSSStyleSheet: "CSSStyleSheet"};
        } else if (obj instanceof HTMLCollection) {
            return {HTMLCollection: "HTMLCollection"};
        } else {
            var props = getAllPropertyNames(obj);
            var attr, value;
            for (var i = 0; i < props.length - 1; ++i) {
                attr = props[i];
                value = obj[attr];
                copy[attr] = cutObject(value);
            }
            return copy;
        }
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

function toJson(obj) {
    return JSON.stringify(cutObject(obj));
}

function logToContainer(event) {
    var container = document.getElementById("container");

    var div = document.createElement("div");
    div.innerHTML = toJson(event);
    container.appendChild(div);
}

$("#ajaxButton").click(
    function () {
        $.ajax({
            url: "/sample.json"

        }).done(
            function (data) {
                var data = typeof data == "string" ? JSON.parse(data) : data;
                $("#ajaxText").val(data.message);
            }
        );
    }
);
$("#ajaxParamButton").click(
    function () {
        $.ajax({
            url: "sample.json",
            type: "post",
            data: {
                "debug_param1": 1,
                "debug_param2": "test"
            }
        }).done(
            function (data) {
                var data = typeof data == "string" ? JSON.parse(data) : data;
                $("#ajaxParamText").text(data.message);
            }
        );
    }
);

$("#errorButton").click(function () {
    throw new Error();
});
$("#testForm").submit(function (e) {
    console.log($("#testy").val());
    e.preventDefault();
});
$("#submitButton").click(function (e) {
    console.log($("#testy").val());
    e.preventDefault();
});

$("#timeoutBtn").click(function () {
    setTimeout(function () {
//        var cancelAfterExecution = setTimeout(function () {
        $("#timeoutDiv").html("in setTimeout callback after 1000ms delay");
//            clearTimeout(cancelAfterExecution);
    }, 1000);

    var cancelableTimeout = setTimeout(function () {
        $("#timeoutDiv").html("This timeout will be cleared and this function should be newer called");
    }, 1000);
    clearTimeout(cancelableTimeout);
});

var $focusInput = $("#focusInput");
$focusInput.focus(function () {
    $("#focusDiv").html("#focusInput focused");
});
$focusInput.blur(function () {
    $("#focusDiv").html("#focusInput unfocued (blur)");
});

$("#sendPasswordBtn").on("click", function (e) {
    var password = $("#passwordField").val();
    $.ajax({
        url: "sample.json",
        type: "post",
        data: {
            "debug_param1": 1,
            "debug_param2": "test",
            "password": password
        }
    }).done(
        function (data) {
            var data = typeof data == "string" ? JSON.parse(data) : data;
            $("#passwordSendResult").text(data.message);
        }
    );
});

$("#postMessageBtn").on("click", function (e) {
    $("#postMessageIframe").get(0).contentWindow.postMessage("Hello world!", "/");
});

var $runAnimationBtn = $("#runAnimationBtn");
$runAnimationBtn.click(function (e) {
    $runAnimationBtn.animate({"margin-left": 400}, 250).animate({"margin-left": 0}, 250);
})
