define(
    ["jquery", "app/recordsSources/AdminUserRecordsSource", "react", "app/react/records-old/RecordsContainer"],
    function ($, AdminUserRecordsSource, React,RecordsContainer) {
        $(function () {
            React.render(
                React.createElement(RecordsContainer, {RecordsSource: AdminUserRecordsSource}),
                $("#recordsContainer")[0]
            );
        });
    });