define(
    ["jquery", "react", "app/react/records/RecordsContainer", "app/react/common/Header",
        "app/react/EmailConfirmationAlert"],
    function ($, React,RecordsContainer, Header, EmailConfirmationAlert) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(React.createElement(RecordsContainer), $("#recordsContainer")[0]);
            React.render(React.createElement(EmailConfirmationAlert), $("#emailConfirmationAlert")[0]);
        });
    });
