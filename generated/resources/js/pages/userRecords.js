define(["jquery", "react", "app/react/records-old/RecordsContainer", "app/recordsSources/UserRecordsSource"],
    function ($, React,RecordsContainer, UserRecordsSource) {

    $(function () {
        React.render(
            React.createElement(RecordsContainer, {RecordsSource: UserRecordsSource}),
            document.body
        );
    });

});
