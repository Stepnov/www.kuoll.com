define(["jquery", "react", "app/react/common/Header", "app/User", "jquery.cookie", "jquery.qtip",
    "css!/resources/css/jquery.qtip", "app/NavigationBox"], function ($, React, Header, User) {

    $(function () {
        // React.render(React.createElement(Header), $("#header")[0]);

        var $customerId = $(".customerId");
        var $userId = $(".userId");
        var userToken = $.cookie("userToken");
        if (userToken) {
            User.getInfo(function (user) {
                $(".ifNotLogged").hide();
                $(".ifLogged").show();
                $customerId.text(user.orgId).qtip("set", "content.text", "Customer unique ID");
                $userId.text(user.userId).qtip("set", "content.text", "Your unique ID");

                $("#asyncScript,#syncScript,#npmPackage").each(function(index, elm) {
                    var $elm = $(elm);
                    $elm.html($elm.html().replace("YOUR_API_KEY", user.apiKey))
                });
            });
        }

        $.merge($customerId, $userId).qtip({
            content: {
                text: $('<span><a href="/signup.html" target="_blank">Sign up</a> or <a href="/login.html" target="_blank">log in</a> to get your API_KEY</span>')
            },
            style: {
                classes: "qtip-light"
            },
            position: {
                my: "top center",
                at: "bottom center",
                target: "event"
            },
            hide: {
                fixed: true,
                delay: 1000
            }
        });
        $customerId.qtip("set", "content.text", $('<span><a href="/login.html" target="_blank">Log in</a> to get your API_KEY</span>'));
        $userId.qtip("set", "content.text", $('<span><a href="/login.html" target="_blank">Log in</a> to get your USER_ID</span>'));

        $("a[data-id]").on("click", function (e) {
            var $elem = $("#" + e.target.getAttribute("data-id"));
            var top = $elem.position().top;
            window.scrollTo(0, top + 40);
            $elem.addClass("navigated");
            setTimeout(function () {
                $elem.removeClass("navigated");
            }, 500);
        });
    });

});
