define(["jquery", "app/utils/UrlUtils", "app/utils/api", "app/NavigationBox"], function ($, UrlUtils, api) {

    $(function () {
        var $nameField = $("#nameField");
        var $companyField = $("#companyField");
        var $emailField = $("#emailField");
        var $phoneField = $("#phoneField");
        var $messageField = $("#messageField");
        var $errorBox = $("#errorMessageBox");
        var $form = $("#requestForm");

        var message = UrlUtils.getParameterByName("message");
        if (message) {
            $messageField.val(message);
        }

        function showErrorMsg(message) {
            $('#errorMessageBox').text(message).show();
            $errorBox.text(message).css("visibility", "visible");
            if ("localhost" !== document.location.hostname && mixpanel) {
                mixpanel.track("Demo request error", {
                    "Error message": message
                });
            }
        }

        function showSuccessMsg() {
            $('#successMessageBox').show();
            if ("localhost" !== document.location.hostname && mixpanel) {
                mixpanel.track("Demo requested");
            }
        }

        $form.on("submit", function (e) {
            $('#errorMessageBox').hide();
            $('#successMessageBox').hide();

            api("requestDemo", {
                name: $nameField.val(),
                company: $companyField.val(),
                email: $emailField.val(),
                phone: $phoneField.val(),
                message: $messageField.val()
            }, function () {
                showSuccessMsg();
            }, function (errorMsg) {
                showErrorMsg(errorMsg);
            });

            e.preventDefault();
        });
    });

});
