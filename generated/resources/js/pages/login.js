define(["jquery", "app/utils/UrlUtils", "app/google-signin", "app/utils/api", "analytics/segment", "jquery.cookie"], function ($, UrlUtils, GoogleSignIn, api, segment) {
    var $emailField = $('#emailField');
    var $passwordField = $('#passwordField');

    function login(event) {
        var $errorMessageBox = $('#errorMessageBox');
        var $suggestSignupBox = $('#suggestSignupBox');
        $errorMessageBox.text("").hide();
        $suggestSignupBox.text("").hide();
        api("loginForm", {
            email: $emailField.val(),
            password: $passwordField.val(),
            planName: UrlUtils.getParameterByName("plan") || $.cookie("kuoll-preselected-plan")
        }, function (resp) {
            $.cookie("userToken", resp.userToken, {domain: window.config.cookieDomain, path: "/"});
            $.removeCookie("isDemoUser");
            if ("localhost" !== document.location.hostname && mixpanel) {
                mixpanel.register({
                    "User token": resp.userToken,
                    "Kuoll user id": resp.userId
                });
            }
            segment.identify(resp);
            var redirectUrl = UrlUtils.getParameterByName("redirectTo") || (resp.subscriptionCreated ? "billing.html" : "issues-dashboard.html");
            if (resp.subscriptionCreated) {
                $.removeCookie("kuoll-preselected-plan");
            }
            document.location = "/" + redirectUrl;
        }, function (errorMsg) {
            if (errorMsg === "no user") {
                var $link = $("<a/>", {
                    href: "/signup.html?email=" + encodeURIComponent($emailField.val()),
                    text: "Register"
                });
                $suggestSignupBox.text("Your email not found.  ")
                    .append($link)
                    .append("?")
                    .show();
                $("#signUpBtnContainer").show();
            } else if (errorMsg === "incorrect password") {
                var $resetLink = $("<a/>", {
                    href: "/reset-password.html",
                    text: "Reset password",
                    style: "display: inline-block"
                });
                $errorMessageBox.text("The password you’ve entered is incorrect. ")
                    .append($resetLink)
                    .append("?")
                    .show();
            } else {
                $errorMessageBox.text(errorMsg).show();
            }
        });

        event.preventDefault();
    }

    var $signInForm = $('#sign-in-form');
    $signInForm.submit(login);

    GoogleSignIn.init({
        onSuccess: function () {
            $signInForm.hide();
        },
        onLogout: function () {
            $signInForm.show();
        },
        checkUserToken: true
    });

    var redirectUrl = config.apiServer + "slack/redirect_url";
    $(".slack-link").attr("href",
        encodeURI("https://slack.com/oauth/authorize?scope=identity.basic,identity.email&client_id=" + config.slackClientId
            + "&state=authenticate&redirect_uri=" + redirectUrl));
});