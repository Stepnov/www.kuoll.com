define(["jquery", "react", "app/react/common/Header", "app/User", "jquery.cookie"], function ($, React,Header, User) {

    $(function () {
        React.render(React.createElement(Header), $("#header")[0]);

        var userToken = $.cookie("userToken");
        if (userToken) {
            User.getInfo(function (user) {
                $(".customerId").text(user.orgId);
            }, function () {
                $(".ifNotLogged").show();
                $(".ifLogged").hide();
            });
        } else {
            $(".ifNotLogged").show();
            $(".ifLogged").hide();
        }
    });

});
