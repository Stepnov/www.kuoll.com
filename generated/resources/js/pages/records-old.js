define(
    ["jquery", "app/recordsSources/OwnRecordsSource", "react", "app/react/common/Header",
        "app/react/records-old/RecordsContainer", "app/react/EmailConfirmationAlert"],
    function ($, OwnRecordsSource, React,Header, RecordsContainer, EmailConfirmationAlert) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(
                React.createElement(RecordsContainer, {RecordsSource: OwnRecordsSource}),
                $("#recordsContainer")[0]
            );
            React.render(React.createElement(EmailConfirmationAlert), $("#emailConfirmationAlert")[0]);
        });
    });