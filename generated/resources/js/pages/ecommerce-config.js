define(
    ["jquery", "react", "app/react/common/Header", "app/react/ecommerce-config/StepPairs", "bootstrap", "jquery.cookie", "jquery.qtip"],
    function ($, React, Header, StepPairs) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(React.createElement(StepPairs), $("#ecommerce-config")[0]);
        });
    }
);
