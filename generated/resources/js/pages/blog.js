define(["jquery", "app/utils/api", "jquery.flexslider", "jquery.imagesloaded", "masonry", "rrssb", "jquery.cookie", "jquery.trumbowyg",
    "jquery.trumbowyg.colors", "jquery.trumbowyg.upload", "jquery.trumbowyg.base64", "jquery.trumbowyg.preformatted"], function ($, api) {

    $(document).ready(function() {

        /* ======= Blog Featured Post Slideshow - Flexslider ======= */
        $('.blog-slider').flexslider({
            animation: "fade",
            slideshowSpeed: 8000
        });


        /* ======= Blog page masonry ======= */
        /* Ref: http://desandro.github.io/masonry/index.html */

        var $container = $('#blog-mansonry');
        $container.imagesLoaded(function(){
            $container.masonry({
                itemSelector : '.post'
            });
        });

        /* ======= Blog page searchbox ======= */
        /* Ref: http://thecodeblock.com/expanding-search-bar-with-jquery-tutroial/ */
        var submitIcon = $('.searchbox-icon');
        var inputBox = $('.searchbox-input');
        var searchBox = $('.searchbox');
        var isOpen = false;
        submitIcon.on('click', function(){
            if(isOpen === false){
                searchBox.addClass('searchbox-open');
                inputBox.focus();
                isOpen = true;
            } else {
                searchBox.removeClass('searchbox-open');
                inputBox.focusout();
                isOpen = false;
            }
        });

        submitIcon.mouseup(function(){
            return false;
        });
        searchBox.mouseup(function(){
            return false;
        });
        $(document).mouseup(function(){
            if(isOpen === true){
                $('.searchbox-icon').css('display','block');
                submitIcon.click();
            }
        });

        function buttonUp(){
            var inputVal = $('.searchbox-input').val();
            inputVal = $.trim(inputVal).length;
            if( inputVal !== 0){
                $('.searchbox-icon').css('display','none');
            } else {
                $('.searchbox-input').val('');
                $('.searchbox-icon').css('display','block');
            }
        }

        inputBox.keyup(function() {
            buttonUp();
        });

        //Make sure the "Go" button is not shown when resize the browser window from mobile to desktop
        $(window).resize(function () {
            $('.searchbox-icon').css('display', 'block');
            searchBox.removeClass('searchbox-open');
        });

        var userToken = $.cookie("userToken");
        if (!userToken) {
            if ((document.location.pathname.indexOf("/add") !== -1 || document.location.pathname.indexOf("/edit") !== -1)) {
                document.location.pathname = "/blog";
            }
        } else {
            api("isUserAdmin", {
                userToken: userToken
            }, function (resp) {
                if (resp.isAdmin) {
                    $(".add-post-wrapper").show();
                    $(".edit-post-wrapper").show();
                    $(".delete-post-wrapper").show();
                }
            });
        }

        function savePost(url, id) {
            var intro;
            var content;
            var fullContent = $("#post-content").val().split("{intro-cut}");
            if (fullContent.length > 1) {
                intro = fullContent[0];
                content = fullContent.slice(1).join();
            } else {
                content = fullContent.join();
                intro = content.substring(0, Math.min(200, content.length));
            }
            var $savingSpinner = $("#saving-spinner").show();
            api(url, {
                userToken: userToken,
                id: id,
                title: $("#post-title").val(),
                authorName: $("#post-author-name").val(),
                authorUrl: $("#post-author-url").val(),
                imgUrl: $("#post-image").val(),
                featured: $("#post-featured")[0].checked,
                intro: intro,
                content: content
            }, function () {
                $savingSpinner.hide();
            });
        }

        var match = /\/blog\/(\d+)/.exec(document.location.pathname);
        if (match) {
            var postId = match[1];
            $(".post-form").submit(function (e) {
                savePost("/blog/" + postId, postId);
                e.preventDefault();
            });

            $(".edit-post-btn").attr("href", "/blog/" + postId + "/edit");
            $(".delete-post-btn").on("click", function () {
                api("deletePost", {
                    userToken: userToken,
                    postId: postId
                }, function () {
                    document.location = "/blog";
                });
            });
        } else {
            $(".post-form").submit(function (e) {
                savePost("/blog");
                e.preventDefault();
            });
        }

        $("textarea#post-content").trumbowyg({
            semantic: false,
            btnsDef: {
                image: {
                    dropdown: ['insertImage', 'upload', 'base64'],
                    ico: 'insertImage'
                },
                linkImproved: {
                    dropdown: ['createLink', 'editLink', 'unlink'],
                    ico: 'link'
                }
            },
            btns: ['viewHTML',
                '|', 'formatting',
                '|', 'btnGrp-design',
                '|', 'linkImproved',
                '|', 'image',
                '|', 'btnGrp-justify',
                '|', 'btnGrp-lists',
                '|', 'foreColor', 'backColor',
                '|', 'preformatted',
                '|', 'horizontalRule']
        });

    });

});
