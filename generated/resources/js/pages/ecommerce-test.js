define(["jquery", "app/User", "jquery.cookie", "jquery.fittext", "bootstrap"], function ($, User) {

    $("#startBtn").on("click", function () {
        function askToLogin() {
            window.alert("Login to your Kuoll account to start recording please");
            document.location = "/login.html?redirectTo=ecommerce-test.html";
        }

        var userToken = $.cookie("userToken");
        if (userToken) {
            var kuoll = window.kuoll || window.kuollDev;
            if (kuoll) {
                kuoll.isRecordActive().then(function (active) {
                    if (active) {
                        delete localStorage.kuollRecordId;
                        kuoll.stopRecord();
                    } else {
                        User.getInfo(function (user) {
                            kuoll.startRecord({
                                orgId: user.orgId,
                                API_KEY: user.apiKey,
                                kuollUserId: user.userId,
                                localRecording: false
                            });
                        }, function () {
                            askToLogin();
                        });
                    }
                })
            }
        } else {
            askToLogin();
        }
    });


    if (!apiKeyParam) {
        User.getInfo(function (user) {
            startRecord(user.apiKey);
        });
    }

    $('.ecommerce-btn').on('click', function (e) {
        var action = $(e.target).data('action');
        dataLayer.push({
            eventAction: action,
            ecommerce: {
                eventAction: action
            }
        });
    });

    $('.error-btn').on('click', function (e) {
        var error = $(e.target).data('error');
        eval("(function throwError" + error + "() { throw new Error('Artificial error " + error + "'); }())");
    });

    //FS.identify($.cookie("userToken"));

});
