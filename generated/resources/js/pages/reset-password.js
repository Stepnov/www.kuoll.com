define(["jquery", "app/utils/api"], function ($, api) {

    $(function () {
        var $errorMessageBox = $('#errorMessageBox');
        var $infoMessageBox = $('#infoMessageBox');
        var $emailField = $('#emailField');

        $("#resetForm").submit(function (e) {
            $errorMessageBox.text("").css("visibility", "collapse");
            $infoMessageBox.text("").css("visibility", "collapse");

            api("resetPassword", {
                email: $emailField.val()
            }, function () {
                $infoMessageBox.text("Check your mailbox to get link for resetting your password").css("visibility", "visible");
            }, function (errorMsg) {
                if (errorMsg === "no user") {
                    $errorMessageBox.text("There is no user with such email").css("visibility", "visible");
                } else {
                    $errorMessageBox.text(errorMsg).css("visibility", "visible");
                }
            });

            e.preventDefault();
        });
    });

});
