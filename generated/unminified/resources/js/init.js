define(function () {
    var apiServer = document.createElement("a"), contentServer = document.createElement("a");

    var isDev = (document.location.host === "localhost:9000");
    var isStage = (document.location.host === "stage.kuoll.com.s3-website.eu-central-1.amazonaws.com");
    var isWww = (document.location.host === "www.kuoll.com");

    apiServer.href = isDev ? "http://localhost:8080" : (isStage ? "https://stage.kuoll.com" : "https://api.kuoll.com");
    contentServer.href = isDev ? "http://localhost:9000" : (isStage ? "http://stage.kuoll.com.s3-website.eu-central-1.amazonaws.com" : document.location.protocol + "//app.kuoll.com");

    window.config = {
        isWww: isWww,
        isDev: isDev,
        isStage: isStage,
        contentServer: contentServer.href,
        unsecure: isDev ? "http://localhost:8080" : (isStage ? "http://stage.kuoll.com" : "http://api.kuoll.com"),
        apiServer: apiServer.href,
        cookieDomain: isDev ? "localhost" : (isStage ? ".stage.kuoll.com.s3-website.eu-central-1.amazonaws.com" : ".kuoll.com"),

        slackClientId: "5013105915.44840319781",

        stripeKey: isDev ? "pk_test_CMCm0uAF3T3vYrOU5fZQ4Et2" : (isStage ? "pk_test_1N6zDQMrdbG7sDR0CSpt5H2b" : "pk_live_N4ymJrfdW4dxlpHpT9Zq0JIL")
    };

    var appShortcut;
    appShortcut = isWww? "//app.kuoll.com/resources/js": (config.isDev ? "/unminified/resources/js" : "/resources/js");
    var libJs = isWww? "//app.kuoll.com/lib/js/": "/lib/js/";

    requirejs.config({
        basePath: isWww ? '//app.kuoll.com/': '../..',

        paths: {
            'app': appShortcut,
            'analytics': appShortcut + "/analytics",

            'jquery': libJs + 'jquery/jquery.min',
            'jquery.cookie': libJs + 'jquery-cookie/jquery.cookie',
            'jquery.jeditable': libJs + 'jeditable/jquery.jeditable',
            'jquery.fittext': libJs + 'jquery-fittext/jquery.fittext',
            'jquery.qtip': libJs + 'qtip2/jquery.qtip.min',
            'jquery.ui': libJs + 'jquery-ui/jquery-ui.min',
            'jquery.imagesloaded': libJs + 'imagesloaded/imagesloaded.pkgd.min',
            'jquery.fitvids': libJs + 'fitvids/jquery.fitvids',
            'jquery.flexslider': libJs + 'flexslider/jquery.flexslider-min',
            'jquery.placeholder': libJs + 'jquery.placeholder/jquery.placeholder.min',
            'jquery.backtotop': libJs + 'jquery.back-to-top/jquery.backtotop',
            'jquery.browser': libJs + 'jquery.browser/jquery.browser.min',
            'onboard': libJs + 'onboard',

            'masonry': libJs + 'masonry/masonry',

            'html5shiv': libJs + 'html5shiv/html5shiv.min',
            'respond': libJs + 'respond/respond.min',

            'rrssb': libJs + 'rrssb/rrssb.min',

            "jquery.trumbowyg": libJs + "trumbowyg/trumbowyg.min",
            "jquery.trumbowyg.colors": libJs + "trumbowyg/trumbowyg.colors.min",
            "jquery.trumbowyg.upload": libJs + "trumbowyg/trumbowyg.upload.min",
            "jquery.trumbowyg.base64": libJs + "trumbowyg/trumbowyg.base64.min",
            "jquery.trumbowyg.preformatted": libJs + "trumbowyg/trumbowyg.preformatted",

            'react': libJs + "react/react",
            

            'bootstrap': libJs + 'bootstrap/bootstrap.min',
            'bootstrap.hover-dropdown': libJs + 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',

            'google-signin': 'https://apis.google.com/js/platform',
            'google-charts': 'https://www.gstatic.com/charts/loader',


            'youtube-api': 'https://www.youtube.com/iframe_api?',

            'ua-parser': libJs + 'ua-parser-js/ua-parser.min',

            'highlightjs': libJs + 'highlightjs/highlight.pack',

            'jasmine': libJs + 'jasmine-core/jasmine',
            'jasmine-html': libJs + 'jasmine-core/jasmine-html',
            'jasmine-boot': libJs + 'jasmine/boot',

            'stripe-checkout': 'https://checkout.stripe.com/checkout'
        },

        shim: {
            'jquery.fittext': ['jquery'],
            'jquery.fitvids': ['jquery'],
            'jquery.flexslider': ['jquery'],
            'jquery.imagesloaded': ['jquery'],
            'masonry': ['jquery'],
            'bootstrap': ['jquery'],
            'bootstrap.hover-dropdown': ['jquery'],
            'jquery.placeholder': ['jquery'],
            'jquery.backtotop': ['jquery'],
            'rrssb': ['jquery'],

            "jquery.trumbowyg": ["jquery"],
            "jquery.trumbowyg.colors": ["jquery", "jquery.trumbowyg"],
            "jquery.trumbowyg.upload": ["jquery", "jquery.trumbowyg"],
            "jquery.trumbowyg.base64": ["jquery", "jquery.trumbowyg"],
            "jquery.trumbowyg.preformatted": ["jquery", "jquery.trumbowyg"],
            'jasmine-boot': ['jasmine', 'jasmine-html'],
            'jasmine-html': ['jasmine']
        },
        map: {
            '*': {
                'css': libJs + 'require-css/css.js'
            }
        }
    });

    var path = document.location.pathname;
    if (path === "/test/test.html") {
        requirejs(["app/test/test-boot"]);
    } else {
        // if path includes file extension, remove it
        if (path.lastIndexOf(".") > path.lastIndexOf("/")) {
            path = path.slice(0, path.lastIndexOf("."));
        }

        if (path === "/" || path === "") {
            path = "/index";
        }

        if (path.indexOf("/blog/") === 0) {
            path = "/blog";
        }

        if (path === "/play" && document.location.protocol === "https:") {
            document.location.protocol = "http:";
            return;
        }

        if (!path) {
            throw new Error("Can not define module to load");
        }

        var root = document.body.parentElement;
        if ((root.classList && root.classList.contains("ie8")) || root.className.indexOf("ie8") !== -1) {
            requirejs(["html5shiv", "respond"]);
        }

        if (!isWww) {
            requirejs(["app/pages" + path]);
        } else {
            requirejs(["app/onboard"]);
        }

        requirejs(["analytics/mixpanel"], function () { }, function () {
            console.log("Could not load mixpanel");
            window.mixpanel = null;
        });
        requirejs(["analytics/google-analytics", "analytics/yandex-metrika", "analytics/facebook-pixel", "analytics/segment"],
            function () {
            }, function (e) {
                console.log("Could not load module", e.requireModules);
            });

        if (path !== "/test") {
            requirejs(["app/utils/NotificationsDisplayer"]);
        }
    }

    // TODO vlad: move this to some header-specific file
    requirejs(["jquery"], function ($) {
        var $tryDemoSubmenu = $("#tryDemoSubmenu");
        $("#tryDemoMenu").click(function (e) {
            $tryDemoSubmenu.toggle();
            e.stopPropagation();
        });
        $tryDemoSubmenu.click(function (e) {
            e.stopPropagation();
        });
        $(document.body).click(function () {
            $tryDemoSubmenu.hide();
        })
    });

});
