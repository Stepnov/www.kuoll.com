var highlightingNodes = [];

var mouseNodes;

var isAutonomous = true;

var treeMirror = new TreeMirror(document, {
    createElement: function (nodeData) {
        if (nodeData.tagName == "STYLE" || nodeData.tagName == "LINK") {
            var node = isStylesheetAlreadyAdded(nodeData.tagName, nodeData.attributes);
            if (node) {
                return node;
            } else {
                return null;
            }
        }
        return null;
    }
});

var resourceHashByUrl;
var resourcePathByHash;
var resourcePaths;

function isStylesheetAlreadyAdded(tagName, attributes) {
    for (var i = 0; i < document.styleSheets.length; ++i) {
        var stylesheet = document.styleSheets[i];
        var node = stylesheet.ownerNode;
        if (node.tagName == tagName) {
            var attrNames = ["href", "src", "rel"];
            for (var j = 0; j < attrNames.length; ++j) {
                var curAttr = node.getAttribute(attrNames[j]);
                var newAttr = attributes[attrNames[j]];
                if ((curAttr && !newAttr) || (!curAttr && newAttr) || (curAttr && newAttr && curAttr !== newAttr)) {
                    break;
                }
            }
            if (j == attrNames.length)
                return node;
        }
    }
    return false;
}

/**
 * Calculates element's offset relative to the document and element's size.
 *
 * @param elem
 * @returns {{top: number, left: number, width: number, height: number}}
 */
function getPosition(elem) {
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docEl = document.documentElement;

    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    var clientTop = docEl.clientTop || body.clientTop || 0;
    var clientLeft = docEl.clientLeft || body.clientLeft || 0;

    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return {
        top: Math.round(top),
        left: Math.round(left),
        width: Math.round(box.width),
        height: Math.round(box.height)
    };
}

function highlightNode(selector) {
    for (var i in highlightingNodes) {
        if (highlightingNodes[i].selector == selector)
            return;
    }

    var node;
    if ("document" === selector) {
        node = document;
    } else {
        node = document.querySelector(selector);
    }
    if (!node) return;

    var highlightingNode = document.createElement("div");
    highlightingNode.className += " kuollHighlightingNode";

    var position = getPosition(node);
    highlightingNode.style.left = position.left + "px";
    highlightingNode.style.top = position.top + "px";
    highlightingNode.style.width = position.width + "px";
    highlightingNode.style.height = position.height + "px";

    document.body.appendChild(highlightingNode);

    highlightingNodes.push({
        node: highlightingNode,
        selector: selector
    });
}

function scrollTo(left, top) {
    window.scroll(left, top);
}

function scrollToElement(path) {
    if (!path) return;

    var node = document.querySelector(path);
    if (!node) return;

    var position = Position.get(node);
    window.scroll(position.left, position.top);
}

function createMousePointer(point, opacity, index) {
    var node = document.createElement("div");

    node.className += " kuollMousePointer";
    node.style.left = point.x - 2 + "px";
    node.style.top = point.y - 4 + "px";
    node.style.opacity = opacity;

    if (point.type === "click") {
        node.className += " kuollMousePointerClick";
        node.title = "Mouse click";
        node.style.opacity = 1;
    } else if (opacity == 1) {
        node.className += " kuollMousePointerMain";
        node.title = "Last mouse position";
        node.style.opacity = 1;
    } else {
        node.style.zIndex = 100000 + index;
        node.style.opacity = opacity;
    }

    document.body.appendChild(node);
    mouseNodes.push(node);
}

function createMouseTrail(trail) {
    trail = JSON.parse(trail);

    if (mouseNodes) {
        removeMouseTrail();
    }
    mouseNodes = [];

    var opacityStep = (1.0 - 0.2) / trail.length;
    var opacity = 1;
    for (var i = trail.length - 1; i >= 0; --i) {
        createMousePointer(trail[i], opacity, i);
        opacity -= opacityStep;
    }
}
function removeMouseTrail() {
    for (var i = 0; i < mouseNodes.length; ++i) {
        var node = mouseNodes[i];
        if (node.parentNode) {
            node.parentNode.removeChild(node);
        }
    }
    mouseNodes = null;
}

function unhighlightNode(selector) {
    for (var i in highlightingNodes) {
        if (highlightingNodes[i].selector == selector) {
            document.body.removeChild(highlightingNodes[i].node);
            delete highlightingNodes[i];
            return;
        }
    }
}

function unhighlightAll() {
    for (var i in highlightingNodes) {
        document.body.removeChild(highlightingNodes[i].node);
    }
    highlightingNodes = [];
}

function resetDom() {
    function cleanUpNode(node) {
        var children = Array.prototype.slice.call(node.children);
        for (var i = 0; i < children.length; ++i) {
            var child = children[i];
            if (!child.dataset["preserveOnSnapshotReload"]) {
                node.removeChild(child);
            }
        }
    }
    cleanUpNode(document.head);
    cleanUpNode(document.body);
}

function setSnapshot(baseSnapshot, mutations, sequent) {
    resetDom();
    treeMirror.initialize(baseSnapshot.rootId, baseSnapshot.children);
    for (var i = 0; i < mutations.length; ++i) {
        var mutation = mutations[i];
        if (mutation.isSplitMutation) {
            for (var j = i + 1; j < mutations.length; ++j) {
                if (mutations[j].isSplitMutation) {
                    for (var key in mutations[j]) {
                        var prop = mutations[j][key];
                        if (mutations[j].hasOwnProperty(key) && prop instanceof Array) {
                            mutation[key] = mutation[key].concat(mutations[j][key]);
                        }
                    }
                    i = j;
                } else {
                    break;
                }
            }
        }
        var mutationReport = treeMirror.applyChanged(mutation.removed, mutation.addedOrMoved, mutation.attributes, mutation.text, mutation.propertiesChanged);
    }
    if (sequent) {
        notifySequentInfoUpdated(mutationReport, sequent);
        scrollTo(sequent.scrollX, sequent.scrollY);
    }
    replaceResources();
}

function nextSnapshot(mutation, sequent) {
    var mutationReport;
    if (mutation.length) {
        for (var i = 0; i < mutation.length; ++i) {
            var m = mutation[i];
            if (m.isSplitMutation) {
                for (var j = i + 1; j < mutation.length; ++j) {
                    if (mutation[j].isSplitMutation) {
                        for (var key in mutation[j]) {
                            var prop = mutation[j][key];
                            if (mutation[j].hasOwnProperty(key) && prop instanceof Array) {
                                m[key] = m[key].concat(mutation[j][key]);
                            }
                        }
                        i = j;
                    } else {
                        break;
                    }
                }
            }
            mutationReport = treeMirror.applyChanged(m.removed, m.addedOrMoved, m.attributes, m.text, m.propertiesChanged || m.valueChanged);
        }
    } else {
        mutationReport = treeMirror.applyChanged(mutation.removed, mutation.addedOrMoved, mutation.attributes, mutation.text, mutation.propertiesChanged);
    }
    notifySequentInfoUpdated(mutationReport, sequent);
    scrollTo(sequent.scrollX, sequent.scrollY);
    replaceResources();
}

function sendMessage(data) {
    window.parent.postMessage(JSON.stringify(data), "*");
}

function callCallback(token) {
    sendMessage({
        action: "callCallback",
        token: token
    })
}

function notifySequentInfoUpdated(mutationReport, sequent) {
    if (mutationReport && sequent && sequent.mutations) {
        mutationReport.frameNum = sequent.frameNum;
        mutationReport.sequentNum = sequent.sequentNum;
        mutationReport.action = "updateSequentInfo";
        sendMessage(mutationReport);
    }
}

addEventListener("message", function (event) {
    var kuollOrigins = ["http://localhost:9000", "http://app.kuoll.com", "https://app.kuoll.com"];
    if (kuollOrigins.indexOf(event.origin) === -1) {
        return;
    }

    var data = JSON.parse(event.data);

    if (data.action == "highlight") {
        highlightNode(data.selector);
    } else if (data.action == "unhighlight") {
        if (data.selector) {
            unhighlightNode(data.selector);
        } else {
            unhighlightAll();
        }
    } else if (data.action == "scroll") {
        scrollTo(data.left, data.top);
    } else if (data.action === "scrollToElement") {
        scrollToElement(data.path);
    } else if (data.action == "setSnapshot") {
        setSnapshot(JSON.parse(data.baseSnapshot), data.mutations, data.sequent);
    } else if (data.action == "nextSnapshot") {
        nextSnapshot(data.mutation, data.sequent);
    } else if (data.action == "createMouseTrail") {
        createMouseTrail(data.mouseTrail);
    } else {
        throw new Error("Unknown message received", data);
    }

}, false);

function replaceResources() {
    var urlForResources = document.location.protocol + "//" + document.location.host +
        "/getResource?hash=";

    var baseTag = document.getElementsByTagName("base")[0];
    var baseHref = baseTag ? baseTag.getAttribute("href") : document.baseURI;
    var questionMarkIndex = baseHref.indexOf("?");
    var hrefLength = baseHref.length;
    var hrefWithoutParams = baseHref.substring(0, questionMarkIndex == -1 ? hrefLength : questionMarkIndex);
    hrefLength = hrefWithoutParams.length;
    var isHrefEndsWithSlash = (hrefWithoutParams.substring(hrefLength - 1) === "/");
    var endIndex = (isHrefEndsWithSlash ? hrefLength - 1 : hrefLength);
    var base = hrefWithoutParams.substring(0, endIndex);

    var origin = document.location.protocol + "//" + document.location.host;

    if (isAutonomous) {
        replaceStyleSheets(urlForResources, origin, base);
        replaceDomImageUrls(urlForResources, origin, base);
        replaceDomStyleUrls(urlForResources, origin, base);
        replaceCssUrls(urlForResources, origin, base);
    }
}

if (typeof(resourceHashByUrl) === "undefined"
    || typeof(resourcePathByHash) === "undefined"
    || typeof(resourcePaths) === "undefined") {
    isAutonomous = false;
    resourceHashByUrl = {};
    resourcePathByHash = {};
    resourcePaths = [];
}
window.addEventListener("load", function () {
    replaceResources();
});

document.addEventListener("click", function (e) {
    e.preventDefault();
}, true);



///////////////////////////
// Replacing resource links
///////////////////////////

function getResourceHashByUrls(urls) {
    for (var i in urls) {
        var url = urls[i];
        var hash = resourceHashByUrl[url];
        if (hash) {
            return hash;
        } else {
            var _url = url.indexOf("https://") != -1 ?
                url.replace("https://", "http://")
                : url.replace("http://", "https://");
            hash = resourceHashByUrl[_url];
            if (hash) {
                return hash;
            }
        }
    }
    return null;
}

function replaceCssUrls(urlForResources, host, baseHost) {
    for (var i = 0; i < document.styleSheets.length; ++i) {
        var styleSheet = document.styleSheets[i];
        if (!styleSheet || !styleSheet.cssRules) continue;
        var resourcePath;
        if (styleSheet.href) {
            var hash = styleSheet.href.substring(styleSheet.href.lastIndexOf("=") + 1);
            resourcePath = resourcePathByHash[hash];
        }
        for (var j = 0; j < styleSheet.cssRules.length; ++j) {
            var cssRule = styleSheet.cssRules[j];
            if (cssRule instanceof CSSStyleRule || cssRule instanceof CSSFontFaceRule) {
                var style = cssRule.style;
                if (!style) continue;
                for (var k = 0; k < style.length; ++k) {
                    var propertyName = style[k];
                    var css = style[propertyName];
                    if (typeof css == "string") {
                        if (propertyName == "fill" || propertyName == "stroke") {
                            //style[propertyName] = replaceSvgUrls(css);
                        }
                        if (css.indexOf("url(") != -1 && isAutonomous) {
                            var newCss = replaceResourceUrls(style.cssText, urlForResources, host, baseHost, resourcePath);
                            if (style.cssText != newCss) {
                                var selector = cssRule.cssText.substring(0, cssRule.cssText.indexOf("{")).trim();
                                styleSheet.removeRule(j);
                                styleSheet.addRule(selector, newCss, j);
                            }
                        }
                    }
                }
            }
        }
    }
}

function appendToResourcePaths(url, host) {
    var originalUrls = [];
    for (var i = 0; i < resourcePaths.length; ++i) {
        if (url.substring(0, 2) == "//") {
            originalUrls.push(document.location.protocol + url);
        } else if (url.substring(0, 1) == "/") {
            originalUrls.push(resourcePaths[i] + url);
        } else {
            originalUrls.push(url.replace(host, resourcePaths[i]));
        }
    }
    return originalUrls;
}

function replaceResourceUrls(string, urlForResources, host, base, resourcePath) {
    // RegExp for css values like 'url("http://some.com/file.png")'
    return string.replace(/url\(['"]{0,2}(.*?)['"]{0,2}\)/g, function (match, p1, offset, string) {
        var url = p1;
        if (url.indexOf("data:") != 0) {
            var urls = [];
            var curBase = base;
            if (curBase) {
                while(curBase.length > 8) { // stop when path contains only 'http[s]://'
                    urls.push(curBase + "/" + url);
                    curBase = curBase.substring(0, curBase.lastIndexOf("/"));
                }
            }

            if (url.indexOf(host) != -1) {
                if (url.indexOf(document.location.host) != -1) {
                    return match;
                } else {
                    var path = resourcePath;
                    if (path) {
                        while(path.length > 8) { // stop when path contains only 'http[s]://'
                            urls.push(url.replace(host, path));
                            path = path.substring(0, path.lastIndexOf("/"));
                        }
                    }

                    urls.push(url.replace(host, base || ""));
                    urls.push(url.replace(host, base.match(/http[s]?:\/\/[^\/]+/)[0])); // RegExp returns origin of base url
                    urls.push(url.replace(host, ""));
                }
            } else {
                if (url.substring(0, 1) == "/") {
                    urls.push( base + url);
                } else {
                    urls.push(url);
                }
            }
            var originalUrls = appendToResourcePaths(url, host);
            var resourceHash = getResourceHashByUrls(urls.concat(originalUrls));
            return resourceHash ? "url(" + urlForResources + resourceHash + ")" : match;

        } else {
            return match;
        }
    })
}

function replaceSvgUrls(css) {
    return css.replace(/url\(['"]{0,2}(#.*?)['"]{0,2}\)/g, function (match, p1, offset, string) {
        return "url(" + document.location.href + p1 + ")";
    })
}

function replaceDomStyleUrls(urlForResources, host, baseHost) {
    var iter = document.createNodeIterator(document.body);
    var node;
    while((node = iter.nextNode()) != null) {
        if (node.hasAttribute && node.hasAttribute("style")) {
            var style = node.getAttribute("style");
            if (style.indexOf("url(") != -1) {
                var newStyle = replaceResourceUrls(style, urlForResources, host, baseHost, "");
                node.setAttribute("style", newStyle);
            }
        }
    }
}

function replaceUrlAttribute(node, attr, urlForResources, host, base) {
    if (node.hasAttribute(attr)) {
        var value = node.getAttribute(attr);
        if (value.indexOf("data:") !== 0 && value.indexOf(document.location.host) === -1) {
            var hash;
            if (value.substr(0, 2) === "//") {
                value = document.location.protocol + value;
                hash = getResourceHashByUrls([value]);
            } else if (value.substr(0, 1) === "/") {
                var valueArr = [];
                for (var i = 0; i < resourcePaths.length; ++i) {
                    valueArr.push(resourcePaths[i] + value);
                }
                if (base) {
                    while(base.length > 8) {
                        valueArr.push(base + value);
                        base = base.substring(0, base.lastIndexOf("/"));
                    }
                }
                valueArr.push(host + value);
                hash = getResourceHashByUrls(valueArr);
            } else {
                var urls = [];
                if (base) {
                    while(base.length > 8) { // stop when path contains only 'http[s]://'
                        urls.push(base + "/" + value);
                        base = base.substring(0, base.lastIndexOf("/"));
                    }
                }
                hash = getResourceHashByUrls(urls);
            }
            if (!hash) return;
            node.setAttribute(attr, urlForResources + hash);
        }
    }
    //if (node.hasAttribute(attr)) {
    //    var value = node.getAttribute(attr);
    //    if (value.indexOf("data:") != 0 && value.indexOf(document.location.host) == -1) {
    //        var hash;
    //        if (value.indexOf(host) != -1 || value.indexOf("getResource") != -1) {
    //            return;
    //        }
    //
    //        var valueArr = [value, host + value];
    //        var hasLeadingSlash = (value.substr(0, 1) === "/");
    //        if (!hasLeadingSlash) {
    //            valueArr.push(host + "/" + value);
    //        }
    //        if (value.substr(0, 2) == "//") {
    //            value = document.location.protocol + value;
    //            hash = getResourceHashByUrls([value]);
    //        } else {
    //            if (!hasLeadingSlash) {
    //                value = "/" + value;
    //            }
    //            for (var i = 0; i < resourcePaths.length; ++i) {
    //                valueArr.push(resourcePaths[i] + value);
    //            }
    //            hash = getResourceHashByUrls(valueArr);
    //        }
    //        if (!hash) {
    //            console.error("Can not find resource hash for url: " + value);
    //            return;
    //        }
    //        node.setAttribute(attr, urlForResources + hash);
    //    }
    //}
}

function replaceDomImageUrls(urlForResources, host, base) {
    var images = document.images;
    for (var i = 0; i < images.length; ++i) {
        var image = images[i];
        replaceUrlAttribute(image, "src", urlForResources, host, base);
    }
}

function replaceStyleSheets(urlForResources, host, base) {
    function processLoadedStylesheet() {
        var urlForResources = document.location.protocol + "//" + document.location.host +
            "/getResource?hash=";

        var baseTag = document.getElementsByTagName("base")[0];
        var baseHref = baseTag ? baseTag.getAttribute("href") : document.baseURI;
        var questionMarkIndex = baseHref.indexOf("?");
        var hrefLength = baseHref.length;
        var hrefWithoutParams = baseHref.substring(0, questionMarkIndex == -1 ? hrefLength : questionMarkIndex);
        hrefLength = hrefWithoutParams.length;
        var isHrefEndsWithSlash = (hrefWithoutParams.substring(hrefLength - 1) === "/");
        var endIndex = (isHrefEndsWithSlash ? hrefLength - 1 : hrefLength);
        var base = hrefWithoutParams.substring(0, endIndex);

        var origin = document.location.protocol + "//" + document.location.host;
        replaceCssUrls(urlForResources, origin, base);
    }

    var linkTags = document.querySelectorAll("link[rel='stylesheet']");
    for (var i = 0; i < linkTags.length; ++i) {
        var link = linkTags[i];
        var href = link.getAttribute("href");
        link.addEventListener("load", processLoadedStylesheet);
        replaceUrlAttribute(link, "href", urlForResources, host, base);
    }
}

function getPath(elt) {
    function getIndexInParent(elt) {
        var index = 1;
        while (elt = elt.previousElementSibling) {
            index++;
        }
        return index;
    }

    if (!elt) {
        throw new Error();
    } else if ("HTML" === elt.tagName) {
        return "html";
    } else if ("BODY" === elt.tagName) {
        return "body";
    } else if ("HEAD" === elt.tagName) {
        return "head";
    } else if (window === elt) {
        return "window";
    } else if (document === elt) {
        return "document";
    }

    var tagName = elt.tagName;

    if (typeof tagName === "undefined" || tagName === "") {
        if ("#text" === elt.nodeName) {
            tagName = "";
        } else if ("#document-fragment" === elt.nodeName) {
            tagName = "";
        } else {
            reportErrorFromInjectScript("tagName is unknown. elt.nodeName=" + elt.nodeName);
            throw new Error();
        }
    }

    if (elt.id) {
        var firstChar = elt.id.charAt(0);
        if (firstChar >= 0 && firstChar <= 9) {
            // ID selector works incorrectly if it starts with a number; need to escape first char in this case
            return "#\\\\3" + firstChar + " " + elt.id.substring(1);
        } else {
            return "#" + elt.id;
        }
    } else {
        var path;
        if (elt.parentNode == null) {
            //shadow dom?
            //path = elt.host ? getPath(elt.host) + "::shadow>" + tagName : elt.nodeName + [""].concat(Array.prototype.slice.call(elt.classList)).join(".");
            //reportErrorFromInjectScript("elt.parentNode == null. elt.nodeName=" + elt.nodeName);
            return null;
        } else {
            var index = getIndexInParent(elt);
            path = index ? (getPath(elt.parentNode) + ">" + tagName + ":nth-child(" + index + ")")
                : getPath(elt.parentNode) + ">" + tagName;
        }

        //// TODO DEV_MODE: check if path was built correctly. For debug purposes only
        //if (document.querySelector(path) !== elt) {
        //    window.console_orig.error("getPath works incorrectly");
        //}

        return path;
    }
}

function getPathWrapper(elt) {
    try {
        return getPath(elt);
    } catch (e) {
        return null;
    }
}