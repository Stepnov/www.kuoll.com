var TreeMirror = (function () {
    function TreeMirror(root, delegate) {
        this.root = root;
        this.delegate = delegate;
        this.idMap = {};
    }
    TreeMirror.prototype.initialize = function (rootId, children) {
        this.idMap = {};
        this.idMap[rootId] = this.root;
        for (var i = 0; i < children.length; i++)
            this.deserializeNode(children[i], this.root);
    };

    /*
     * NOTE: Applying the changes can result in an attempting to add a child
     * to a parent which is presently an ancestor of the parent. This can occur
     * based on random ordering of moves. The way we handle this is to first
     * remove all changed nodes from their parents, then apply.
     */
    TreeMirror.prototype.applyChanged = function (removed, addedOrMoved, attributes, text, propertiesChanged, prevMutationReport) {
        var _this = this;

        function putFields(dest, src) {
            Object.keys(src).forEach(function (name) {
                dest[name] = src[name];
            })
        }

        var mutationReport = prevMutationReport || {};
        removed.forEach(function (data) {
            var node = _this.deserializeNode(data);
            if (node && node.parentNode) {
                var info = {
                    tagName: node.tagName,
                    text: node.data,
                    nodePath: getPathWrapper(node),
                    parentPath: getPathWrapper(node.parentNode)
                };

                if (mutationReport[data.id]) {
                    putFields(mutationReport[data.id], info);
                } else {
                    mutationReport[data.id] = info;
                }
            }
        });
        addedOrMoved.forEach(function (data) {
            var node = _this.deserializeNode(data);
            var parent = _this.deserializeNode(data.parentNode);
            var previous = _this.deserializeNode(data.previousSibling);

            if (node && node.parentNode)
                node.parentNode.removeChild(node);

            if (node && parent) {
                try {
                    parent.insertBefore(node, previous ? previous.nextSibling : parent.firstChild);
                } catch (e) {
                    console.error(e, parent, node, data);
                    throw e;
                }

                var info = {
                    tagName: node.tagName,
                    nodePath: getPathWrapper(node),
                    parentPath: getPathWrapper(parent)
                };
                if (mutationReport[data.id]) {
                    putFields(mutationReport[data.id], info);
                } else {
                    mutationReport[data.id] = info;
                }
            }

        });
        attributes.forEach(function (data) {
            var node = _this.deserializeNode(data);
            if (node) {
                Object.keys(data.attributes).forEach(function (attrName) {
                    var newVal = data.attributes[attrName];

                    if (newVal === null) {
                        node.removeAttribute(attrName);
                    } else {
                        try {
                            if (!_this.delegate || !_this.delegate.setAttribute || !_this.delegate.setAttribute(node, attrName, newVal)) {
                                node.setAttribute(attrName, newVal);
                            }
                        } catch (e) {
                            console.warn(e);
                        }
                    }

                    var info = {
                        nodePath: getPathWrapper(node)
                    };
                    if (mutationReport[data.id]) {
                        putFields(mutationReport[data.id], info);
                    } else {
                        mutationReport[data.id] = info;
                    }
                });
            }
        });
        text.forEach(function (data) {
            var node = _this.deserializeNode(data);
            if (node) {
                var info = {
                    text: data.textContent,
                    parentPath: getPathWrapper(data.parentNode)
                };

                node.textContent = data.textContent;

                if (mutationReport[data.id]) {
                    putFields(mutationReport[data.id], info);
                } else {
                    mutationReport[data.id] = info;
                }
            }
        });
        if (propertiesChanged && propertiesChanged.length > 0) {
            propertiesChanged.forEach(function (data) {
                var node = _this.deserializeNode(data);
                if (node) {
                    // for backward compatibility
                    if (data.properties === undefined) {
                        data.properties = {
                            value: data.value
                        };
                        delete data.value;
                    }

                    for (var key in data.properties) {
                        node[key] = data.properties[key];
                    }

                    var info = {
                        nodePath: getPathWrapper(node)
                    };
                    if (mutationReport[data.id]) {
                        putFields(mutationReport[data.id], info);
                    } else {
                        mutationReport[data.id] = info;
                    }
                }
            });
        }
        removed.forEach(function (data) {
            var node = _this.deserializeNode(data);
            if (node && node.parentNode) {
                node.parentNode.removeChild(node);
            }
            delete _this.idMap[data.id];
        });

        return mutationReport;
    };
    TreeMirror.prototype.deserializeNode = function (nodeData, parent) {
        var _this = this;
        if (nodeData === null)
            return null;
        var node = this.idMap[nodeData.id];
        if (node) {
            return node;
        }
        var doc = this.root.ownerDocument;
        if (doc === null)
            doc = this.root;
        var mustAppend = true;
        switch (nodeData.nodeType) {
            case Node.COMMENT_NODE:
                node = doc.createComment(nodeData.textContent);
                break;
            case Node.TEXT_NODE:
                node = doc.createTextNode(nodeData.textContent);
                break;
            case Node.DOCUMENT_TYPE_NODE:
                node = doc.doctype;
                mustAppend = false;
                break;
            case Node.ELEMENT_NODE:
                if (nodeData.tagName == "HTML" || nodeData.tagName == "HEAD" || nodeData.tagName == "BODY") {
                    var nodes = document.getElementsByTagName(nodeData.tagName);
                    if (nodes.length == 0) {
                        throw new Error(nodeData.tagName + " isn't found");
                    }
                    node = nodes[0];
                    mustAppend = false;
                } else {
                    if (this.delegate && this.delegate.createElement)
                        node = this.delegate.createElement(nodeData);
                    if (!node)
                        node = doc.createElement(nodeData.tagName);
                }

                if (nodeData.attributes) {
                    Object.keys(nodeData.attributes).forEach(function (name) {
                        try {
                            if (!_this.delegate || !_this.delegate.setAttribute || !_this.delegate.setAttribute(node, name, nodeData.attributes[name])) {
                                node.setAttribute(name, nodeData.attributes[name]);
                            }
                        } catch (e) {
                            console.warn(e);
                        }
                    });
                }
                break;
            case Node.DOCUMENT_FRAGMENT_NODE:
                if (this.delegate && this.delegate.createDocumentFragment)
                    node = this.delegate.createDocumentFragment();
                if (!node)
                    node = doc.createDocumentFragment();
                break;
        }
        if (!node) {
            console.error("TreeMirror can not create node", nodeData);
            return null;
        }
        this.idMap[nodeData.id] = node;
        if (nodeData.childNodes) {
            for (var i = 0; i < nodeData.childNodes.length; i++)
                this.deserializeNode(nodeData.childNodes[i], node);
        }
        if (parent && mustAppend)
            parent.appendChild(node);
        return node;
    };
    return TreeMirror;
})();