define(["jquery", "react", "app/react/play/Stacktrace", "app/utils/api", "jquery.cookie"], function ($, React, Stacktrace, api) {

    const IssueFilterForm = React.createClass({

        getInitialState: function () {
            return {
                filter: this.props.filter || {type: "", value: "", stacktrace: null},

                updating: false,
                updated: true,
                failed: false,
                errorMsg: null
            }
        },

        propTypes: {
            newFilter: React.PropTypes.bool,
            filter: React.PropTypes.object,

            onAdded: React.PropTypes.func,
            onUpdated: React.PropTypes.func,
            onDeleted: React.PropTypes.func
        },

        onFilterValueChange: function (e) {
            if (this.state.updating)
                return;

            let filter = this.state.filter;
            filter.value = e.target.value;
            this.setState({
                filter: filter,
                updated: false
            });
        },

        onFilterTypeChange: function (e) {
            if (this.state.updating)
                return;
            
            let filter = this.state.filter;
            filter.type = e.target.value;
            this.setState({
                filter: filter,
                updated: false
            });
        },

        addFilter: function (e) {
            e.preventDefault();

            if (this.state.updating)
                return;

            this.setState({
                updating: true,
                updated: false,
                failed: false,
                errorMsg: null
            });

            api("add_issue_filter", {
                userToken: $.cookie("userToken"),
                type: this.state.filter.type,
                value: this.state.filter.value
            }, (resp) => {
                let filter = this.state.filter;
                filter.id = resp.id;
                this.props.onAdded(filter);
                this.setState({
                    updated: false,
                    updating: false,
                    failed: false,
                    filter: {
                        type: filter.type,
                        value: ""
                    }
                });
            }, (errorMsg) => {
                this.setState({
                    failed: true,
                    updating: false,
                    errorMsg
                });
            });
        },

        updateFilter: function (e) {
            e.preventDefault();

            if (this.state.updating)
                return;

            this.setState({
                updating: true,
                updated: false,
                failed: false,
                errorMsg: null
            });

            api("update_issue_filter", {
                userToken: $.cookie("userToken"),
                filterId: this.state.filter.id,
                newType: this.state.filter.type,
                newValue: this.state.filter.value
            }, () => {
                this.setState({
                    updated: true,
                    updating: false,
                    failed: false
                });
                this.props.onUpdated(this.state.filter);
            }, (errorMsg) => {
                this.setState({
                    failed: true,
                    updating: false,
                    errorMsg
                });
            });
        },

        deleteFilter: function (e) {
            e.preventDefault();
            // TODO dk, vlad: ask user for confirmation?
            this.props.onDeleted(this.props.filter);
        },

        render: function () {
            const defaultFilterType = this.props.newFilter ? "" : this.props.filter.type;
            const defaultFilterValue = this.props.newFilter ? "" : this.props.filter.value;
            const isStacktraceFilter = this.state.filter.type === 'stacktraceHash';
            return (
                <form className={"tr issue-filter-form " + (this.state.failed ? " has-error" : "") 
                    + (this.props.newFilter? " new-issue-filter-form ": "")
                        }
                     onSubmit={this.props.newFilter ? this.addFilter : this.updateFilter}>


                    {this.state.failed ?
                        <label className="control-label">
                            {this.state.errorMsg ||
                            <span><strong>Failed</strong> Please change filter and try again</span>
                            }
                        </label>
                    : null}


                    <span className="td issue-filter-type">
                        {"stacktraceHash" === defaultFilterType ? <h3>Error stack</h3>: ""}
                        {"description" === defaultFilterType ? <h3>Error description</h3>: ""}
                        {"firstStacktraceFile" === defaultFilterType ? <h3>First caller script</h3>: ""}
                        {"lastStacktraceFile" === defaultFilterType ? <h3>Last caller script</h3>: ""}
                        <input type="hidden" name="filterType" value={defaultFilterType} />

                        {!this.props.newFilter ?
                            <button className="btn btn-default" 
                                onClick={this.deleteFilter} title="Delete filter">
                                <i className="fa fa-times"></i> Remove filter
                            </button>
                        : null}
                        
                    </span>

                    <div className="td issue-filter-content">
                        {this.props.newFilter?
                            <h2 style={{color: "#16a085"}}>Create new filter</h2>
                            :
                            ""
                        }

                        {isStacktraceFilter ?
                            <Stacktrace stacktrace={this.state.filter.stacktrace} />
                        :
                            <input type="text" className="form-control input-mono" defaultValue={defaultFilterValue}
                                value={this.state.filter.value} onChange={this.onFilterValueChange}
                                placeholder="Enter value regex. E.g &ldquo;^.* is undefined$&rdquo;"
                                />
                        }

                        <div className="issue-filter-buttons">

                            {!isStacktraceFilter ?
                                <button type="submit" className="btn btn-default btn-highlightable" disabled={this.state.updated} >
                                {this.state.updating ?
                                    <i className="fa fa-spin fa-spinner"></i>
                                    : this.props.newFilter ? 
                                        <span>
                                            <i className="fa fa-plus-square" aria-hidden="true"></i>
                                            Create filter
                                        </span>
                                        : 
                                        <span>
                                            <i className="fa fa-check" aria-hidden="true"></i>
                                            {" "}
                                            Update filter
                                        </span>
                                }
                                </button>
                            : null}
                        </div>
                    </div>
                </form>
            )
        }

    });

    const IssueFilterList = React.createClass({

        getInitialState: () => ({
            filters: []
        }),

        propTypes: {

        },

        onFilterAdded: function (filter) {
            let filters = this.state.filters;
            filters.push(filter);
            this.setState({filters});
        },

        onFilterUpdated: function (filter) {
            api("update_issue_filter", {
                userToken: $.cookie("userToken"),
                filterId: filter.id,
                newType: filter.type,
                newValue: filter.value
            }, () => {
                let filters = this.state.filters;
                for (let i = 0; i < filters.length; ++i) {
                    if (filters[i].id === filter.id) {
                        filters[i] = filter;
                    }
                }
                this.setState({
                    filters
                });
            });
        },

        onFilterDeleted: function (filter) {
            api("delete_issue_filter", {
                userToken: $.cookie("userToken"),
                filterId: filter.id
            }, () => {
                let filters = [];
                for (let i = 0; i < this.state.filters.length; ++i) {
                    if (this.state.filters[i].id !== filter.id) {
                        filters.push(this.state.filters[i]);
                    }
                }
                this.setState({
                    filters
                });
            });
        },

        render: function () {
            return (
                <div>
                    <h1>JavaScript error filters</h1>
                    <div className="table">
                        {this.state.filters.length == 0?  
                            <div>
                                <h3 style={{color: "#f39c12"}}>You have not created any filters yet</h3>
                            </div>
                            : ""
                        }
                        {this.state.filters.map((filter) => (
                            <IssueFilterForm key={filter.id} filter={filter}
                                            onUpdated={this.onFilterUpdated} onDeleted={this.onFilterDeleted}/>
                        ))}

                        <IssueFilterForm newFilter={true} onAdded={this.onFilterAdded}/>
                    </div>
                </div>
            )
        },

        componentDidMount: function () {
            api("get_issue_filters", {
                userToken: $.cookie("userToken")
            }, (resp) => {
                this.setState({
                    filters: resp.filters
                });
            });
        }

    });

    return IssueFilterList;

});