define(["jquery", "react", "app/utils/UrlUtils", "app/utils/api", "app/react/play/NativeListener",
    "app/react/integration/ZendeskTicketForm", "app/react/integration/JiraIssueForm", "jquery.ui", "jquery.cookie"],
    function ($, React,UrlUtils, api, NativeListener, ZendeskTicketForm, JiraIssueForm) {

    var ShareBox = React.createClass({displayName: "ShareBox",

        /* State declaration */
        getInitialState: function () {
            return {
                sharingState: "share",
                lastErrorMessage: undefined
            };
        },

        propTypes: {
            recordCode: React.PropTypes.string.isRequired,
            activeSequent: React.PropTypes.object
        },

        updateSharingState: function (state, error) {
            this.setState({
                sharingState: state,
                lastErrorMessage: error
            });
        },

        onShareRecord: function (e) {
            var $emailField = $(React.findDOMNode(this.refs.email));
            var recordCode = this.props.recordCode;
            var frameNum = this.props.activeSequent.frameNum;
            var sequentNum = this.props.activeSequent.sequentNum;

            var self = this;
            api("sendShareRecordEmail", {
                email: $emailField.val(),
                recordCode: recordCode,
                frameNum: frameNum,
                sequentNum: sequentNum
            }, function () {
                self.updateSharingState("shared");
                window.setTimeout(function () {
                    self.updateSharingState("share");
                }, 5000);
            }, function (error) {
                self.updateSharingState("error", error);
            });
            this.updateSharingState("sending");
            $(React.findDOMNode(this.refs.dropdownToggle)).dropdown("toggle");
            e.preventDefault();
        },

        onCreateTicket: function () {
            $(React.findDOMNode(this.refs.dropdownToggle)).dropdown("toggle");
        },

        render: function () {
            var zendeskEnabled = $.cookie("zendesk-enabled") === "true";
            var jiraEnabled = $.cookie("jira-enabled") === "true";
            return (
                React.createElement("div", {className: "btn-group", id: "shareRecordDropdown"}, 
                    React.createElement("button", {type: "button", className: "btn btn-default dropdown-toggle", id: "shareRecordBtn", 
                        "data-toggle": "dropdown", 
                        "aria-expanded": "false", title: "Share this record", ref: "dropdownToggle"}, 
                    
                        "sending" == this.state.sharingState ?
                            React.createElement("span", null, 
                                React.createElement("i", {className: "fa fa-spinner fa-spin"}), 
                                "Sending...") :
                            "shared" == this.state.sharingState ?
                                React.createElement("span", null, 
                                    React.createElement("i", {className: "fa fa-check"}), 
                                    "Shared!") :
                                "error" == this.state.sharingState ?
                                    React.createElement("span", {title: this.state.lastErrorMessage}, 
                                        React.createElement("i", {className: "fa fa-times-circle"}), 
                                        "Error!") :
                                    React.createElement("span", null, 
                                        React.createElement("i", {className: "fa fa-share-alt"}), 
                                    " Share "), 
                        
                        React.createElement("span", {className: "caret"})
                    ), 
                    React.createElement(NativeListener, {stopClick: true}, 
                        React.createElement("div", {className: "dropdown-menu", id: "shareRecordDropdownPanel", role: "menu"}, 
                            React.createElement("div", {className: "shareRecordText"}, "Send this record to"), 
                            React.createElement("form", {id: "shareRecordForm", onSubmit: this.onShareRecord}, 
                                React.createElement("div", {className: "form-group"}, 
                                    React.createElement("div", {className: "input-group"}, 
                                        React.createElement("input", {className: "form-control emailInput", id: "shareRecordEmailField", type: "email", 
                                            placeholder: "Email", ref: "email"}), 
                                        React.createElement("span", {className: "input-group-btn"}, 
                                            React.createElement("button", {className: "btn btn-default", id: "shareRecordSubmitBtn", type: "submit"}, 
                                                "Send record"
                                            )
                                        )
                                    )
                                )
                            ), 

                        zendeskEnabled ?
                            React.createElement(ZendeskTicketForm, {onCreate: this.onCreateTicket, updateSharingState: this.updateSharingState})
                            : null, 
                        jiraEnabled ?
                            React.createElement(JiraIssueForm, {onCreate: this.onCreateTicket, updateSharingState: this.updateSharingState})
                            : null
                        )
                    )
                )
            )
        }
    });

    return ShareBox;

});
