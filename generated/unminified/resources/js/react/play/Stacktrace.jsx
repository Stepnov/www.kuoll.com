define(["react"], function (React) {

    var Stacktrace = React.createClass({

        /* Props declaration */
        propTypes: {
            stacktrace: React.PropTypes.array.isRequired,
            description: React.PropTypes.string,
            method: React.PropTypes.string
        },

        methodInfo: {
            "window.setTimeout": "https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/setTimeout",
            "window.setInterval": "https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/setInterval",
            "window.clearTimeout": "https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/clearTimeout",
            "window.clearInterval": "https://developer.mozilla.org/en-US/docs/Web/API/WindowTimers/clearInterval"
        },

        render: function () {
            var methodInfo = this.methodInfo[this.props.method];
            return (
                <pre className="stacktrace" >
                {methodInfo ?
                    <div>
                        <a href={methodInfo} className="stacktrace-method" target="_blank">window.setTimeout</a>
                    </div>
                : null}

                    <div className="stacktrace-description">
                        {this.props.description}
                    </div>

                    <table className="stacktrace">
                        <tbody>
                    {this.props.stacktrace.map(function (stack, index) {
                        var uri = stack.uri[stack.uri.length - 1] != "/" ?
                            stack.uri.slice(stack.uri.lastIndexOf("/") + 1)
                            : stack.uri;
                        var shortenedUri = uri.slice(0, 100);
                        return (
                            <tr key={index} className="stacktrace-item ">
                                <td className="stacktrace-item-divider stacktrace-function">{"  at "}{stack.function}</td>
                                <td> </td>
                                <td title={stack.uri} className="textOverflowEllipsis stacktrace-item-source-file" >
                                    {uri}
                                </td>
                                <td className="stacktrace-item-source stacktrace-lineNum">{" "}{stack.line}</td>
                                <td>:</td>
                                <td className="stacktrace-item-source">{stack.columnNumber}</td>
                            </tr>
                        )
                    })}
                        </tbody>
                    </table>
                </pre>
            )
        }

    });

    return Stacktrace;

});