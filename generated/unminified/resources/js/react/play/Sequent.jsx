define(["jquery", "react", "app/react/play/SequentInfo", "app/utils/TipUtils", "app/utils/SequentUtils"], function ($, React,SequentInfo, TipUtils, SequentUtils) {

    var Sequent = React.createClass({

        /* Props declaration */
        propTypes: {
            frame: React.PropTypes.object.isRequired,
            sequent: React.PropTypes.object.isRequired,
            isHighlighted: React.PropTypes.bool.isRequired,
            isActive: React.PropTypes.bool.isRequired,
            onClickCallback: React.PropTypes.func.isRequired
        },

        render: function () {
            var sequent = this.props.sequent;
            var rendering = SequentInfo.findRendering(sequent);

            var classNames =
                rendering.getCssClasses()
                    .concat([
                        "sequentDiv",
                        "series-" + sequent.seriesNum,
                        (this.props.isHighlighted ? "series-highlighting" : ""),
                        (this.props.isActive ? "sequent-highlighting" : "")
                    ]).join(" ");
            var iconClass = rendering.getIconClass();
            if (rendering.getColor()) {
                var style = {
                    color: rendering.getColor()
                };
            }

            return (
                <div id={"sequent-" + this.props.sequent.sequentNum} className={classNames} style={style}>
                    <div className="sequentIconPad">
                        <i className={"fa " + iconClass}></i>
                    </div>
                </div>
            )
        },

        componentDidMount: function () {
            var $node = $(React.findDOMNode(this));
            var onClickCallback = this.props.onClickCallback;
            var sequent = this.props.sequent;

            $node.on("click", function () {
                if (onClickCallback) {
                    onClickCallback(sequent);
                }
            });
            $node.on("mouseover", function () {
                if ($node.qtip().id) return;

                var sequent = this.props.sequent;
                SequentUtils.parseRawData(sequent);
                TipUtils.makeSimpleTip($node, SequentInfo.getSequentTip(this.props.frame, sequent), true);
            }.bind(this));
        }

    });

    return Sequent;

});