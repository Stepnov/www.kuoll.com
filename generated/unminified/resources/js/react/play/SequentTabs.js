define(["react", "app/react/play/SequentInfo", "app/utils/SequentUtils", "bootstrap"],
    function (React, SequentInfo, SequentUtils) {

    function padWithLeadingZeros(string) {
        return new Array(5 - string.length).join("0") + string;
    }

    function unicodeCharEscape(charCode) {
        return "\\u" + padWithLeadingZeros(charCode.toString(16));
    }

    function unicodeEscape(string) {
        return string.split("")
            .map(function (char) {
                var charCode = char.charCodeAt(0);
                return charCode > 127 ? unicodeCharEscape(charCode) : char;
            })
            .join("");
    }

    var SequentTabs = React.createClass({displayName: "SequentTabs",

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {
            sequent: React.PropTypes.object,
            activeFrame: React.PropTypes.object,
            goToPrevSequent: React.PropTypes.func.isRequired,
            issue: React.PropTypes.object
        },

        goToPrevSequent: function (sequentNum) {
            if (typeof sequentNum != "number") {
                this.props.goToPrevSequent(this.props.sequent.previousSequentNum);
            } else {
                this.props.goToPrevSequent(sequentNum);
            }
        },

        render: function () {
            var previousSequentNum = this.props.sequent ? this.props.sequent.previousSequentNum : 0;
            var previousSequentExist = previousSequentNum != 0 &&
                SequentUtils.getSequentByNum(previousSequentNum, this.props.activeFrame) != null;

            return (
                React.createElement("div", {id: "sequentTabs"}, 

                    React.createElement("p", null, 
                        React.createElement(SequentInfo, {sequent: this.props.sequent, goToPrevSequent: this.goToPrevSequent, 
                            activeFrame: this.props.activeFrame})
                    ), 

                    React.createElement("p", null, 
                        React.createElement("span", {id: "previousSequentButton", onClick: this.goToPrevSequent, 
                            className: {display: previousSequentExist ? "" : "hidden"}, 
                            title: "Go to causing sequent"}, 
                            React.createElement("i", {className: "fa fa-arrow-left"})
                        ), 

                        React.createElement("a", {id: "sequent-json", style: {float: "right"}, 
                            title: "See the raw event JSON, in new tab", 
                           href: "data:application/json;base64," + btoa(unicodeEscape(JSON.stringify(this.props.sequent, null, 2))), 
                           target: "_blank"}, "Raw Event ", React.createElement("i", {className: "fa fa-external-link", "aria-hidden": "true"}))
                    )

                )
            )
        }

    });

    return SequentTabs;

});
