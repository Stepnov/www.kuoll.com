define(
    ["jquery", "react", "app/react/play/SequentInfo", "app/react/play/SnapshotSlider",
        "app/react/play/RecordInfo", "app/react/play/RecordPlayback", "app/react/play/SnapshotUrl", "app/react/play/Timeline",
        "app/react/play/Snapshot", "app/react/play/MetaDataUpdater", "app/utils/SequentUtils", "app/utils/SequentLoader",
        "app/utils/UrlUtils", "app/utils/SnapshotMessaging", "ua-parser", "app/utils/api"],
    function ($, React,SequentInfo, SnapshotSlider, RecordInfo, RecordPlayback, SnapshotUrl, Timeline,
              Snapshot, MetaDataUpdater, SequentUtils, SequentLoader, UrlUtils, SnapshotMessaging, UAParser, api) {

    var PlayContainer = React.createClass({displayName: "PlayContainer",

        /*State declaration*/
        getInitialState: function () {
            return {
                record: undefined,
                activeFrame: undefined,
                activeSeriesNum: undefined,
                activeSequent: undefined,
                frames: undefined,
                mutationSequents: [],
                mouseSequents: [],
                snapshotUrl: undefined,
                checkForNewSequents: false,
                interactiveMode: false,
            };
        },

        recordCode: undefined,
        lastViewedSequents: {},
        initialSequentNum: undefined,
        initialFrameNum: undefined,

        updateUrl: function (sequentNum, frameNum) {
            var search = "?recordCode=" + this.recordCode +
                (this.state.issue ? "&issueId=" + this.state.issue.id : "") +
                "&frameNum=" + (frameNum || this.state.activeFrame.frameNum) +
                "&sequentNum=" + (sequentNum || this.state.activeSequent.sequentNum) +
                (this.state.interactiveMode ? "&interactiveMode=true" : "");
            if (history.pushState) {
                var url = document.location.pathname + search;
                history.pushState("", "", url);
            } else {
                // for old IE
                location.hash = search;
            }
        },

        changeActiveSequent: function (sequentOrNum) {
            var sequent = typeof sequentOrNum == "object" ? sequentOrNum : SequentUtils.getSequentByNum(sequentOrNum, this.state.activeFrame);

            if (this.state.activeSequent && this.state.activeSequent.sequentNum == sequent.sequentNum
                && this.state.activeSequent.frameNum == sequent.frameNum) {
                return;
            }

            this.setState({
                activeSequent: sequent,
                activeSeriesNum: sequent.seriesNum
            }, function () {
                this.updateUrl();
                this.getSnapshotSlider().moveSlider(sequent);
                SequentLoader.scrollTo(sequent.sequentNum);
                this.lastViewedSequents[sequent.frameNum] = sequent.sequentNum;
                SnapshotMessaging.createMouseTrail(sequent.mouseTrail);
            });
        },

        changeActiveFrame: function (frameOrNum, sequentNum) {
            var frame;
            if (typeof frameOrNum == "object") {
                frame = frameOrNum;
            } else {
                frame = SequentUtils.getFrame(frameOrNum, this.state.frames);
            }

            if (this.state.activeFrame && this.state.activeFrame.frameNum === frame.frameNum) {
                var sequent = SequentUtils.getSequentByNum(sequentNum, this.state.activeFrame);
                if (sequent) {
                    this.changeActiveSequent(sequent);
                    return;
                }
            }

            if (typeof sequentNum === "undefined" || sequentNum === null) {
                sequentNum = this.lastViewedSequents[frame.frameNum];
            }

            var self = this;
            this.setState({
                activeFrame: frame
            }, function () {
                SequentLoader.updateActiveFrame({
                    frame: this.state.activeFrame,
                    sequentNum: sequentNum,
                    callback: function () {
                        if (!sequentNum) {
                            var sequents = frame.sequents;
                            sequentNum = sequents[0].sequentNum;
                        }
                        self.changeActiveSequent(sequentNum);
                    }
                });
            });
        },

        onSequentsInitiallyLoaded: function () {
            var sequents = this.state.activeFrame.sequents;
            var sequent, sequentNum;
            if (this.initialFrameNum == this.state.activeFrame.frameNum) {
                sequentNum = this.initialSequentNum;
                sequent = SequentUtils.getSequentByNum(sequentNum, this.state.activeFrame);
            }
            if (!sequent) {
                sequent = sequents[0];
            }
            if (!sequent) {
                throw new Error("Can not define initial sequent");
            }

            this.changeActiveSequent(sequent);
        },

        onNewSequentsLoaded: function (callback) {
            this.setState({
                activeFrame: this.state.activeFrame
            }, callback);
        },

        getSnapshot: function () {
            return this.refs.snapshot;
        },

        getSnapshotSlider: function () {
            return this.refs.snapshotSlider;
        },

        moveSlider: function () {
            return this.getSnapshotSlider().moveSlider.apply(this.getSnapshotSlider(), Array.prototype.slice.call(arguments));
        },

        getTimeline: function () {
            return this.refs.timeline;
        },

        loadRecordData: function (recordCode, issueId, onLoadCallback) {
            var self = this;
            api("meta", {
                recordCode: recordCode,
                issueId: issueId
            }, function (resp) {
                var frames = resp.frames;
                if (!frames || !frames.length) {
                    self.setState({
                        frames: []
                    });
                } else {
                    onLoadCallback(resp);
                }
            });
        },

        defineInitialFrameAndSequent: function (frames, issue) {
            var frameNumParam = UrlUtils.getParameterByName("frameNum");
            var sequentNumParam = UrlUtils.getParameterByName("sequentNum");
            var frameNum, sequentNum;

            if (frameNumParam && sequentNumParam) {
                frameNum = frames[0].frameNum;
                sequentNum = 1;

                for (var i = 0; i < frames.length; ++i) {
                    if (frames[i].frameNum > parseInt(frameNumParam)) {
                        frameNum = frames[i].frameNum;
                        sequentNum = 1;
                        break;
                    } else if (frames[i].frameNum === parseInt(frameNumParam)) {
                        frameNum = frames[i].frameNum;
                        sequentNum = parseInt(sequentNumParam);
                        break;
                    }
                }
            } else if (issue) {
                frameNum = issue.endFrameNum;
                if (SequentUtils.getFrame(frameNum, frames) !== null) {
                    sequentNum = issue.endSequentNum;
                } else {
                    frameNum = frames[frames.length - 1].frameNum;
                    sequentNum = 1;
                }
            } else if (this.state.mutationSequents.length) {
                var firstSnapshot = this.state.mutationSequents[0];
                frameNum = firstSnapshot.frameNum;
                sequentNum = firstSnapshot.sequentNum;
            } else {
                var firstFrame = frames[0];
                frameNum = firstFrame.frameNum;
                sequentNum = firstFrame.sequents ? firstFrame.sequents[0].sequentNum : 1;
            }
            return {
                frameNum: frameNum,
                sequentNum: sequentNum
            }
        },

        onSnapshotSliderMovedCallback: function (newSnapshotIndex) {
            if (!this.state.mutationSequents) return;

            var snapshotSequent = this.state.mutationSequents[newSnapshotIndex];
            var sequentNum = snapshotSequent.sequentNum;
            var frame = SequentUtils.getFrame(snapshotSequent.frameNum, this.state.frames);
            var sequentIsLoaded = frame && frame.sequents && sequentNum <= frame.sequents[frame.sequents.length - 1].sequentNum;
            if (snapshotSequent.frameNum === this.state.activeFrame.frameNum && sequentIsLoaded) {
                var sequent = SequentUtils.getSequentByNum(sequentNum, frame);
                this.setState({
                    activeSeriesNum: sequent.seriesNum
                });
            } else {
                this.setState({
                    activeSeriesNum: 0
                })
            }
        },

        onSnapshotUpdated: function (snapshotUrl) {
            this.setState({
                snapshotUrl: snapshotUrl
            });
        },

        render: function () {
            if (!this.state || !this.state.frames) {
                return (
                    React.createElement("div", {id: "main-caption-container", style: {textAlign: "center"}}, 
                        React.createElement("h1", null, "Welcome to the ", React.createElement("br", null), 
                            "Kuoll record play page"), 
                        React.createElement("p", null, " "), 
                        React.createElement("p", null, " "), 
                        React.createElement("h2", {id: "main-caption"}, 
                            React.createElement("i", {className: "fa fa-spinner fa-pulse"}), " Loading the record..."
                        ), 
                        React.createElement("p", null

                        )
                    )
                )
            }

            if (this.state.frames.length === 0) {
                return (
                    React.createElement("div", {className: "panel panel-warning error-msg"}, 
                        React.createElement("div", {className: "panel-heading"}, "Hello, the record is empty right now"), 
                        React.createElement("div", {className: "panel-content"}, "It may take up to 5 seconds to upload the user session record to the server.", React.createElement("br", null), 
                            "Please, ensure there is a call of ", React.createElement("a", {href: "/quick-start.html#create-issue"}, React.createElement("code", null, "kuoll(\"createIssue\", params)")), " to persist the record of user session."
                        )
                    )
                )
            }

            return (
                React.createElement("div", {className: "playContainer"}, 

                    React.createElement("div", {id: "headerBox"}, 

                        React.createElement("div", {id: "header-bottom"}, 
                            React.createElement("a", {href: "/records.html", id: "recordsLink", 
                               title: "Your records"}, 
                                React.createElement("img", {src: "/resources/img/cat.png", style: {height: "36px"}})
                            ), 

                            React.createElement(SnapshotUrl, {snapshotUrl: this.state.snapshotUrl, sequent: this.state.activeSequent, 
                                frame: this.state.activeFrame, record: this.state.record}), 

                            React.createElement("span", {id: "record-info-wrapper", style: {marginRight: "8px"}}, 
                                React.createElement("span", {id: "info-icon-wrapper"}, 
                                    React.createElement("span", {className: "wistia_embed wistia_async_21owcwity4 popover=true popoverContent=link", 
                                          style: {display: "inline"}, title: "Explanaitional demo video"}, 
                                        React.createElement("a", {href: "#"}, 
                                            React.createElement("i", {className: "fa fa-question-circle"})
                                        )
                                    )
                                )
                            ), 

                            React.createElement("div", {id: "loading-spinner", style: {visibility: "hidden"}}, 
                                React.createElement("i", {className: "fa fa-spinner fa-pulse"})
                            ), 

                            React.createElement(RecordPlayback, {moveSlider: this.moveSlider, changeFrame: this.changeActiveFrame, 
                                            activeFrame: this.state.activeFrame, activeSequent: this.state.activeSequent, 
                                            frames: this.state.frames, mutationSequents: this.state.mutationSequents}), 

                            React.createElement(SnapshotSlider, {onSliderMoved: this.onSnapshotSliderMovedCallback, getSnapshot: this.getSnapshot, 
                                            mutationSequents: this.state.mutationSequents, ref: "snapshotSlider", 
                                            mouseSequents: this.state.mouseSequents, changeActiveFrame: this.changeActiveFrame, 
                                            frames: this.state.frames})
                        )

                    ), 

                    React.createElement(Timeline, {ref: "timeline", frames: this.state.frames, 
                        activeFrame: this.state.activeFrame, issue: this.state.issue, 
                        onChangeActiveFrame: this.changeActiveFrame, onChangeActiveSequent: this.changeActiveSequent, 
                        activeSeriesNum: this.state.activeSeriesNum, activeSequent: this.state.activeSequent}), 

                    React.createElement(Snapshot, {mutationSequents: this.state.mutationSequents, 
                        getSlider: this.getSnapshotSlider, recordCode: this.recordCode, 
                        activeSequent: this.state.activeSequent, activeFrame: this.state.activeFrame, 
                        onSnapshotUpdated: this.onSnapshotUpdated, 
                        frames: this.state.frames, issue: this.state.issue, ref: "snapshot"}), 

                    React.createElement(MetaDataUpdater, {recordStatus: this.state.record.status, recordCode: this.recordCode, 
                        lastFrame: this.state.frames[this.state.frames.length - 1], issue: this.state.issue, 
                        onDataLoaded: this.onNewMetaDataLoaded, totalMutationSequents: this.state.mutationSequents.length, 
                        totalMouseSequents: this.state.mouseSequents.length, 
                        interactiveMode: this.state.interactiveMode, changeActiveSequent: this.changeActiveSequent})

                )
            )
        },

        parseUserAgent: function (uaString) {
            var uaParser = new UAParser(uaString);
            return uaParser.getResult();
        },

        filterEmptyFrames: function (frames) {
            return frames.filter(function (frame) {
                return !!frame.totalSequents;
            });
        },

        preprocessMutationSequents: function (mutationSequents, frames) {
            // treat baseSnapshots as mutationSequent with sequentNum=0 for more convenient processing
            var frameIndex = 0;
            for (var i = 0; frameIndex < frames.length; ++i) {
                var frame = frames[frameIndex];
                if (!mutationSequents[i] || mutationSequents[i].frameNum === frame.frameNum) {
                    mutationSequents.splice(i, 0, { // insert a synthetic mutationSequent before first sequent of the frame
                        frameNum: frame.frameNum,
                        sequentNum: 0,
                        time: frame.startTime
                    });
                    ++frameIndex;
                }
            }
            return mutationSequents;
        },

        onNewMetaDataLoaded: function (data) {
            /* TODO vlad: Extract meta data preprocessing somewhere for better isolation and performance
            * (we make unnecessary state updates now)
            */

            var newFrames = this.filterEmptyFrames(data.frames);
            var currentFrames = this.state.frames;
            if (newFrames.length >= 1) {
                var lastFrame = currentFrames.pop();
                currentFrames = currentFrames.concat(newFrames);
                currentFrames[currentFrames.length - newFrames.length].sequents = lastFrame.sequents;

                this.setState({
                    activeFrame: SequentUtils.getFrame(this.state.activeFrame.frameNum, currentFrames),
                    frames: currentFrames
                })
            }
            var record = data.record;
            if (record.recordInfo && record.recordInfo.userAgent) {
                record.recordInfo.userAgent = this.parseUserAgent(record.recordInfo.userAgent);
            }
            this.setState({
                record: record,
                mutationSequents: this.preprocessMutationSequents(data.mutationSequents, currentFrames),
                mouseSequents: data.mouseSequents,
                frames: currentFrames
            })
        },

        componentWillMount: function () {
            if (document.location.protocol === "https:") {
                if (!$.cookie("redirected")) {
                    $.cookie("redirected", 1, {domain: window.config.cookieDomain, path: "/"}); // FIXME @Vlad change cookie to URL param
                    document.location.href = document.location.href.replace("https:", "http:");
                    return;
                }
            }
            $.removeCookie('redirected', { path: '/' });

            var recordCode = UrlUtils.getParameterByName("recordCode");
            var issueId = UrlUtils.getParameterByName("issueId");

            var interactiveMode = UrlUtils.getParameterByName("interactiveMode");

            this.loadRecordData(recordCode, issueId, function (data) {
                var frames = this.filterEmptyFrames(data.frames);

                if (!frames.length) {
                    this.setState({
                        frames: frames
                    });
                    return;
                }

                var init = this.defineInitialFrameAndSequent(frames, data.issue);

                var mutationSequents = this.preprocessMutationSequents(data.mutationSequents, frames);
                var mouseSequents = data.mouseSequents;

                var record = data.record;
                if (record.recordInfo && record.recordInfo.userAgent) {
                    record.recordInfo.userAgent = this.parseUserAgent(record.recordInfo.userAgent);
                }

                var initFrame = SequentUtils.getFrame(init.frameNum, frames);
                this.initialSequentNum = init.sequentNum;
                this.initialFrameNum = init.frameNum;
                this.recordCode = recordCode;
                this.setState({
                    record: record,
                    frames: frames,
                    activeSequent: SequentUtils.getSequentByNum(init.sequentNum, initFrame),
                    activeFrame: initFrame,
                    mutationSequents: mutationSequents,
                    mouseSequents: mouseSequents,
                    issue: data.issue,

                    interactiveMode: (interactiveMode == "true")
                }, function () {
                    this.updateUrl(this.initialSequentNum, this.initialFrameNum);
                    SequentLoader.init(recordCode, this.state.activeFrame, init.sequentNum, this.state.issue,
                        this.onSequentsInitiallyLoaded, this.onNewSequentsLoaded, this.getTimeline);
                });
            }.bind(this));
        }

    });

    return PlayContainer;

});
