define(["jquery", "react", "app/react/play/Frame"], function ($, React,Frame) {

    var FrameLine = React.createClass({displayName: "FrameLine",

        /* Props declaration */
        propTypes: {
            frames: React.PropTypes.array.isRequired,
            onFrameClick: React.PropTypes.func.isRequired,
            activeFrameNum: React.PropTypes.number.isRequired
        },

        render: function () {
            var activeFrameNum = this.props.activeFrameNum;
            var self = this;
            return (
                React.createElement("div", {className: "frameLine", id: "frameLine"}, 
                this.props.frames.map(function(frame) {
                    return (
                        React.createElement(Frame, {isActive: frame.frameNum === activeFrameNum, frameNum: frame.frameNum, 
                            key: frame.frameNum, onClick: self.props.onFrameClick})
                    )
                })
                )
            )
        }

    });

    return FrameLine;

});
