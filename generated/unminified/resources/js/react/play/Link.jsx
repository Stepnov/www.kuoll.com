define(["react"], function (React) {

    var Link = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {

            };
        },

        /* Props declaration */
        propTypes: {
            href: React.PropTypes.string.isRequired,
            children: React.PropTypes.string
        },

        highlightUrl: function (url) {
            var regex = /\/\/([^/:]+)(.*)/;
            var split = regex.exec(url);
            return (
                <span className="url">
                    <span className="host">{split[1]}</span>
                    <span className="path">{split[2]}</span>
                </span>
            );
        },

        render: function () {
            return (
                <a {...this.props}>
                {this.highlightUrl(this.props.children || this.props.href)}
                </a>
            )
        }

    });

    return Link;

});
