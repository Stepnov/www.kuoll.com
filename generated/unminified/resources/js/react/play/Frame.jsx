define(["react"], function (React) {

    var Frame = React.createClass({

        /* Props declaration */
        propTypes: {
            isActive: React.PropTypes.bool.isRequired,
            onClick: React.PropTypes.func.isRequired,
            frameNum: React.PropTypes.number.isRequired
        },

        render: function () {
            var isActive = this.props.isActive;
            var onClick = function () {
                this.props.onClick(this.props.frameNum);
            }.bind(this);
            return (
                <div className={"frameDiv" + (isActive ? " activeFrameDiv" : "")} onClick={onClick}><i
                    className="fa fa-angle-right"></i></div>
            )
        }

    });

    return Frame;

});
