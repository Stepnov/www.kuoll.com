define(["jquery", "react", "app/utils/api", "app/User", "ua-parser",
    "app/react/records/RecordsListSettings", "app/react/records/Record", "jquery.cookie"],
    function ($, React,api, User, UAParser, RecordsListSettings, Record) {

    var initialLoadingStarted = false;

    var RecordsContainer = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                settings: undefined,

                loading: 0,

                ownRecordsLoaded: false,
                teamRecordsLoaded: false,

                records: []
            };
        },

        render: function () {
            var records = this.getRecordsToShow();
            var self = this;
            return (
                <div>
            {initialLoadingStarted && this.state.loading && !this.state.records.length ? null :
                <RecordsListSettings onSettingsChanged={this.onNewSettings} />
                }
            {!this.state.settings ? null :
                records.length === 0 ?
                    <div className="records-loading-wrapper">
                    {!initialLoadingStarted || this.state.loading === 0 ? [
                        <p>Loading your records..</p>,
                        <i className="fa fa-spinner fa-spin" aria-hidden="true"></i>
                    ] : this.state.records.length ?
                        <p>No records satisfy your criteria</p>
                        :
                        <div className="container">
                            <div className="row">
                                <div className="col-md-4 col-md-offset-2 ">
                                    <div className="">
                                        <h3>1. Add Kuoll to Your Web App</h3>
                                        <p>
                                            Check <a href="/quick-start.html">Kuoll API documentation
                                        </a> to install single line of script to your website.</p>
                                        <p>This option allows web application customers create Kuoll records.</p>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div>
                                        <h3> 2. Install Chrome Extension</h3>
                                        <div>
                                            Alternatively, you can make Kuoll record of <strong>any
                                        </strong> web site using <strong>free</strong> <a
                                            href="https://chrome.google.com/webstore/detail/kuoll/ljfpdodjbnkpbaenppoiifjkdlhfjcgn">Kuoll
                                            extension for Chrome</a>.
                                            <p>
                                                <a href="https://chrome.google.com/webstore/detail/kuoll/ljfpdodjbnkpbaenppoiifjkdlhfjcgn">
                                                    <img src="/resources/img/ChromeWebStore_BadgeWBorder_v2_206x58.png"/>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        }
                    </div>
                    :
                    records.map(function (record) {
                        return (
                            <Record record={record} key={record.recordCode}
                                onRecordDeleted={self.onRecordDeleted} onRecordRestored={self.onRecordRestored} />
                        )
                    })
                }
                </div>
            )
        },

        onNewSettings: function (settings) {
            if (settings.showTeam && !this.state.teamRecordsLoaded) {
                this.loadTeamRecords();
            }
            if (settings.showOwn && !this.state.ownRecordsLoaded) {
                this.loadOwnRecords();
            }
            if (this.state.settings && settings.order !== this.state.settings.order) {
                this.setState({
                    records: this.state.records.sort(function (a, b) {
                        if (self.state.settings.order === "lastIssue") {
                            return b.lastIssueTime - a.lastIssueTime;
                        } else {
                            return b.startTime - a.startTime;
                        }
                    })
                });
            }
            this.setState({ settings: settings });
        },

        onNewRecordsLoaded: function (records, isTeamRecord) {
            var self = this;
            records.forEach(function (record) {
                if (isTeamRecord) {
                    record.team = true;
                } else {
                    record.own = true;
                }
                if (record.recordInfo && record.recordInfo.userAgent) {
                    record.recordInfo.userAgent = new UAParser(record.recordInfo.userAgent).getResult();
                }
            });
            records = this.state.records
                .concat(records)
                .sort(function (a, b) {
                    if (self.state.settings.order === "lastIssue") {
                        return b.lastIssueTime - a.lastIssueTime;
                    } else {
                        return b.startTime - a.startTime;
                    }
                });
            this.setState({
                records: records,
                loading: this.state.loading - 1
            })
        },

        loadTeamRecords: function () {
            this.setState({
                teamRecordsLoaded: true,
                loading: this.state.loading + 1
            });

            var self = this;
            initialLoadingStarted = true;
            User.getInfo(function (user) {
                api("getOrgRecords", {
                    orgId: user.orgId
                }, function (resp) {
                    self.onNewRecordsLoaded(resp.records.filter(function (record) {
                        return record.userId !== user.userId;
                    }), true);
                });
            })
        },

        loadOwnRecords: function () {
            this.setState({
                ownRecordsLoaded: true,
                loading: this.state.loading + 1
            });

            var self = this;
            initialLoadingStarted = true;
            api("getRecords", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                self.onNewRecordsLoaded(resp.records, false);
            });
        },

        onRecordDeleted: function (recordCode) {
            api("api/deleteRecord", {
                recordCode: recordCode
            });
            var records = this.state.records;
            records.forEach(function (record) {
                if (record.recordCode === recordCode) {
                    record.deleted = true;
                }
            });
            this.setState({
                records: records
            });
        },

        onRecordRestored: function (recordCode) {
            api("restoreRecord", {
                recordCode: recordCode
            });
            var records = this.state.records;
            records.forEach(function (record) {
                if (record.recordCode === recordCode) {
                    record.deleted = false;
                }
            });
            this.setState({
                records: records
            });
        },

        /**
         * Filters loaded records and returns only those that must be shown now based on user settings
         */
        getRecordsToShow: function () {
            var settings = this.state.settings;
            return this.state.records.filter(function (record) {
                if (record.own && !settings.showOwn) {
                    return false;
                }
                if (record.team && !settings.showTeam) {
                    return false;
                }
                if (settings.userIdFilter && record.externalUserId.indexOf(settings.userIdFilter) === -1) {
                    return false;
                }
                if (record.issuesCount === 0 && !settings.showRecordsWithoutIssues) {
                    return false;
                }
                return true;
            });
        }

    });

    return RecordsContainer;

});