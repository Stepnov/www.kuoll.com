define(["jquery", "react", "app/react/LinkedStateMixin"], function ($, React,LinkedStateMixin) {

    var RecordsListSettings = React.createClass({displayName: "RecordsListSettings",

        mixins: [LinkedStateMixin],

        getInitialState: function () {
            return {
                settings: {
                    showOwn: true,
                    showTeam: true,
                    userIdFilter: "",
                    order: "record",
                    showRecordsWithoutIssues: true
                },
                order: {
                    issue: false,
                    record: true
                }
            }
        },

        /* Props declaration */
        propTypes: {
            onSettingsChanged: React.PropTypes.func.isRequired
        },

        render: function () {
            return (
                React.createElement("div", {className: "settings-wrapper row"}, 
                    React.createElement("div", {className: "col-xs-12 col-sm-4"}, 
                        React.createElement("div", {className: "settings-group row"}, 
                            React.createElement("p", null, "Records to show:"), 
                            React.createElement("label", {className: "col-xs-4 col-sm-12"}, 
                                React.createElement("input", {type: "checkbox", checkedLink: this.linkState("settings.showOwn", this.onSettingsChanged)}), 
                                "Own"
                            ), 
                            React.createElement("label", {className: "col-xs-4 col-sm-12"}, 
                                React.createElement("input", {type: "checkbox", checkedLink: this.linkState("settings.showTeam", this.onSettingsChanged)}), 
                                "Team"
                            ), 
                            React.createElement("label", {className: "horizontal-line col-xs-4 col-sm-12"}, 
                                React.createElement("input", {type: "checkbox", ref: "showRecordsWithoutIssues", 
                                    checkedLink: this.linkState("settings.showRecordsWithoutIssues", this.onSettingsChanged)}), 
                                "Show records without issues"
                            )
                        )
                    ), 

                    React.createElement("div", {className: "col-xs-12 col-sm-4"}, 
                        React.createElement("div", {className: "settings-group"}, 
                            React.createElement("p", null, "Filter by:"), 
                            React.createElement("label", null, 
                                "User ID", 
                                React.createElement("input", {type: "text", className: "form-control", valueLink: this.linkState("settings.userIdFilter", this.onSettingsChanged)})
                            )
                        )
                    ), 

                    React.createElement("div", {className: "col-xs-12 col-sm-4"}, 
                        React.createElement("div", {className: "settings-group"}, 
                            React.createElement("p", null, "Order by:"), 
                            React.createElement("form", {ref: "orderForm", className: "row", checkedLink: this.linkState("order", this.onSettingsChanged)}, 
                                React.createElement("label", {className: "col-xs-6 col-sm-12"}, 
                                    React.createElement("input", {type: "radio", name: "orderBtn", value: "issue"}), 
                                    "last issue time"
                                ), 
                                React.createElement("label", {className: "col-xs-6 col-sm-12"}, 
                                    React.createElement("input", {type: "radio", name: "orderBtn", value: "record"}), 
                                    "record time"
                                )
                            )
                        )
                    )
                )
            )
        },

        onSettingsChanged: function () {
            var settings = {
                showOwn: this.state.settings.showOwn,
                showTeam: this.state.settings.showTeam,
                userIdFilter: this.state.settings.userIdFilter,
                order: this.state.order.issue ? "lastIssue" : "record",
                showRecordsWithoutIssues: this.state.settings.showRecordsWithoutIssues
            };
            this.props.onSettingsChanged(settings);
        },

        componentDidMount: function () {
            this.onSettingsChanged();
        }

    });

    return RecordsListSettings;

});
