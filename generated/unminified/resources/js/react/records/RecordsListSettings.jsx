define(["jquery", "react", "app/react/LinkedStateMixin"], function ($, React,LinkedStateMixin) {

    var RecordsListSettings = React.createClass({

        mixins: [LinkedStateMixin],

        getInitialState: function () {
            return {
                settings: {
                    showOwn: true,
                    showTeam: true,
                    userIdFilter: "",
                    order: "record",
                    showRecordsWithoutIssues: true
                },
                order: {
                    issue: false,
                    record: true
                }
            }
        },

        /* Props declaration */
        propTypes: {
            onSettingsChanged: React.PropTypes.func.isRequired
        },

        render: function () {
            return (
                <div className="settings-wrapper row">
                    <div className="col-xs-12 col-sm-4">
                        <div className="settings-group row">
                            <p>Records to show:</p>
                            <label className="col-xs-4 col-sm-12">
                                <input type="checkbox" checkedLink={this.linkState("settings.showOwn", this.onSettingsChanged)}/>
                                Own
                            </label>
                            <label className="col-xs-4 col-sm-12">
                                <input type="checkbox" checkedLink={this.linkState("settings.showTeam", this.onSettingsChanged)}/>
                                Team
                            </label>
                            <label className="horizontal-line col-xs-4 col-sm-12">
                                <input type="checkbox" ref="showRecordsWithoutIssues"
                                    checkedLink={this.linkState("settings.showRecordsWithoutIssues", this.onSettingsChanged)}/>
                                Show records without issues
                            </label>
                        </div>
                    </div>

                    <div className="col-xs-12 col-sm-4">
                        <div className="settings-group">
                            <p>Filter by:</p>
                            <label>
                                User ID
                                <input type="text" className="form-control" valueLink={this.linkState("settings.userIdFilter", this.onSettingsChanged)}/>
                            </label>
                        </div>
                    </div>

                    <div className="col-xs-12 col-sm-4">
                        <div className="settings-group">
                            <p>Order by:</p>
                            <form ref="orderForm" className="row"  checkedLink={this.linkState("order", this.onSettingsChanged)}>
                                <label className="col-xs-6 col-sm-12">
                                    <input type="radio" name="orderBtn" value="issue"/>
                                    last issue time
                                </label>
                                <label className="col-xs-6 col-sm-12">
                                    <input type="radio" name="orderBtn" value="record" />
                                    record time
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
            )
        },

        onSettingsChanged: function () {
            var settings = {
                showOwn: this.state.settings.showOwn,
                showTeam: this.state.settings.showTeam,
                userIdFilter: this.state.settings.userIdFilter,
                order: this.state.order.issue ? "lastIssue" : "record",
                showRecordsWithoutIssues: this.state.settings.showRecordsWithoutIssues
            };
            this.props.onSettingsChanged(settings);
        },

        componentDidMount: function () {
            this.onSettingsChanged();
        }

    });

    return RecordsListSettings;

});
