define(["jquery", "react", "app/react/play/Stacktrace", "app/react/BrowserIcon", "app/react/ConversionSteps", "app/react/OsIcon",
        "app/react/DeviceIcon", "app/react/play/LongText", "app/react/issue-filter/IssueFilterDropdown",
        "app/utils/api", "app/utils/DateUtils", "app/utils/UrlUtils", "ua-parser", "jquery.cookie"],
    function ($, React, Stacktrace, BrowserIcon, ConversionSteps, OsIcon, DeviceIcon, LongText, IssueFilterDropdown, api, DateUtils, UrlUtils, UAParser) {

    var IssuesDashboard = React.createClass({displayName: "IssuesDashboard",

        /* State declaration */
        getInitialState: function () {
            return {
                issuesGroups: []
            };
        },

        /* Props declaration */
        propTypes: {

        },

        render: function () {
            return (
                React.createElement("div", null, 
                React.createElement("h2", {style: {marginBottom: "30px"}}, 
                    UrlUtils.getParameterByName("type") === "xhr" ? "Server" : UrlUtils.getParameterByName("type") === "console" ? "Console" : "JS", " errors for the last 2 days"
                ), 
                React.createElement("p", null, 
                    "Use filters" + ' ' + 
                    "(", React.createElement("i", {className: "fa fa-filter", style: {color: "#2ecc71"}, "aria-hidden": "true"}), ")" + ' ' +
                    "to hide errors from this page.",  
                    " ", 
                    "Manage active filters on set ", React.createElement("a", {href: "/errorFilters.html"}, "Error filters"), " page."
                
                ), 

                React.createElement("table", {className: "table table-hover"}, 
                    React.createElement("thead", null, 
                    React.createElement("th", null, "Occurrences"), 
                    React.createElement("th", null, "Platform"), 
                    React.createElement("th", {title: "When error happended"}, "Since"), 
                    React.createElement("th", null, "Latest"), 
                    React.createElement("th", null, "Stack trace"), 
                    React.createElement("th", {title: "Hide specific errors"}, "Filter")
                    ), 
                    React.createElement("tbody", null, 
                    this.state.issuesGroups.map(function (group, index) {
                        const firstIssue = group.firstIssue;
                        return (
                            React.createElement("tr", {key: index}, 
                                React.createElement("td", {className: "centerText"}, 
                                    React.createElement("p", null, 
                                        React.createElement("a", {href: "/issues-group.html?id=" + group.id, target: "_blank", 
                                            title: "See all records for this issue"
                                            }, 
                                                React.createElement("span", {className: "issuesNumberInGroup"}, group.issuesCount
                                                )
                                        )
                                    ), 
                                    React.createElement("p", null, 
                                        React.createElement("a", {href: "/play.html?recordCode=" + firstIssue.recordCode +
                                            "&issueId=" + firstIssue.id, target: "_blank", 
                                           title: "Recorded " + DateUtils.formatDate(firstIssue.time)}, 
                                            "Example" 
                                            
                                        )
                                    )
                                ), 
                                React.createElement("td", null, 
                                    React.createElement("span", {className: "browser-icons"}, 
                                        Object.keys(group.platforms.browser).map(function (browser) {
                                            return (
                                                React.createElement(BrowserIcon, {userAgent: {browser: group.platforms.browser[browser]}, 
                                                             withVersion: false})
                                            )
                                        })
                                    ), 
                                    React.createElement("span", {className: "os-icons"}, 
                                        Object.keys(group.platforms.os).map(function (os) {
                                            return (
                                                React.createElement(OsIcon, {userAgent: {os: group.platforms.os[os]}})
                                            )
                                        })
                                    ), 
                                    React.createElement("span", {className: "device-icons"}, 
                                        Object.keys(group.platforms.device).map(function (device) {
                                            return (
                                                React.createElement(DeviceIcon, {userAgent: {device: group.platforms.device[device]}})
                                            )
                                        })
                                    )
                                ), 
                                React.createElement("td", {className: "first-occurrence"}, 
                                    DateUtils.formatDate(group.firstOccurrence)
                                ), 
                                React.createElement("td", {className: "last-occurrence"}, 
                                    DateUtils.formatDate(group.lastOccurrence)
                                ), 
                                React.createElement("td", {style: {maxWidth: "50%"}}, 
                                    React.createElement(Stacktrace, {stacktrace: group.stacktrace, description: group.description})
                                ), 
                                React.createElement("td", null, 
                                    React.createElement(IssueFilterDropdown, {issue: firstIssue})
                                )
                            )
                        )
                    })
                    )
                ), 

                this.state.issuesGroups.length === 0 ? 
                    React.createElement("div", {className: "jumbo"}, 
                        React.createElement("p", null, 
                            "No errors caught yet."
                        ), 
                        React.createElement("h2", {style: {color: "#8e44ad"}}, "Install Kuoll to your web application to see error reports"), 
                        React.createElement("p", null, 
                            React.createElement("a", {href: "#onboard", className: "btn btn-lg btn-success"}, "Install")
                        )

                    )
                :""

                )
            )
        },

        componentDidMount: function () {
            const self = this;
            const type = UrlUtils.getParameterByName("type") === "xhr" ? "Server error" :
                UrlUtils.getParameterByName("type") === "console" ? "consoleError" : "JavaScript error";
            api("get_issues_groups", {
                userToken: $.cookie("userToken"),
                type: type
            }, function (resp) {
                const issuesGroups = resp.issuesGroups;
                issuesGroups.forEach(function (group) {
                    const platforms = {
                        browser: {},
                        os: {},
                        device: {}
                    };
                    group.userAgents.forEach(function (ua) {
                        const result = new UAParser(ua).getResult();
                        if (result.browser)
                            platforms.browser[result.browser.name] = result.browser;
                        if (result.os)
                            platforms.os[result.os.name] = result.os;
                        if (result.device)
                            platforms.device[result.device.vendor] = result.device;
                    });
                    group.platforms = platforms;
                    delete group.userAgents;
                });
                self.setState({
                    issuesGroups: issuesGroups
                })
            });
        }

    });

    return IssuesDashboard;

});