define(["jquery", "react", "app/react/play/Stacktrace", "app/react/BrowserIcon", "app/react/LossStep", "app/react/OsIcon",
        "app/react/DeviceIcon", "app/react/play/LongText", "app/react/issue-filter/IssueFilterDropdown",
        "app/utils/api", "app/utils/DateUtils", "app/utils/UrlUtils", "ua-parser", "jquery.cookie"],
    function ($, React, Stacktrace, BrowserIcon, LossStep, OsIcon, DeviceIcon, LongText, IssueFilterDropdown, api, DateUtils, UrlUtils, UAParser) {

    function calculateStepsStatistics(row) {
        const step = {
            loadStepLoss: 0,
            loadStepConvDrop: 0,
            loadStepAffectedPct: 0,

            addStepLoss: 0,
            addStepConvDrop: 0,
            addStepAffectedPct: 0,

            checkoutStepLoss: 0,
            checkoutStepConvDrop: 0,
            checkoutStepAffectedPct: 0,

            affectedPercent: 0,
            conversionDropPercent: 0,

            convSaved: 1
        };

        row.stages.forEach(function (stage) {
            if (
                "load" === stage.stepPair.startStep &&
                "add" === stage.stepPair.endStep
            ) {
                step.loadStepLoss = stage.lossPercent;
                step.loadStepConvDrop = stage.conversionDropPercent;
                step.loadStepAffectedPct = stage.affectedPercent;
                step.convSaved *= (1 - stage.lossPercent);

                step.affectedPercent = Math.max(step.affectedPercent, stage.affectedPercent);
                step.conversionDropPercent = Math.max(step.conversionDropPercent, stage.conversionDropPercent);

            } else if (
                "add" === stage.stepPair.startStep &&
                "checkout" === stage.stepPair.endStep
            ) {
                step.addStepLoss = stage.lossPercent;
                step.addStepConvDrop = stage.conversionDropPercent;
                step.addStepAffectedPct = stage.affectedPercent;
                step.convSaved *= (1 - stage.lossPercent);

                step.affectedPercent = Math.max(step.affectedPercent, stage.affectedPercent);
                step.conversionDropPercent = Math.max(step.conversionDropPercent, stage.conversionDropPercent);

            } else if (
                "checkout" === stage.stepPair.startStep &&
                "purchase" === stage.stepPair.endStep
            ) {
                step.checkoutStepLoss = stage.lossPercent;
                step.checkoutStepConvDrop = stage.conversionDropPercent;
                step.checkoutStepAffectedPct = stage.affectedPercent;
                step.convSaved *= (1 - stage.lossPercent);

                step.affectedPercent = Math.max(step.affectedPercent, stage.affectedPercent);
                step.conversionDropPercent = Math.max(step.conversionDropPercent, stage.conversionDropPercent);

            } else if ( 
                "remove" === stage.stepPair.startStep ||
                "remove" === stage.stepPair.endStep
            ) {
                // OK for now, should not happen in future
            } else {
                console.error("Broken step: " + JSON.stringify(stage));
            }

            
        });

        step.converionDrop = 1 - step.convSaved;

        return step;
    }

    const IssuesDashboard = React.createClass({displayName: "IssuesDashboard",

        /* State declaration */
        getInitialState: function () {
            return {
                errors: [],
                summary: {},
                stageSummary: [],
                loading: false
            };
        },

        /* Props declaration */
        propTypes: {

        },

        render: function () {
            const summary = this.state.summary;
            const stageSummary = this.state.stageSummary;

            if (this.state.loading) {
                return (
                    React.createElement("div", {id: "main-caption-container", style: {textAlign: "center"}}, 
                        React.createElement("h2", {id: "main-caption"}, 
                            React.createElement("i", {className: "fa fa-spinner fa-pulse"}), " Loading data..."
                        )
                    )
                );
            }

            return (
                React.createElement("div", null, 
                    
                React.createElement("h2", {style: {marginBottom: "30px"}}, 
                    "Detected revenue losses, last 10 days"
                ), 

                React.createElement("table", {className: "table table-responsive tableLosses"}, 
                    React.createElement("thead", null, 
                        React.createElement("tr", null, 
                            React.createElement("th", {colSpan: "3", className: "mergedTh"}, React.createElement("div", {className: "mergedThText"}, "Totals, through funnel")), 
                            React.createElement("th", {colSpan: "3", className: "mergedTh"}, React.createElement("div", {className: "mergedThText"}, "User steps")), 
                            React.createElement("th", {rowSpan: "2"}, "Error description")
                        ), 
                        React.createElement("tr", null, 
                        React.createElement("th", {title: "Revenue loss, %", style: {textAlign: "center"}}, 
                            "Revenue loss", 
                            " ", 
                            React.createElement("span", {className: "fa-stack fa-stack-small"}, 
                                React.createElement("i", {className: "fa fa-circle fa-stack-2x", style: {color: "#c0392b"}}), 
                                React.createElement("i", {className: "fa fa-usd fa-stack-1x fa-inverse"})
                            )
                        ), 
                        React.createElement("th", {title: "Affected users, %", style: {textAlign: "center"}}, 
                            "Affected users", 
                            " ", 
                            React.createElement("span", {className: "fa-stack fa-stack-small"}, 
                                React.createElement("i", {className: "fa fa-circle fa-stack-2x", style: {color: "#2980b9"}}), 
                                React.createElement("i", {className: "fa fa-user fa-stack-1x fa-inverse"})
                            )
                        ), 
                        React.createElement("th", {title: "Conversion drop, %", style: {textAlign: "center"}}, 
                            "Conversion drop", 
                            " ", 
                            React.createElement("span", {className: "fa-stack fa-stack-small"}, 
                                    React.createElement("i", {className: "fa fa-circle fa-stack-2x", style: {color: "#16a085"}}), 
                                    React.createElement("i", {className: "fa fa-filter fa-stack-1x fa-inverse"})
                            )
                        ), 
                        React.createElement("th", {title: "Affected conversion to Add to cart", style: {textAlign: "center"}}, " Up to Add to cart"), 
                        React.createElement("th", {title: "Affected conversion to Check-out", style: {textAlign: "center"}}, "Up to Check-out"), 
                        React.createElement("th", {title: "Affected conversion to Purchase", style: {textAlign: "center"}}, "Up to Purchase")
                        )
                        ), 
                        React.createElement("tbody", null, 
                        this.state.errors.map((row, errorIndex) => {
                                const step = calculateStepsStatistics(row);

                                return (
                                    React.createElement("tr", {className: errorIndex % 2 ? "row-odd" : "row-even"}, 

                                        React.createElement("td", {className: "stages-total-cell lossesNumberCell"}, 
                                            React.createElement("span", {style: {color: "#bdc3c7"}}, "("), 
                                            ((1 - step.convSaved) * 100).toFixed(4), "%", 
                                            React.createElement("span", {style: {color: "#bdc3c7"}}, ")")

                                        ), 
                                        React.createElement("td", {className: "lossesNumberCell"}, 
                                            (step.affectedPercent * 100).toFixed(2), "%"
                                        ), 
                                        React.createElement("td", {className: "lossesNumberCell"}, 
                                            (step.conversionDropPercent * 100).toFixed(2), "%"
                                        ), 

                                        React.createElement("td", {style: {textAlign: "center"}, className: "alignBottom"}, 
                                            React.createElement(LossStep, {
                                                revenueLossPct: step.loadStepLoss, 
                                                conversionDropPct: step.loadStepConvDrop, 
                                                sessionsAffectedPct: step.loadStepAffectedPct, 
                                                stepName: "Add to cart"}
                                            )
                                        ), 
                                        React.createElement("td", {style: {textAlign: "center"}, className: "alignBottom"}, 
                                            React.createElement(LossStep, {
                                                revenueLossPct: step.addStepLoss, 
                                                conversionDropPct: step.addStepConvDrop, 
                                                sessionsAffectedPct: step.addStepAffectedPct, 
                                                stepName: "Checkout"}
                                            )
                                        ), 
                                        React.createElement("td", {style: {textAlign: "center"}, className: "alignBottom"}, 
                                            React.createElement(LossStep, {
                                                revenueLossPct: step.checkoutStepLoss, 
                                                conversionDropPct: step.checkoutStepConvDrop, 
                                                sessionsAffectedPct: step.checkoutStepAffectedPct, 
                                                stepName: "Purchase"}
                                            )
                                        ), 

                                        React.createElement("td", {style: {maxWidth: "70%"}}, 
                                            React.createElement("a", {target: "_blank", className: "descriptionLink", 
                                               href: "/issues-group.html?hash=" + row.stacktraceHash}, 
                                                row.firstIssue.description
                                            )
                                        )

                                    )
                                );
                            }
                        )
                        )
                    ), 


                    React.createElement("table", {className: "table table-hover table-responsive table-striped table-bordered"}, 
                        React.createElement("thead", null, 
                        React.createElement("th", null, "Step"), 
                        React.createElement("th", null, "Converted"), 
                        React.createElement("th", null, "Sessions"), 
                        React.createElement("th", null, "Agv. Conversion")
                        ), 
                        React.createElement("tbody", null, 
                        React.createElement("tr", null, 
                            React.createElement("td", null, "Global"), 
                            React.createElement("td", null, summary.conversionCount), 
                            React.createElement("td", null, summary.recordsCount), 
                            React.createElement("td", null, (summary.conversionCount / summary.recordsCount * 100).toFixed(2), " %")
                        ), 
                        stageSummary.map((stage, i) => (
                            React.createElement("tr", {key: i}, 
                                React.createElement("td", null, "from ", stage.stepPair.startStep, " to ", stage.stepPair.endStep), 
                                React.createElement("td", null, stage.converted), 
                                React.createElement("td", null, stage.records), 
                                React.createElement("td", null, (stage.converted / stage.records * 100).toFixed(2), " %")
                            )
                        ))
                        )
                    ), 

                    this.state.errors.length === 0 ?
                        React.createElement("div", {className: "jumbo"}, 
                            React.createElement("p", null, 
                                "No analytics gathered yet."
                            ), 
                            React.createElement("h2", {style: {color: "#8e44ad"}}, "Install Kuoll to your web application to see error" + ' ' +
                                "reports"), 
                            React.createElement("p", null, 
                                React.createElement("a", {href: "#onboard", className: "btn btn-lg btn-success"}, "Install")
                            )

                        )
                        : ""


                )
            )
        },

        componentDidMount: function () {
            const self = this;
            const timePeriod = UrlUtils.getParameterByName("timePeriod") || "10d";
            this.setState({
                loading: true
            });
            api("get_conversion_analytics", {
                userToken: $.cookie("userToken"),
                timePeriod: timePeriod,
                sortByLossPercent: true
            }, function (resp) {
                self.setState({
                    errors: resp.errors,
                    summary: resp.summary,
                    stageSummary: resp.stageSummary,
                    loading: false
                })
            });
        }

    });

    return IssuesDashboard;

});