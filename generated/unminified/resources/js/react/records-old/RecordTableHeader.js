define(["react"], function (React) {
    var RecordTableHeader = React.createClass({displayName: "RecordTableHeader",

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {},

        render: function () {
            return (
                React.createElement("thead", null, 
                    React.createElement("tr", {className: "myTableHeader"}, 
                        React.createElement("th", {className: "descriptionColumn"}, "Description"), 
                        React.createElement("th", {className: "durationColumn"}, "Duration"), 
                        React.createElement("th", {className: "startTimeColumn"}, "Created"), 
                        React.createElement("th", {className: "lastOpenedColumn"}, "Last opened"), 
                        React.createElement("th", {className: "autonomousModeColumn"}, "Autonomous"), 
                        React.createElement("th", {className: "deleteRecordColumn"}, "Delete")
                    )
                )
            )
        }

    });
    return RecordTableHeader;
});
