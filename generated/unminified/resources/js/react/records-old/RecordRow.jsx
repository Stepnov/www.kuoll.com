define(
    ["jquery", "react", "app/utils/DateUtils", "app/utils/api", "app/integration/Zendesk", "app/react/records/RecordDescription", "app/react/records-old/DeleteRecordCell"],
    function ($, React,DateUtils, api, Zendesk, RecordDescription, DeleteRecordCell) {
        var RecordRow = React.createClass({

            /* State declaration */
            getInitialState: function () {
                return {
                    deleted: false
                };
            },

            /* Props declaration */
            propTypes: {
                record: React.PropTypes.object.isRequired
            },

            onRecordDeleteCallback: function () {
                this.setState({
                    deleted: true
                });
                api("api/deleteRecord", {
                    recordCode: this.props.record.recordCode
                });
            },

            onRecordRestoreCallback: function () {
                this.setState({
                    deleted: false
                });
                api("restoreRecord", {
                    recordCode: this.props.record.recordCode
                });
            },

            render: function () {
                var record = this.props.record;
                var isCompleted = record.status === "completed";
                var duration = DateUtils.formatDuration(isCompleted ? record.completeTime - record.startTime : new Date() - record.startTime);
                return (
                    <tr className={"recordsTableRow" + (this.state.deleted ? " deletedRecord" : "")}>
                        <RecordDescription record={record} recordDeleted={this.state.deleted}/>
                        <td className="durationColumn hidable">
                            <span className="recordDuration" title={duration.long}>{duration.short}</span>
                            {" "}
                            <span className="recordIsGoing">
                                <small className='badge'>{!isCompleted ? "running" : ""}</small>
                            </span>
                        </td>
                        <td className="startTimeColumn hidable">{DateUtils.formatDate(record.startTime)}</td>
                        <td className="lastOpenedColumn hidable">{record.lastPlayedTime ? DateUtils.formatDate(record.lastPlayedTime) : "Not played yet"}</td>
                        <td className="autonomousModeColumn hidable">
                            <span className={record.autonomousMode ? "autonomousColumnOn" : "autonomousColumnOff"}>
                                <i className={record.autonomousMode ? "fa fa-plus-square" : "fa fa-minus-square"}></i>
                            </span>
                        </td>
                        <DeleteRecordCell deleted={this.state.deleted} onDelete={this.onRecordDeleteCallback}
                            onRestore={this.onRecordRestoreCallback}/>
                    </tr>
                )
            }

        });

        return RecordRow;
});
