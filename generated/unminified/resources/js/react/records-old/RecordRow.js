define(
    ["jquery", "react", "app/utils/DateUtils", "app/utils/api", "app/integration/Zendesk", "app/react/records/RecordDescription", "app/react/records-old/DeleteRecordCell"],
    function ($, React,DateUtils, api, Zendesk, RecordDescription, DeleteRecordCell) {
        var RecordRow = React.createClass({displayName: "RecordRow",

            /* State declaration */
            getInitialState: function () {
                return {
                    deleted: false
                };
            },

            /* Props declaration */
            propTypes: {
                record: React.PropTypes.object.isRequired
            },

            onRecordDeleteCallback: function () {
                this.setState({
                    deleted: true
                });
                api("api/deleteRecord", {
                    recordCode: this.props.record.recordCode
                });
            },

            onRecordRestoreCallback: function () {
                this.setState({
                    deleted: false
                });
                api("restoreRecord", {
                    recordCode: this.props.record.recordCode
                });
            },

            render: function () {
                var record = this.props.record;
                var isCompleted = record.status === "completed";
                var duration = DateUtils.formatDuration(isCompleted ? record.completeTime - record.startTime : new Date() - record.startTime);
                return (
                    React.createElement("tr", {className: "recordsTableRow" + (this.state.deleted ? " deletedRecord" : "")}, 
                        React.createElement(RecordDescription, {record: record, recordDeleted: this.state.deleted}), 
                        React.createElement("td", {className: "durationColumn hidable"}, 
                            React.createElement("span", {className: "recordDuration", title: duration.long}, duration.short), 
                            " ", 
                            React.createElement("span", {className: "recordIsGoing"}, 
                                React.createElement("small", {className: "badge"}, !isCompleted ? "running" : "")
                            )
                        ), 
                        React.createElement("td", {className: "startTimeColumn hidable"}, DateUtils.formatDate(record.startTime)), 
                        React.createElement("td", {className: "lastOpenedColumn hidable"}, record.lastPlayedTime ? DateUtils.formatDate(record.lastPlayedTime) : "Not played yet"), 
                        React.createElement("td", {className: "autonomousModeColumn hidable"}, 
                            React.createElement("span", {className: record.autonomousMode ? "autonomousColumnOn" : "autonomousColumnOff"}, 
                                React.createElement("i", {className: record.autonomousMode ? "fa fa-plus-square" : "fa fa-minus-square"})
                            )
                        ), 
                        React.createElement(DeleteRecordCell, {deleted: this.state.deleted, onDelete: this.onRecordDeleteCallback, 
                            onRestore: this.onRecordRestoreCallback})
                    )
                )
            }

        });

        return RecordRow;
});
