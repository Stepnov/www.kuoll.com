define(["react"], function (React) {
    var DeleteRecordCell = React.createClass({displayName: "DeleteRecordCell",

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {
            deleted: React.PropTypes.bool.isRequired,
            onDelete: React.PropTypes.func.isRequired,
            onRestore: React.PropTypes.func.isRequired
        },

        render: function () {
            var deleteNode;
            if (this.props.deleted) {
                deleteNode = (
                    React.createElement("a", {className: "noStrikeout", title: "Restore deleted record", onClick: this.props.onRestore}, "Restore"))
            } else {
                deleteNode = (
                    React.createElement("i", {className: "fa fa-times noStrikeout", title: "Delete record", onClick: this.props.onDelete}));
            }
            return (
                React.createElement("td", {className: "deleteRecordColumn"}, 
                deleteNode
                )
            )
        }

    });
    return DeleteRecordCell;
})
