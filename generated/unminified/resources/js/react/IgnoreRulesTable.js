define(["jquery", "react", "app/react/IgnoreRule", "app/react/IgnoreRuleForm", "app/utils/api", "jquery.cookie"], function ($, React,IgnoreRule, IgnoreRuleForm, api) {

    var IgnoreRulesTable = React.createClass({displayName: "IgnoreRulesTable",

        /* State declaration */
        getInitialState: function () {
            return {
                rules: []
            };
        },

        render: function () {
            var self = this;
            return (
                React.createElement("div", null, 
                    React.createElement("div", null, 
                        React.createElement("h3", {className: "settings-form-header"}, "Add New Rule to Exclude an Element"), 
                        React.createElement(IgnoreRuleForm, {onSubmit: this.addRule})
                    ), 

                    React.createElement("div", null, 
                        React.createElement("h3", {className: "settings-form-header"}, "Enabled Excluded Elements"), 
                        this.state.rules.map(function (rule) {
                            return (
                                React.createElement(IgnoreRule, {rule: rule, onRuleDeleted: self.onRuleDeleted, onRuleUpdated: self.onRuleUpdated})
                            )
                        })
                        )
                )
            )
        },

        onRuleDeleted: function (key) {
            var rules = this.state.rules.filter(function (rule) { return rule.key != key });
            this.setState({
                rules: rules
            });
        },

        onRuleUpdated: function (key, domain, selector) {
            var rules = this.state.rules;
            for (var i = 0; i < rules.length; ++i) {
                if (rules[i].key == key) {
                    rules[i].domain = domain;
                    rules[i].selector = selector;
                }
            }
            this.setState({
                rules: rules
            });
        },

        addRule: function (domain, selector) {
            var self = this;
            api("addIgnoreRule", {
                userToken: $.cookie("userToken"),
                domain: domain,
                selector: selector
            }, function (resp) {
                var rules = self.state.rules;
                var rule = {
                    key: resp.ruleKey,
                    domain: domain,
                    selector: selector
                };
                rules.push(rule);

                self.setState({
                    rules: rules
                });
            });
        },

        componentDidMount: function () {
            var self = this;
            api("getIgnoreRules", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                self.setState({
                    rules: resp.rules || []
                });
            });
        }

    });

    return IgnoreRulesTable;

});