define(["jquery", "react", "app/react/IgnoreRuleForm", "app/utils/api", "jquery.cookie"], function ($, React,IgnoreRuleForm, api) {

    var IgnoreRule = React.createClass({displayName: "IgnoreRule",

        /* State declaration */
        getInitialState: function () {
            return {
                editing: false
            };
        },

        /* Props declaration */
        propTypes: {
            rule: React.PropTypes.object.isRequired,
            onRuleDeleted: React.PropTypes.func.isRequired,
            onRuleUpdated: React.PropTypes.func.isRequired
        },

        render: function () {
            var rule = this.props.rule;
            var selectorText = rule.selector;
            return (
                React.createElement("div", {className: "form-horizontal rule-wrapper"}, 
                this.state.editing ?
                    React.createElement(IgnoreRuleForm, {onSubmit: this.updateRule, rule: rule, onDelete: this.deleteRule})
                    :
                    React.createElement("div", {className: ""}, 
                        React.createElement("div", {className: "form-group"}, 
                            React.createElement("label", {className: "col-md-3 control-label"}, "Domain"), 
                            React.createElement("div", {className: "col-md-9 "}, 
                                React.createElement("input", {className: "form-control", disabled: true, value: rule.domain})
                            )
                        ), 
                        React.createElement("div", {className: "form-group"}, 
                            React.createElement("label", {className: "col-md-3 control-label", htmlFor: "css-selectror"}, "CSS Selector"), 
                            React.createElement("div", {className: "col-md-9 "}, 
                                React.createElement("input", {className: "form-control", disabled: true, value: rule.selector, htmlId: "css-selectror"})
                            )
                        ), 
                        React.createElement("div", {className: "form-group"}, 
                            React.createElement("div", {className: "col-sm-offset-3 col-sm-9"}, 
                                React.createElement("button", {className: "btn btn-default", onClick: this.startEditing}, React.createElement("i", {className: "fa fa-pencil-square-o"}), " Edit"), "  ", 
                                React.createElement("button", {className: "btn btn-danger", onClick: this.deleteRule}, React.createElement("i", {className: "fa fa-times"}), " Remove")
                            )
                        )
                    )
                
                )
            )
        },

        startEditing: function () {
            this.setState({
                editing: true
            })
        },

        deleteRule: function () {
            var self = this;
            api("deleteIgnoreRule", {
                userToken: $.cookie("userToken"),
                ruleKey: this.props.rule.key
            }, function () {
                self.props.onRuleDeleted(self.props.rule.key);
                self.setState({
                    editing: false
                });
            });
        },

        updateRule: function (domain, selector, key) {
            var self = this;
            api("updateIgnoreRule", {
                userToken: $.cookie("userToken"),
                key: key,
                domain: domain,
                selector: selector
            }, function () {
                self.props.onRuleUpdated(key, domain, selector);
                self.setState({
                    editing: false
                });
            });
        }

    });

    return IgnoreRule;

});