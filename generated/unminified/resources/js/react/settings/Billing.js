define(["jquery", "react", "app/utils/api", "app/react/settings/SubscriptionPlanSelect", "app/react/settings/CardManagement", "app/react/settings/CouponForm", "app/react/settings/Check", "app/checkout", "jquery.cookie"], function ($, React, api, SubscriptionPlanSelect, CardManagement, CouponForm, Check, checkout) {

    var Billing = React.createClass({displayName: "Billing",

        /* State declaration */
        getInitialState: function () {
            return {
                billing: null,
                billingFrequency: null,

                reportsPerMonth: -1,

                committingChanges: false,
                changesCommittedSuccessfully: false,
                commitChangesErrorMsg: null,

                pendingChanges: null,

                check: null
            };
        },

        /* Props declaration */
        propTypes: {

        },

        onBillingUpdated: function(billing, reportsPerMonth) {
            var newState = {
                billing: billing,
                changesCommittedSuccessfully: false
            };
            if (reportsPerMonth !== undefined) {
                newState.reportsPerMonth = reportsPerMonth;
            }
            var planParts = newState.billing.planName.split("_");
            newState.billing.planName = planParts[0];
            newState.billingFrequency = planParts[1] || "month";
            this.setState(newState, this.calculatePlanChangeCost);
        },

        handleBillingFrequencyChange: function (changeEvent) {
            var pendingChanges = this.state.pendingChanges || {};
            pendingChanges.billingFrequency = changeEvent.target.value;
            this.setState({
                pendingChanges: pendingChanges
            }, this.calculatePlanChangeCost);
        },

        onSelectedPlanChanged: function (newPlan) {
            var pendingChanges = this.state.pendingChanges || {};
            pendingChanges.plan = newPlan;
            this.setState({
                pendingChanges: pendingChanges
            }, this.calculatePlanChangeCost);

        },

        calculatePlanChangeCost: function () {
            if (this.hasPendingChanges() && this.state.pendingChanges.plan != "hobby" && !this.state.billing.trialExpirationDate) {
                var self = this;
                var plan = (this.state.pendingChanges.plan || this.state.billing.planName)
                    + "_"
                    + (this.state.pendingChanges.billingFrequency || this.state.billingFrequency);
                api("billing/get_check", {
                    userToken: $.cookie("userToken"),
                    newPlan: plan
                }, function (resp) {
                    self.setState({
                        check: resp.check,
                        changesCommittedSuccessfully: false
                    });
                });
            } else {
                this.setState({
                    check: null
                });
            }
        },

        commitPendingChanges: function () {
            function onChangesSuccessfullyApplied(resp) {
                self.onBillingUpdated(resp.billing, resp.reportsPerMonth);
                self.setState({
                    changesCommittedSuccessfully: true,
                    committingChanges: false,
                    pendingChanges: null,
                    check: null
                });
            }

            function onFailedToApplyChanges(errorMsg) {
                self.setState({
                    committingChanges: false,
                    commitChangesErrorMsg: errorMsg
                });
            }

            function showSpinner() {
                self.setState({
                    changesCommittedSuccessfully: false,
                    committingChanges: true
                });
            }

            var self = this;

            var newPlan = this.state.pendingChanges.plan || this.state.billing.planName;
            if (newPlan != "hobby") {
                newPlan += "_" + (this.state.pendingChanges.billingFrequency || this.state.billingFrequency);
            }

            if (this.state.billing.customerId == null) {

                checkout.openCheckoutForm(newPlan, null, function (token) {
                    showSpinner();
                    api("billing/create_customer", {
                        planName: newPlan,
                        userToken: $.cookie("userToken"),
                        paymentSourceToken: token.id
                    }, onChangesSuccessfullyApplied, onFailedToApplyChanges);
                });

            } else if (this.state.billing.last4CardDigits || this.state.billing.trialExpirationDate || newPlan == "hobby") {

                showSpinner();
                api("billing/change_plan", {
                    userToken: $.cookie("userToken"),
                    planName: newPlan
                }, onChangesSuccessfullyApplied, onFailedToApplyChanges);

            } else {

                checkout.openCheckoutForm(newPlan, null, function (token) {
                    showSpinner();
                    api("billing/change_plan", {
                        userToken: $.cookie("userToken"),
                        planName: newPlan,
                        paymentSourceToken: token.id
                    }, onChangesSuccessfullyApplied, onFailedToApplyChanges);
                });

            }
        },

        resetPendingChanges: function () {
            this.setState({
                pendingChanges: null,
                check: null
            });
        },

        hasPendingChanges: function () {
            return !(this.state.pendingChanges == null ||
                ((!this.state.pendingChanges.plan || this.state.pendingChanges.plan == this.state.billing.planName)
                && (!this.state.pendingChanges.billingFrequency || this.state.pendingChanges.billingFrequency == this.state.billingFrequency))
                || this.state.billing.planName == "hobby" && !this.state.pendingChanges.plan);
        },

        render: function () {
            if (!this.state.billing) {
                return null;
            }

            var billingFrequency = this.state.pendingChanges ? this.state.pendingChanges.billingFrequency || this.state.billingFrequency : this.state.billingFrequency;

            return (
                React.createElement("div", null, 
                    React.createElement("h1", null, "Billing"), 

                    React.createElement("p", null, 
                        "You have ", React.createElement("strong", null, this.state.billing.reportsLeft), " reports left. You will have ", React.createElement("strong", null, 
                        this.state.reportsPerMonth), " reports in the beginning of the next month.", 
                        this.state.billing.expirationDate ?
                            React.createElement("span", null, " Subscription is paid up to ", React.createElement("strong", null, 
                                new Date(this.state.billing.expirationDate * 1000).toLocaleDateString()
                                )
                            )
                        : null, "."
                    ), 

                    React.createElement("div", {id: "subscription-plan-select-wrapper"}, 
                        React.createElement(SubscriptionPlanSelect, {onBillingUpdated: this.onBillingUpdated, billing: this.state.billing, 
                                                billingFrequency: this.state.billingFrequency, 
                                                pendingChanges: this.state.pendingChanges, onSelectedPlanChanged: this.onSelectedPlanChanged})
                    ), 

                    React.createElement("div", {style: {margin: "50px 0"}}, 

                        React.createElement("div", {className: "panel panel-info"}, 
                            React.createElement("div", {className: "panel-heading"}, 
                                React.createElement("h3", {className: "panel-title"}, "Choose your billing frequency")
                            ), 
                            React.createElement("div", {className: "panel-body"}, 
                                React.createElement("div", {className: "radio"}, 
                                    React.createElement("label", null, 
                                        React.createElement("input", {type: "radio", name: "billingFrequency", value: "month", checked: billingFrequency === 'month', 
                                            onChange: this.handleBillingFrequencyChange}
                                        ), 
                                        " ", 
                                        "Pay monthly"
                                    )
                                ), 
                                React.createElement("div", {className: "radio"}, 
                                    React.createElement("label", null, 
                                        React.createElement("input", {type: "radio", name: "billingFrequency", value: "year", checked: billingFrequency === 'year', 
                                            onChange: this.handleBillingFrequencyChange}), 
                                        " ", 
                                        "Pay annually with discount. Pay for 10 months, get 2 months free."
                                    )
                                )
                            )
                        )
                    ), 

                    React.createElement("div", {className: "row", style: {marginTop: "30px", marginBottom: "50px"}}, 

                        React.createElement("div", {className: "col-md-12"}, 
                            this.state.commitChangesErrorMsg ?
                                React.createElement("div", {className: "alert alert-warning", role: "alert"}, 
                                    this.state.commitChangesErrorMsg
                                )
                            : null, 

                            this.state.changesCommittedSuccessfully ?
                                React.createElement("div", {className: "alert alert-success", role: "alert"}, 
                                    "Billing successfully updated!"
                                )
                            : null, 

                            this.state.billing.trialExpirationDate && this.hasPendingChanges() ?
                                React.createElement("div", {className: ""}, 
                                    React.createElement("div", {className: "alert alert-info", role: "alert"}, 
                                        "Trial period is active. All billing changes are free!"
                                    )
                                )
                            : null
                        ), 

                        React.createElement("div", {className: "col-md-12"}, 
                            React.createElement(Check, {billingFrequency: billingFrequency, check: this.state.check})
                        ), 

                        React.createElement("div", {className: "col-md-6"}, 
                            React.createElement("button", {id: "updatePaymentDetailsButton", className: "btn btn-lg btn-block btn-primary", 
                                    disabled: !this.hasPendingChanges(), onClick: this.commitPendingChanges}, 
                                this.state.committingChanges ?
                                    React.createElement("i", {className: "fa fa-spinner fa-spin"})
                                : "Update payment details"
                            )
                        ), 
                        React.createElement("div", {className: "col-md-6"}, 
                            React.createElement("button", {id: "resetPaymentDetailsButton", className: "btn btn-lg btn-block btn-default", 
                                    disabled: !this.hasPendingChanges(), onClick: this.resetPendingChanges}, 
                                "Reset payment details"
                            )
                        )
                    ), 

                    this.state.billing.customerId ?
                        React.createElement(CardManagement, {trialExpirationDate: this.state.billing.trialExpirationDate, 
                                    planName: this.state.billing.planName, onBillingUpdated: this.onBillingUpdated, 
                                    customerId: this.state.billing.customerId, card: this.state.billing.last4CardDigits})
                    : null, 

                    React.createElement(CouponForm, {coupon: this.state.billing.coupon, amountOff: this.state.billing.couponAmountOff, 
                                percentOff: this.state.billing.couponPercentOff, onBillingUpdated: this.onBillingUpdated}), 

                    React.createElement("h4", null, "Refund Policy"), 
                    React.createElement("p", null, 
                        "If you are not 100% satisfied with your purchase, you may contact us within 120 days from" + ' ' +
                        "the purchase date and we will provide a full refund of your purchase."
                    )

                )
            )
        },

        componentDidMount: function () {
            var self = this;
            api("billing/get_billing", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                self.onBillingUpdated(resp.billing, resp.reportsPerMonth);
            });

            $(window).on("beforeunload", function () {
                if (self.hasPendingChanges()) {
                    // return user to billing tab if they try to leave page from another tab
                    $("#tabs-nav").find("a[href=#tab-billing]").click();
                    return "You have uncommitted changes to your billing details. Are you sure you want to leave now?";
                }
            });
        }

    });

    return Billing;

});