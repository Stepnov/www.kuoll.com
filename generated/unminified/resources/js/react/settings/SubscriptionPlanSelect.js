define(["jquery", "react", "app/User", "app/utils/api", "app/checkout", "jquery.cookie"], function ($, React, User, api, checkout) {

    var SetPlanButton = React.createClass({displayName: "SetPlanButton",
        render: function () {
            var props = this.props;
            var activePlan = props.currentPlan.startsWith(props.planName);
            if (activePlan) {
                return (
                    React.createElement("span", {className: "start btn activePlanLabel activePlanLabel-" + props.planName, 
                            "data-plan-name": props.planName}, 
                        React.createElement("strong", null, "Active")
                    )
                )
            } else {
                return (
                    React.createElement("button", {className: "start btn " + (props.primary ? "btn-primary" : "btn-default"), 
                            onClick: props.onClick, "data-plan-name": props.planName}, 
                        props.caption
                    )
                )
            }
        }
    });

    var SubscriptionPlanSelect = React.createClass({displayName: "SubscriptionPlanSelect",
    
        /* State declaration */
        getInitialState: function () {
            return {
                switchPlanErrorMsg: ""
            };
        },
        
        /* Props declaration */
        propTypes: {
            onBillingUpdated: React.PropTypes.func.isRequired,
            onSelectedPlanChanged: React.PropTypes.func.isRequired,
            pendingChanges: React.PropTypes.object,
            billing: React.PropTypes.object.isRequired,
            billingFrequency: React.PropTypes.oneOf(["month", "year"]).isRequired
        },

        handleSubscriptionButtonClick: function (e) {
            var planName = e.target.dataset.planName;
            this.props.onSelectedPlanChanged(planName);
        },
    
        render: function () {
            var billing = this.props.billing;
            var plan = this.props.pendingChanges ? this.props.pendingChanges.plan || billing.planName : billing.planName;
            return (
                React.createElement("div", {className: "form-inline"}, 
                React.createElement("div", {className: " price-grid"}, 
                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-md-12"}, 
                            React.createElement("div", {className: "alert alert-warning " + (this.state.switchPlanErrorMsg ? "" : "hidden")}, 
                                "Could not switch plan because of the error: ", this.state.switchPlanErrorMsg
                            )
                        )
                    ), 

                    React.createElement("div", {className: "row"}, 
                        React.createElement("div", {className: "col-md-4 col-sm-6", style: {textAlign: "center"}}, 
                            React.createElement("div", {className: "panel panel-default " + 
                                    (plan == "hobby" ? "activePlan": "") +
                                    " activePlan-" + plan
                                    }, 
                                React.createElement("div", {className: "panel-heading"}, 
                                    React.createElement("h2", {className: "plan-name", style: {color: "#9b59b6"}}, "Hobby")
                                ), 
                                React.createElement("div", {className: "panel-body"}, 
                                    React.createElement("h2", {className: "price"}, "$0 ", React.createElement("br", null), 
                                        React.createElement("sub", null, 
                                            "forever"
                                        )
                                    ), 
                                    React.createElement("p", {className: "lead"}
                                    ), 

                                    React.createElement("p", {className: "whyt"}, "100 bug reports"), 
                                    React.createElement("p", null, "2 days report lifetime"), 
                                    React.createElement("p", {className: "whyt"}, "Email alerts"), 
                                    React.createElement("p", {className: ""}, "Email support ", React.createElement("br", null), "  "), 


                                    React.createElement("p", {className: "cart1"}
                                    ), 
                                    React.createElement("p", {className: "small"}, "No obligations ", React.createElement("br", null), " ")
                                )
                            ), 

                            React.createElement("p", {className: "cart2"}, 
                                React.createElement(SetPlanButton, {currentPlan: plan, planName: "hobby", 
                                            caption: "Downgrade", 
                                            onClick: this.handleSubscriptionButtonClick, primary: true})
                            )

                        ), 

                        React.createElement("div", {className: "col-md-4 col-sm-6", style: {textAlign: "center"}}, 
                            React.createElement("div", {className: "panel panel-default " + 
                                    (plan == "team_month"? "panel-info": "")}, 
                                React.createElement("div", {className: "panel-heading"}, 
                                    React.createElement("div", {className: "recommended-plan"}, 
                                        "Most recommended"
                                    ), 
                                    React.createElement("h2", {className: "plan-name", style: {color: "#27ae60"}}, "Team")
                                ), 
                                React.createElement("div", {className: "panel-body"}, 
                                    React.createElement("h2", {className: "price"}, 
                                        this.props.billingFrequency  == "month"? "$99": "$82.50", 
                                        React.createElement("br", null), 
                                        React.createElement("sub", null, 
                                            "per month / team"
                                        )
                                    ), 

                                    React.createElement("p", {className: "whyt"}, "1 000 000 user session"), 
                                    React.createElement("p", null, "180 days report lifetime"), 
                                    React.createElement("p", {className: "whyt"}, "JIRA integration"), 
                                    React.createElement("p", {className: ""}, "Priority phone and email support"), 
                                    React.createElement("p", {className: "small"}, "Refund without questions,", React.createElement("br", null), " 14 days free")

                                )
                            ), 

                            React.createElement("p", null, " "), 

                            React.createElement("p", {className: "cart2"}, 
                                React.createElement(SetPlanButton, {currentPlan: plan, planName: "team", 
                                            caption: "Upgrade", 
                                            onClick: this.handleSubscriptionButtonClick, primary: true})

                            )

                        ), 
                        React.createElement("div", {className: "col-md-4 col-sm-6 col-sm-offset-3 col-md-offset-0 col-lg-offset-0", style: {textAlign: "center"}}, 
                            React.createElement("div", {className: "panel panel-default " + 
                                    (plan == "team_month"? "panel-info": "")}, 
                                React.createElement("div", {className: "panel-heading"}, 
                                    React.createElement("h2", {className: "plan-name", style: {color: "#2980b9"}}, "Enterprise")
                                ), 
                                React.createElement("div", {className: "panel-body"}, 
                                    React.createElement("h2", {className: "price"}, 
                                        "Custom price", 
                                        React.createElement("br", null), 
                                        React.createElement("sub", null, 
                                            " "
                                        )
                                    ), 

                                    React.createElement("p", {className: "whyt"}, "Unlimited bug reports"), 
                                    React.createElement("p", null, "Unlimited report lifetime"), 
                                    React.createElement("p", {className: "whyt"}, "Custom integrations"), 
                                    React.createElement("p", {className: ""}, "Priority phone and email support"), 
                                    React.createElement("p", {className: "small"}, "Free trial available, ", React.createElement("br", null), "money-back guarantee")
                                )
                            ), 
                            React.createElement("p", null, " "), 
                            React.createElement("p", {className: "cart2"}, 
                                React.createElement("a", {href: "mailto:corp@kuoll.com", className: "start btn btn-default"}, 
                                    "Contact us"
                                )
                            )

                        )
                    )


                )/*  container */

                )
            )	
        },

        componentDidMount: function () {
            this.setState({
                billing: this.props.billing
            });
        }
        
    });
    
    return SubscriptionPlanSelect;    

});