define(["jquery", "react", "jquery.qtip"], function ($, React) {

    var Price = React.createClass({

        propTypes: {
            price: React.PropTypes.number.isRequired
        },

        render: function () {
            var isNegative = this.props.price < 0;
            var absPrice = Math.abs(this.props.price / 100);
            return (
                <span className="inline-price">
                    {isNegative ?
                        <span title={"-$" + absPrice}>
                            (${absPrice})
                        </span>
                    :
                        <span title={"$" + absPrice}>
                            ${absPrice}
                        </span>
                    }
                </span>
            )
        }
    });

    var Check = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {

            };
        },

        /* Props declaration */
        propTypes: {
            check: React.PropTypes.object,
            billingFrequency: React.PropTypes.oneOf(["month", "year"]).isRequired
        },

        render: function () {
            if (!this.props.check) return null;

            var check = this.props.check;

            return (
                <div id="billing-update-check">
                    <h2 className="check-caption">Payment summary</h2>
                    <table className="table table-striped">
                        <tbody>
                        <tr className="subscription-cost">
                            <td>
                                {this.props.billingFrequency == "month" ? "Monthly" : "Annual"} subscription amount is
                            </td>
                            <td><Price price={check.baseCost}/></td>
                            <td></td>
                        </tr>
                        {check.proratedAmount ?
                            <tr className="prorated-amount">
                                <td>Prorated amount is</td>
                                <td><Price price={check.proratedAmount}/></td>
                                <td></td>
                            </tr>
                        : null }
                        {check.coupon ?
                            <tr className="coupon">
                                <td colSpan="3">
                                    Coupon {check.coupon} is applied
                                    {check.discountExpirationDate ?
                                    <span> and active until {new Date(check.discountExpirationDate * 1000).toLocaleDateString()}</span>
                                    : null}
                                    .
                                </td>
                            </tr>
                        : null }
                        <tr className="next-payment">
                            <td>Total amount for the next payment is</td>
                            <td><Price price={check.nextPayment}/></td>
                            <td className="check-item-explanation">
                                {check.nextPayment < 0 ?
                                    <i className="fa fa-question-circle" aria-hidden="true" title="Payment amount is negative because you're switching to a cheaper plan or using a coupon. Wo won't charge you. We will add excess money to your virtual credit and it will be used for future payments automatically"/>
                                : null}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            )
        },

        componentDidUpdate: function () {
            $(React.findDOMNode(this)).find(".check-item-explanation").find(".fa[title]:not([data-hasqtip])").each(function (index, node) {
                $(node).qtip({
                    content: {
                        text: $(node).attr("title")
                    },
                    style: {
                        classes: "qtip-bootstrap"
                    },
                    position: {
                        my: "middle left",
                        at: "middle right",
                        viewport: true
                    }
                });
            });
        },

    });

    return Check;

});