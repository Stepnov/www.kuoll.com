define(["jquery", "react", "jquery.qtip"], function ($, React) {

    var Price = React.createClass({displayName: "Price",

        propTypes: {
            price: React.PropTypes.number.isRequired
        },

        render: function () {
            var isNegative = this.props.price < 0;
            var absPrice = Math.abs(this.props.price / 100);
            return (
                React.createElement("span", {className: "inline-price"}, 
                    isNegative ?
                        React.createElement("span", {title: "-$" + absPrice}, 
                            "($", absPrice, ")"
                        )
                    :
                        React.createElement("span", {title: "$" + absPrice}, 
                            "$", absPrice
                        )
                    
                )
            )
        }
    });

    var Check = React.createClass({displayName: "Check",

        /* State declaration */
        getInitialState: function () {
            return {

            };
        },

        /* Props declaration */
        propTypes: {
            check: React.PropTypes.object,
            billingFrequency: React.PropTypes.oneOf(["month", "year"]).isRequired
        },

        render: function () {
            if (!this.props.check) return null;

            var check = this.props.check;

            return (
                React.createElement("div", {id: "billing-update-check"}, 
                    React.createElement("h2", {className: "check-caption"}, "Payment summary"), 
                    React.createElement("table", {className: "table table-striped"}, 
                        React.createElement("tbody", null, 
                        React.createElement("tr", {className: "subscription-cost"}, 
                            React.createElement("td", null, 
                                this.props.billingFrequency == "month" ? "Monthly" : "Annual", " subscription amount is"
                            ), 
                            React.createElement("td", null, React.createElement(Price, {price: check.baseCost})), 
                            React.createElement("td", null)
                        ), 
                        check.proratedAmount ?
                            React.createElement("tr", {className: "prorated-amount"}, 
                                React.createElement("td", null, "Prorated amount is"), 
                                React.createElement("td", null, React.createElement(Price, {price: check.proratedAmount})), 
                                React.createElement("td", null)
                            )
                        : null, 
                        check.coupon ?
                            React.createElement("tr", {className: "coupon"}, 
                                React.createElement("td", {colSpan: "3"}, 
                                    "Coupon ", check.coupon, " is applied", 
                                    check.discountExpirationDate ?
                                    React.createElement("span", null, " and active until ", new Date(check.discountExpirationDate * 1000).toLocaleDateString())
                                    : null, 
                                    "."
                                )
                            )
                        : null, 
                        React.createElement("tr", {className: "next-payment"}, 
                            React.createElement("td", null, "Total amount for the next payment is"), 
                            React.createElement("td", null, React.createElement(Price, {price: check.nextPayment})), 
                            React.createElement("td", {className: "check-item-explanation"}, 
                                check.nextPayment < 0 ?
                                    React.createElement("i", {className: "fa fa-question-circle", "aria-hidden": "true", title: "Payment amount is negative because you're switching to a cheaper plan or using a coupon. Wo won't charge you. We will add excess money to your virtual credit and it will be used for future payments automatically"})
                                : null
                            )
                        )
                        )
                    )
                )
            )
        },

        componentDidUpdate: function () {
            $(React.findDOMNode(this)).find(".check-item-explanation").find(".fa[title]:not([data-hasqtip])").each(function (index, node) {
                $(node).qtip({
                    content: {
                        text: $(node).attr("title")
                    },
                    style: {
                        classes: "qtip-bootstrap"
                    },
                    position: {
                        my: "middle left",
                        at: "middle right",
                        viewport: true
                    }
                });
            });
        },

    });

    return Check;

});