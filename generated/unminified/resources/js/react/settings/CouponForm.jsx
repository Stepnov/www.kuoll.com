define(["jquery", "react", "app/utils/api"], function ($, React, api) {

    var CouponForm = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                coupon: "",

                applyingCoupon: false,
                error: ""
            };
        },

        /* Props declaration */
        propTypes: {
            coupon: React.PropTypes.string,
            amountOff: React.PropTypes.number,
            percentOff: React.PropTypes.number,
            duration: React.PropTypes.oneOf("forever", "once", "repeating"),
            duration_in_months: React.PropTypes.number,
            onBillingUpdated: React.PropTypes.func.isRequired
        },

        onCouponChange: function(event) {
            this.setState({
                coupon: event.target.value.toUpperCase(),
                error: ""
            });
        },

        setCoupon: function () {
            var self = this;
            this.setState({
                applyingCoupon: true
            });

            api("billing/apply_coupon", {
                userToken: $.cookie("userToken"),
                coupon: this.state.coupon
            }, function (resp) {
                self.setState({
                    applyingCoupon: false
                });
                self.props.onBillingUpdated(resp.billing);
            }, function (error) {
                self.setState({
                    applyingCoupon: false,
                    error: error
                });
            });
        },

        render: function () {
            return (
                <div className="" id="coupon-form">
                    <div className="col-sm-12" style={{marginTop: '50px', marginBottom: '50px'}}>
                        {this.props.coupon ?
                            <p>
                                Coupon <span className="label label-success">{this.props.coupon}</span> will give you a
                                {" "}
                                {this.props.amountOff ? (this.props.amountOff / 100) + "$ discount" : null}
                                {this.props.percentOff ? this.props.percentOff + "% discount" : null}
                                {this.props.duration === "repeating"  ? " for the next " + this.props.duration_in_months + " months" : null}
                                {this.props.duration === "once"  ? " on the next payment" : null}
                                {this.props.duration === "forever"  ? " on every payment" : null}
                                .
                            </p>
                        : null}
                        <p>
                            Coupon 
                            {" "}
                            <span className="btn-group">
                                <input value={this.state.coupon} onChange={this.onCouponChange} className="form-control"
                                    id="couponInput" style={{float: "left", borderTopRightRadius: 0, borderBottomRightRadius: 0}}/>
                                <button className="btn btn-success" onClick={this.setCoupon} type="button">
                                    {this.state.applyingCoupon ?
                                        <i className="fa fa-spinner fa-spin"></i>
                                    :
                                        <span>
                                            <i className="fa fa-check" aria-hidden="true"></i> Apply
                                        </span>
                                    }
                                </button>
                            </span>
                            {this.state.error ?
                            " " + this.state.error
                            : null}
                        </p>
                    </div>
                </div>
            )
        }

    });

    return CouponForm;

});