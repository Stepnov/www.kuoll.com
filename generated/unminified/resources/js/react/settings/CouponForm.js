define(["jquery", "react", "app/utils/api"], function ($, React, api) {

    var CouponForm = React.createClass({displayName: "CouponForm",

        /* State declaration */
        getInitialState: function () {
            return {
                coupon: "",

                applyingCoupon: false,
                error: ""
            };
        },

        /* Props declaration */
        propTypes: {
            coupon: React.PropTypes.string,
            amountOff: React.PropTypes.number,
            percentOff: React.PropTypes.number,
            duration: React.PropTypes.oneOf("forever", "once", "repeating"),
            duration_in_months: React.PropTypes.number,
            onBillingUpdated: React.PropTypes.func.isRequired
        },

        onCouponChange: function(event) {
            this.setState({
                coupon: event.target.value.toUpperCase(),
                error: ""
            });
        },

        setCoupon: function () {
            var self = this;
            this.setState({
                applyingCoupon: true
            });

            api("billing/apply_coupon", {
                userToken: $.cookie("userToken"),
                coupon: this.state.coupon
            }, function (resp) {
                self.setState({
                    applyingCoupon: false
                });
                self.props.onBillingUpdated(resp.billing);
            }, function (error) {
                self.setState({
                    applyingCoupon: false,
                    error: error
                });
            });
        },

        render: function () {
            return (
                React.createElement("div", {className: "", id: "coupon-form"}, 
                    React.createElement("div", {className: "col-sm-12", style: {marginTop: '50px', marginBottom: '50px'}}, 
                        this.props.coupon ?
                            React.createElement("p", null, 
                                "Coupon ", React.createElement("span", {className: "label label-success"}, this.props.coupon), " will give you a", 
                                " ", 
                                this.props.amountOff ? (this.props.amountOff / 100) + "$ discount" : null, 
                                this.props.percentOff ? this.props.percentOff + "% discount" : null, 
                                this.props.duration === "repeating"  ? " for the next " + this.props.duration_in_months + " months" : null, 
                                this.props.duration === "once"  ? " on the next payment" : null, 
                                this.props.duration === "forever"  ? " on every payment" : null, 
                                "."
                            )
                        : null, 
                        React.createElement("p", null, 
                            "Coupon",  
                            " ", 
                            React.createElement("span", {className: "btn-group"}, 
                                React.createElement("input", {value: this.state.coupon, onChange: this.onCouponChange, className: "form-control", 
                                    id: "couponInput", style: {float: "left", borderTopRightRadius: 0, borderBottomRightRadius: 0}}), 
                                React.createElement("button", {className: "btn btn-success", onClick: this.setCoupon, type: "button"}, 
                                    this.state.applyingCoupon ?
                                        React.createElement("i", {className: "fa fa-spinner fa-spin"})
                                    :
                                        React.createElement("span", null, 
                                            React.createElement("i", {className: "fa fa-check", "aria-hidden": "true"}), " Apply"
                                        )
                                    
                                )
                            ), 
                            this.state.error ?
                            " " + this.state.error
                            : null
                        )
                    )
                )
            )
        }

    });

    return CouponForm;

});