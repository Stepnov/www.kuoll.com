define(["jquery", "react", "app/checkout", "app/utils/api"], function ($, React, checkout, api) {

    var CardManagement = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
            };
        },

        /* Props declaration */
        propTypes: {
            trialExpirationDate: React.PropTypes.number,
            customerId: React.PropTypes.string.isRequired,
            card: React.PropTypes.string,
            planName: React.PropTypes.string,
            onBillingUpdated: React.PropTypes.func.isRequired
        },

        promptCard: function () {
            var self = this;
            checkout.openCheckoutForm(null, this.props.customerId, function (token) {
                api("billing/attach_card", {
                    token: token.id,
                    userToken: $.cookie("userToken")
                }, function (resp) {
                    self.props.onBillingUpdated(resp);
                });
            });
        },

        detachCard: function () {
            var self = this;
            api("billing/detach_card", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                self.props.onBillingUpdated(resp);
            });
        },

        render: function () {
            return (
                <div id="card-management-form" className="panel  panel-info">
                    <div className="panel-heading">
                        <h3 className="panel-title">Payment card</h3>
                    </div>
                    <div className="panel-body">
                        {this.props.card ?
                        <div className="row">
                            <div className="col-md-3" style={{"paddingTop": "8px"}}>
                                <strong>Card number</strong>
                            </div>
                            <div className="col-md-4">
                                <span  className="paymentCard "  >
                                    <i className="fa fa-fw fa-credit-card" aria-hidden="true" style={{color: "rgba(189, 195, 199,1.0)"}}> </i>
                                    {" "}
                                    <span className="paymentCardStarz">•••• •••• •••• </span>
                                    {this.props.card}</span>
                                {this.props.trialExpirationDate ?
                                <span>
                                    Your trial period is expiring on <strong>
                                    {new Date(this.props.trialExpirationDate * 1000).toLocaleDateString()}</strong>
                                    . We won't charge you until then.
                                </span>
                                : null}
                            </div>
                            <div className="col-md-2">
                                <button className="btn btn-default" onClick={this.promptCard}>Change card</button>
                            </div>
                            <div className="col-md-2">
                                <button className="btn btn-warning" onClick={this.detachCard}>Detach card</button>
                            </div>
                        </div>
                        :
                        this.props.trialExpirationDate ?
                            (
                                this.props.planName == "hobby" ?
                                    null
                                    :
                                    <div>
                                        <p>
                                            Your trial period expires on {" "}
                                            <strong>{new Date(this.props.trialExpirationDate * 1000).toLocaleDateString()}</strong>
                                            . Please, provide billing details
                                        </p>
                                        <p>
                                            <button className="btn btn-primary btn-lg" onClick={this.promptCard}>
                                                Provide billing details
                                            </button>
                                        </p>
                                    </div>
                            )
                        :
                        <div>
                            <p>
                                You account is limited to hobby plan. Please, provide billing details below.
                            </p>
                            <button className="btn btn-primary btn-lg" onClick={this.promptCard}>Provide billing details</button>
                        </div>
                        }
                    </div>
                </div>
            )
        }

    });

    return CardManagement;

});