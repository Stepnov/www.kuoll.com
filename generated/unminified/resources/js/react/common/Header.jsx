define(["jquery", "react", "app/react/common/IssuesQuotaExceededNotification", "app/google-signin", "app/utils/api", "jquery.cookie", "bootstrap"], function ($, React, IssuesQuotaExceededNotification, GoogleSignIn, api) {

    var Header = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                userToken: $.cookie("userToken")
            };
        },

        /* Props declaration */
        propTypes: {},

        onLogoutClick: function (e) {
            if ("localhost" != document.location.hostname && mixpanel) {
                mixpanel.identify("");
                mixpanel.unregister("User id");
                mixpanel.unregister("User token");
            }

            api("logout", {
                userToken: this.state.userToken
            });
            GoogleSignIn.logout();
            $.removeCookie("userToken", {domain: window.config.cookieDomain, path: "/"});
            document.location = "/";
            e.preventDefault();
        },

        render: function () {
            const isQuickStartPage = (document.location.pathname === "/quick-start.html");
            return (
                <nav className="navbar navbar-fixed-top navbar-inverse" role="navigation">
                    <div className="container">
                        <div className="row" >
                        <div className="col-sm-12" >
                        {!isQuickStartPage ?
                            <div className="navbar-header">
                                <a className="navbar-brand" href="//www.kuoll.com/">
                                    <img src="/resources/img/cat.png" className="inAppCatty" />
                                </a>
                            </div>
                            : null
                            }
                            <div id="navbar" className="collapse navbar-collapse">
                                {this.state.userToken ? 
                                    <ul className="nav navbar-nav navbar-left">
                                        <li className="dropdown">
                                            <div className="dropdown-toggle dropdown-toggle-ala-slack" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i className="fa fa-fw fa-bug"></i>
                                                {" "}
                                                Errors
                                                <span className="caret"></span>
                                            </div>
                                            <ul className="dropdown-menu dropdown-menu-ala-slack" aria-labelledby="dropdownMenu1" id="header-dropdown-menu">
                                                <li>
                                                    <a href="/issues-dashboard.html?type=js" title="">JavaScipt errors</a>
                                                </li>
                                                <li>
                                                    <a href="/issues-dashboard.html?type=xhr" title="">XmlHttpRequest errors</a>
                                                </li>
                                                <li>
                                                    <a href="/issues-dashboard.html?type=console" title="">Console errors</a>
                                                </li>
                                                <li>
                                                    <a href="/records.html" title="Your records">Reports by Users</a>
                                                </li>
                                                <li>
                                                    <a href="/errorFilters.html" title="Organization dashboard">Error filters</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                : null }
                                <ul className="nav navbar-nav navbar-right">
                                    <li className="dropdown" >
                                        <div className="dropdown-toggle dropdown-toggle-ala-slack" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i className="fa fa-fw fa-user"></i>
                                            {" "}
                                            Settings
                                            <span className="caret"></span>
                                        </div>
                                        <ul className="dropdown-menu dropdown-menu-ala-slack" aria-labelledby="dropdownMenu1" id="menuDropdownSettings">
                                            {this.state.userToken ? [
                                                <li>
                                                    <a href="/org.html">
                                                        <i className="fa fa-fw fa-users fa-fw"></i>
                                                        {" "}
                                                        Team users
                                                    </a>
                                                </li>,
                                                <li className="dropdown-header">
                                                        <i className="fa fa-fw fa-comments fa-fw"></i>
                                                        {" "}
                                                        Integrations
                                                </li>,
                                                <li>
                                                    <a href="/settings.html#tab-slack">
                                                        <i className="slack fa-fw"></i>
                                                        {" "}
                                                        Slack
                                                    </a>
                                                </li>,
                                                <li>
                                                    <a href="/settings.html#tab-telegram">
                                                        <i className="fa fa-fw fa-telegram fa-fw"></i>
                                                        {" "}
                                                        Telegram
                                                    </a>
                                                </li>,
                                                <li className="dropdown-header">
                                                        <i className="fa fa-fw fa-user-circle fa-fw"></i>
                                                        {" "}
                                                        Account
                                                </li>,
                                                <li>
                                                    <a href="/settings.html#tab-security">
                                                        <i className="fa fa-fw fa-user-circle fa-fw"></i>
                                                        {" "}
                                                        Account and security
                                                    </a>
                                                </li>,
                                                <li>
                                                    <a href="/billing.html">
                                                        <i className="fa fa-fw fa-credit-card fa-fw"></i>
                                                        {" "}
                                                        Billing and usage
                                                    </a>
                                                </li>,
                                                <li>
                                                    <a href="/support.html">
                                                        <i className="fa fa-fw fa-support fa-fw"></i>
                                                        {" "}
                                                        Support
                                                    </a>
                                                </li>,
                                                <li onClick={this.onLogoutClick}>
                                                    <a href="/logout" id="logoutLink" className="logoutLink">
                                                        <i className="fa fa-fw fa-sign-out fa-fw"></i>
                                                        {" "}
                                                        <span>Logout</span>
                                                    </a>
                                                </li>
                                            ] : [
                                                <li>
                                                    <a href="/signup.html">
                                                        <i className="fa fa-fw fa-user-plus"></i>
                                                        {" "}
                                                        <span>Sign up</span>
                                                    </a>
                                                </li>,
                                                <li>
                                                    <a href="/login.html">
                                                        <i className="fa  fa-fw fa-sign-in"></i>
                                                        {" "}
                                                        <span>Login</span>
                                                    </a>
                                                </li>
                                            ]}
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>

                    <IssuesQuotaExceededNotification />
                </nav>
            )
        },

        componentDidMount: function () {
            if (this.state.userToken) {
                GoogleSignIn.init();
            }
        }

    });

    return Header;

});
