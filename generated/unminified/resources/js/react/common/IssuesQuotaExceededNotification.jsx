define(["jquery", "react", "app/User"], function ($, React, User) {

    var IssuesQuotaExceededNotification = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                quotaExceeded: false,
                closed: false
            };
        },

        /* Props declaration */
        propTypes: {

        },

        closeNotification: function () {
            this.setState({
                closed: true
            });
        },

        render: function () {
            if (!this.state.quotaExceeded || this.state.closed) return null;

            return (
                <div id="issuesQuotaNotificationContainer">
                    <div className="container" id="issuesQuotaNotification">
                    <div className="row">
                    <div className="col-md-12">
                    <div >
                        <img className="issuesQuotaNotificationIcon" src="/resources/img/cat.png"></img>
                        <span className="issuesQuotaNotificationClose" onClick={this.closeNotification}>
                            <i className="fa fa-times" aria-hidden="true"></i>
                        </span>
                        <div className="issuesQuotaNotificationMessageText">
                            <h3 className="issuesQuotaNotificationMessageTitle">
                                Error quota exceeded
                            </h3>
                            <p className="issuesQuotaNotificationMessageContent">
                                Please, see <a href="/billing.html">billing and usage</a> page
                            </p>
                        </div>
                        <div style={{clear: "both"}}></div>
                    </div>
                    </div>
                    </div>
                    </div>
                </div>
            )
        },

        componentDidMount: function () {
            var self = this;
            User.getInfo(function (user) {
                self.setState({
                    quotaExceeded: user.issuesQuotaExceeded
                })
            });
        }

    });

    return IssuesQuotaExceededNotification;

});