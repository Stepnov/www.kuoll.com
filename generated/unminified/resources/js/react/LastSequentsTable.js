define(["jquery", "react", "app/react/Table", "app/utils/UrlUtils", "app/utils/api"], function ($, React,Table, UrlUtils, api) {

    var LastSequentsTable = React.createClass({displayName: "LastSequentsTable",

        columns: {
            sequentNum: "Num",
            type: "Type"
        },

        cells: {
            type: function (sequent) {
                var sequentType = "";
                if (sequent.sequentClass) sequentType += sequent.sequentClass + " / ";
                sequentType += sequent.sequentType;
                if (sequent.sequentSubtype) sequentType += " / " + sequent.sequentSubtype;
                return (
                    React.createElement("span", null, sequentType)
                )
            }
        },

        statics: {
            active: false
        },

        /* State declaration */
        getInitialState: function () {
            return {
                recordCode: UrlUtils.getParameterByName("recordCode"),
                frameNum: UrlUtils.getParameterByName("frameNum"),
                sequents: []
            };
        },

        render: function () {
            return (
                React.createElement(Table, {columns: this.columns, cells: this.cells, data: this.state.sequents})
            )
        },

        componentDidMount: function () {
            var self = this;
            window.setInterval(function () {
                if (!LastSequentsTable.active) return;
                api("getLastSequents", {
                    recordCode: self.state.recordCode,
                    frameNum: self.state.frameNum
                }, function (resp) {
                    self.setState({
                        sequents: resp.sequents
                    });
                });
            }, 1000);
        }

    });

    return LastSequentsTable;

});