define(["jquery", "react", "app/react/Table", "app/utils/api", "jquery.cookie"], function ($, React,Table, api) {

    var UsersTable = React.createClass({displayName: "UsersTable",

        columns: {
            userId: "Id",
            email: "Email",
            creationTime: "Creation time",
            loginCount: "Login count"
        },

        cells: {
            userId: function (user) {
                return (
                    React.createElement("a", {href: "/userRecords.html?orgId=" + user.orgId + "&userId=" + user.userId}, user.userId)
                )
            }
        },

        /* State declaration */
        getInitialState: function () {
            return {
                userToken: null,
                users: []
            };
        },

        /* Props declaration */
        propTypes: {},

        render: function () {
            return (
                React.createElement(Table, {columns: this.columns, cells: this.cells, data: this.state.users})
            )
        },

        componentDidMount: function () {
            var self = this;
            api("admin/getUsers", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                if (resp.users) {
                    var users = resp.users.sort(function (a, b) {
                        return a.loginCount < b.loginCount
                    });
                    self.setState({
                        users: users
                    })
                }
            });
        }

    });

    return UsersTable;

});