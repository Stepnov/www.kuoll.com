define(["jquery", "react", "app/react/Table", "app/utils/api", "app/utils/DateUtils", "jquery.cookie"], function ($, React,Table, api, DateUtils) {

    var ScriptUsersTable = React.createClass({displayName: "ScriptUsersTable",

        columns: {
            domain: "Domain",
            loadingCount: "embedScript loaded",
            lastLoadTimestamp: "Last load date",
            users: "User info"
        },

        cells: {
            lastLoadTimestamp: function (row) {
                return DateUtils.formatDate(row.lastLoadTimestamp);
            },
            users: function (row) {
                return (
                    React.createElement("div", null, 
                        row.users.map(function (user) {
                            return (
                                React.createElement("div", {key: user.userId}, 
                                    "User ID: ", user.userId, ";" + ' ' +
                                    "Org ID: ", user.orgId, ";" + ' ' +
                                    "Plan name: ", user.planName, ";" + ' ' +
                                    "Issues quota left: ", user.issuesLeft
                                )
                            )
                        })
                    )
                )
            }
        },

        /* State declaration */
        getInitialState: function () {
            return {
                userToken: null
            };
        },

        render: function () {
            return (
                React.createElement(Table, {columns: this.columns, data: this.state.scriptUsers, cells: this.cells})
            )
        },

        componentDidMount: function () {
            var self = this;
            api("getScriptUsers", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                if (resp.scriptUsers) {
                    var scriptUsers = resp.scriptUsers;
                    self.setState({
                        scriptUsers: scriptUsers
                    })
                }
            });
        }

    });

    return ScriptUsersTable;

});
