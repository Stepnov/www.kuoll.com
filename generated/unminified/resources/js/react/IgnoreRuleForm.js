define(["jquery", "react"], function ($, React) {

    var IgnoreRuleForm = React.createClass({displayName: "IgnoreRuleForm",

        /* State declaration */
        getInitialState: function () {
            return {
                domainErrorMsg: null,
                selectorErrorMsg: null
            };
        },

        /* Props declaration */
        propTypes: {
            rule: React.PropTypes.object,
            onSubmit: React.PropTypes.func.isRequired,
            onDelete: React.PropTypes.func
        },

        render: function () {
            var rule = this.props.rule;
            return (
                React.createElement("div", null, 
                    React.createElement("form", {className: "form-horizontal", onSubmit: this.onSubmit}, 
                        React.createElement("div", {className: "form-group " + (this.state.domainErrorMsg ? " has-error" : "")}, 
                            React.createElement("label", {className: "control-label col-md-3"}, "CSS selector"), 
                            React.createElement("div", {className: "col-md-9"}, 
                                React.createElement("input", {className: "domain-input form-control", type: "text", ref: "domain", 
                                       placeholder: "Domain (RegExp)"}), 
                                React.createElement("span", {className: "help-block"}, this.state.domainErrorMsg)
                            )
                        ), 
                        React.createElement("div", {className: "form-group " + (this.state.selectorErrorMsg ? " has-error" : "")}, 
                            React.createElement("label", {className: "control-label col-md-3"}, "CSS selector"), 
                            React.createElement("div", {className: "col-md-9"}, 
                                React.createElement("textarea", {className: "selector-input form-control", ref: "selector", 
                                          placeholder: "CSS selector"}), 
                                React.createElement("span", {className: "help-block"}, this.state.selectorErrorMsg)
                            )
                        ), 
                        React.createElement("div", {className: "form-group"}, 
                            React.createElement("div", {className: "col-md-9 col-md-offset-3"}, 
                                rule ?
                                    React.createElement("div", {className: "delete-btn btn btn-danger", onClick: this.props.onDelete}, 
                                        React.createElement("i", {className: "fa fa-times"})
                                    )
                                    : null, 
                                React.createElement("button", {className: "rule-save btn btn-lg btn-success", type: "submit"}, 
                                    React.createElement("i", {className: "fa fa-plus"}), " ", rule ? "Save the rule" : "Add new rule"
                                )
                            )
                        )
                    )
                )
            )
        },

        componentDidMount: function () {
            var $domain = $(React.findDOMNode(this.refs.domain));
            var $selector = $(React.findDOMNode(this.refs.selector));
            if (!$domain.val()) $domain.val(this.props.rule ? this.props.rule.domain : "");
            if (!$selector.val()) $selector.val(this.props.rule ? this.props.rule.selector : "");
        },

        onSubmit: function (e) {
            var key = this.props.rule ? this.props.rule.key : null;
            var $domain = $(React.findDOMNode(this.refs.domain));
            var $selector = $(React.findDOMNode(this.refs.selector));
            var domain = $domain.val() || ".*";
            var selector = $selector.val();

            e.preventDefault();

            var hasErrors = false;
            try {
                new RegExp(domain);
            } catch (e) {
                hasErrors = true;
                this.setState({
                    domainErrorMsg: e.message
                });
            }
            try {
                document.querySelector(selector);
            } catch (e) {
                hasErrors = true;
                this.setState({
                    selectorErrorMsg: "Must be a valid CSS selector"
                });
            }
            if (hasErrors) {
                return;
            }

            this.props.onSubmit(domain, selector, key);
            $domain.val("");
            $selector.val("");
            this.setState({
                domainErrorMsg: "",
                selectorErrorMsg: ""
            })
        }

    });

    return IgnoreRuleForm;

});