define(["jquery", "react"], function ($, React) {

    var DeviceIcon = React.createClass({displayName: "DeviceIcon",

        /* Props declaration */
        propTypes: {
            userAgent: React.PropTypes.object
        },

        render: function () {
            var iconClass;
            var title;
            var userAgent = this.props.userAgent;
            if (userAgent && userAgent.device) {
                var vendor = userAgent.device.vendor;
                if (vendor) {
                    title = userAgent.device.vendor + " " + userAgent.device.model;
                    iconClass = "fa-mobile";
                } else {
                    title = "Desktop";
                    iconClass = "fa-laptop";
                }
            } else {
                title = "No information about user's device";
                iconClass = "fa-laptop default-icon";
            }

            return (
                React.createElement("i", {className: "fa fa-fw deviceTypeIcon " + iconClass, title: title}, " ")
            )
        }

    });

    return DeviceIcon;

});
