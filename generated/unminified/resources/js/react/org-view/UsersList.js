define(["jquery", "react", "app/utils/api", "app/utils/DateUtils"], function ($, React, api, DateUtils) {

    const UsersList = React.createClass({displayName: "UsersList",

        getInitialState: function () {
            return {
                users: [],
                invitations: []
            };
        },

        propTypes: {
            isAdmin: React.PropTypes.bool,
            newInvitations: React.PropTypes.array,
            onInvitationCanceled: React.PropTypes.func.isRequired
        },

        makeAdmin: function (user) {
            return () => {
                if (window.confirm("Give administration privilege to " + user.email +
                        "? Only new admin will be able to undone this action")) {
                    api('org/change_admin', {
                        userToken: $.cookie('userToken'),
                        newAdminUserId: user.userId
                    }, () => {
                        document.location.reload();
                    }, (errorMsg) => {
                        this.setState({
                            errorMsg
                        });
                    });
                }
            }
        },

        cancelInvitation: function (inv) {
            return () => {
                api('org/cancel_invitation', {
                    userToken: $.cookie('userToken'),
                    invitationCode: inv.code
                }, () => {
                    this.props.onInvitationCanceled(inv);
                    this.setState({
                        invitations: this.state.invitations.filter(i => i.code !== inv.code)
                    });
                });
            }
        },

        render: function () {
            const invitations = this.state.invitations.concat(this.props.newInvitations);
            return (
                React.createElement("div", null, 
                    React.createElement("h3", null, "User roles"), 
                    React.createElement("table", {className: "table table-hover"}, 
                    React.createElement("thead", null, 
                        React.createElement("th", null, "Email"), 
                        React.createElement("th", null, "Team role")
                    ), 

                    React.createElement("tbody", null, 
                    this.state.users.map((user) => (
                        React.createElement("tr", {className: ""}, 
                            React.createElement("td", {className: ""}, user.email), 
                            React.createElement("td", {className: ""}, 
                                user.isAdmin ?
                                    React.createElement("div", {className: "btn btn-default disabled"}, "Admin")
                                :
                                !user.isAdmin && this.props.isAdmin ?
                                    React.createElement("div", {className: "btn btn-default ", onClick: this.makeAdmin(user)}, "Make admin")
                                : null
                            )
                        )
                    )), 
                    invitations.map((inv) => (
                        React.createElement("tr", {className: ""}, 
                            React.createElement("td", {className: ""}, inv.email), 
                            React.createElement("td", {className: ""}, 
                                React.createElement("div", {className: "btn btn-default disabled"}, 
                                    "Invited ", DateUtils.formatDate(inv.creationDate)
                                ), 
                                " ", 
                                React.createElement("button", {className: "btn btn-default", onClick: this.cancelInvitation(inv)}, 
                                    React.createElement("i", {className: "fa fa-times cancel-invitation-btn", title: "Cancel invitation"
                                    }), 
                                    "Cancel"
                                )
                            )
                        )
                    ))
                    )
                    )
                )
            )
        },

        componentDidMount: function () {
            api("org/get_users", {
                userToken: $.cookie("userToken")
            }, (resp) => {
                this.setState({
                    users: resp.users,
                    invitations: resp.invitations
                });
            });
        }

    });

    return UsersList;

});