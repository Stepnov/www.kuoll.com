define(["jquery", "react", "app/react/BrowserIcon", "app/react/OsIcon", "app/react/DeviceIcon", "app/react/issue-filter/IssueFilterDropdown",
        "app/utils/DateUtils", "app/utils/api"],
    function ($, React, BrowserIcon, OsIcon, DeviceIcon, IssueFilterDropdown, DateUtils, api) {

    var builtInIssueTypes = {
        "consoleError": "Console Error",
        "JavaScript error": "JavaScript Error",
        "Server error": "Server error"
    };

    var IssueRow = React.createClass({displayName: "IssueRow",
    
        /* State declaration */
        getInitialState: function () {
            return {
                isDeleted: false
            };
        },
        
        /* Props declaration */
        propTypes: {
            issue: React.PropTypes.object.isRequired
        },

        toggleIssue: function (e) {
            e.stopPropagation();
            api(this.state.isDeleted ? "restoreIssue" : "deleteIssue", {
                issueId: this.props.issue.id
            });
            this.setState({
                isDeleted: !this.state.isDeleted
            });
        },
    
        render: function () {
            var link = document.createElement("a");
            link.href = this.props.issue.url;
            var domain = link.host;

            var isBuiltInIssueType = builtInIssueTypes[this.props.issue.type];
            var issueType = isBuiltInIssueType || (this.props.issue.type === "error" ? "End user report" : this.props.issue.type);

            return (
                React.createElement("tr", {className: "issue" + (this.state.isDeleted ? " deleted" : "")}, 
                    React.createElement("td", {className: "open-record-column"}, 
                        React.createElement("a", {href: "/play.html?recordCode=" + this.props.issue.recordCode + "&frameNum=" +
                    this.props.issue.endFrameNum + "&sequentNum=" + this.props.issue.endSequentNum}, 
                            React.createElement("i", {className: "fa fa-link"})
                        )
                    ), 
                    React.createElement("td", {className: "id-column"}, this.props.issue.externalUserId), 
                    React.createElement("td", {className: "website-column"}, domain), 
                    React.createElement("td", null, 
                        React.createElement(BrowserIcon, {userAgent: this.props.issue.userAgent, withText: true})
                    ), 
                    React.createElement("td", null, 
                        React.createElement("span", {title: new Date(this.props.issue.time).toUTCString()}, 
                            DateUtils.formatDate(this.props.issue.time)
                        )
                    ), 
                    React.createElement("td", null, issueType), 
                    React.createElement("td", null, 
                        React.createElement(OsIcon, {userAgent: this.props.issue.userAgent, withText: true}), 
                        React.createElement(DeviceIcon, {userAgent: this.props.issue.userAgent})
                    ), 
                    React.createElement("td", {className: "description-column"}, 
                        isBuiltInIssueType ?
                            React.createElement("code", null, this.props.issue.description)
                            : this.props.issue.description
                    ), 
                    React.createElement("td", null), 
                    React.createElement("td", {className: "deleteIssueIcon"}, 
                        React.createElement("i", {className: "fa fa-times", "aria-hidden": "true", onClick: this.toggleIssue}), 
                        this.state.isDeleted ? "Restore" : null
                    ), 
                    React.createElement("td", null, 
                        React.createElement(IssueFilterDropdown, {issue: this.props.issue})
                    )
                )
            )
        }
        
    });
    
    return IssueRow;    

});