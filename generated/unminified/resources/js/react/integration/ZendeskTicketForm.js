define(["jquery", "react", "app/utils/MessageUtils", "app/utils/api", "jquery.cookie"], function ($, React,MessageUtils, api) {

    var ZendeskTicketForm = React.createClass({displayName: "ZendeskTicketForm",

        /* State declaration */
        getInitialState: function () {
            return {
                ticketTitleError: "",
                ticketCommentError: ""
            };
        },

        /* Props declaration */
        propTypes: {
            onCreateTicket: React.PropTypes.func.isRequired,
            updateSharingState: React.PropTypes.func.isRequired
        },

        authenticateUser: function (subdomain, title, comment, recordLink, success, error) {
            var url = "https://" + subdomain + ".zendesk.com/oauth/authorizations/new?" +
                "&response_type=code" +
                "&redirect_uri=" + encodeURIComponent("http://localhost:8080/zendesk/handle_user_decision") +
                "&client_id=kuoll" +
                "&scope=" + encodeURIComponent("read write");
            window.open(url, "_blank", "width=500,height=500");
            MessageUtils.onMessage("zendesk-authentication-finished", function () {
                this.createTicket(title, comment, recordLink, success, error);
            });
        },

        createTicket: function (title, comment, recordLink, success, error) {
            var self = this;
            api("zendesk/create_ticket", {
                userToken: $.cookie("userToken"),
                title: title,
                comment: comment,
                recordLink: recordLink
            }, function (resp) {
                if (resp.done) {
                    success();
                } else if (resp.subdomain) {
                    self.authenticateUser(resp.subdomain, title, comment, recordLink, success, error);
                } else {
                    window.open("/settings.html#tab-zendesk");
                }
            }, error);
        },

        onSubmit: function (e) {
            var $title = $(React.findDOMNode(this.refs.ticketTitle));
            var $comment = $(React.findDOMNode(this.refs.ticketComment));
            var title = $title.val();
            var comment = $comment.val();

            var hasErrors = false;
            if (!title) {
                this.setState({
                    ticketTitleError: "Title can not be empty"
                });
            }
            if (!comment) {
                this.setState({
                    ticketCommentError: "Comment can not be empty"
                });
            }
            if (hasErrors) {
                return;
            }

            var self = this;
            this.createTicket(title, comment, document.location.href,
                function () {
                    self.props.updateSharingState("shared");
                    window.setTimeout(function () {
                        self.props.updateSharingState("share");
                    }, 5000);
                }, function (error) {
                    self.props.updateSharingState("error", error);
                });
            this.props.updateSharingState("sending");

            e.preventDefault();

            this.props.onCreateTicket();
        },

        render: function () {
            return (
                React.createElement("div", null, 
                    React.createElement("p", null, "Create a Zendesk ticket with this record"), 
                    React.createElement("form", {id: "create-ticket-form", onSubmit: this.onSubmit}, 
                        React.createElement("div", {className: "form-group"}, 
                            React.createElement("input", {className: "form-control", id: "ticket-title-fld", type: "text", 
                                placeholder: "Ticket title", ref: "ticketTitle"}), 
                            React.createElement("span", {className: "help-block", ref: "ticketTitleMsg"}, this.state.ticketTitleError)
                        ), 
                        React.createElement("div", {className: "form-group"}, 
                            React.createElement("textarea", {className: "form-control", id: "ticket-comment-fld", type: "text", 
                                placeholder: "Ticket comment", ref: "ticketComment"}), 
                            React.createElement("span", {className: "help-block", ref: "ticketCommentMsg"}, this.state.ticketCommentError)
                        ), 
                        React.createElement("div", {className: "form-group"}, 
                            React.createElement("input", {type: "submit", className: "btn btn-success", value: "Create"}), 
                            React.createElement("span", {className: "help-block", ref: "ticketCommentMsg"})
                        )
                    )
                )
            )
        }

    });

    return ZendeskTicketForm;

});