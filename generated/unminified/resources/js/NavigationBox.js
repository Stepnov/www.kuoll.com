define(["jquery", "app/utils/api", "app/google-signin", "jquery.cookie"], function ($, api, GoogleSignIn) {

    (function initNavBox() {
        var userToken = $.cookie("userToken");
        if (userToken) {
            GoogleSignIn.init();
            $("#logoutBtn").on("click", function (e) {
                if ("localhost" != document.location.hostname && mixpanel) {
                    mixpanel.track("Logout");
                    mixpanel.reset();
                }

                api("logout", {userToken: userToken});
                GoogleSignIn.logout();
                $.removeCookie("userToken", {domain: config.cookieDomain, path: "/"});
                document.location = "/";
                e.preventDefault();
            });
        }
        $(".hide-if-logged-in").css("display", "none");
        $(".hide-if-not-logged-in").css("display", "inline-block");
        if (typeof chrome != "undefined" && chrome.app && chrome.app.isInstalled) {
            $('.hide-if-extension-installed').css("display", "inline-block");
        }
    })();

});
