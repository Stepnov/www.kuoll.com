function showRunKuollBox() {
    $.ajax("/runKuollBox.html").done(function (data) {
        $(".runKuoll").html(data);
        $("body").addClass("isKuollInstalled");
    });
}
function onSuccessWebstoreInstall() {
    showRunKuollBox();
}

function onFailureWebstoreInstall() {
    console.log("onFailureWebstoreInstall " + JSON.stringify(arguments));
}

function installKuoll() {
    return chrome.webstore.install('https://chrome.google.com/webstore/detail/ljfpdodjbnkpbaenppoiifjkdlhfjcgn', onSuccessWebstoreInstall, onFailureWebstoreInstall);
}

$(window).load(function () {
    if (!$.browser.desktop || !$.browser.chrome) {
        $("body").addClass("isUnsupportedBrowser");
    } else {
        $("body").addClass("isSupportedBrowser");
    }

    if ($(".kuollIsInstalled").size() > 0) {
        showRunKuollBox();
    }
});