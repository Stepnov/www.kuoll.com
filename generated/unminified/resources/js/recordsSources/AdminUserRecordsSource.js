define(["jquery", "app/utils/api", "jquery.cookie"], function ($, api) {

    function getRecords(loadCallback, failCallback) {
        var userToken = $.cookie("userToken");
        if (!userToken) {
            var errorMsg = "userToken cookie is not valid";
            if (failCallback) {
                failCallback(errorMsg);
            } else {
                console.error(errorMsg);
            }
        }

        api("admin/getAllRecords", {
            userToken: userToken
        }, loadCallback, failCallback);
    }

    var AdminUserRecordsSource = {
        getRecords: getRecords
    };

    return AdminUserRecordsSource;

});
