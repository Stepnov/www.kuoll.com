define(["jquery", "stripe-checkout", "app/utils/api", "app/User", "jquery.cookie"], function ($, StripeCheckout, api, User) {

    StripeCheckout = window.StripeCheckout;

    var friendlyPlanName = {
        "team_month": "Kuoll, Inc.",
        "team_year": "Kuoll, Inc.",
        "starter_month": "Kuoll, Inc.",
        "starter_year": "Kuoll, Inc."
    };

    var planDescription = {
        "team_month": "Team plan, monthly",
        "team_year": "Team plan, yearly",
        "starter_month": "Starter plan, monthly",
        "starter_year": "Yearly plan, yearly"
    };

    var handler = StripeCheckout.configure({
        key: window.config.stripeKey,
        locale: 'auto'
    });

    function openCheckoutForm(planName, customerId, callback) {
        User.getInfo(function (user) {
            handler.open({
                name: friendlyPlanName[planName],
                description: planDescription[planName],
                email: user.email,
                image: "/resources/img/cat-circle.png",
                panelLabel: "Save",
                token: function (token) {
                    if (callback) {
                        callback(token);
                    } else {
                        api("billing/attach_card", {
                            token: token.id,
                            email: token.email,
                            customerId: customerId
                        });
                    }
                }
            });
        });
    }

    $(window).on('popstate', function() {
        handler.close();
    });

    return {
        openCheckoutForm: openCheckoutForm
    };

});
