define(
    ["jquery", "react", "app/react/common/Header", "app/react/issues-dashboard/LossesDashboard", "app/onboard"],
    function ($, React, Header, LossesDashboard) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(React.createElement(LossesDashboard), $("#losses-dashboard")[0]);
        });
    });                                                                                 