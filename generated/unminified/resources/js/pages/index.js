define(["jquery", "app/google-signin", "app/utils/api", "youtube-api", "app/onboard"], function ($, GoogleSignIn, api) {

    $(function () {
        GoogleSignIn.init();

        (function() {
            var userToken = $.cookie("userToken");
            if (userToken) {
                $(".hide-if-logged-in").css("display", "none");
                $(".hide-if-not-logged-in").css("display", "inline-block");
            }
        }());

        var $subscriptionForm = $("#subscriptionForm");
        var $emailInput = $("#emailInput");
        var $nameInput = $("#nameInput");
        var $fieldContainers = $(".fieldContainer");

        var $inputs = $subscriptionForm.find("input");
        $inputs.on("focus", function (e) {
            var $target = $(e.currentTarget);
            if (!$target.val()) {
                $fieldContainers.has($target).children(".fieldBlink").show();
            }
        });
        $inputs.on("blur", function (e) {
            var $target = $(e.currentTarget);
            $fieldContainers.has($target).children(".fieldBlink").hide();
        });
        $inputs.on("keydown", function (e) {
            var $target = $(e.target);
            $fieldContainers.has($target).children(".fieldBlink").hide();
            if (e.keyCode === 13) {
                submitForm(e);
            }
        });
        $inputs.find(".fieldContainer").click(function (e) {
            $(e.target).find("input").focus();
        });
        $inputs.find(".fieldBorder").click(function (e) {
            $(e.target).find("input").focus();
        });

        $("#logout").click(function (e) {
            if ("localhost" != document.location.hostname && mixpanel) {
                mixpanel.identify("");
                mixpanel.unregister("User id");
                mixpanel.unregister("User token");
            }

            api("logout", {
                userToken: $.cookie("userToken")
            });
            GoogleSignIn.logout();
            document.location = "/";
            e.preventDefault();
        });

        var timesSameEmail = 0;

        function haveFun() {
            var $theButton = $subscriptionForm.find('.join');

            timesSameEmail = Math.max(timesSameEmail + 1, 2);
            if (timesSameEmail === 2) {
                $theButton.text('✓ Already subscribed, thanks');
            } else if (timesSameEmail === 3) {
                $theButton.text('✓ Already subscribed, and you know it :)');
            } else if (timesSameEmail === 4) {
                $theButton.text('✓ Please, bring your friends here!');
            } else if (timesSameEmail === 5) {
                $theButton.text('✓ Stop clicking this button!');
            } else if (timesSameEmail === 6) {
                $theButton.text('✓✓✓✓✓✓✓ Enough!');
            } else if (timesSameEmail >= 7) {
                $theButton.text('You have pressed this button ' + timesSameEmail + ' times!');
            }
        }

        function submitForm(e) {
            e.preventDefault();
            var email = $emailInput.val();

            var $theButton = $subscriptionForm.find('.join');
            api("saveEmail", {
                email: email,
                name: $nameInput.val()
            }, function (resp) {
                $theButton.text('✓ Subscribed, thank you!').css({"background-color": "#2ecc71"});
                ga('send', 'event', 'email', 'submit', email, 1);
                $theButton.text('× Error. Please, enter your email').css({"background-color": "#e74c3c"});
                $emailInput.focus();
                console.log(resp);
                ga('send', 'event', 'email', 'submit', email, 0);
            }, function (errorMsg) {
                if (errorMsg === 'invalid') {
                    $theButton.text('× Please, enter valid email').css({"background-color": "#9b59b6"});
                    $emailInput.focus();
                    ga('send', 'event', 'email', 'submit', email, -1);
                } else if (errorMsg === 'occupied') {
                    $theButton.css({"background-color": "#9b59b6"});
                    haveFun();
                    ga('send', 'event', 'email', 'submit', email, timesSameEmail);
                }
            });
            mixpanel.people.set({
                $email: email
            });
        }
        $subscriptionForm.submit(submitForm);
        $(".join").click(submitForm);

        $(".asciiNavBarContainer").show();

        function onPlayerStateChange(event) {
            var stateName = false;
            if (event.data == YT.PlayerState.PLAYING) {
                stateName = 'playing';
                console.log(event.data);
            }else if (event.data === YT.PlayerState.PAUSED){
                stateName = 'paused';
            } else if(event.data === YT.PlayerState.BUFFERING){
                stateName = 'buffering';
            } else if(event.data === YT.PlayerState.ENDED){
                stateName = 'ended'
            }
            if(stateName){
                ga('send','event', 'video','click', stateName);
            }
        }

        var readyPlayer;
        var play = false;
        function onPlayerReady(e) {
            readyPlayer = e.target;
            if(play) {
                readyPlayer.playVideo();
            }
        }

        function initYt() {
            var playerWidth = $("#playerWidthContainer").width();
            var width = Math.round(playerWidth);
            var height = Math.round(3 * playerWidth / 4);
            $("#playerContainer").show();
            try {
                player = new YT.Player('player', {
                    height: height,
                    width: width,
                    videoId: 'XIdBN_mQYKc',
                    events: {
                        'onStateChange': onPlayerStateChange,
                        'onReady': onPlayerReady
                    }
                });
            } catch(e) {
                console.log(e);
                setTimeout(initYt, 300);
            }
        }
        var player;
        setTimeout(initYt, 300);

        $(".watchVideo").click(function () {
            play = true;
            try {
                readyPlayer.playVideo();
            } catch(e){}
        });
    });

    $("#logoutBtn")[0].addEventListener("click", function (e) {
        if ("localhost" !== document.location.hostname && mixpanel) {
            mixpanel.track("Logout");
            mixpanel.reset();
        }

        api("logout", {
            userToken: $.cookie("userToken")
        });
        GoogleSignIn.logout();
        document.location = "/";
        e.preventDefault();
    }, true);

});