define(["jquery", "react", "app/react/admin/UsersTable"], function ($, React,UsersTable) {

    $(function () {
        React.render(React.createElement(UsersTable), $("#table")[0]);
    });

});
