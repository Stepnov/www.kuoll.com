define(["jquery", "react", "app/react/admin/ScriptUsersTable", "app/react/admin/ImplicitRecordsInfoTable"], function ($, React,ScriptUsersTable, ImplicitRecordsInfoTable) {

    $(function () {
        React.render(React.createElement(ScriptUsersTable), $("#scriptUsers")[0]);
        React.render(React.createElement(ImplicitRecordsInfoTable), $("#implicitRecordsInfo")[0]);
    });

});