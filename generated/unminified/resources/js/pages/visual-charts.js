define(
    ["jquery", "react", "app/react/visual-charts/VisualCharts", "app/react/common/Header"],
    function ($, React, VisualCharts, Header) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(React.createElement(VisualCharts), $("#visual-charts")[0]);
        });
    });