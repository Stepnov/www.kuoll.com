define(
    ["jquery", "react", "app/react/common/Header", "app/react/settings/Billing", "app/react/IgnoreRulesTable",
        "app/react/IgnoreUrlList", "app/react/issue-filter/IssueFilterList",
        "app/utils/api", "app/User", "bootstrap", "jquery.cookie", "jquery.qtip"],
    function ($, React, Header, Billing, IgnoreRulesTable, IgnoreUrlList, IssueFilterList, api, User) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(React.createElement(IssueFilterList), $("#errorFilters")[0]);
            // React.render(React.createElement(IgnoreRulesTable), $("#rules-table")[0]);
            // React.render(React.createElement(IgnoreUrlList), $("#tab-excluded-urls")[0]);

        });
    }
);
