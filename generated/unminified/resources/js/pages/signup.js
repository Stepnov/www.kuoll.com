define(["jquery", "app/utils/UrlUtils", "app/utils/TipUtils", "app/google-signin", "app/utils/api", "analytics/segment", "jquery.cookie"], function ($, UrlUtils, TipUtils, GoogleSignIn, api, segment) {

    function deleteSharedRecordData(sharedRecordToken) {
        api("deleteSharedRecordData", {
            sharedRecordToken: sharedRecordToken
        });
    }

    var $emailField = $('#emailField');
    var $passwordField = $('#passwordField');
    var $repeatPasswordField = $('#repeatPasswordField');

    var sharedRecordToken = UrlUtils.getParameterByName("sharedRecordToken");

    var invitationCode = UrlUtils.getParameterByName("invitationCode");

    var redirectToRecord, alreadyLogged, recordLink;
    if (sharedRecordToken) {
        var userToken = $.cookie("userToken");
        if (userToken) {
            api("checkUserToken", {
                userToken: userToken
            }, function (resp) {
                if (resp.userExists) {
                    alreadyLogged = true;
                }
            });
        }
        api("getSharedRecordData", {
            token: sharedRecordToken
        }, function (resp) {
            $emailField.val(resp.email);
            redirectToRecord = true;
            recordLink = "/play.html?recordCode=" + resp.recordCode;
            if (resp.frameNum) {
                recordLink += "&frameNum=" + resp.frameNum;
            }
            if (resp.sequentNum) {
                recordLink += "&sequentNum=" + resp.sequentNum;
            }
            recordLink += "&showVideo=1";
            if (alreadyLogged) {
                document.location = recordLink;
                deleteSharedRecordData(sharedRecordToken);
            }
        });
    }

    var email = UrlUtils.getParameterByName("email");
    if (email) {
        $emailField.val(decodeURIComponent(email));
    }

    var password = UrlUtils.getParameterByName("password");
    if (password) {
        password = decodeURIComponent(password);
        $passwordField.val(password);
        $repeatPasswordField.val(password);
    }

    function showErrorMsg(errorMsg) {
        var $errorBox = $('#errorMessageBox');
        $errorBox.text(errorMsg).css("visibility", "visible");
        for (var i = 1; i <= 2; ++i) {
            $errorBox
                .animate({"padding-left": "18"}, 75)
                .animate({"padding-left": "14"}, 75)
        }
        if ("localhost" != document.location.hostname && mixpanel) {
            mixpanel.track("Signup error", {
                "Error message": errorMsg
            })
        }
    }

    function register(event) {
        var $errorMessageBox = $('#errorMessageBox');
        $errorMessageBox.text("").css("visibility", "collapse");
        var errorMsg;
        if ($passwordField.val() !== $repeatPasswordField.val()) {
            errorMsg = "Passwords must be equal";
        } else if (!$emailField.val() || !$passwordField.val() || !$repeatPasswordField.val()) {
            errorMsg = "All fields are required";
        } else {
            var $btn = $("#formSubmit").removeClass("btn-primary").addClass("btn-default");
            var $icon = $btn.find("i").hide();
            var $spin = $btn.find(".fa-spin").show() || $("<i class='fa fa-circle-o-notch fa-spin'></i>").insertAfter($icon);

            api("registration", {
                email: $emailField.val(),
                password: $passwordField.val(),
                sendConfirmationEmail: true,
                sharedRecordToken: sharedRecordToken,
                invitationCode: invitationCode,
                planName: UrlUtils.getParameterByName("plan") || $.cookie("kuoll-preselected-plan")
            }, function (resp) {
                $.cookie("userToken", resp.userToken, {domain: window.config.cookieDomain, path: "/"});
                $.removeCookie("isDemoUser");
                if ("localhost" != document.location.hostname && mixpanel) {
                    mixpanel.alias(resp.userId);
                    mixpanel.register({
                        "User token": resp.userToken,
                        "User id": resp.userId
                    });
                }
                segment.identify(resp);
                if (redirectToRecord) {
                    document.location = recordLink;
                    deleteSharedRecordData(sharedRecordToken);
                } else if (resp.subscriptionCreated) {
                    $.removeCookie("kuoll-preselected-plan");
                    document.location = "/issues-dashboard.html#onboardInstall";
                } else {
                    document.location = "/issues-dashboard.html#onboardInstall";
                }

                $icon.show();
                $spin.hide();
                $btn.addClass("btn-primary").removeClass("btn-default");
            }, function (errorMsg) {
                showErrorMsg(errorMsg);
            });
        }

        if (errorMsg) {
            showErrorMsg(errorMsg);
        }
        event.preventDefault();
    }

    var $signUpForm = $('#sign-up-form');
    $signUpForm.submit(register);

    var githubError = UrlUtils.getParameterByName("github_error");
    if (githubError) {
        if (githubError === "no_email") {
            showErrorMsg("You didn't set an email in your GitHub account");
        } else {
            showErrorMsg("Unknown error while signing in using GitHub. We're already investigating the problem. Please use another signin method");
        }
    }
    if (invitationCode) {
        api("org/get_user_invitation", {
            invitationCode: invitationCode
        }, function (resp) {
            $emailField.val(resp.email)
                .attr('readonly', true);
            TipUtils.makeSimpleTip($emailField, 'Invitation was sent to this email. It cannot be changed');
            $('#social-login').hide();
            $('#social-login-disabled-alert').show();
        }, function (errorMsg) {
            showErrorMsg(errorMsg);
        });
    }

    GoogleSignIn.init({
        onSuccess: function () {
            $(".hidden-logged-in").hide();
        },
        onLogout: function () {
            $(".hidden-logged-in").show();
        },
        checkUserToken: true
    });

    var redirectUrl = config.apiServer + "slack/redirect_url";
    $(".slack-link").attr("href",
        encodeURI("https://slack.com/oauth/authorize?scope=identity.basic,identity.email&client_id=" + config.slackClientId
            + "&state=authenticate&redirect_uri=" + redirectUrl));

});