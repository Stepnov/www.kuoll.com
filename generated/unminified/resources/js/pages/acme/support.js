define(["jquery", "app/User", "jquery.cookie"], function ($, User) {

    $("#startBtn").on("click", function (e) {
        $("#hiddenText").show();
        function startWithDefaultUser() {
            console.warn("userToken is undefined or not valid, so record started for default user");
            kuoll.startRecord({
                orgId: "1",
                customerId: "1",
                userId: "1",
                startCallback: function () {
                    $("<p class='alert alert-success'>Recording has started. " +
                    "Not you can go to <a href='acme.html'>ACME web application</a> to see how Kuoll recording works. " +
                    "Recording continues on all pages that contain <code>embedScript.js</code>.</p>")
                        .insertAfter(e.target);
                    $(e.target).hide();
                }
            });
        }

        // ID of fake user created in InitService
        var userToken = $.cookie("userToken");
        if (userToken) {
            User.getInfo(function (user) {
                if (user.userId && user.orgId) {
                    kuoll.startRecord({
                        customerId: user.customerId,
                        orgId: user.orgId,
                        userId: user.userId
                    });
                } else {
                    startWithDefaultUser();
                }
            });
        } else {
            startWithDefaultUser();
        }
    });
    $("#finishBtn").on("click", function () {
        kuoll.stopRecord();
    });

});
