define(
    ["jquery", "react", "app/react/common/Header", "app/User"],
    function ($, React, Header, User) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
        });

        User.getInfo(function (user) {
            $(".api-key").text(user.apiKey);
        });

        var $tabsNav = $("#tabs-nav");
        $tabsNav.on("click", function (e) {
            document.location = e.target.getAttribute("href");
        });
        $tabsNav.find("a[href='" + document.location.pathname + document.location.hash + "']").parent().addClass("active");

    });