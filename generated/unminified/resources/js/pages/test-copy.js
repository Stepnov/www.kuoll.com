define(["jquery", "app/User", "jquery.cookie", "jquery.fittext", "bootstrap"], function ($, User) {

    $(".fitText").fitText();

    $("#errorButton").click(function () {
        throw new Error();
    });
    $("#testForm").submit(function (e) {
        console.log($("#testy").val());
        e.preventDefault();
    });
    $("#submitButton").click(function (e) {
        console.log($("#testy").val());
        e.preventDefault();
    });

    $("#timeoutBtn").click(function () {
        setTimeout(function () {
            $("#timeoutDiv").html("in setTimeout callback after 1000ms delay");
        }, 1000);

        var cancelableTimeout = setTimeout(function () {
            $("#timeoutDiv").html("This timeout will be cleared and this function should be newer called");
        }, 1000);
        clearTimeout(cancelableTimeout);
    });

    var $focusInput = $("#focusInput");
    $focusInput.focus(function () {
        $("#focusDiv").html("#focusInput focused");
    });
    $focusInput.blur(function () {
        $("#focusDiv").html("#focusInput unfocued (blur)");
    });

    $("#sendPasswordBtn").on("click", function (e) {
        var password = $("#passwordField").val();
        $.ajax({
            url: "sample.json",
            type: "post",
            data: {
                "debug_param1": 1,
                "debug_param2": "test",
                "password": password
            }
        }).done(
            function (data) {
                var data = typeof data == "string" ? JSON.parse(data) : data;
                $("#passwordSendResult").text(data.message);
            }
        );
        $.ajax({
            url: "secret.json",
            type: "GET",
            data: {
                "password": password
            }
        }).done(function () {
            // do nothing
        });
    });

    $("#setCookieBtn").on("click", function (e) {
        document.cookie = $("#cookieFld").val();
        e.preventDefault();
    });

    var $runAnimationBtn = $("#runAnimationBtn");
    $runAnimationBtn.click(function () {
        $runAnimationBtn.animate({"margin-left": 400}, 250).animate({"margin-left": 0}, 250);
    });

    $("#postMessageBtn").on("click", function () {
        window.postMessage("Sent!",  document.location.protocol + "//" + document.location.host);
    });
    window.addEventListener("message", function (e) {
        if (typeof e.data === "string") {
            $("#postMessageContainer").text(e.data);
        }
    });


    $("#startBtn").on("click", function () {
        function askToLogin() {
            window.alert("Login to your Kuoll account to start recording please");
            document.location = "/login.html?redirectTo=test.html";
        }

        $("#hiddenText").show();

        var userToken = $.cookie("userToken");
        if (userToken) {
            var kuoll = window.kuoll || window.kuollDev;
            if (kuoll) {
                User.getInfo(function (user) {
                    kuoll.startRecord({
                        orgId: user.orgId,
                        API_KEY: user.apiKey,
                        kuollUserId: user.userId,
                        localRecording: false,
                        ignoreUrls: ["http:\/\/localhost:8080\/secret\.json"]
                    });
                }, function () {
                    askToLogin();
                });
            }
        } else {
            askToLogin();
        }
    });
    $("#finishBtn").on("click", function () {
        kuoll.stopRecord();
    });
    $("#generate-sequents-btn").on("click", function () {
        var $collapseFour = $("#collapseFourControl");
        var i = 0;
        var callback = function () {
            $collapseFour.click();
            console.log("Toggled " + i + " time");
            ++i;
            if (i < 50)
                setTimeout(callback, 30);
        };
        setTimeout(callback, 30);
    });

    $("#promise-btn").on("click", function () {
        new Promise(function (resolve, reject) {
            console.log("In promise");
            resolve("Promise resolved");
        }).then(function (text) {
            $("#promise-resolve-container").text(text);
            console.log(text);
        });
        new Promise(function (resolve, reject) {
            console.log("In promise");
            reject("some error");
        }).catch(function (error) {
            $("#promise-reject-container").text("Expected error happened: " + error);
            console.warn(error);
        });
    });

    //FS.identify($.cookie("userToken"));

});
