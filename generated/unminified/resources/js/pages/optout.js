define(["jquery", "react", "app/react/common/Header"], function ($, React,Header) {
    $(function () {
        React.render(React.createElement(Header), $("#header")[0]);

        var $btn = $("#opt-out-btn");

        if ($.cookie("kuoll_optout") === "1") {
            $btn.addClass("in").text("Opt in to Kuoll");
        } else {
            $btn.addClass("out").text("Opt out of Kuoll");
        }

        $btn.on("click", function () {
            if ($btn.hasClass("out")) {
                $btn.removeClass("out").addClass("in").text("Opt in to Kuoll");
                $.cookie("kuoll_optout", "1", {expires: 9999, domain: window.config.cookieDomain, path: "/"});
            } else {
                $btn.removeClass("in").addClass("out").text("Opt out of Kuoll");
                $.cookie("kuoll_optout", "0", {expires: 9999, domain: window.config.cookieDomain, path: "/"});
            }
        });
    });
});