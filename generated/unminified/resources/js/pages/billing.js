define(
    ["jquery", "react", "app/react/common/Header", "app/react/settings/Billing", "app/User"],
    function ($, React, Header, Billing, User) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
        });

        User.getInfo(function (user) {
            $(".api-key").text(user.apiKey);

            if (user.isAdmin) {
                // TODO @Vlad, please review the code below
                $('#billing-tab-link').show();
                React.render(React.createElement(Billing), $("#tab-billing")[0]);
            }

        });

        var $tabsNav = $("#tabs-nav");
        $tabsNav.on("click", function (e) {
            document.location = e.target.getAttribute("href");
        });

        $tabsNav.find("a[href='" + document.location.pathname + document.location.hash + "']").parent().addClass("active");

    });