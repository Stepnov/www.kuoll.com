define(
    ["jquery", "react", "app/react/common/Header", "app/react/org-view/OrgView", "app/User"],
    function ($, React, Header, OrgView, User) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(React.createElement(OrgView), $("#org-view")[0]);
        });

        User.getInfo(function (user) {
            $(".api-key").text(user.apiKey);
        });

        var $tabsNav = $("#tabs-nav");
        $tabsNav.on("click", function (e) {
            document.location = e.target.getAttribute("href");
        });

        $tabsNav.find("a[href='" + document.location.pathname + document.location.hash + "']").parent().addClass("active");

    });