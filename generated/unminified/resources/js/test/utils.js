define([], function () {

    function validateObject(actual, expected, oldResult) {
        var result = oldResult || {};
        if (!expected || expected == {}) {
            result.pass = true;
            return result;
        }

        if (typeof actual !== "object") {
            result.pass = false;
            result.message = "actual is not js object";
            return result;
        }

        result.pass = true;
        var keys = Object.getOwnPropertyNames(expected);
        for (var i = 0; i < keys.length && result.pass !== false; ++i) {
            var key = keys[i];

            var expectedValue = expected[key];
            if (actual.hasOwnProperty(key)) {
                var actualValue = actual[key];

                if (typeof expectedValue === "object") {
                    if (typeof actualValue == "string") {
                        try {
                            actualValue = JSON.parse(actualValue);
                        } catch (e) {
                            result.pass = false;
                            result.message = "Assumed that field " + key + " of actual is stringified JSON, but it' not";
                        }
                    }
                    validateObject(actualValue, expectedValue, result);
                } else {
                    if (expectedValue != actualValue) {
                        result.pass = false;
                        result.message = "Expected and actual value are different. Expected: " + expectedValue + ", actual: " + actualValue;
                    }
                }
            } else {
                result.pass = false;
                result.message = "Actual should contain own property: " + key;
            }
        }
        return result;
    }

    function containsObject(array, info) {
        var times = 0;
        for (var i = 0; i < array.length; ++i) {
            var result = validateObject(array[i], info);
            if (result.pass) {
                times++;
            }
        }
        return times;
    }

    function getObject(array, info) {
        for (var i = array.length - 1; i >= 0; --i) {
            var result = validateObject(array[i], info);
            if (result.pass) {
                return array[i];
            }
        }
        return null;
    }

    function flatArray(array) {
        return array.map(function (item) {return item[0]});
    }

    function waitForCall(obj, methodName, callback) {
        var spy = jasmine.isSpy(obj[methodName]) ? obj[methodName] : spyOn(obj, methodName);
        spy.and.callFake(function () {
            spy.and.callThrough();
            callback.apply(this, arguments);
        })
    }

    return {
        validateObject: validateObject,
        containsObject: containsObject,
        getObject: getObject,
        flatArray: flatArray,
        waitForCall: waitForCall
    }

});
