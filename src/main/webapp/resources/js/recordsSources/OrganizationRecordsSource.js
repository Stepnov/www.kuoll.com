define(["jquery", "app/utils/UrlUtils", "app/utils/api", "app/User", "jquery.cookie"], function ($, UrlUtils, api, User) {

    function getOrgId(callback, failCallback) {
        var orgId = UrlUtils.getParameterByName("orgId");
        if (orgId) {
            callback(orgId);
        } else {
            var userToken = $.cookie("userToken");
            if (!userToken) {
                failCallback();
            } else {
                User.getInfo(function (user) {
                    callback(user.orgId);
                }, function () {
                    failCallback();
                });
            }
        }
    }

    function getRecords(loadCallback, failCallback) {
        getOrgId(function (orgId) {
            api("getOrgRecords", {
                orgId: orgId
            }, loadCallback, failCallback);
        }, failCallback);
    }

    var OrganizationRecordsSource = {
        getRecords: getRecords
    };

    return OrganizationRecordsSource;

});
