define(["jquery", "app/utils/UrlUtils", "app/utils/api"], function ($, UrlUtils, api) {

    function getRecords(loadCallback, failCallback) {
        var orgId = UrlUtils.getParameterByName("orgId");
        var userId = UrlUtils.getParameterByName("userId");

        if (!userId) {
            var errorMsg = "userId param must be specified";
            console.error(errorMsg);
            if (failCallback) {
                failCallback(errorMsg);
            }
            return;
        }

        api("getRecords", {
            orgId: orgId,
            userId: userId
        }, loadCallback, failCallback);
    }

    var UserRecordsSource = {
        getRecords: getRecords
    };

    return UserRecordsSource;

});
