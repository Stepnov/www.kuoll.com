define(["jquery", "app/utils/api", "jquery.cookie"], function ($, api) {
    var OwnRecordsSource = (function () {
        return {
            getRecords: function (loadCallback, failCallback) {
                var userToken = $.cookie("userToken");
                if (!userToken || "0" === userToken || "null" === userToken || "undefined" === userToken) {
                    if (failCallback)
                        failCallback("userToken cookie is not valid");
                    return;
                }

                api("getRecords", {
                    userToken: userToken
                }, loadCallback, failCallback);
            }
        }
    })();
    return OwnRecordsSource;
});
