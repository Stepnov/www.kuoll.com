define(["jquery", "app/utils/api", "analytics/segment", "jquery.cookie"], function ($, api, segment) {

    var userInfo;

    var isLoading = false;
    var requests = [];

    function shouldRedirect(pathname) {
        var noRedirectPaths = ["/login.html", "/signup.html"];
        for (var i = 0; i < noRedirectPaths.length; ++i) {
            if (pathname.indexOf(noRedirectPaths[i]) == 0) {
                return false;
            }
        }
        return true;
    }

    function success() {
        isLoading = false;

        segment.identify(userInfo);

        requests.forEach(function (request) {
            if (request.callback) {
                request.callback(userInfo);
            }
        });
    }

    function fail(error, redirectLocation) {
        isLoading = false;
        requests.forEach(function (request) {
            if (error === "redirect" && request.redirect !== false) {
                window.location.href = redirectLocation;
            } else {
                if (request.failCallback) {
                    request.failCallback();
                }
            }
        })
    }

    function loadInfo() {
        var userToken = $.cookie("userToken");
        if (!userToken) {
            fail("redirect", "/login.html");
            return;
        }

        isLoading = true;

        api("getUserId", {
            userToken: userToken
        }, function (resp) {
            if (!resp) {
                fail("redirect", "/login.html")
            } else {
                userInfo = resp;
                success();
            }
        }, function () {
            console.error(arguments);
            fail("redirect", "/login.html");
        }, false);
    }

    function getInfo(callback, failCallback, redirect) {
        if (userInfo) {
            if (callback) {
                callback(userInfo);
            }
        } else {
            requests.push({
                callback : callback,
                failCallback: failCallback,
                redirect: redirect
            });
            if (!isLoading) {
                loadInfo();
            }
        }
    }

    return {
        getInfo: getInfo,

        requireAuthenticated: function () {
            getInfo(null, null, true);
        }
    };

});
