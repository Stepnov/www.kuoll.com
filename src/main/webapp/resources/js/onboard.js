define(["jquery", "app/google-signin","app/User", "app/utils/api", "youtube-api"], function ($, GoogleSignIn, User, api) {

    var userHasRecords = false;
    var userHasMessenger = false;

    function calculateNextStep(callback) {
        var userToken = $.cookie("userToken");
        var nextStep;
        try {
            if (userToken) {
                User.getInfo(function (user) {
                    $(".api-key").text(user.apiKey);
                    userHasRecords = user.hasRecords;
                    userHasMessenger = user.isSlackSetup || user.isTelegramSetup;
                    if(!userHasRecords) {
                        nextStep = "#onboardInstall";
                    } else if(!userHasMessenger) {
                        nextStep = "#onboardMessenger";
                    } else {
                        nextStep = "#onboardThanks";
                    }
                    callback(nextStep);
                });
            } else {
                nextStep = "#onboardSignup";
                callback(nextStep);
            }
        } catch(e) {
            console.error(e);
            nextStep = "#onboardSignup";
            callback(nextStep);
        }
    }

    function onboardContainerLoaded() {

        $onboard = $(".onboard");
        $crazyCat = $(".crazyCat");
        var $pageScrollOverflowY = $(".pageScrollOverflowY")[0] || document.body;
        $pageScrollOverflowY = $($pageScrollOverflowY);

        function scrollTo(elt) {
            var top = $(elt).offset().top;
            $('html, body').animate({
                scrollTop: top
            }, 500);
        }
        $(function () {
            $(".btn-scroll-to").click(function(event) {
                event.preventDefault();
                var href = this.hash;
                scrollTo(href);
            });
        });
        function hideOnboard() {
            var hash = document.location.hash;
            hash = hash.replace(/#onboard[A-Za-z]*/, "#");
            if ("#" == hash) {
                hash = "";
            }
            if (document.location.hash != hash) {
                document.location.hash = hash;
            }

            var duration = 250
            $onboard.animate({top: "-100%"}, duration, "swing");
            setTimeout(function () {
                $pageScrollOverflowY.css("overflow-y", "scroll");
                $onboard.css("display", "hidden");
                $crazyCat.hide();
            }, duration);
        }
        $onboard.click(function(event) {
            if(event.target == this) {
                hideOnboard();
            }
        });
        $(".closeFullScreenmodal").click(function(event) {
            hideOnboard();
        });
        window.addEventListener("keyup", function (e) {
            if (e.keyCode == 27) {
                hideOnboard();
            }
        });
        
        function onboardingStep(step) {
            $(".onboardStep").hide();
            var $step = $(step);
            if ($step.length == 0) {
                console.error("Unknown step: " + step);
                step = "#onboardSignup";
                $step = $(step);
            }
            $step.show();
            $onboard.css("display", "block");
            var duration = 350;
            $onboard.animate({top: 0}, duration, "swing");
            setTimeout(function () {
                $pageScrollOverflowY.css("overflow-y", "hidden");
                $crazyCat.show();
                (function() {
                    var userToken = $.cookie("userToken");
                    if (userToken) {
                        $(".hide-if-logged-in,.hidden-logged-in").css("display", "none");
                        $(".hide-if-not-logged-in,.visible-logged-in").css("display", "inline-block");
                    }
                }());

            }, duration);
            if ("#onboardSignup" == step) {
                require(["app/pages/signup"]);
            }
        }   
        function checkOnboardHash() {
            if (document.location.hash.startsWith("#onboard")) {
                calculateNextStep(function(nextStep) {
                    if("#onboardOptional" == document.location.hash &&
                       "#onboardThanks" == nextStep) {
                        hideOnboard();                   
                    } else if (document.location.hash != nextStep) {
                        document.location.hash = nextStep;
                    } else {
                        onboardingStep(nextStep);
                    }
                });
            } else {
                hideOnboard();
            }
        } 

        window.addEventListener("hashchange",function(event){
            checkOnboardHash();
        });
        if (document.location.hash.startsWith("#onboard")) {
            checkOnboardHash();
        }

        $("a[href$='/signup.html']").each(function(i, e) {
            $(e).attr("href", '#onboard');
        });

        $("#copyCodeSnippetInstallKuoll").click(function() {
            function selectText(element) {
                var doc = document
                    , text = doc.getElementById(element)
                    , range, selection
                ;    
                if (doc.body.createTextRange) {
                    range = document.body.createTextRange();
                    range.moveToElementText(text);
                    range.select();
                } else if (window.getSelection) {
                    selection = window.getSelection();        
                    range = document.createRange();
                    range.selectNodeContents(text);
                    selection.removeAllRanges();
                    selection.addRange(range);
                }
            }
            selectText("codeSnippetInstallKuoll");
            document.execCommand('copy');
            $("#codeSnippetInstallKuollCopiedSuccess").css("display", "inline-block");
        });

        var redirectUrl = config.apiServer + "slack/redirect_url";
        $(".slack-link").attr("href",
        encodeURI("https://slack.com/oauth/authorize?scope=identity.basic,identity.email&client_id=" + config.slackClientId
            + "&state=authenticate&redirect_uri=" + redirectUrl));

    } // end onboard container loaded

    var $onboardContainer = $("<div>", {id: "onboardContainer", class: "onboardContainer"});
    $("body").append($onboardContainer);
    var modalHtml = config.isWww ? config.contentServer + "onboardModal.html" : "/onboardModal.html";
    $onboardContainer.load(modalHtml, function (text, result) {
        if (result == "success") {
            onboardContainerLoaded();
        }
    });

    

});

