define([], function () {

    function onMessage(type, callback) {
        window.addEventListener("message", function (e) {
            if (e.origin === document.location.protocol + "//" + document.location.host) {
                var data = e.dataParsed || JSON.parse(e.data);
                e.dataParsed = data;
                if (data.type === type) {
                    callback(data);
                }
            }
        });
    }

    function sendMessage(window, type, data) {
        data.type = type;
        window.postMessage(JSON.stringify(data), "/");
    }

    return {
        onMessage: onMessage,
        sendMessage: sendMessage
    };

});
