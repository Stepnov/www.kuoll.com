define(["jquery", "react", "app/react/Notification", "app/utils/api", "jquery.cookie"], function ($, React,Notification, api) {

    var userToken = $.cookie("userToken");
    var interval;

    function fetchNotificationsToShow() {
        api("getNotifications", {
            userToken: userToken
        }, function (resp) {
            var types = resp.notificationTypes;
            if (types) {
                for (var i = 0; i < types.length; ++i) {
                    var $appendNode = $("<div/>", {id: "user-notification-" + i}).appendTo(document.body);
                    React.render(React.createElement(Notification, {type: types[i]}), $appendNode[0]);
                }
            }
        }, function () {
            window.clearInterval(interval);
        }, false);
    }

    $(function () {
        if (userToken) {
            interval = window.setInterval(fetchNotificationsToShow, 5 * 60 * 1000);
            fetchNotificationsToShow();
        }
    });

});
