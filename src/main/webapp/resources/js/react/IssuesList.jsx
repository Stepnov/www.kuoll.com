define(["jquery", "react", "app/react/IssueRow", "app/react/LinkedStateMixin", "app/utils/TipUtils", "app/utils/api",
        "ua-parser", "jquery.cookie"],
    function ($, React, IssueRow, LinkedStateMixin, TipUtils, api, UAParser) {

    var IssuesList = React.createClass({

        mixins: [LinkedStateMixin],

        /* State declaration */
        getInitialState: function () {
            return {
                issues: [],
                userIdFilter: ""
            };
        },

        render: function () {
            var issues = this.getIssuesToShow();
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h1 className="issues-header">User Issues</h1>
                            <p>
                                Find issues reports by user id
                                <input className="form-control user-id-fld" ref="userIdFld"
                                    valueLink={this.linkState("userIdFilter")} />
                            </p>
                        </div>
                    </div>
                    <div className="row" ref="issue">
                        <div className="col-md-12">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Record</th>
                                        <th>User id</th>
                                        <th>Website</th>
                                        <th>Browser</th>
                                        <th>
                                            <span title="Time user reported the issue">Time</span>
                                        </th>
                                        <th>Type</th>
                                        <th>
                                            <span title="Operating System">OS</span>
                                        </th>
                                        <th className="description-column">
                                            <span title="User message or javascript error stack">Description</span>
                                        </th>
                                        <th>Notes</th>
                                        <th>Delete</th>
                                        <th>Hide</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {issues.map(function (issue) {
                                    return (
                                        <IssueRow issue={issue} />
                                    )
                                })}
                                </tbody>
                            </table>
                            {this.state.issues.length == 0 ?
                                <div className="alert alert-info col-md-offset-2 col-md-8" id="no-issues-alert">
                                    Looks like there is no records . You can <a href="/quick-start.html">
                                    install Kuoll script</a> or look at the <a
                                    href="http://jsbin.com/nusehuwisa/edit?html,output"> JSBin sandbox </a> demo first.
                                </div>
                            : null}
                        </div>
                    </div>
                </div>
            )
        },

        getIssuesToShow: function () {
            var userId = this.state.userIdFilter;
            return userId ? this.state.issues.filter(function (issue) {
                return (issue.externalUserId.indexOf(userId) !== -1);
            }) : this.state.issues;
        },

        componentDidMount: function () {
            var self = this;
            var userToken = $.cookie("userToken");
            if (!userToken) {
                document.location = "/login.html";
                return;
            }

            api("getIssues", {
                userToken: userToken
            }, function (resp) {
                var issues = resp.issues;
                issues.forEach(function (issue) {
                    issue.userAgent = new UAParser(issue.userAgent).getResult();
                });
                self.setState({
                    issues: resp.issues
                });
            });
        },

        componentDidUpdate: function () {
            TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.issue)).find('*[title]:not([data-hasqtip])'));
        }

    });

    return IssuesList;

});