define(["jquery", "react", "app/utils/api", "app/react/common/Header", "google-charts"],
function ($, React, api, Header) {

    const VisualCharts = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {},

        render: function () {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <div ref="curveChart"></div>
                        </div>
                    </div>
                </div>
            )
        },

        componentDidMount: function () {
            const self = this;
            google.charts.load('current', {'packages': ['corechart']});
            google.charts.setOnLoadCallback(function () {

                api("get_issue_time_points", {
                    userToken: $.cookie("userToken"),
                    hours: 24,
                }, function (resp) {
                    const chart = new google.visualization.LineChart($(self.refs.curveChart.getDOMNode())[0]);
                    let chartData = [['Time']];

                    for (let i = 0; i < resp.descriptions.length; ++i) {
                        chartData[0].push(resp.descriptions[i]);
                    }

                    const keys = Object.keys(resp.data);
                    for (let i = 0; i < keys.length; ++i) {
                        const time = parseInt(keys[i]);
                        let date = new Date(time);

                        let frequencies = resp.data[time];
                        if (frequencies.length === 0) {
                            frequencies = new Array(resp.descriptions.length).fill(0);
                        }
                        
                        chartData.push([date].concat(frequencies));
                    }

                    chart.draw(google.visualization.arrayToDataTable(chartData), {
                        title: 'JS errors, last 24h',
                        legend: {
                            position: 'bottom',
                            textStyle: {
                                fontFamily: "monospace"
                            }
                        },
                        pointSize: 5,
                        height: 600,
                    });
                });

            });
        }

    });

    return VisualCharts;

});