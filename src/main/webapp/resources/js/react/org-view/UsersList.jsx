define(["jquery", "react", "app/utils/api", "app/utils/DateUtils"], function ($, React, api, DateUtils) {

    const UsersList = React.createClass({

        getInitialState: function () {
            return {
                users: [],
                invitations: []
            };
        },

        propTypes: {
            isAdmin: React.PropTypes.bool,
            newInvitations: React.PropTypes.array,
            onInvitationCanceled: React.PropTypes.func.isRequired
        },

        makeAdmin: function (user) {
            return () => {
                if (window.confirm("Give administration privilege to " + user.email +
                        "? Only new admin will be able to undone this action")) {
                    api('org/change_admin', {
                        userToken: $.cookie('userToken'),
                        newAdminUserId: user.userId
                    }, () => {
                        document.location.reload();
                    }, (errorMsg) => {
                        this.setState({
                            errorMsg
                        });
                    });
                }
            }
        },

        cancelInvitation: function (inv) {
            return () => {
                api('org/cancel_invitation', {
                    userToken: $.cookie('userToken'),
                    invitationCode: inv.code
                }, () => {
                    this.props.onInvitationCanceled(inv);
                    this.setState({
                        invitations: this.state.invitations.filter(i => i.code !== inv.code)
                    });
                });
            }
        },

        render: function () {
            const invitations = this.state.invitations.concat(this.props.newInvitations);
            return (
                <div>
                    <h3>User roles</h3>
                    <table className="table table-hover">
                    <thead>
                        <th>Email</th>
                        <th>Team role</th>
                    </thead>

                    <tbody>
                    {this.state.users.map((user) => (
                        <tr className="">
                            <td className="">{user.email}</td>
                            <td className="">
                                {user.isAdmin ?
                                    <div className="btn btn-default disabled">Admin</div>
                                :
                                !user.isAdmin && this.props.isAdmin ?
                                    <div className="btn btn-default " onClick={this.makeAdmin(user)}>Make admin</div>
                                : null}
                            </td>
                        </tr>
                    ))}
                    {invitations.map((inv) => (
                        <tr className="">
                            <td className="">{inv.email}</td>
                            <td className="">
                                <div className="btn btn-default disabled">
                                    Invited {DateUtils.formatDate(inv.creationDate)}
                                </div>
                                {" "}
                                <button className="btn btn-default" onClick={this.cancelInvitation(inv)}>
                                    <i className="fa fa-times cancel-invitation-btn" title="Cancel invitation"
                                    ></i>
                                    Cancel
                                </button>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                    </table>
                </div>
            )
        },

        componentDidMount: function () {
            api("org/get_users", {
                userToken: $.cookie("userToken")
            }, (resp) => {
                this.setState({
                    users: resp.users,
                    invitations: resp.invitations
                });
            });
        }

    });

    return UsersList;

});