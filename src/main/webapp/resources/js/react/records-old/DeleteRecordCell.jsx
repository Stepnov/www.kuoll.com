define(["react"], function (React) {
    var DeleteRecordCell = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {};
        },

        /* Props declaration */
        propTypes: {
            deleted: React.PropTypes.bool.isRequired,
            onDelete: React.PropTypes.func.isRequired,
            onRestore: React.PropTypes.func.isRequired
        },

        render: function () {
            var deleteNode;
            if (this.props.deleted) {
                deleteNode = (
                    <a className="noStrikeout" title="Restore deleted record" onClick={this.props.onRestore}>Restore</a>)
            } else {
                deleteNode = (
                    <i className="fa fa-times noStrikeout" title="Delete record" onClick={this.props.onDelete}></i>);
            }
            return (
                <td className="deleteRecordColumn">
                {deleteNode}
                </td>
            )
        }

    });
    return DeleteRecordCell;
})
