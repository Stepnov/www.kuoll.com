define(["jquery", "react", "app/react/play/Stacktrace", "app/react/BrowserIcon", "app/react/LossStep", "app/react/OsIcon",
        "app/react/DeviceIcon", "app/react/play/LongText", "app/react/issue-filter/IssueFilterDropdown",
        "app/utils/api", "app/utils/DateUtils", "app/utils/UrlUtils", "ua-parser", "jquery.cookie"],
    function ($, React, Stacktrace, BrowserIcon, LossStep, OsIcon, DeviceIcon, LongText, IssueFilterDropdown, api, DateUtils, UrlUtils, UAParser) {

    function calculateStepsStatistics(row) {
        const step = {
            loadStepLoss: 0,
            loadStepConvDrop: 0,
            loadStepAffectedPct: 0,

            addStepLoss: 0,
            addStepConvDrop: 0,
            addStepAffectedPct: 0,

            checkoutStepLoss: 0,
            checkoutStepConvDrop: 0,
            checkoutStepAffectedPct: 0,

            affectedPercent: 0,
            conversionDropPercent: 0,

            convSaved: 1
        };

        row.stages.forEach(function (stage) {
            if (
                "load" === stage.stepPair.startStep &&
                "add" === stage.stepPair.endStep
            ) {
                step.loadStepLoss = stage.lossPercent;
                step.loadStepConvDrop = stage.conversionDropPercent;
                step.loadStepAffectedPct = stage.affectedPercent;
                step.convSaved *= (1 - stage.lossPercent);

                step.affectedPercent = Math.max(step.affectedPercent, stage.affectedPercent);
                step.conversionDropPercent = Math.max(step.conversionDropPercent, stage.conversionDropPercent);

            } else if (
                "add" === stage.stepPair.startStep &&
                "checkout" === stage.stepPair.endStep
            ) {
                step.addStepLoss = stage.lossPercent;
                step.addStepConvDrop = stage.conversionDropPercent;
                step.addStepAffectedPct = stage.affectedPercent;
                step.convSaved *= (1 - stage.lossPercent);

                step.affectedPercent = Math.max(step.affectedPercent, stage.affectedPercent);
                step.conversionDropPercent = Math.max(step.conversionDropPercent, stage.conversionDropPercent);

            } else if (
                "checkout" === stage.stepPair.startStep &&
                "purchase" === stage.stepPair.endStep
            ) {
                step.checkoutStepLoss = stage.lossPercent;
                step.checkoutStepConvDrop = stage.conversionDropPercent;
                step.checkoutStepAffectedPct = stage.affectedPercent;
                step.convSaved *= (1 - stage.lossPercent);

                step.affectedPercent = Math.max(step.affectedPercent, stage.affectedPercent);
                step.conversionDropPercent = Math.max(step.conversionDropPercent, stage.conversionDropPercent);

            } else if ( 
                "remove" === stage.stepPair.startStep ||
                "remove" === stage.stepPair.endStep
            ) {
                // OK for now, should not happen in future
            } else {
                console.error("Broken step: " + JSON.stringify(stage));
            }

            
        });

        step.converionDrop = 1 - step.convSaved;

        return step;
    }

    const IssuesDashboard = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                errors: [],
                summary: {},
                stageSummary: [],
                loading: false
            };
        },

        /* Props declaration */
        propTypes: {

        },

        render: function () {
            const summary = this.state.summary;
            const stageSummary = this.state.stageSummary;

            if (this.state.loading) {
                return (
                    <div id="main-caption-container" style={{textAlign: "center"}}>
                        <h2 id="main-caption">
                            <i className="fa fa-spinner fa-pulse"></i> Loading data...
                        </h2>
                    </div>
                );
            }

            return (
                <div>
                    
                <h2 style={{marginBottom: "30px"}}>
                    Detected revenue losses, last 10 days
                </h2>

                <table className="table table-responsive tableLosses">
                    <thead>
                        <tr>
                            <th colSpan="3" className="mergedTh"><div className="mergedThText">Totals, through funnel</div></th>
                            <th colSpan="3" className="mergedTh"><div className="mergedThText">User steps</div></th>
                            <th rowSpan="2">Error description</th>
                        </tr>
                        <tr>
                        <th  title="Revenue loss, %" style={{textAlign: "center"}}>
                            Revenue loss
                            {" "}
                            <span className="fa-stack fa-stack-small">
                                <i className="fa fa-circle fa-stack-2x" style={{color: "#c0392b"}} ></i>
                                <i className="fa fa-usd fa-stack-1x fa-inverse"></i>
                            </span>
                        </th>
                        <th title="Affected users, %" style={{textAlign: "center"}}>
                            Affected users
                            {" "}
                            <span className="fa-stack fa-stack-small">
                                <i className="fa fa-circle fa-stack-2x" style={{color: "#2980b9"}}></i>
                                <i className="fa fa-user fa-stack-1x fa-inverse"></i>
                            </span>
                        </th>
                        <th title="Conversion drop, %" style={{textAlign: "center"}}>
                            Conversion drop
                            {" "}
                            <span className="fa-stack fa-stack-small">
                                    <i className="fa fa-circle fa-stack-2x" style={{color: "#16a085"}}></i>
                                    <i className="fa fa-filter fa-stack-1x fa-inverse"></i>
                            </span>
                        </th>
                        <th title="Affected conversion to Add to cart" style={{textAlign: "center"}}> Up to Add&nbsp;to&nbsp;cart</th>
                        <th title="Affected conversion to Check-out" style={{textAlign: "center"}}>Up to Check-out</th>
                        <th title="Affected conversion to Purchase" style={{textAlign: "center"}}>Up to Purchase</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.errors.map((row, errorIndex) => {
                                const step = calculateStepsStatistics(row);

                                return (
                                    <tr className={errorIndex % 2 ? "row-odd" : "row-even"}>

                                        <td className="stages-total-cell lossesNumberCell">
                                            <span style={{color: "#bdc3c7"}}>(</span>
                                            {((1 - step.convSaved) * 100).toFixed(4)}%
                                            <span style={{color: "#bdc3c7"}}>)</span>

                                        </td>
                                        <td className="lossesNumberCell">
                                            {(step.affectedPercent * 100).toFixed(2)}%
                                        </td>
                                        <td className="lossesNumberCell">
                                            {(step.conversionDropPercent * 100).toFixed(2)}%
                                        </td>

                                        <td style={{textAlign: "center"}} className="alignBottom">
                                            <LossStep
                                                revenueLossPct={step.loadStepLoss}
                                                conversionDropPct={step.loadStepConvDrop}
                                                sessionsAffectedPct={step.loadStepAffectedPct}
                                                stepName="Add to cart"
                                            />                                        
                                        </td>
                                        <td style={{textAlign: "center"}} className="alignBottom">
                                            <LossStep
                                                revenueLossPct={step.addStepLoss}
                                                conversionDropPct={step.addStepConvDrop}
                                                sessionsAffectedPct={step.addStepAffectedPct}
                                                stepName="Checkout"
                                            />
                                        </td>
                                        <td style={{textAlign: "center"}} className="alignBottom">
                                            <LossStep
                                                revenueLossPct={step.checkoutStepLoss}
                                                conversionDropPct={step.checkoutStepConvDrop}
                                                sessionsAffectedPct={step.checkoutStepAffectedPct}
                                                stepName="Purchase"
                                            />
                                        </td>

                                        <td style={{maxWidth: "70%"}}>
                                            <a target="_blank" className="descriptionLink"
                                               href={"/issues-group.html?hash=" + row.stacktraceHash}>
                                                {row.firstIssue.description}
                                            </a>
                                        </td>

                                    </tr>
                                );
                            }
                        )}
                        </tbody>
                    </table>


                    <table className="table table-hover table-responsive table-striped table-bordered">
                        <thead>
                        <th>Step</th>
                        <th>Converted</th>
                        <th>Sessions</th>
                        <th>Agv. Conversion</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Global</td>
                            <td>{summary.conversionCount}</td>
                            <td>{summary.recordsCount}</td>
                            <td>{(summary.conversionCount / summary.recordsCount * 100).toFixed(2)} %</td>
                        </tr>
                        {stageSummary.map((stage, i) => (
                            <tr key={i}>
                                <td>from {stage.stepPair.startStep} to {stage.stepPair.endStep}</td>
                                <td>{stage.converted}</td>
                                <td>{stage.records}</td>
                                <td>{(stage.converted / stage.records * 100).toFixed(2)} %</td>
                            </tr>
                        ))}
                        </tbody>
                    </table>

                    {this.state.errors.length === 0 ?
                        <div className="jumbo">
                            <p>
                                No analytics gathered yet.
                            </p>
                            <h2 style={{color: "#8e44ad"}}>Install Kuoll to your web application to see error
                                reports</h2>
                            <p>
                                <a href="#onboard" className="btn btn-lg btn-success">Install</a>
                            </p>

                        </div>
                        : ""}


                </div>
            )
        },

        componentDidMount: function () {
            const self = this;
            const timePeriod = UrlUtils.getParameterByName("timePeriod") || "10d";
            this.setState({
                loading: true
            });
            api("get_conversion_analytics", {
                userToken: $.cookie("userToken"),
                timePeriod: timePeriod,
                sortByLossPercent: true
            }, function (resp) {
                self.setState({
                    errors: resp.errors,
                    summary: resp.summary,
                    stageSummary: resp.stageSummary,
                    loading: false
                })
            });
        }

    });

    return IssuesDashboard;

});