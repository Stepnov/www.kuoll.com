define(["jquery", "react", "app/utils/api", "jquery.cookie"], function ($, React,api) {

    var IgnoreUrlList = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                editing: -1,
                urls: []
            };
        },

        render: function () {
            var self = this;
            return (
                <div>
                {this.state.urls.map(function (url, index) {
                    var editing = (self.state.editing == index);
                    return (
                        <form key={url} ref={"form-" + index}>
                            <div className="form-group">
                                <div className="input-group">
                                    <input type="text" placeholder="Ignore URL (RegExp)"
                                        className="url-field form-control" readonly={!editing} defaultValue={url}/>
                                    <span className="input-group-btn">
                                        {editing ?
                                            <button className="btn btn-success" type="button" onClick={function () {self.saveUrl(index)}}>Save</button>
                                            : [
                                            <button className="btn btn-warning" type="button" onClick={function () {self.editUrl(index)}}>Edit</button>,
                                            <button className="btn btn-danger" type="button" onClick={function () {self.deleteUrl(index)}}>Delete</button>
                                        ]}
                                    </span>
                                </div>
                            </div>
                        </form>
                    );
                })}
                    <form>
                        <div className="form-group">
                            <div className="input-group">
                                <input type="text" placeholder="Ignore URL (RegExp)" className="form-control" ref="newUrl"/>
                                <span className="input-group-btn">
                                    <button className="btn btn-success" type="button" onClick={this.addUrl}>Add</button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            )
        },

        saveUrl: function (urlIndex) {
            var $form = $(React.findDOMNode(this.refs["form-" + urlIndex]));
            var $urlField = $form.find(".url-field");
            // TODO check if RegExp valid

            var urls = this.state.urls;
            var oldUrl = urls[urlIndex];
            urls[urlIndex] = $urlField.val();
            this.setState({
                urls: urls,
                editing: -1
            });

            api("update-ignore-url", {
                userToken: $.cookie("userToken"),
                oldUrl: oldUrl,
                newUrl: $urlField.val()
            });
        },

        deleteUrl: function (urlIndex) {
            var urls = this.state.urls;
            var url = urls[urlIndex];
            urls = urls.slice(0, urlIndex).concat(urls.slice(urlIndex + 1));
            var oldEditing = this.state.editing;
            this.setState({
                urls: urls,
                editing: oldEditing == urlIndex ? -1 : 
                    (oldEditing > urlIndex ? oldEditing - 1 : oldEditing)  
            });

            api("delete-ignore-url", {
                userToken: $.cookie("userToken"),
                url: url
            });
        },

        editUrl: function (urlIndex) {
            this.setState({
                editing: urlIndex
            });
            $(React.findDOMNode(this.refs["form-" + urlIndex])).find(".url-field").focus();
        },

        addUrl: function () {
            var $newUrl = $(React.findDOMNode(this.refs.newUrl));
            var url = $newUrl.val();
            // TODO: validate
            var urls = this.state.urls;
            urls.push(url);
            this.setState({
                urls: urls
            });

            api("add-ignore-url", {
                userToken: $.cookie("userToken"),
                url: url
            });
            $newUrl.val("");
        },

        componentDidMount: function () {
            var self = this;
            api("get-ignore-urls", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                self.setState({
                    urls: resp.urls
                });
            });
        }

    });

    return IgnoreUrlList;

});