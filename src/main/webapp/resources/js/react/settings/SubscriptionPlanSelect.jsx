define(["jquery", "react", "app/User", "app/utils/api", "app/checkout", "jquery.cookie"], function ($, React, User, api, checkout) {

    var SetPlanButton = React.createClass({
        render: function () {
            var props = this.props;
            var activePlan = props.currentPlan.startsWith(props.planName);
            if (activePlan) {
                return (
                    <span className={"start btn activePlanLabel activePlanLabel-" + props.planName}
                            data-plan-name={props.planName}>
                        <strong>Active</strong>
                    </span>
                )
            } else {
                return (
                    <button className={"start btn " + (props.primary ? "btn-primary" : "btn-default")}
                            onClick={props.onClick} data-plan-name={props.planName}>
                        {props.caption}
                    </button>
                )
            }
        }
    });

    var SubscriptionPlanSelect = React.createClass({
    
        /* State declaration */
        getInitialState: function () {
            return {
                switchPlanErrorMsg: ""
            };
        },
        
        /* Props declaration */
        propTypes: {
            onBillingUpdated: React.PropTypes.func.isRequired,
            onSelectedPlanChanged: React.PropTypes.func.isRequired,
            pendingChanges: React.PropTypes.object,
            billing: React.PropTypes.object.isRequired,
            billingFrequency: React.PropTypes.oneOf(["month", "year"]).isRequired
        },

        handleSubscriptionButtonClick: function (e) {
            var planName = e.target.dataset.planName;
            this.props.onSelectedPlanChanged(planName);
        },
    
        render: function () {
            var billing = this.props.billing;
            var plan = this.props.pendingChanges ? this.props.pendingChanges.plan || billing.planName : billing.planName;
            return (
                <div className="form-inline">
                <div className=" price-grid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className={"alert alert-warning " + (this.state.switchPlanErrorMsg ? "" : "hidden")}>
                                Could not switch plan because of the error: {this.state.switchPlanErrorMsg}
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-4 col-sm-6" style={{textAlign: "center"}}>
                            <div className={"panel panel-default " + 
                                    (plan == "hobby" ? "activePlan": "") +
                                    " activePlan-" + plan
                                    } >
                                <div className="panel-heading">
                                    <h2 className="plan-name" style={{color: "#9b59b6"}}>Hobby</h2>
                                </div>
                                <div className="panel-body">
                                    <h2 className="price">$0 <br/>
                                        <sub>
                                            forever
                                        </sub> 
                                    </h2>
                                    <p className="lead">
                                    </p>

                                    <p className="whyt">100 bug reports</p>
                                    <p>2 days report lifetime</p>
                                    <p className="whyt">Email alerts</p>
                                    <p className="">Email support <br/>&nbsp; </p>


                                    <p className="cart1">
                                    </p>
                                    <p className="small">No obligations <br/>&nbsp;</p>
                                </div>
                            </div>

                            <p className="cart2">
                                <SetPlanButton currentPlan={plan} planName="hobby"
                                            caption={"Downgrade"}
                                            onClick={this.handleSubscriptionButtonClick} primary={true} />
                            </p>

                        </div>

                        <div className="col-md-4 col-sm-6" style={{textAlign: "center"}}>
                            <div className={"panel panel-default " + 
                                    (plan == "team_month"? "panel-info": "")} >
                                <div className="panel-heading">
                                    <div className="recommended-plan">
                                        Most recommended
                                    </div>
                                    <h2 className="plan-name" style={{color: "#27ae60" }}>Team</h2>
                                </div>
                                <div className="panel-body">
                                    <h2 className="price">
                                        {this.props.billingFrequency  == "month"? "$99": "$82.50"} 
                                        <br/>
                                        <sub>
                                            per month / team
                                        </sub>
                                    </h2>

                                    <p className="whyt">1 000 000 user session</p>
                                    <p>180 days report lifetime</p>
                                    <p className="whyt">JIRA integration</p>
                                    <p className="">Priority phone and email support</p>
                                    <p className="small">Refund without questions,<br/> 14 days free</p>

                                </div>
                            </div>

                            <p> </p>

                            <p className="cart2">
                                <SetPlanButton currentPlan={plan} planName="team"
                                            caption={"Upgrade"}
                                            onClick={this.handleSubscriptionButtonClick} primary={true} />

                            </p>

                        </div>                        
                        <div className="col-md-4 col-sm-6 col-sm-offset-3 col-md-offset-0 col-lg-offset-0" style={{textAlign: "center"}}>
                            <div className={"panel panel-default " + 
                                    (plan == "team_month"? "panel-info": "")} >
                                <div className="panel-heading">
                                    <h2 className="plan-name" style={{color: "#2980b9" }}>Enterprise</h2>
                                </div>
                                <div className="panel-body">
                                    <h2 className="price">
                                        Custom price
                                        <br/>
                                        <sub>
                                            &nbsp;
                                        </sub>
                                    </h2>

                                    <p className="whyt">Unlimited bug reports</p>
                                    <p>Unlimited report lifetime</p>
                                    <p className="whyt">Custom integrations</p>
                                    <p className="">Priority phone and email support</p>
                                    <p className="small">Free trial available, <br/>money-back guarantee</p>
                                </div>
                            </div>
                            <p> </p>
                            <p className="cart2">
                                <a href="mailto:corp@kuoll.com" className="start btn btn-default">
                                    Contact us
                                </a>
                            </p>

                        </div>
                    </div>


                </div>{/*  container */}

                </div>
            )	
        },

        componentDidMount: function () {
            this.setState({
                billing: this.props.billing
            });
        }
        
    });
    
    return SubscriptionPlanSelect;    

});