define(["jquery", "react", "app/react/Table", "app/utils/api", "jquery.cookie"], function ($, React,Table, api) {

    var ImplicitRecordsInfoTable = React.createClass({

        columns: {
            orgId: "Org ID",
            loadingCount: "embedScript loaded",
            adminEmail: "Admin email"
        },

        /* State declaration */
        getInitialState: function () {
            return {
                userToken: null
            };
        },

        render: function () {
            return (
                <Table columns={this.columns} data={this.state.orgData}/>
            )
        },

        componentDidMount: function () {
            var self = this;
            api("getImplicitRecordsInfo", {
                userToken: $.cookie("userToken")
            }, function (resp) {
                if (resp.orgData) {
                    var orgData = resp.orgData.sort(function (a, b) {
                        return a.loadingCount < b.loadingCount
                    });
                    self.setState({
                        orgData: orgData
                    })
                }
            });
        }

    });

    return ImplicitRecordsInfoTable;

});
