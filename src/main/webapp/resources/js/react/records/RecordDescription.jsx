define(
    ["jquery", "react", "app/utils/api", "jquery.jeditable"],
    function ($, React,api) {
        var RecordDescription = React.createClass({

            /* State declaration */
            getInitialState: function () {
                return {};
            },

            /* Props declaration */
            propTypes: {
                record: React.PropTypes.object.isRequired,
                recordDeleted: React.PropTypes.bool.isRequired
            },

            onDescriptionClick: function (e) {
                if (e.currentTarget.getAttribute("jeditableEnabled") || this.props.recordDeleted) {
                    e.preventDefault();
                }
            },

            onEditIconClick: function (e) {
                if (!this.props.recordDeleted) {
                    var $recordDescription = $(React.findDOMNode(this.refs.recordDescription));
                    $recordDescription.attr("jeditableEnabled", true).trigger("editIconClicked");
                }
            },

            render: function () {
                var record = this.props.record;
                return (
                    <td className="descriptionColumn hidable">
                        <a className={"description" + (record.description ? "" : " defaultDescription")}
                            href={"/play.html?recordCode=" + record.recordCode}
                            title="View record" onClick={this.onDescriptionClick} ref="recordDescription"
                            target="_blank">{record.description}</a>
                        <i className="fa fa-pencil descriptionEditIcon" title="Click to edit description..."
                            onClick={this.onEditIconClick}></i>
                    </td>
                )
            },

            componentDidMount: function () {
                var $recordDescription = $(React.findDOMNode(this.refs.recordDescription));
                var record = this.props.record;

                var placeholder;
                placeholder = (record.firstFrameUrl ? "Record: " + record.firstFrameUrl :
                    (record.recordInfo && record.recordInfo.firstUrl ? "Record: " + record.recordInfo.firstUrl :
                        "(No description)"));

                $recordDescription.editable(
                    function (value, settings) {
                        api("updateRecordDescription", {
                            description: value,
                            recordCode: record.recordCode
                        });
                        this.removeAttribute("jeditableEnabled");
                        if (!value) {
                            $(this).addClass("defaultDescription");
                        } else {
                            $(this).removeClass("defaultDescription");
                        }
                        return value;
                    }, {
                        event: "editIconClicked",
                        onblur: "submit",
                        placeholder: placeholder,
                        style: "color: black;"
                    });
            }

        });
        return RecordDescription;
});
