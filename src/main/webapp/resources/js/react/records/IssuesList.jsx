define(["jquery", "react", "app/utils/DateUtils", "app/utils/TipUtils"], function ($, React,DateUtils, TipUtils) {

    var IssuesList = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {

            };
        },

        /* Props declaration */
        propTypes: {
            record: React.PropTypes.object.isRequired,
            issues: React.PropTypes.array.isRequired
        },

        render: function () {
            var issues = this.props.issues;
            var record = this.props.record;
            return (
                <div className="issues-list row" ref="issuesList">
                {issues.length === 0 ?
                    <div className="issues-loading-wrapper">
                        <i className="fa fa-spinner fa-spin" aria-hidden="true"></i>
                    </div> :
                    issues.map(function (issue) {
                        function onClick() {
                            window.open("/play.html?recordCode=" + issue.recordCode + "&issueId=" + issue.id);
                        }
                        return (
                            <div className="col-md-6 col-lg-4" key={issue.id}>
                                <div className="issue row">
                                    <div className="go-to-issue-btn col-xs-1 col-sm-1 col-md-1 col-lg-1" title="Go to the issue" onClick={onClick}>
                                        <i className="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                    </div>
                                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <div className="issue-description-wrapper">
                                            <p className="caption">Description</p>
                                            {issue.description ?
                                                <p>{issue.description}</p>
                                                    :
                                                <p className="low-priority">&lt;No description&gt;</p>}
                                        </div>
                                    </div>
                                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <div className="issue-type-wrapper">
                                            <p className="caption">Type</p>
                                            <p>{issue.type}</p>
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <div className="issue-time-wrapper">
                                            <p className="caption">Time</p>
                                            <p>{DateUtils.formatDate(issue.time)}</p>
                                        </div>
                                        <div className="issue-time-after-record-wrapper">
                                            <p className="caption">From record start</p>
                                            <p>{DateUtils.formatDuration(issue.time - record.startTime).short}</p>
                                        </div>
                                        <div className="issue-time-ago-wrapper">
                                            <p className="caption">Ago</p>
                                            <p>{DateUtils.formatDuration(Date.now() - issue.time).short}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
                </div>
            )
        },

        componentDidMount: function () {
            TipUtils.makeSimpleTip($(React.findDOMNode(this.refs.issuesList)).find('[title!=""]'));
        }

    });

    return IssuesList;

});
