define(["jquery", "react", "app/react/play/Stacktrace", "app/react/BrowserIcon", "app/react/OsIcon",
        "app/react/DeviceIcon", "app/react/play/LongText", "app/utils/api", "app/utils/DateUtils", "app/utils/UrlUtils", "ua-parser", "jquery.cookie"],
    function ($, React, Stacktrace, BrowserIcon, OsIcon, DeviceIcon, LongText, api, DateUtils, UrlUtils, UAParser) {

    // http://stackoverflow.com/a/34890276/3478389
    function groupBy(array, key) {
        return array.reduce(function(result, obj) {
            (result[obj[key]] = result[obj[key]] || []).push(obj);
            return result;
        }, {});
    }

    var IssuesGroup = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                issuesGroup: {
                    issues: {},
                    platforms: {
                        browser: {},
                        os: {},
                        device: {}
                    },
                    stacktrace: []
                }
            };
        },

        /* Props declaration */
        propTypes: {

        },

        render: function () {
            const self = this;
            let rowIndex = 0;

            const recordCodes = Object.keys(this.state.issuesGroup.issues);
            const allIssues = recordCodes
                .map(r => this.state.issuesGroup.issues[r]).reduce((a, b) => a.concat(b), []); // flatMap

            const recordDurations = allIssues
                .map(i => i.effectiveRecordDuration)
                .filter(d => !!d);
            const averageRecordDuration = recordDurations.reduce((a, b) => a + b, 0) / recordDurations.length;

            const averageFramesPerRecord = allIssues.map(i => i.totalFrames).reduce((a, b) => a + b, 0) / allIssues.length;

            return (
                <div className="container">
                    <div className="row">
                        <div className=" col-md-12">
                            <h1>{this.state.issuesGroup.description}<br/>
                            <small>JavaScript Error group</small>
                            </h1>
                        </div>

                        <div className=" col-md-6">
                            <h2>Story</h2>
                            <p>
                                First found {DateUtils.formatDate(this.state.issuesGroup.firstOccurrence)}.
                                {" "}
                                Happened {this.state.issuesGroup.issuesCount} times. 
                                {" "}
                                Last time {DateUtils.formatDate(this.state.issuesGroup.lastOccurrence)}.
                            </p>

                            <h2>Avg. stats</h2>
                            <p>
                                {DateUtils.formatDuration(averageRecordDuration).short} is average session duration
                                . {averageFramesPerRecord.toFixed(1)} pages views per session
                                {/*. 2:23 before error first happens.*/}
                                {/*1:19 until session is closed*/}
                                {/*. 13 seconds is average page load time.*/}
                            </p>

                            <h3>[By hour report goes here]</h3>

                            <h3>[By stage report goes here]</h3>

                            
                        </div>

                        <div className="col-md-6">
                            <h2>Stack trace example</h2>
                            <Stacktrace stacktrace={this.state.issuesGroup.stacktrace} 
                                description={this.state.issuesGroup.description}/>

                            <div>
                                <h4>Browsers</h4>
                                {Object.keys(this.state.issuesGroup.platforms.browser).map(function (browser) {
                                    return (
                                        <BrowserIcon userAgent={{browser: self.state.issuesGroup.platforms.browser[browser]}}
                                                     withVersion={false}/>
                                    )
                                })}

                                <h4>Operating systems</h4>
                                {Object.keys(this.state.issuesGroup.platforms.os).map(function (os) {
                                    return (
                                        <OsIcon userAgent={{os: self.state.issuesGroup.platforms.os[os]}} />
                                    )
                                })}

                                <h4>Devices</h4>
                                {Object.keys(this.state.issuesGroup.platforms.device).map(function (device) {
                                    return (
                                        <DeviceIcon userAgent={{device: self.state.issuesGroup.platforms.device[device]}} />
                                    )
                                })}
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className=" col-md-12">
                            <h2>Recorded user sessions</h2>
                            <table className="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th title="Session duration">Duration</th>
                                    <th title="Pages viewed">Pages</th>
                                    <th>Browser</th>
                                    <th>User&nbsp;ID</th>
                                    <th title="User email">Email</th>
                                    <th title="Page load time">Load</th>
                                </tr>
                                </thead>
                                <tbody>
                                {recordCodes.map(function (recordCode) {
                                    return self.state.issuesGroup.issues[recordCode].map((issue) => {
                                            ++rowIndex;
                                            return (
                                                <tr key={rowIndex}>
                                                    <td>{rowIndex}</td>
                                                    <td>
                                                        <a href={"/play.html?recordCode=" + recordCode +
                                                        "&issueId=" + issue.issueId} target="_blank">
                                                            {DateUtils.formatDate(issue.time)}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        {DateUtils.formatDuration(issue.recordDuration).short}
                                                        {issue.isRecordCompleted ? " and counting" : null}
                                                    </td>
                                                    <td>{issue.totalFrames}</td>
                                                    <td>Chrome</td>
                                                    <td>{issue.externalUserId}</td>
                                                    <td>{issue.externalUserEmail}</td>
                                                    <td>13s</td>
                                                </tr>
                                            );
                                        }
                                    )
                                })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            )
        },

        componentDidMount: function () {
            const self = this;
            const id = UrlUtils.getParameterByName("id");
            const hash = UrlUtils.getParameterByName("hash");
            api("get_issues_group_by_hash", {
                issuesGroupId: id,
                stacktraceHash: hash,
                userToken: $.cookie("userToken")
            }, function (resp) {
                var issuesGroup = resp.issuesGroup;
                issuesGroup.issues = groupBy(issuesGroup.issues, "recordCode");

                var platforms = {
                    browser: {},
                    os: {},
                    device: {}
                };
                issuesGroup.userAgents.forEach(function (ua) {
                    var result = new UAParser(ua).getResult();
                    if (result.browser)
                        platforms.browser[result.browser.name] = result.browser;
                    if (result.os)
                        platforms.os[result.os.name] = result.os;
                    if (result.device)
                        platforms.device[result.device.vendor] = result.device;
                });
                issuesGroup.platforms = platforms;
                delete issuesGroup.userAgents;

                self.setState({
                    issuesGroup: issuesGroup
                });
            });
        }

    });

    return IssuesGroup;

});