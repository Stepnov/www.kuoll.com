define(["react"], function (React) {

    var maxLength = 50;

    var LongText = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                shortened: true
            };
        },

        /* Props declaration */
        propTypes: {
            text: React.PropTypes.string.isRequired,
            shortened: React.PropTypes.bool
        },

        onTextClick: function () {
            const isTextSelected = !window.getSelection().isCollapsed;
            if (typeof this.props.shortened === "undefined" && ("" + this.props.text).length > maxLength && !isTextSelected) {
                this.setState({
                    shortened: false
                });
            }
        },

        render: function () {
            var rawText = "" + this.props.text;
            rawText = rawText.replace("\r\n", "\n");
            var isTextTooLong = rawText.length > maxLength;
            var isShortened = this.props.shortened || (typeof this.props.shortened === "undefined" && this.state.shortened);

            var text, className;
            if (isShortened) {
                if (isTextTooLong) {
                    text = rawText.substring(0, maxLength - 3);
                    className = "longText shortened";
                } else {
                    text = rawText;
                    className = "";
                }
            } else {
                if (isTextTooLong) {
                    text = rawText;
                    className = "longText";
                } else {
                    text = rawText;
                    className = "";
                }
            }
            return (
                <span className={className} onClick={this.onTextClick}>
                    {text.split("\n").map(function (line) { return <div className="long-text-line">{line}</div>;})}
                    {isShortened && isTextTooLong ?
                    <span className="long-text-ellipses">&#x3C;&#x2026;&#x3E;</span>
                    : null}
                </span>
            )
        }

    });

    return LongText;

});