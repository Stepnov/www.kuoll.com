define(["jquery", "react", "app/utils/UrlUtils", "app/utils/api", "app/react/play/NativeListener",
    "app/react/integration/ZendeskTicketForm", "app/react/integration/JiraIssueForm", "jquery.ui", "jquery.cookie"],
    function ($, React,UrlUtils, api, NativeListener, ZendeskTicketForm, JiraIssueForm) {

    var ShareBox = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {
                sharingState: "share",
                lastErrorMessage: undefined
            };
        },

        propTypes: {
            recordCode: React.PropTypes.string.isRequired,
            activeSequent: React.PropTypes.object
        },

        updateSharingState: function (state, error) {
            this.setState({
                sharingState: state,
                lastErrorMessage: error
            });
        },

        onShareRecord: function (e) {
            var $emailField = $(React.findDOMNode(this.refs.email));
            var recordCode = this.props.recordCode;
            var frameNum = this.props.activeSequent.frameNum;
            var sequentNum = this.props.activeSequent.sequentNum;

            var self = this;
            api("sendShareRecordEmail", {
                email: $emailField.val(),
                recordCode: recordCode,
                frameNum: frameNum,
                sequentNum: sequentNum
            }, function () {
                self.updateSharingState("shared");
                window.setTimeout(function () {
                    self.updateSharingState("share");
                }, 5000);
            }, function (error) {
                self.updateSharingState("error", error);
            });
            this.updateSharingState("sending");
            $(React.findDOMNode(this.refs.dropdownToggle)).dropdown("toggle");
            e.preventDefault();
        },

        onCreateTicket: function () {
            $(React.findDOMNode(this.refs.dropdownToggle)).dropdown("toggle");
        },

        render: function () {
            var zendeskEnabled = $.cookie("zendesk-enabled") === "true";
            var jiraEnabled = $.cookie("jira-enabled") === "true";
            return (
                <div className="btn-group" id="shareRecordDropdown">
                    <button type="button" className="btn btn-default dropdown-toggle" id="shareRecordBtn"
                        data-toggle="dropdown"
                        aria-expanded="false" title="Share this record" ref="dropdownToggle">
                    {
                        "sending" == this.state.sharingState ?
                            <span>
                                <i className="fa fa-spinner fa-spin"></i>
                                Sending...</span> :
                            "shared" == this.state.sharingState ?
                                <span>
                                    <i className="fa fa-check"></i>
                                    Shared!</span> :
                                "error" == this.state.sharingState ?
                                    <span title={this.state.lastErrorMessage}>
                                        <i className="fa fa-times-circle"></i>
                                        Error!</span> :
                                    <span>
                                        <i className="fa fa-share-alt"></i>
                                    &nbsp;Share&nbsp;</span>
                        }
                        <span className="caret"></span>
                    </button>
                    <NativeListener stopClick>
                        <div className="dropdown-menu" id="shareRecordDropdownPanel" role="menu">
                            <div className="shareRecordText">Send this record to</div>
                            <form id="shareRecordForm" onSubmit={this.onShareRecord}>
                                <div className="form-group">
                                    <div className="input-group">
                                        <input className="form-control emailInput" id="shareRecordEmailField" type="email"
                                            placeholder="Email" ref="email"/>
                                        <span className="input-group-btn">
                                            <button className="btn btn-default" id="shareRecordSubmitBtn" type="submit">
                                                Send record
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>

                        {zendeskEnabled ?
                            <ZendeskTicketForm onCreate={this.onCreateTicket} updateSharingState={this.updateSharingState} />
                            : null }
                        {jiraEnabled ?
                            <JiraIssueForm onCreate={this.onCreateTicket} updateSharingState={this.updateSharingState} />
                            : null }
                        </div>
                    </NativeListener>
                </div>
            )
        }
    });

    return ShareBox;

});
