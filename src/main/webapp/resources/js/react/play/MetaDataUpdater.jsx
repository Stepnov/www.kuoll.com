define(["jquery", "react", "app/utils/SequentLoader", "app/utils/SequentUtils", "app/utils/api"], function ($, React,SequentLoader, SequentUtils, api) {

    var MetaDataUpdated = React.createClass({

        /* State declaration */
        getInitialState: function () {
            return {

            };
        },

        /* Props declaration */
        propTypes: {
            onDataLoaded: React.PropTypes.func.isRequired,

            recordStatus: React.PropTypes.string,
            recordCode: React.PropTypes.string.isRequired,
            lastFrame: React.PropTypes.object,

            issue: React.PropTypes.object,

            totalMutationSequents: React.PropTypes.number.isRequired,
            totalMouseSequents: React.PropTypes.number.isRequired,

            interactiveMode: React.PropTypes.bool.isRequired,

            changeActiveSequent: React.PropTypes.func.isRequired
        },

        render: function () {
            return null;
        },

        getMetaDataUpdateInterval: function () {
            return this.props.interactiveMode ? 1000 : 20000;
        },

        metaDataFetchingTimeoutId: 0,

        startMetaDataFetching: function () {
            if (this.props.issue) return;

            var self = this;
            $("#loading-spinner").css({visibility: "visible"});
            api("getNewMetaData", {
                recordCode: this.props.recordCode,
                lastFrameNum: this.props.lastFrame.frameNum
            }, function (resp) {
                var isStateChanged = (
                    resp.frames.length > 1
                    || resp.mutationSequents.length > self.props.totalMutationSequents
                    || resp.mouseSequents.length > self.props.totalMouseSequents
                    || resp.record.status !== self.props.recordStatus
                    || resp.frames[0].totalSequents != self.props.lastFrame.totalSequents
                );
                if (isStateChanged) {
                    self.props.onDataLoaded(resp);
                    self.checkForNewSequents();
                }
                self.metaDataFetchingTimeoutId = window.setTimeout(self.startMetaDataFetching, self.getMetaDataUpdateInterval());
                $("#loading-spinner").css({visibility: "hidden"});
            });
        },


        checkForNewSequents: function () {
            if (this.props.issue) return;

            var self = this;
            var frame = self.props.lastFrame;
            if (!frame.sequents) {
                return;
            }
            api("checkNewSequents", {
                recordCode: self.props.recordCode,
                frameNum: frame.frameNum,
                sequentsTotal: frame.sequents.length
            }, function (resp) {
                if (resp.sequentsLeft > 0) {
                    var lastSequent = frame.sequents[frame.sequents.length - 1];
                    SequentLoader.loadSequents(lastSequent.sequentNum, resp.sequentsLeft, frame, function () {
                        if (self.props.interactiveMode) {
                            var lastSequent = frame.sequents[frame.sequents.length - 1];
                            if (self.props.lastFrame.frameNum != lastSequent.frameNum) {
                                self.props.changeActiveFrame(lastSequent.frameNum, lastSequent.sequentNum);
                            } else {
                                self.props.changeActiveSequent(lastSequent.sequentNum);
                            }
                        }
                    }, true, true);
                }
            });
        },

        componentDidMount: function () {
            this.startMetaDataFetching();
        },

        componentDidUpdate: function () {
            if (this.props.recordStatus !== "started" || this.props.issue) {
                if (this.metaDataFetchingTimeoutId) {
                    window.clearTimeout(this.metaDataFetchingTimeoutId);
                }
            }
        }

    });

    return MetaDataUpdated;

});