define(["jquery", "react", "app/react/play/Frame"], function ($, React,Frame) {

    var FrameLine = React.createClass({

        /* Props declaration */
        propTypes: {
            frames: React.PropTypes.array.isRequired,
            onFrameClick: React.PropTypes.func.isRequired,
            activeFrameNum: React.PropTypes.number.isRequired
        },

        render: function () {
            var activeFrameNum = this.props.activeFrameNum;
            var self = this;
            return (
                <div className="frameLine" id="frameLine">
                {this.props.frames.map(function(frame) {
                    return (
                        <Frame isActive={frame.frameNum === activeFrameNum} frameNum={frame.frameNum}
                            key={frame.frameNum} onClick={self.props.onFrameClick}/>
                    )
                })}
                </div>
            )
        }

    });

    return FrameLine;

});
