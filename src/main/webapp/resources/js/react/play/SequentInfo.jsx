define(["jquery", "react", "app/utils/DateUtils", "app/utils/SequentUtils", "app/utils/SnapshotMessaging",
        "app/react/play/LongText", "app/react/play/Stacktrace", "app/react/play/SnapshotElementPath", "highlightjs"],
    function ($, React, DateUtils, SequentUtils, SnapshotMessaging, LongText, Stacktrace, SnapshotElementPath, HighlightJS) {

    var SequentInfo = React.createClass({

        statics: {
            findRendering: findRendering,
            getSequentTip: getSequentTip
        },

        /* State declaration */
        getInitialState: function () {
            return {
                mutationInfo: null
            };
        },

        /* Props declaration */
        propTypes: {
            sequent: React.PropTypes.object,
            activeFrame: React.PropTypes.object,
            goToPrevSequent: React.PropTypes.func.isRequired
        },

        goToPrevSequent: function (e, sequentNum) {
            this.props.goToPrevSequent(sequentNum);
            e.preventDefault();
        },

        goToSequent: function (sequentNum) {
            return function (e) {
                this.props.goToPrevSequent(sequentNum);
                e.preventDefault();
            }.bind(this);
        },

        render: function () {
            if (!this.props.sequent) return null;

            var rendering = findRendering(this.props.sequent);
            return rendering.prepare.bind(this)(this.props.sequent);
        },

        componentDidMount: function () {
            window.addEventListener("message", function (event) {
                if (["http://localhost:8080", "http://api.kuoll.com", "https://api.kuoll.com"].indexOf(event.origin) != -1) {
                    var data = JSON.parse(event.data);
                    if (data.action == "updateSequentInfo") {
                        this.setState({
                            mutationInfo: data
                        });
                    }
                }
            }.bind(this));
        }

    });

    function parseCookie(cookie) {
        var cookieObj = {};
        cookie.split(';').forEach(function(val, index) {
            var tokens = val.split("=");

            if (index == 0) {
                cookieObj["__key"] = tokens[0];
            }
            cookieObj[tokens[0]] = tokens[1];
        });
        return cookieObj;
    }

    function findRendering(sequent) {
        try {
            var l1 = sequentRendering[sequent.sequentType];
            if (!l1) {
                return DEFAULT_RENDERING[sequent.sequentType];
            }
            if (l1.isRendering) {
                return l1;
            }

            var l2;
            var classes = Object.getOwnPropertyNames(l1);
            for (var i = 0; i < classes.length; ++i) {
                if (classes[i].split("|").indexOf(sequent.sequentClass) != -1) {
                    l2 = l1[classes[i]];
                }
            }
            if (!l2) {
                l2 = l1.other;
            }
            if (!l2) {
                return DEFAULT_RENDERING[sequent.sequentType];
            }

            if (l2.isRendering) {
                return l2;
            }

            var l3 = l2[sequent.sequentSubtype] || (l2.other && l2.other[sequent.sequentSubtype]) || l2.default;

            if (!l3) {
                return DEFAULT_RENDERING[sequent.sequentType];
            }
            return l3;
        } catch (e) {
            console.error(e.stack);
            return DEFAULT_RENDERING[sequent.sequentType];
        }
    }

    //noinspection JSUnusedLocalSymbols
    function getSequentTip(frame, sequent) {
        SequentUtils.parseRawData(sequent);

        var titleTemplate = SequentInfo.findRendering(sequent).config().titleTemplate;
        var title = (function (sequent, template) {
            return template.replace(/{{([\w.\[\]\(\)\s,=\?:"\|]+)}}/g,
                function (match, p1, offset, string) {
                    return eval(p1);
                })
        })(sequent, titleTemplate);

        return title;
    }

    function createRendering(info) {
        return {
            prepare: function (sequent) {
                SequentUtils.parseRawData(sequent);

                try {
                    var sequentInfo = info.prepare.bind(this)(sequent);
                    return sequentInfo;
                } catch (e) {
                    console.debug(e);
                    return (
                        <div>
                            Error while preparing sequent info
                        </div>
                    );
                }
            },

            config: function () {
                return info;
            },

            isRendering: true,

            getCssClasses: function () {
                return info.cssClasses;
            },

            getIconClass: function () {
                return info.iconClass;
            },

            getColor: function () {
                return info.color;
            }
        }
    }

    var DEFAULT_RENDERING = {
        "event": createRendering({
            cssClasses: ["eventSequent", "executionSequentGroup"],
            iconClass: 'fa-asterisk',
            prepare: prepareDefaultEventSequent,
            titleTemplate: 'Browser event {{sequent.rawData.type}} happened'
        }),
        "interception": createRendering({
            cssClasses: ["interceptionSequent", "executionSequentGroup"],
            iconClass: 'fa-terminal',
            prepare: prepareDefaultInterceptionSequent,
            titleTemplate: 'Method {{sequent.sequentClass}}.{{sequent.rawData.methodName}} was invoked'
        })
    };

    function shortString(str) {
        if (str === null || str === undefined) {
            console.warn("1 argument was expected");
            return "";
        }

        if (str.length > 40) {
            str = str.slice(0, 30) + "(...)" + str.slice(str.length - 10);
        }
        return str;
    }

    var sequentRendering = {
        "event": {
            "MouseEvent|PointerEvent|MSEventObj": createRendering({
                cssClasses: ["mouseClickSequent", "userActionSequentGroup"],
                iconClass: 'fa-location-arrow',
                prepare: prepareMouseSequent,
                titleTemplate: 'Mouse click: {{shortString(sequent.rawData.target ? sequent.rawData.target.HTMLElementPath : sequent.rawData.srcElement.HTMLElementPath)}}'
            }),
            "KeyboardEvent": createRendering({
                cssClasses: ["keyboardSequent", "userActionSequentGroup"],
                iconClass: 'fa-keyboard-o',
                prepare: prepareKeyboardSequent,
                titleTemplate: 'Key: {{makeKeyString(sequent.rawData.altKey, sequent.rawData.ctrlKey, sequent.rawData.shiftKey, sequent.rawData.metaKey, sequent.rawData.charCode || sequent.rawData.keyCode || sequent.rawData.which)}}'
            }),
            XMLHttpRequestProgressEvent: createRendering({
                cssClasses: ["xhrResponseSequent", "networkingSequentGroup"],
                iconClass: 'fa-long-arrow-left',
                prepare: prepareXhrResponseSequent,
                titleTemplate: 'XHR response: {{sequent.rawData.target.status}} {{sequent.rawData.target.statusText}} {{shortString(sequent.rawData.target.responseURL)}}'
            }),
            MessageEvent: createRendering({
                cssClasses: ["messageSequent", "networkingSequentGroup"],
                iconClass: 'fa-envelope-o',
                prepare: prepareMessageSequent,
                titleTemplate: 'Message from {{sequent.rawData.origin}}'
            }),
            ProgressEvent: {
                "load": createRendering({
                    cssClasses: ["executionSequentGroup", "xhrLoadSequent"],
                    iconClass: 'fa-check-square',
                    prepare: prepareXhrLoadSequent,
                    titleTemplate: 'XHR complete'
                }),
            },
            other: {
                "scroll": createRendering({
                    cssClasses: ["scrollSequent", "visualChangeSequentGroup"],
                    iconClass: 'fa-arrows-v',
                    prepare: prepareScrollSequent,
                    titleTemplate: 'Scroll to x: {{sequent.scrollX}}; y: {{sequent.scrollY}}'
                }),
                "[initialPageLoad]": createRendering({
                    cssClasses: ["initialLoadSequent", "executionSequentGroup"],
                    iconClass: 'fa-circle',
                    prepare: prepareInitialLoadSequent,
                    titleTemplate: 'Record start'
                }),
                "[createIssue]": createRendering({
                    cssClasses: ["createIssueSequent", "executionSequentGroup"],
                    iconClass: 'fa-exclamation-circle',
                    prepare: prepareCreateIssueSequent,
                    titleTemplate: 'New issue'
                }),
                "error": createRendering({
                    cssClasses: ["errorSequent", "executionSequentGroup"],
                    iconClass: 'fa-times-circle',
                    prepare: prepareErrorSequent,
                    titleTemplate: 'JS Error {{shortString(sequent.rawData.message)}}:{{sequent.rawData.lineno}}'
                }),
                "visibilitychange": createRendering({
                    cssClasses: ["visibilitySequent", "visualChangeSequentGroup"],
                    iconClass: 'fa-folder-o',
                    prepare: prepareVisibilitySequent,
                    titleTemplate: 'Visibility change'
                }),
                "beforeunload": createRendering({
                    cssClasses: ["beforeUnloadSequent", "executionSequentGroup"],
                    iconClass: 'fa-sign-out',
                    prepare: prepareBeforeUnloadSequent,
                    titleTemplate: 'Page beforeunload'
                }),
                "resize": createRendering({
                    cssClasses: ["resizeSequent", "visualChangeSequentGroup"],
                    iconClass: 'fa-arrows-alt',
                    prepare: prepareResizeSequent,
                    titleTemplate: 'window.resize'
                }),
                "focus": createRendering({
                    cssClasses: ["focusSequent", "executionSequentGroup"],
                    iconClass: 'fa-plus-square-o',
                    prepare: prepareFocusSequent,
                    titleTemplate: 'Focus on {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "blur": createRendering({
                    cssClasses: ["focusSequent", "executionSequentGroup"],
                    iconClass: 'fa-minus-square-o',
                    prepare: prepareBlurSequent,
                    titleTemplate: 'Focus blur {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "DOMFocusIn": createRendering({
                    cssClasses: ["focusSequent", "executionSequentGroup"],
                    iconClass: 'fa-plus-square-o',
                    prepare: prepareFocusSequent,
                    titleTemplate: 'DOM focus in {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "DOMFocusOut": createRendering({
                    cssClasses: ["focusSequent", "executionSequentGroup"],
                    iconClass: 'fa-minus-square-o',
                    prepare: prepareBlurSequent,
                    titleTemplate: 'DOM focus lost: {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "readystatechange": createRendering({
                    cssClasses: ["readyStateChangeSequent", "networkingSequentGroup"],
                    iconClass: 'fa-check-circle-o',
                    prepare: prepareReadyStateChangeSequent,
                    titleTemplate: 'XHR state {{readyStateByNumber[sequent.rawData.target.readyState]}}'
                }),
                "input": createRendering({
                    cssClasses: ["inputSequent", "userActionSequentGroup"],
                    iconClass: 'fa-pencil-square-o',
                    prepare: prepareInputSequent,
                    titleTemplate: 'Input event {{shortString(sequent.rawData.target.HTMLElementPath)}}'
                }),
                "load": createRendering({
                    cssClasses: ["executionSequentGroup", "loadSequent"],
                    iconClass: 'fa-check-square',
                    prepare: prepareLoadSequent,
                    titleTemplate: 'onload'
                }),
                "DOMContentLoaded": createRendering({
                    cssClasses: ["executionSequentGroup", "domContentLoadedSequent"],
                    iconClass: 'fa-check-square',
                    prepare: prepareDOMContentLoadedSequent,
                    titleTemplate: 'document.DOMContentLoaded'
                }),
                // Used 2 icons for 6 different sequent types
                "dragstart": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragStartSequent"],
                    iconClass: 'fa-hand-rock-o',
                    prepare: prepareDragStartSequent,
                    titleTemplate: 'Drag start'
                }),
                "drop": createRendering({
                    cssClasses: ["userActionSequentGroup", "dropSequent"],
                    iconClass: 'fa-hand-paper-o',
                    prepare: prepareDropSequent,
                    titleTemplate: 'Drag drop'
                }),
                "dragleave": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragLeaveSequent"],
                    iconClass: 'fa-hand-rock-o',
                    prepare: prepareDragLeaveSequent,
                    titleTemplate: 'Drag leave'
                }),
                "dragend": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragEndSequent"],
                    iconClass: 'fa-hand-paper-o',
                    prepare: prepareDragEndSequent,
                    titleTemplate: 'Drag end'
                }),
                "dragover": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragOverSequent"],
                    iconClass: 'fa-hand-rock-o',
                    prepare: prepareDragOverSequent,
                    titleTemplate: 'Drag over'
                }),
                "dragenter": createRendering({
                    cssClasses: ["userActionSequentGroup", "dragEnterSequent"],
                    iconClass: 'fa-hand-rock-o',
                    prepare: prepareDragEnterSequent,
                    titleTemplate: 'Drag enter'
                })
            }
        },
        "mutation": createRendering({
            cssClasses: ["domMutationSequent", "visualChangeSequentGroup"],
            iconClass: 'fa-code',
            prepare: prepareDomMutationSequent,
            titleTemplate: 'DOM mutation'
        }),
        interception: {
            XMLHttpRequest: {
                "open": createRendering({
                    cssClasses: ["xhrRequestSequent", "networkingSequentGroup"],
                    iconClass: 'fa-circle-o',
                    prepare: prepareOpenXhrSequent,
                    titleTemplate: 'XMLHttpRequest open: {{sequent.rawData.args[0]}} {{shortString(sequent.rawData.args[1])}}'
                }),
                "send": createRendering({
                    cssClasses: ["xhrRequestSequent", "networkingSequentGroup"],
                    iconClass: 'fa-long-arrow-right',
                    prepare: prepareSendXhrSequent,
                    titleTemplate: 'XMLHttpRequest send'
                }),
                abort: createRendering({
                    cssClasses: ["xhrAbortSequent", "networkingSequentGroup"],
                    iconClass: 'fa-ban',
                    prepare: prepareAbortXhrSequent,
                    titleTemplate: 'XMLHttpRequest abort'
                })
            },
            Console: {
                log: createRendering({
                    cssClasses: ["consoleSequent", "executionSequentGroup"],
                    iconClass: 'fa-terminal',
                    prepare: prepareConsoleSequent,
                    titleTemplate: 'console {{shortString(sequent.rawData.args[0])}}'
                })
            },
            History: {
                pushState: createRendering({
                    cssClasses: ["historyPushSequent", "executionSequentGroup"],
                    iconClass: 'fa-caret-square-o-down',
                    prepare: prepareHistoryPushSequent,
                    titleTemplate: 'history.pushState {{shortString(sequent.rawData.args[2])}}'
                })
            },
            Promise: {
                constructor: createRendering({
                    cssClasses: ["execution"],
                    color: "#dd6",
                    iconClass: 'fa-bookmark',
                    prepare: function (sequent) {
                        return (
                            <span>
                                <div>
                                    <strong>New Promise</strong>
                                </div>
                                <div>
                                    <Stacktrace stacktrace={sequent.stack} />
                                </div>
                            </span>
                        )
                    },
                    titleTemplate: 'Promise created'
                }),
                onFulfilled: createRendering({
                    cssClasses: ["execution"],
                    color: "#5c5",
                    iconClass: 'fa-bookmark',
                    prepare: function (sequent) {
                        return (
                            <span>
                                <div>
                                    <a href="" onClick={this.goToSequent(sequent.previousSequentNum)}>Previously created promise
                                    </a> successfully executed. Stacktrace:
                                </div>
                                <div>
                                    <Stacktrace stacktrace={sequent.stack} />
                                </div>
                            </span>
                        )
                    },
                    titleTemplate: 'Promise successfully executed'
                }),
                onRejected: createRendering({
                    cssClasses: ["execution"],
                    color: "#f77",
                    iconClass: 'fa-bookmark',
                    prepare: function (sequent) {
                        return (
                            <span>
                                <div>
                                    <a href="" onClick={this.goToSequent(sequent.previousSequentNum)}>Previously created promise
                                    </a> successfully executed. Stacktrace:
                                </div>
                                <div>
                                    <Stacktrace stacktrace={sequent.stack} />
                                </div>
                            </span>
                        )
                    },
                    titleTemplate: 'Promise rejected'
                })
            },
            Window: {
                "setTimeout": createRendering({
                    cssClasses: ["timerSequent", "executionSequentGroup"],
                    iconClass: 'fa-clock-o',
                    prepare: prepareSetTimeoutSequent,
                    titleTemplate: 'window.setTimeout'
                }),
                "clearTimeout": createRendering({
                    cssClasses: ["timerSequent", "executionSequentGroup"],
                    iconClass: 'fa-circle-o',
                    prepare: prepareClearTimeoutSequent,
                    titleTemplate: 'window.clearTimeout'
                }),
                "setInterval": createRendering({
                    cssClasses: ["intervalSequent", "executionSequentGroup"],
                    iconClass: 'fa-history',
                    prepare: prepareSetIntervalSequent,
                    titleTemplate: 'window.setInterval'
                }),
                "clearInterval": createRendering({
                    cssClasses: ["intervalSequent", "executionSequentGroup"],
                    iconClass: 'fa-undo',
                    prepare: prepareClearIntervalSequent,
                    titleTemplate: 'window.clearInterval'
                }),
                "postMessage": createRendering({
                    cssClasses: ["postMessageSequent", "executionSequentGroup"],
                    iconClass: 'fa-envelope',
                    prepare: preparePostMessageSequent,
                    titleTemplate: 'Message was sent to origin {{shortString(sequent.rawData.args[1])}}'
                })
            },

            "[cookie_set]": createRendering({
                cssClasses: ["executionSequentGroup", "setCookieSequent"],
                iconClass: "fa-sticky-note-o",
                prepare: prepareSetCookieSequent,
                titleTemplate: "Cookies {{shortString(sequent.rawData.value)}}"
            }),

            "[setTimeout-callback]": {
                default: createRendering({
                    cssClasses: ["timerSequent", "executionSequentGroup"],
                    iconClass: 'fa-clock-o',
                    prepare: prepareTimeoutCallbackSequent,
                    titleTemplate: 'window.setTimeout callback'
                })
            },
            "[setInterval-callback]": {
                default: createRendering({
                    cssClasses: ["intervalSequent", "executionSequentGroup"],
                    iconClass: 'fa-history',
                    prepare: prepareIntervalCallbackSequent,
                    titleTemplate: 'window.setInterval callback'
                })
            }
        }
    };

    var mouseKeyTypeByNumber = [
        "Left",
        "Middle",
        "Right"
    ];

    var readyStateByNumber = ["UNSENT", "OPENED", "HEADERS_RECEIVED", "LOADING", "DONE"];

    var keyNameByCode = {
        8: "Backspace",
        32: "Spacebar",
        13: "Enter"
    };

    function makeKeyString(alt, ctrl, shift, meta, keyCode) {
        var key = keyNameByCode[keyCode];
        if (!key) {
            key = String.fromCharCode(keyCode);
        }

        return (alt ? "Alt + " : "")
            + (ctrl ? "Ctrl + " : "")
            + (shift ? "Shift + " : "")
            + (meta ? "Meta + " : "")
            + key;
    }

    function map(array, callback) {
        return array.map(function () {
            try {
                return callback.apply(this, arguments);
            } catch (e) {
                console.warn(e);
            }
        });
    }

    function prepareLoadSequent(sequent) {
        if (sequent.rawData.target.HTMLElementPath == "document") {
            return (
                <div>Document loaded</div>
            )
        }
        var scriptSrc, attributes = sequent.rawData.target.attributes;
        if (attributes) {
            for (var i = 0; i < attributes.length; ++i) {
                var attr = attributes[i];
                if (attr.name == "src") {
                    scriptSrc = attr.value;
                    break;
                }
            }
        }
        return (
            <div>
                Resource {scriptSrc ? <code><LongText text={scriptSrc} /></code> : ""} was loaded.
            </div>
        )
    }

    function prepareXhrLoadSequent(sequent) {
        var scriptSrc = sequent.rawData.target.responseURL;
        return (
            <div>
                XMLHttpRequest {scriptSrc ? <span>to <code><LongText text={scriptSrc} /></code></span> : ""} completed.
            </div>
        )
    }

    function prepareDOMContentLoadedSequent() {
        return (
            <div>
                The document loading finished.
            </div>
        )
    }

    function prepareKeyboardSequent(sequent) {
        var rawData = sequent.rawData;
        var keyString = makeKeyString(rawData.altKey, rawData.ctrlKey, rawData.shiftKey, rawData.metaKey, rawData.charCode || rawData.keyCode || rawData.which);

        return (
            <div>
                Key pressed: <code className="keyboardButton">{keyString}</code>. Focus in <SnapshotElementPath path={rawData.target.HTMLElementPath} />
            </div>
        )
    }

    function prepareMouseSequent(sequent) {
        var keys = [].concat(
            sequent.rawData.ctrlKey ? "Ctrl" : undefined,
            sequent.rawData.altKey ? "Alt" : undefined,
            sequent.rawData.shiftKey ? "Shift" : undefined,
            sequent.rawData.metaKey ? "Meta" : undefined
        );
        var keyString = (keys.filter(function (elem) {
                return !!elem;
            }).join(", ") || "No") + " key modifiers";

        var target = sequent.rawData.target || sequent.rawData.srcElement;
        return (
            <div>
                <b>{mouseKeyTypeByNumber[sequent.rawData.button]}</b> mouse click on{" "}
                <SnapshotElementPath path={target.HTMLElementPath} /> {keyString}
            </div>
        )
    }

    function prepareInitialLoadSequent(sequent) {
        function highlightedStartParams() {
            var formattedParams = JSON.stringify(JSON.parse(sequent.rawData.startParams), null, 2);
            return HighlightJS.highlight("json", formattedParams).value;
        }

        return (
            <div>
                <strong>Record started</strong>
                {sequent.rawData.startParams ?
                <span>
                    <pre className="start-params-json-block"
                         dangerouslySetInnerHTML={{__html: highlightedStartParams()}}></pre>
                </span>
                : null }
            </div>
        )
    }

    function prepareCreateIssueSequent(sequent) {
        var type = sequent.rawData.issueType;
        var description = sequent.rawData.description;
        return (
            <div>
                <p>
                    <strong>New issue</strong> {type ? <code><LongText text={type} /></code> : <b>empty</b>}
                </p>
                <strong>Description</strong><br /> {description ? <code><LongText text={description} /></code> : <b>empty</b>}
            </div>
        )
    }

    function prepareXhrResponseSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent && prevSequent.previousSequentNum) {
            var initialSequent = SequentUtils.getSequentByNum(prevSequent.previousSequentNum, this.props.activeFrame);
            if (initialSequent) {
                var initialSequentNode = (
                    <div>
                        Initial sequent is{" "}
                        <a onClick={this.goToSequent(prevSequent.previousSequentNum)}>{initialSequent.sequentSubtype} {initialSequent.sequentType}</a>
                    </div>
                )
            }
        }

        return (                                                                                                 
            <span>
                <div>
                    XMLHttpRequest received response from server.
                    Status <strong>{sequent.rawData.target.status} {sequent.rawData.target.statusText}</strong>.
                    Response body:
                </div>
                <pre>
                    <LongText text={sequent.rawData.target.response} />
                </pre>
                <div>
                    Response headers:
                </div>
                <pre>
                    <LongText text={(sequent.rawData.target.responseHeaders)} />
                </pre>
                <div>
                    <a href="" onClick={this.goToSequent(sequent.previousSequentNum)}>The XHR send request</a> caused this response.
                </div>
                {initialSequentNode}
            </span>
        )
    }

    function prepareAbortXhrSequent(sequent) {
        return (
            <span>
                <div>
                    <a onClick={this.goToSequent(sequent.previousSequentNum)}>XMLHttpRequest</a> was aborted.
                </div>
            </span>
        )
    }

    function prepareReadyStateChangeSequent(sequent) {
        var readyState = sequent.rawData.target.readyState;
        var isDone = (readyState == 4);
        var self = this;

        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent && prevSequent.previousSequentNum) {
            var initialSequent = SequentUtils.getSequentByNum(prevSequent.previousSequentNum, this.props.activeFrame);
            if (initialSequent) {
                var initialSequentNode = (
                    <div>
                        Initial sequent is{" "}
                        <a onClick={self.goToSequent(prevSequent.previousSequentNum)}>{initialSequent.sequentSubtype} {initialSequent.sequentType}</a>
                    </div>
                )
            }
        }

        return (
            <span>
                <div>
                    State of network request changed to <b>{readyStateByNumber[readyState]}</b>
                </div>

            {!isDone ? "" : ([
                <div>
                    XMLHttpRequest received response from server.
                    Status <strong>{sequent.rawData.target.status} {sequent.rawData.target.statusText}</strong>.
                    Response body:
                </div>,
                <pre>
                    <LongText text={sequent.rawData.target.response} />
                </pre>,
                <div>
                    Response headers:
                </div>,
                <pre>
                    <LongText text={(sequent.rawData.target.responseHeaders)} />
                </pre>,
                <div>
                    <a href="" onClick={this.goToSequent(sequent.previousSequentNum)}>The XHR send request</a> caused this response.
                </div>,
                initialSequentNode
            ])}
            </span>
        )
    }

    function prepareVisibilitySequent(sequent) {
        return (
            <div>
                Page was {sequent.rawData.hidden ? "hidden" : "reveal"}.
            </div>
        )
    }

    function prepareBeforeUnloadSequent(sequent) {
        return (
            <p><strong>Page unloaded</strong></p>
        )
    }

    function prepareSetTimeoutSequent(sequent) {
        return (
            <div>
                <h3>setTimeout call</h3>

                <Stacktrace stacktrace={sequent.stack} method="window.setTimeout"/>
            </div>
        )
    }

    function prepareSetIntervalSequent(sequent) {
        return (
            <div>
                <h3>setInterval call</h3>

                <Stacktrace stacktrace={sequent.stack} method="window.setInterval"/>
            </div>
        )
    }

    function prepareClearTimeoutSequent(sequent) {
        return (
            <span>
                <div>
                    <code>window.clearTimeout()</code> has been called. It canceled{" "}
                    <a href="#" onClick={this.goToSequent(sequent.previousSequentNum)}>previous setTimeout</a>
                </div>

                <div>
                    Here is call stack trace:
                </div>

                <Stacktrace stacktrace={sequent.stack} method="window.clearTimeout"/>
            </span>
        )
    }

    function prepareClearIntervalSequent(sequent) {
        return (
            <span>
                <div>
                    <code>window.clearInterval()</code> has been called. It canceled{" "}
                    <a href="#" onClick={this.goToSequent(sequent.previousSequentNum)}>previous setInterval</a>
                </div>

                <div>
                    Here is call stack trace:
                </div>

                <Stacktrace stacktrace={sequent.stack} method="window.clearInterval"/>
            </span>
        )
    }

    function prepareTimeoutCallbackSequent (sequent) {
        return (
            <span>
                Invokation of callback passed to window.setTimeout
                . Caused by <a href="#" onClick={this.goToSequent(sequent.previousSequentNum)}>this sequent</a>
                . Stacktrace:
                <p><Stacktrace stacktrace={sequent.stack} /></p>
            </span>
        )
    }

    function prepareIntervalCallbackSequent (sequent) {
        return (
            <span>
                Invokation of callback passed to window.setInterval
                . Caused by <a href="#" onClick={this.goToSequent(sequent.previousSequentNum)}>this sequent</a>
                . Stacktrace:
                <p><Stacktrace stacktrace={sequent.stack} /></p>
            </span>
        )
    }

    function prepareResizeSequent(sequent) {
        return (
            <div>
                Window was resized. New width is <b>{sequent.rawData.width}</b> and height is <b>{sequent.rawData.height}</b>.
            </div>
        )
    }

    function prepareScrollSequent(sequent) {
        var offset;
        if (sequent.scrollX != 0) {
            offset = (
                <span>
                    <b>{sequent.scrollY}px from top</b> and <b>{sequent.scrollX}px from left</b>
                </span>
            )
        } else {
            offset = (
                <span>
                    <b>{sequent.scrollY}px from top</b>
                </span>
            )
        }

        return (
            <div>
                Page was scrolled to {offset}.
            </div>
        );
    }

    function prepareOpenXhrSequent(sequent) {
        return (
            <div>
                Created new XMLHttpRequest with <code>{sequent.rawData.args[0]}</code> method to{" "}
                <code>
                    <LongText text={sequent.rawData.args[1]} />
                </code>
                .{sequent.previousSequentNum ?
                <span> Caused by <a href="" onClick={this.goToSequent(sequent.previousSequentNum)}>this sequent</a></span>
                : null}
            </div>
        )
    }

    function prepareSendXhrSequent(sequent) {
        var requestBody;
        if (sequent.rawData.args[0]) {
            requestBody = (
                <span>
                    Request body:{" "}
                    <pre>
                        <LongText text={sequent.rawData.args[0]} />
                    </pre>
                </span>
            )
        }

        return (
            <div>
                <div>
                    XMLHttpRequest sent. {requestBody || "No additional data added."}
                </div>
            </div>
        );
    }

    function prepareConsoleSequent(sequent) {
        return (
            <span>
                <div>
                    Text was written to browser's console:
                </div>

                <pre>
                    <LongText text={sequent.rawData.args[0]} />
                </pre>
            </span>
        );
    }

    function prepareErrorSequent(sequent) {
        var firstFramePresent = sequent.stack && sequent.stack[0];
        var filename, lineNum;
        if (firstFramePresent) {
            filename = sequent.stack[0].uri.slice(sequent.stack[0].uri.lastIndexOf("/") + 1);
            lineNum = sequent.stack[0].line;
        } else {
            filename = sequent.rawData.filename;
            lineNum = sequent.rawData.lineno;
        }
        return (
            <span>
                <div>
                    Uncaught <code>Error</code>. File{" "}
                    <code>
                        <LongText text={filename} />
                    </code> at line{" "}
                    <b>{lineNum}</b>.
                </div>
                <p>
                    <code>
                        <LongText text={sequent.rawData.message} />
                    </code>
                </p>

            {sequent.stack && sequent.stack && sequent.stack.length ? [

                <Stacktrace stacktrace={sequent.stack}/>] :
                null }
            </span>
        )
    }

    function prepareDomMutationSequent(sequent) {
        var mutationInfo = this.state.mutationInfo;
        if (!mutationInfo || !(mutationInfo.frameNum == sequent.frameNum && mutationInfo.sequentNum == sequent.sequentNum)) {
            return null;
        }
        var mutations = SequentUtils.parseMutations(sequent);
        var addedOrMoved = mutations.addedOrMoved, removed = mutations.removed, attributes = mutations.attributes,
            text = mutations.text, propertiesChanged = mutations.propertiesChanged;

        if (addedOrMoved.length) {
            var addedNodesElement = (
                <span>
                    <div>Added nodes: </div>
                    <ul>
                            {map(addedOrMoved, function (node, index) {
                                var elem = mutationInfo[node.id];
                                if (node.nodeType == Node.TEXT_NODE) {
                                    return (
                                        <li key={"added-" + index}>
                                            Text node{" "}
                                            <code>
                                                <LongText text={node.textContent} />
                                            </code> added to parent{" "}
                                            <SnapshotElementPath path={elem.parentPath} />
                                        </li>
                                    )
                                } else if (node.nodeType == Node.COMMENT_NODE) {
                                    // no need to display adding of HTML comments
                                    return null;
                                } else {
                                    return (
                                        <li key={"added-" + index}>
                                            <code>{elem.tagName}</code> CSS path{" "}
                                            <SnapshotElementPath path={elem.nodePath}/> added to parent{" "}
                                            <SnapshotElementPath path={elem.parentPath} />
                                        </li>
                                    )
                                }
                            })}
                            {map(text, function createTextNode(node, index) {
                                var elem = mutationInfo[node.id];
                                return (
                                    <li key={"added-" + index}>
                                        Text node{" "}
                                        <code>
                                            <LongText text={elem.text} />
                                        </code> added to parent{" "}
                                        <SnapshotElementPath path={elem.parentPath} />
                                    </li>
                                )
                            })}
                    </ul>
                </span>
            )
        }
        if (removed.length) {
            var removedNodesElement = (
                <span>
                    <div>Removed nodes: </div>
                    <ul>
                            {map(removed, function (node, index) {
                                var elem = mutationInfo[node.id];
                                var description;
                                if (elem) {
                                    if (elem.tagName) {
                                        description = (
                                            <span>
                                                Node
                                                <code>{elem.tagName}</code>
                                            </span>
                                        )
                                    } else {
                                        description = (
                                            <span>
                                                Text node{" "}
                                                <code>
                                                    <LongText text={elem.text} />
                                                </code>
                                            </span>
                                        )
                                    }
                                    return (
                                        <li key={"added-" + index}>
                                        {description} removed from parent{" "}
                                            <SnapshotElementPath path={elem.parentPath} />
                                        </li>
                                    )
                                }
                            })}
                    </ul>
                </span>
            )
        }
        if (attributes.length) {
            var attributesElement = (
                <span>
                    <div>Attributes value changed: </div>
                    <ul>
                            {map(attributes, function (attrInfo, indexOuter) {
                                var attributes = attrInfo.attributes;
                                var elem = mutationInfo[attrInfo.id];
                                return Object.keys(attributes).map(function (attrName, indexInner) {
                                    return (
                                        <li key={indexOuter + "_" + indexInner}>
                                            Attribute{" "}
                                            <code>
                                                <LongText text={attrName} />
                                            </code> of node{" "}
                                            <SnapshotElementPath path={elem.nodePath} /> changed to{" "}
                                            <code>
                                                <LongText text={attributes[attrName]} />
                                            </code>
                                        </li>
                                    )
                                })
                            })}
                    </ul>
                </span>
            )
        }
        if (propertiesChanged && propertiesChanged.length) {
            var valueChangedElement = (
                <span>
                    <div>HTML properties changed: </div>
                    <ul>
                    {map(propertiesChanged, function (info, indexOuter) {
                        var keys = Object.keys(info.properties);
                        var elem = mutationInfo[info.id];
                        return map(keys, function (key, indexInner) {
                            return (
                                <li key={indexOuter + "_" + indexInner}>
                                    Value of property <code>{key}</code> of element
                                    <SnapshotElementPath path={elem.nodePath} />
                                    changed to <code><LongText text={info.properties[key]} /></code>
                                </li>
                            )
                        });
                    })}
                    </ul>
                </span>
            )
        }

        return (
            <span>
                {addedNodesElement}
                {removedNodesElement}
                {attributesElement}
                {valueChangedElement}
            </span>
        )
    }

    function prepareFocusSequent(sequent) {
        return (
            <div>
                On focus <SnapshotElementPath path={sequent.rawData.target.HTMLElementPath} />
            </div>
        )
    }

    function prepareBlurSequent(sequent) {
        return (
            <div>
                On focusout <SnapshotElementPath path={sequent.rawData.target.HTMLElementPath} />
            </div>
        )
    }

    function prepareHistoryPushSequent(sequent) {
        return (
            <div>
                history.push method called. Url to push:{" "}
                <code>
                    <LongText text={sequent.rawData.args[2]} />
                </code>
                , page title:{" "}
                <code>
                    <LongText text={sequent.rawData.args[1]} />
                </code>
            </div>
        )
    }

    function prepareInputSequent(sequent) {
        return (
            <div>
                Value changed of {" "}
                <SnapshotElementPath path={sequent.rawData.target.HTMLElementPath} />
            </div>
        )
    }

    function prepareMessageSequent(sequent) {
        var ports;
        if (sequent.rawData.ports && sequent.rawData.ports.length > 0) {
            ports = (
                <span>
                    in ports <LongText text={sequent.rawData.ports.join(", ")} />
                </span>
            )
        }

        var data;
        if (sequent.rawData.data) {
            data = (
                <span>
                    <div>
                        Data that sent:
                    </div>
                    <pre>
                        <LongText text={sequent.rawData.data} />
                    </pre>
                </span>
            )
        } else {
            data = (
                <div>
                    No data sent
                </div>
            )
        }

        return (
            <span>
                <div>
                    Message sent to <code><LongText text={sequent.rawData.origin}/></code> {ports}.
                </div>
                {data}
            </span>
        )
    }

    function prepareSetCookieSequent(sequent) {
        var cookie = parseCookie(sequent.rawData.value);
        var oldValue = sequent.rawData.oldValue;
        var key = cookie.__key;
        return (
            <span>
                <h3>Cookies updated</h3>

            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Property</th>
                    <th>Value</th>
                </tr>
                </thead>

                {typeof cookie.__key != "undefined" ? <tr>
                    <td>Key</td>
                    <td>
                        <LongText text={cookie.__key} />
                    </td>
                </tr> : null}
                {typeof oldValue != "undefined" ? <tr>
                    <td>Previous value</td>
                    <td>
                        <LongText text={oldValue} />
                    </td>
                </tr> : null}
                {typeof cookie[key] != "undefined" ? <tr>
                    <td>Value</td>
                    <td>
                        <LongText text={cookie[key]} />
                    </td>
                </tr> : null}
                {typeof cookie.path != "undefined" ? <tr>
                    <td>Path</td>
                    <td>
                        <LongText text={cookie.path} />
                    </td>
                </tr> : null}
                {typeof cookie.domain != "undefined" ? <tr>
                    <td>Domain</td>
                    <td>{cookie.domain}</td>
                </tr> : null}
                {typeof cookie["max-age"] != "undefined" ? <tr>
                    <td>Max age</td>
                    <td>{cookie["max-age"]} seconds</td>
                </tr> : null}
                {typeof cookie.expires != "undefined" ? <tr>
                    <td>Expires</td>
                    <td>{cookie.expires}</td>
                </tr> : null}
                {typeof cookie.secure != "undefined" ? <tr>
                    <td>Secured</td>
                    <td>{cookie.secure}</td>
                </tr> : null}
            </table>
            </span>
        )
    }

    function prepareDragStartSequent(sequent) {
        var targetPath = sequent.rawData.target.HTMLElementPath;
        return (
            <span>
                On dragstart <SnapshotElementPath path={targetPath} />
            </span>
        )
    }

    function prepareDropSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        SequentUtils.parseRawData(prevSequent);
        var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        var targetPath = sequent.rawData.target.HTMLElementPath;
        return (
            <span>
                <SnapshotElementPath path={draggablePath} /> has been dropped to
                <SnapshotElementPath path={targetPath} />
                <br/>
                Drag started <a onClick={this.goToSequent(sequent.previousSequentNum)}>sequent</a>
            </span>
        )
    }

    function prepareDragLeaveSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent) {
            SequentUtils.parseRawData(prevSequent);
            var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        }
        var targetPath = sequent.rawData.target.HTMLElementPath;

        return (
            <span>
                {draggablePath ?
                    <SnapshotElementPath path={draggablePath} /> :
                    "A draggable element" } has left the element <SnapshotElementPath path={targetPath} />
                {prevSequent ?
                    <span>Drag started <a onClick={this.goToSequent(sequent.previousSequentNum)}>sequent</a></span> :
                    null
                }
            </span>
        )
    }

    function prepareDragEndSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent) {
            SequentUtils.parseRawData(prevSequent);
            var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        }

        return (
            <span>
                Dragging an element
                {draggablePath ?
                    <SnapshotElementPath path={draggablePath} /> : null } has been ended
                    . {prevSequent ?
                <span>Drag started <a onClick={this.goToSequent(sequent.previousSequentNum)}>sequent</a></span> : null
                }
            </span>
        )
    }

    function prepareDragOverSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent) {
            SequentUtils.parseRawData(prevSequent);
            var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        }
        var targetPath = sequent.rawData.target.HTMLElementPath;

        return (
            <span>
            {draggablePath ? <SnapshotElementPath path={draggablePath} /> : "A draggable element" } has been dragged over <SnapshotElementPath path={targetPath} />
                . {prevSequent ?
                <span>Dragging was started on <a onClick={this.goToSequent(sequent.previousSequentNum)}>this sequent</a>.</span> : null
                }
            </span>
        )
    }

    function prepareDragEnterSequent(sequent) {
        var prevSequent = SequentUtils.getSequentByNum(sequent.previousSequentNum, this.props.activeFrame);
        if (prevSequent) {
            SequentUtils.parseRawData(prevSequent);
            var draggablePath = prevSequent.rawData.target.HTMLElementPath;
        }
        var targetPath = sequent.rawData.target.HTMLElementPath;

        return (
            <span>
            {draggablePath ? <SnapshotElementPath path={draggablePath} /> : "A draggable element" } has been dragged into <SnapshotElementPath path={targetPath} />
                . {prevSequent ?
                <span>Dragging was started on <a onClick={this.goToSequent(sequent.previousSequentNum)}>this sequent</a>.</span> : null
                }
            </span>
        )
    }

    function preparePostMessageSequent(sequent) {
        let message;
        try {
            message = typeof sequent.rawData.args[0] === 'object' ?
                JSON.stringify(sequent.rawData.args[0], null, 2) : sequent.rawData.args[0];
        } catch (e) {
            message = sequent.rawData.args[0];
        }
        return (
            <span>
                <div>
                    <code>window.postMessage</code> was called. Message was sent to origin{" "}
                    <code><LongText text={sequent.rawData.args[1]}/></code>.
                </div>
                <div>
                    Message that was sent:
                    <pre>
                        <LongText text={message}/>
                    </pre>
                </div>
                <div>Stacktrace:</div>
                <div>
                    <Stacktrace stacktrace={sequent.stack}/>
                </div>
            </span>
        )
    }

    function prepareDefaultEventSequent(sequent) {
        return (
            <span>
                Browser event of type <code>{sequent.rawData.type}</code> happened.
            </span>
        )
    }

    function prepareDefaultInterceptionSequent(sequent) {
        var args = Array.prototype.slice.call(sequent.rawData.args || [])
            .map(function (arg) {
                try {
                    return JSON.stringify(JSON.parse(arg));
                } catch (e) {
                    return JSON.stringify(arg);
                }
            }).join("\n");
        return (
            <span>
                <div>Method <code>{sequent.sequentClass}.{sequent.rawData.methodName}</code> was invoked.</div>
            {sequent.rawData.args ?
                <div>
                    <h3>Arguments: </h3>
                    <code><LongText text={args}></LongText></code>
                </div>
            : null }
            </span>
        )
    }

    return SequentInfo;

});