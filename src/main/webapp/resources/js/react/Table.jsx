define(["react"], function (React) {

    var Table = React.createClass({

        /* Props declaration */
        propTypes: {
            data: React.PropTypes.array,
            columns: React.PropTypes.object.isRequired,
            cells: React.PropTypes.object
        },

        render: function () {
            var self = this;
            return (
                <table className="table table-striped">
                    <thead>
                {Object.getOwnPropertyNames(self.props.columns).map(function (columnName) {
                    return (
                        <th key={columnName}>
                            {self.props.columns[columnName]}
                        </th>
                    )
                })}
                    </thead>
                    <tbody>
                {(this.props.data || []).map(function (data, index) {
                    return (
                        <tr key={index}>
                            {Object.getOwnPropertyNames(self.props.columns).map(function (columnName) {
                                var cellContent;
                                if (self.props.cells && self.props.cells[columnName]) {
                                    cellContent = self.props.cells[columnName](data);
                                } else {
                                    cellContent = data[columnName];
                                }
                                return (
                                    <td key={columnName}>
                                        {cellContent}
                                    </td>
                                )
                            })}
                        </tr>
                    )
                })}
                    </tbody>
                </table>
            )
        }

    });

    return Table;

});
