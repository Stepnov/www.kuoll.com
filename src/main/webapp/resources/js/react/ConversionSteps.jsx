define(["jquery", "react", "app/react/LossStep"], function ($, React, LossStep) {

    var ConversionSteps = React.createClass({

        /* Props declaration */
        propTypes: {
            loadStepLoss: React.PropTypes.number,
            loadStepConvDrop: React.PropTypes.number,
            loadStepAffected: React.PropTypes.number,

            addStepLoss: React.PropTypes.number,
            addStepConvDrop: React.PropTypes.number,
            addStepAffected: React.PropTypes.number,

            checkoutStepLoss: React.PropTypes.number,
            checkoutStepConvDrop: React.PropTypes.number,
            checkoutStepAffected: React.PropTypes.number
        },

        getDefaultProps: function () {
            return {
                loadStepLoss: 0,
                loadStepConvDrop: 0,
                loadStepAffectedPct: 0,
    
                addStepLoss: 0,
                addStepConvDrop: 0,
                addStepAffectedPct: 0,
    
                checkoutStepLoss: 0,
                checkoutStepConvDrop: 0,
                checkoutStepAffectedPct: 0,


                withVersion: true
            };
        },

        render: function () {

            return (
                <div className="stepChart">
                    <LossStep
                        revenueLossPct={this.props.loadStepLoss}
                        conversionDropPct={this.props.loadStepConvDrop}
                        sessionsAffectedPct={this.props.loadStepAffectedPct}
                        stepName="Add to cart"
                    />
                    <LossStep
                        revenueLossPct={this.props.addStepLoss}
                        conversionDropPct={this.props.addStepConvDrop}
                        sessionsAffectedPct={this.props.addStepAffectedPct}
                        stepName="Checkout"
                    />
                    <LossStep
                        revenueLossPct={this.props.checkoutStepLoss}
                        conversionDropPct={this.props.checkoutStepConvDrop}
                        sessionsAffectedPct={this.props.checkoutStepAffectedPct}
                        stepName="Purchase"
                    />

                </div>
            )
        }

    });

    return ConversionSteps;

});