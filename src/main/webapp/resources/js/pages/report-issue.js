define(["jquery", "app/utils/api"], function ($, api) {

    $(function () {
        var $nameField = $("#nameField");
        var $companyField = $("#companyField");
        var $emailField = $("#emailField");
        var $messageField = $("#messageField");
        var $errorBox = $("#errorMessageBox");
        var $form = $("#feedbackForm");

        function showErrorMsg(message) {
            $('#errorMessageBox').text(message).show();
            $errorBox.text(message).css("visibility", "visible");
            if ("localhost" !== document.location.hostname && mixpanel) {
                mixpanel.track("Report issue error", {
                    "Error message": message
                });
            }
        }

        function showSuccessMsg() {
            $('#successMessageBox').show();
            if ("localhost" !== document.location.hostname && mixpanel) {
                mixpanel.track("Issue reported");
            }
        }

        $form.on("submit", function (e) {
            $('#errorMessageBox').hide();
            $('#successMessageBox').hide();

            api("reportIssue", {
                name: $nameField.val(),
                company: $companyField.val(),
                email: $emailField.val(),
                content: $messageField.val()
            }, function () {
                showSuccessMsg();
            }, function (errorMsg) {
                showErrorMsg(errorMsg);
            });

            e.preventDefault();
        });
    });

});