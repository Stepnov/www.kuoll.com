define(["jquery", "react", "app/react/LastSequentsTable"], function ($, React,LastSequentsTable) {

    $("#sequentsLoadingCheckbox").on("click", function () {
        LastSequentsTable.active = !LastSequentsTable.active;
    });

    React.render(React.createElement(LastSequentsTable), document.body);

});
