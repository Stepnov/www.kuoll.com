define(["jquery", "app/utils/UrlUtils", "app/utils/api"], function ($, UrlUtils, api) {

    var resetTokenErrorMsg = "Reset token is not specified. Open email we've just sent you, copy link and paste it in address bar";

    $(function () {
        var $errorMessageBox = $('#errorMessageBox');
        var $infoMessageBox = $('#infoMessageBox');
        var $passwordField = $('#passwordField');

        var resetToken = UrlUtils.getParameterByName("resetToken");
        if (resetToken) {
            resetToken = decodeURIComponent(resetToken);
        } else {
            $errorMessageBox.text(resetTokenErrorMsg).css("visibility", "visible");
        }

        $("#setPasswordForm").submit(function (e) {
            $errorMessageBox.text("").css("visibility", "collapse");
            $infoMessageBox.text("").css("visibility", "collapse");

            api("setNewPassword", {
                password: $passwordField.val(),
                token: resetToken
            }, function (resp) {
                $.cookie("userToken", resp.userToken, {domain: window.config.cookieDomain, path: "/"});
                $.removeCookie("isDemoUser");
                if ("localhost" !== document.location.hostname && mixpanel) {
                    mixpanel.register({
                        "User token": resp.userToken,
                        "Kuoll user id": resp.userId
                    });
                }
                document.location = "/issues-dashboard.html#onboardOptional";
            }, function (errorMsg) {
                if (errorMsg === "bad token") {
                    $errorMessageBox.text(resetTokenErrorMsg).css("visibility", "visible");
                } else {
                    $errorMessageBox.text(errorMsg).css("visibility", "visible");
                }
            });

            e.preventDefault();
        });
    });

});

