define(["jquery", "app/utils/api", "bootstrap"], function ($, api) {

    $("#buggyOne").click(function () {
        $("#ajaxText").removeClass("ajaxText");
    });

    $(function () {
        $("#hideMeSlowly").click(function () {
            $("#jumbotron").slideUp();
        });
    });
    $("#ajaxButton").on("click", function () {
        $.ajax({
            url: "sample.json",
            type: "post",
            data: {
                "debug_param1": 1,
                "debug_param2": "test"
            }
        }).done(
            function (data) {
                data = typeof data == "string" ? JSON.parse(data) : data;
                $("#ajaxText").val(data.message);
            }
        );
    });

    $("#startBtn").on("click", function (e) {
        function askToLogin() {
            window.alert("Login to your Kuoll account to start recording please");
            document.location = "/login.html?redirectTo=demo-with-bug.html";
        }

        $("#hiddenText").show();

        var userToken = $.cookie("userToken");
        if (userToken) {
            User.getInfo(function (user) {
                kuoll.startRecord({
                    orgId: user.orgId,
                    API_KEY: user.apiKey,
                    kuollUserId: user.userId,
                    ignoreUrls: ["http:\/\/localhost:8080\/secret\.json"]
                });
            }, function () {
                askToLogin();
            }, false);
        } else {
            askToLogin();
        }
    });

    $(function () {
       kuoll.createPopup("3fc47e1c92ea09bc", null, "right");
    });

});
