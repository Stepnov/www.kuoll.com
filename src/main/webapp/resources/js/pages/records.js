define(
    ["jquery", "app/recordsSources/OwnRecordsSource", "react", "app/react/common/Header",
        "app/react/IssuesList"],
    function ($, OwnRecordsSource, React,Header, IssuesList) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(
                React.createElement(IssuesList, {RecordsSource: OwnRecordsSource}),
                $("#issuesContainer")[0]
            );
        });
    });