define(["jquery", "react", "app/react/common/Header", "app/google-signin", "app/utils/api", "app/onboard", "youtube-api"], function ($, React, Header, GoogleSignIn, api) {

     $(function () {
        $(".btn-scroll-to").click(function(event) {
            event.preventDefault();
            var href = this.hash;
            var top = $(href).offset().top;
            console.log(top);
            $('html, body').animate({
                scrollTop: top
            }, 500);
        });
    });

    React.render(React.createElement(Header), $("#header")[0]);    

    if (!document.location.hash.startsWith("#onboard")) {
        document.location.hash = "#onboard";
    }
});

