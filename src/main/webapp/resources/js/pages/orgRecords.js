define(
    ["jquery", "react", "app/react/common/Header", "app/react/records-old/RecordsContainer",
        "app/recordsSources/OrganizationRecordsSource"],
    function ($, React,Header, RecordsContainer, OrganizationRecordsSource) {

    $(function () {
        React.render(React.createElement(Header), $("#header")[0]);
        React.render(
            React.createElement(RecordsContainer, {RecordsSource: OrganizationRecordsSource}),
            $("#recordsContainer")[0]
        );
    });

});