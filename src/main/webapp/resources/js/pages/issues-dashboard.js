define(
    ["jquery", "react", "app/react/common/Header", "app/react/issues-dashboard/IssuesDashboard", "app/onboard"],
    function ($, React, Header, IssuesDashboard) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(React.createElement(IssuesDashboard), $("#issues-dashboard")[0]);
        });
    });                                                                                 