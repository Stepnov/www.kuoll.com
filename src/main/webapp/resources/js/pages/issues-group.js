define(
    ["jquery", "react", "app/react/common/Header", "app/react/issues-group/IssuesGroup"],
    function ($, React, Header, IssuesGroup) {
        $(function () {
            React.render(React.createElement(Header), $("#header")[0]);
            React.render(React.createElement(IssuesGroup), $("#issues-group")[0]);
        });
    });