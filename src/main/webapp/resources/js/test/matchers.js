define(["app/test/utils"], function (Utils) {

    return {

        toHaveBeenCalledWithSequent: function (util, customEqualityTesters) {
            return {
                compare: function (actual, expected, times) {
                    var result = { pass: false };
                    times = times || 1;

                    if (!jasmine.isSpy(actual)) {
                        throw new Error("Expected a spy, but got " + jasmine.pp(actual) + ".");
                    }

                    if (!actual.calls.any()) {
                        result.message = "Expected spy " + actual.and.identity() + " to have been called with " + jasmine.pp(expected) + " at least " + times + " times but it was never called.";
                        return result;
                    }

                    var args = Utils.flatArray(actual.calls.allArgs());
                    var comparationResult = Utils.containsObject(args, expected);
                    if (comparationResult) {
                        if (comparationResult >= times) {
                            result.pass = true;
                            result.message = "Expected spy " + actual.and.identity() + " not to have been called with " + jasmine.pp(expected) + " at least " + times + " times but it was.";
                        } else {
                            result.message = "Expected spy " + actual.and.identity() + " to have been called with " + jasmine.pp(expected) + " at least " + times + " times but it was called " + comparationResult + " times";
                        }
                    } else {
                        result.message = "Expected spy " + actual.and.identity() + " to have been called with " + jasmine.pp(expected) + " at least " + times + " times but it was never called with such arguments";
                    }

                    return result;
                }
            }
        }

    };

});
