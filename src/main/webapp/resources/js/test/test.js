define(["jquery", "app/test/matchers", "app/test/utils", "jasmine", "jasmine-html", "jasmine-boot"], function ($, matchers, Utils) {

    return function () {
            describe("Recording", function () {

                function expectSequentSaved(info, times) {
                    return expect(kuollRecording.saving.saveSequent).toHaveBeenCalledWithSequent(info, times);
                }

                beforeEach(function () {
                    jasmine.addMatchers(matchers);
                    spyOn(kuollRecording.saving, "saveSequent").and.callThrough();
                });

                it("should record timeout sequents", function (done) {
                    var timeout = setTimeout(function () {}, 100);
                    clearTimeout(timeout);

                    expectSequentSaved({ sequentSubtype: "setTimeout" });
                    expectSequentSaved({ sequentSubtype: "clearTimeout" });
                    done();
                });

                it("should record interval sequents", function (done) {
                    var count = 0;
                    var interval = setInterval(function () {
                        count++;
                        if (count >= 2) {
                            clearInterval(interval);
                            expectSequentSaved({ sequentSubtype: "setInterval" }, 2);
                            expectSequentSaved({ sequentSubtype: "clearInterval" });
                            done();
                        }
                    }, 100);
                });

                it("should record console log sequent", function () {
                    console.log("Test");

                    expectSequentSaved({
                        sequentClass: "Console",
                        sequentSubtype: "log",
                        rawData: {
                            args: {
                                0: "Test"
                            }
                        }
                    })
                });

                it("should record postMessage interception", function () {
                    var origin = document.location.protocol + "//" + document.location.host;
                    window.postMessage("msg", origin);

                    expectSequentSaved({
                        sequentClass: "Window",
                        sequentSubtype: "postMessage",
                        rawData: {
                            args: {
                                0: "msg",
                                1: origin
                            }
                        }
                    });
                });

                describe("XMLHttpRequest", function () {

                    it("should record xhr opening", function () {
                        var xhr = new XMLHttpRequest();
                        xhr.open("GET", "/sample.json");
                        expectSequentSaved({ sequentSubtype: "open", sequentClass: "XMLHttpRequest" })
                    });

                    it("should record xhr sending", function () {
                        var xhr = new XMLHttpRequest();
                        xhr.open("GET", "/sample.json");
                        xhr.send("data");
                        expectSequentSaved({
                            sequentSubtype: "send",
                            sequentClass: "XMLHttpRequest",
                            rawData: {
                                args: {
                                    0: "data"
                                }
                            }
                        });
                    });

                    it("should record xhr with response callback", function (done) {
                        var xhr = new XMLHttpRequest();
                        xhr.open("GET", "/sample.json");
                        xhr.onload = function () {
                            expectSequentSaved({ sequentSubtype: "readystatechange" });
                            done();
                        };
                        xhr.send();
                    });

                    it("should record xhr with response callback which causes DOM mutations", function (done) {
                        var xhr = new XMLHttpRequest();
                        xhr.open("GET", "/sample.json");
                        xhr.onload = function () {
                            var node = document.createElement("noscript");
                            document.head.appendChild(node);
                            expectSequentSaved({ sequentSubtype: "readystatechange" });
                            done();
                        };
                        xhr.send();
                    });

                });

                describe("Mutations", function () {

                    it("should save onclick attribute with another name", function (done) {
                        var $container = $("#on-click");
                        $container.append('<div onclick="console.log(\'click\')" />');

                        Utils.waitForCall(kuollRecording.saving, "saveSequent", function (sequent) {
                            expect(sequent).not.toBe(null);
                            expect(sequent.mutations).not.toBe(null);
                            expect(sequent.mutations).toContain("_onclick");
                            done();
                        });
                    });

                    it("should not save mutation of node with kuoll-ignore attribute", function (done) {
                        var $container = $("#ignore");

                        kuollRecording.saving.saveSequent.calls.reset();
                        $container.append('<div kuoll-ignore="true"><span>Some text</span></div>');
                        window.setTimeout_orig(function () {
                            expect(kuollRecording.saving.saveSequent).not.toHaveBeenCalled();
                            done();
                        }, 500);
                    });

                    it("should not save adding script node", function () {
                        var $container = $("#script-add");

                        kuollRecording.saving.saveSequent.calls.reset();
                        $container.append("<script></script>");
                        expect(kuollRecording.saving.saveSequent).not.toHaveBeenCalled();
                    });

                });

            });

    }


});
