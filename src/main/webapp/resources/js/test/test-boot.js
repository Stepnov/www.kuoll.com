define(["jquery", "app/test/test"], function ($, test) {

    if (kuoll.isRecordActive()) {
        var recordingInfo = kuollRecording.getRecordingInfo();
        test(recordingInfo.recordCode, recordingInfo.frameNum);
        jasmine.run();
    } else {
        // Hard-coded IDs of default user
        kuoll.startRecord({
            customerId: "0",
            kuollUserId: "1",
            startCallback: function (response) {
                if (response.errorMsg) {
                    throw new Error("Error while record starting", response.errorMsg);
                }
                var recordCode = response.recordCode;
                test(recordCode, 1);

                var setInterval = window.setInterval;
                var clearInterval = window.clearInterval;
                var interval = setInterval(function () {
                    if (window.kuollRecording && window.kuollRecording.saving) {
                        clearInterval(interval);
                        jasmine.run();
                    }
                }, 200)
            }
        });
    }

});
