var fs = require('fs');

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        react: {
            files: {
                expand: true,
                cwd: 'generated/unminified/resources/js/react',
                src: ['**/*.jsx'],
                dest: 'generated/unminified/resources/js/react',
                ext: '.js'
            }
        },

        copy: {
            src: {
                expand: true,
                cwd: 'src/main/webapp',
                src: ['**'],
                dest: 'generated/'
            },
            unminified: {
                files: [{
                    expand: true,
                    cwd: 'src/main/webapp/resources/js',
                    src: ['**/*.js', '**/*.jsx'],
                    dest: 'generated/unminified/resources/js/'
                }, {
                    expand: true,
                    cwd: 'generated/unminified/cdn/embedScript',
                    src: '**/*.js',
                    dest: 'generated/cdn/embedScript'
                }]
            },

            react: {
                expand: true,
                cwd: 'generated/unminified/resources/js/react',
                src: ['**/*.js'],
                dest: 'generated/resources/js/react'
            },

            scripts: {
                expand: true,
                cwd: "generated/unminified/resources/js/",
                src: ['**/*.js'],
                dest: "generated/resources/js"
            },

            stylesheets: {
                expand: true,
                cwd: 'src/main/webapp',
                src: ['**/*.css', '!**/*.min.css', '!demo-todo-app/node_modules'],
                dest: 'generated',
                ext: '.css',
                extDot: 'last'
            }
        },

        uglify: {
            options: {
                screwIE8: true,
                sourceMap: true,
                sourceMapRoot: "@@MAP_PATH" // for grunt-replace plugin
            },

            react: {
                expand: true,
                src: ['generated/resources/js/react/**/*.js']
            },

            embedScriptProd: {
                expand: true,
                src: ['generated/cdn/embedScript/prod/*.js']
            },

            scripts: {
                expand: true,
                src: ['generated/resources/js/**/*.js', '!generated/resources/js/react/**/*.js']
            }
        },

        replace: {
            options: {
                patterns: [{
                    match: "MAP_PATH",
                    replacement: function (a, b, c, path) {
                        var substringForSlice = "/generated/";
                        var index = path.indexOf(substringForSlice) + substringForSlice.length;
                        // Leave only part of path after "/webapp/"
                        return "http://localhost:9000/unminified/" + path.slice(index, path.lastIndexOf("/") + 1);
                    }
                }]
            },
            scripts: {
                expand: true,
                cwd: 'generated',
                src: ['resources/**/*.js.map', 'demo-todo-app/**/*.js.map', '!demo-todo-app/node_modules'],
                dest: 'generated'
            }
        },

        csswring: {
            options: {
                map: {
                    inline: false
                }
            },

            stylesheets: {
                expand: true,
                cwd: "generated",
                src: ['resources/**/*.css', '!resources/**/*.min.css'],
                dest: "generated",
                ext: '.min.css'
            }
        },

        bower: {
            install: {
                options: {
                    targetDir: "generated/lib/"
                }
            }
        },

        newer: {
            options: {
                // Fix of bug https://github.com/tschaub/grunt-newer/issues/40#issuecomment-132520453
                override: function (details, include) {
                    //check if the file was created after details.time and force run
                    var stats = fs.statSync(details.path);
                    include(stats.ctime.getTime() > details.time.getTime());
                }
            }
        },

        mustacheGenerate: {
            options: {
                output: '.html',
                dontMinify: true,
                partials: {
                    src: ["src/main/webapp/partials"]
                },
                logLevel: 0
            },
            files: {
                expand: true,
                cwd: "src/main/webapp/pages",
                src: ['**/*.mustache'],
                dest: 'generated/'
            }
        },

        watch: {
            options: {
                livereload: true,
                atBegin: true,
                interval: 1000,
                default: false
                // spawn: false,
                // interrupt: true,
                // debounceDelay: 3000
            },

            bower: {
                files: ['bower.json'],
                tasks: ['bower:install']
            },

            npm: {
                files: ['package.json'],
                tasks: ['auto_install']
            },

            react: {
                files: ['src/main/webapp/resources/js/react/**/*.jsx', 'Gruntfile.js'],
                tasks: ['reactjs']
            },

            scripts: {
                files: ['src/main/webapp/resources/**/*.js'],
                tasks: ['scripts']
            },


            embedScript: {
                files: ['generated/unminified/cdn/embedScript/**/*.js'],
                tasks: ['embedScript']
            },

            stylesheets: {
                files: ['src/**/*.css'],
                tasks: ['stylesheets']
            },

            src: {
                files: ['src/main/webapp/**', '!**/*.jsx', '!**/*.js', '!**/*.css'],
                tasks: ['newer:copy:src']
            },

            mustache: {
                files: ['src/main/webapp/pages/*.mustache', 'src/main/webapp/partials/*.mustache'],
                tasks: ['mustacheGenerate']
            }
        },

        auto_install: {
            local: {
                options: {
                    bower: false
                }
            }
        },

        clean: {
            react: ["generated/reactjs", "generated/unminified/resources/js/react"],
            scripts: ["generated/admin/", "generated/resources", "generated/unminified/resources/js"],
            embedScript: ["generated/cdn/embedScript/", "generated/unminified/cdn"],
            lib: ["generated/lib"]
        },

        aws: grunt.file.readJSON('aws-keys.json'),

        aws_s3: {
            options: {
                accessKeyId: '<%= aws.AWSAccessKeyId %>',
                secretAccessKey: '<%= aws.AWSSecretKey %>',
                region: 'eu-central-1',
                differential: true,
                uploadConcurrency: 5,
                downloadConcurrency: 5
            },

            production: {
                options: {
                    bucket: 'www.kuoll.com'
                },
                files: [
                    {expand: true, cwd: 'generated/', src: ['**'], dest: '/'}
                ]
            },

            stage: {
                options: {
                    bucket: 'stage.kuoll.com'
                },
                files: [
                    {expand: true, cwd: 'generated/', src: ['**'], dest: '/'}
                ]
            }
        },

        connect: {
            server: {
                options: {
                    livereload: true,
                    port: 9000,
                    base: 'generated',
                    middleware: function(connect, options, middlewares) {
                        middlewares.unshift(function(req, res, next) {
                            res.setHeader('Access-Control-Allow-Origin', '*');
                            res.setHeader('Access-Control-Allow-Methods', '*');
                            next();
                        });
                        return middlewares;
                    }
                }
            }
        },

        ts: {
            default: {
                baseDir: 'src/main/webapp/demo',
                src: 'src/main/webapp/demo/**/*.ts',
                outDir: 'generated/demo',
                options: {
                    module: 'system'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('csswring');
    grunt.loadNpmTasks('grunt-react');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-replace');
    grunt.loadNpmTasks('grunt-auto-install');
    grunt.loadNpmTasks('grunt-mustache-generate');
    grunt.loadNpmTasks('grunt-aws-s3');

    grunt.registerTask('default', ['connect:server', 'watch']);
    grunt.registerTask('reactjs', ['newer:copy:unminified', 'newer:react', 'newer:copy:react']);
    grunt.registerTask('scripts', ['newer:copy:unminified', 'newer:copy:scripts']);
    grunt.registerTask('build', ['newer:copy', 'bower', 'react', 'scripts', 'embedScript', 'stylesheets']);
    grunt.registerTask('embedScript', ['newer:copy:unminified']);

    grunt.registerTask('stylesheets', ['newer:copy:stylesheets']);

    grunt.registerTask('deploy', ['uglify', 'csswring', 'replace', 'aws_s3:production']);
    grunt.registerTask('deploy_stage', ['aws_s3:stage']);
};